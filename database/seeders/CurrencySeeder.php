<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'name' 		    => 'Bitcoin',
            'code' 	        => 'BTC',
            'price' 	    => 520853174,
            'highPrice' 	=> 528206880,
            'lowPrice' 	    => 505853459,
            'marketPrice' 	=> 9569814,
            'change' 	    => 0.00,
            'image' 	    => 'BTC.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'Ethereum',
            'code' 	        => 'ETH',
            'price' 	    => 39007280,
            'highPrice' 	=> 39814800,
            'lowPrice' 	    => 37165952,
            'marketPrice' 	=> 4446056,
            'change' 	    => +2.13,
            'image' 	    => 'ETH.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'TetherUS',
            'code' 	        => 'USDT',
            'price' 	    => 14546,
            'change' 	    => +0.45,
            'highPrice' 	=> 14599,
            'lowPrice' 	    => 14475,
            'marketPrice' 	=> 888267,
            'image' 	    => 'USDT.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'BNB',
            'code' 	        => 'BNB',
            'price' 	    => 5626851,
            'change' 	    => -1.41,
            'highPrice' 	=> 5820000,
            'lowPrice' 	    => 5436599,
            'marketPrice' 	=> 846975,
            'image' 	    => 'BNB.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'Dogecoin',
            'code' 	        => 'DOGE',
            'price' 	    => 5370,
            'change' 	    => -1.92,
            'highPrice' 	=> 5475,
            'lowPrice' 	    => 5237,
            'marketPrice' 	=> 684447,
            'image' 	    => 'DOGE.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'BUSD',
            'code' 	        => 'BUSD',
            'price' 	    => 14531,
            'change' 	    => +0.36,
            'highPrice' 	=> 14589,
            'lowPrice' 	    => 14476,
            'marketPrice' 	=> 137532,
            'image' 	    => 'BUSD.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'Polkadot',
            'code' 	        => 'DOT',
            'price' 	    => 346541,
            'change' 	    => 0.00,
            'highPrice' 	=> 354693,
            'lowPrice' 	    => 333149,
            'marketPrice' 	=> 106076,
            'image' 	    => 'DOT.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'Zilliqa',
            'code' 	        => 'ZIL',
            'price' 	    => 1812,
            'change' 	    => +1.23,
            'highPrice' 	=> 1910,
            'lowPrice' 	    => 1676,
            'marketPrice' 	=> 20183,
            'image' 	    => 'ZIL.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'Tokocrypto',
            'code' 	        => 'TKO',
            'price' 	    => 34105,
            'change' 	    => -2.67,
            'highPrice' 	=> 36382,
            'lowPrice' 	    => 33200,
            'marketPrice' 	=> 3630,
            'image' 	    => 'TKO.png',
        ]);

        DB::table('currencies')->insert([
            'name' 		    => 'Swipe',
            'code' 	        => 'SXP',
            'price' 	    => 30566,
            'change' 	    => +4.00,
            'highPrice' 	=> 31500,
            'lowPrice' 	    => 28172,
            'marketPrice' 	=> 2854,
            'image' 	    => 'SXP.png',
        ]);
    }
}

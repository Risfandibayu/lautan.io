<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user           = NEW User;
        $user->type     = 'superadmin';
        $user->name     = 'Super Admin';
        $user->email    = 'superadmin@lautan.io';
        $user->phone    = '1234567890';
        $user->password = Hash::make('rahasia123');
        $user->save();

        $user           = NEW User;
        $user->type     = 'admin';
        $user->name     = 'Admin';
        $user->email    = 'admin@lautan.io';
        $user->phone    = '1234567890';
        $user->password = Hash::make('rahasia123');
        $user->save();
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            'name' 		=> 'BCA',
            'code' 	    => '014',
        ]);

        DB::table('banks')->insert([
            'name' 		=> 'BNI',
            'code' 	    => '009',
        ]);

        DB::table('banks')->insert([
            'name' 		=> 'BRI',
            'code' 	    => '002',
        ]);

        DB::table('banks')->insert([
            'name' 		=> 'Mandiri',
            'code' 	    => '008',
        ]);

        DB::table('banks')->insert([
            'name' 		=> 'Danamon',
            'code' 	    => '011',
        ]);

        DB::table('banks')->insert([
            'name' 		=> 'Permata',
            'code' 	    => '013',
        ]);

        DB::table('banks')->insert([
            'name' 		=> 'CIMB Niaga',
            'code' 	    => '022'
        ]);
    }
}

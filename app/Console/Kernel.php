<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\Tokocrypto',
        'App\Console\Commands\Bscscan',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cron:tokocrypto')->cron('*/3 * * * *')
        ->onSuccess(function () {
            \Log::info('cron tokocrypto running');
        })
        ->onFailure(function () {
            \Log::info('cron tokocrypto failed');
        })
        // ->environments(['production'])
        // ->withoutOverlapping()
        ->onOneServer();

        $schedule->command('cron:bscscan')->cron('*/3 * * * *')
        ->onSuccess(function () {
            \Log::info('cron bscscan running');
        })
        ->onFailure(function () {
            \Log::info('cron bscscan failed');
        })
        // ->environments(['production'])
        // ->withoutOverlapping()
        ->onOneServer();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

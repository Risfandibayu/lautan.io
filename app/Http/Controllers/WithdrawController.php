<?php

namespace App\Http\Controllers;

include __DIR__.'/SendMe.php';
use Illuminate\Http\Request;
use App\Libs\Binance;
use App\Libs\Ipaymu;
use App\Libs\Idcash;
use App\Models\AgentFee;
use App\Models\AgentRole;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\Balance;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserPin;
use DateTime;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use SendMe;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if(!Auth::user()->tokocryptoKey OR !Auth::user()->tokocryptoSecret) {
        //     return redirect('/tokocrypto')->with('warning','Silahkan hubungkan akun Tokocrypto Anda terlebih dahulu sebelum dapat mengakses menu Penarikan');
        if(Auth::user()->status != 3) {
            return redirect('/verification')->with('warning','Silahkan lakukan verifikasi terlebih dahulu sebelum dapat mengakses menu Penarikan');
        } else {
            $menu = 'balance';
            $row                = Currency::where('active',1)->orderBy('name')->get();
            // dd($row);
            $bank               = Bank::where('active',1)->orderBy('name')->get();
            $userbank                  = DB::table('user_bank')
                                            ->select('user_bank.*', 'user_bank.id as bid', 'banks.*')
                                            ->join('banks', 'banks.code', '=', 'user_bank.bank_code')
                                            ->where('user_id', Auth::user()->id)
                                            ->get();
            return view('withdraw.withdraw')->with(compact('menu','row','bank','userbank'));
        }
    }

    public function getCryptoCurrency(Request $request){
        $id = $request->code;
        $amont = $request->amont;
        // dd($amont);
        $rows = Currency::where('active',1)->where('id',$id)->first();
        $codes = $rows->code;
        $amont= $request->amont;
        $binance = new Binance;
        $send['url'] = '/ticker/24hr';
        $body['symbol'] = $codes;
        $send['body'] = $body;
        $post = $binance->callApi($send);
        if($post){
            $coin['code'] = $post->symbol;
            $coin['price'] = $post->lastPrice;
            $price = $post->lastPrice;
            $coin['change'] = $post->priceChangePercent;
            $coin['highPrice'] = $post->highPrice;
            $coin['lowPrice'] = $post->lowPrice;
            $coin['marketPrice'] = $post->quoteVolume;
            $coin['idrvalue'] = $amont * $price;
            $idrval = $amont * $price;
            $coin['tkoFee'] = round($idrval * (0.10 / 100), 2);
            $coin['grossFee'] = round( $idrval * (1.90 / 100), 2);
            $coin['agentFee'] = round($idrval * (0.75 / 100), 2);
            $coin['lautanFee'] = round($idrval * (1.15 / 100), 2);
            $coin['netFee'] =round( $idrval * (2 / 100), 2);
            $coin['platformFee'] = 5000;
            $coin['transTax'] = round($idrval * (0.26 / 100), 2);
            $coin['totalFee'] = $coin['tkoFee'] + $coin['agentFee'] + $coin['lautanFee'] + $coin['platformFee'] + $coin['transTax'];
        
            $binance = new Binance;
            $send['url'] = '/ticker/24hr';
            $body['symbol'] = $codes;
            $send['body'] = $body;
            $post = $binance->callApi($send);
            if ($post) {
               $price = $post->lastPrice;
               $coin['feeInCoin'] = round($coin['totalFee'] / $price,5);
            }
        }else{
           dd($post);
        }
        return response()->json(['status'=>200,'data'=>$coin]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInfo()
    {
        // dd(session()->all());
        $menu = 'balance';

        return view('withdraw.info')->with(compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postInfo(Request $request)
    {
        // dd($request->all());
        $currency                   = Currency::where('active',1)->where('id',$request->input('code'))->first();
        
        $bank_acc                   = db::table('user_bank')->where('id', $request->bank_acc)->first();
        $bank                       = Bank::where('code',$bank_acc->bank_code)->first();

        $binance                                = NEW Binance;
        $send['url']                            = '/ticker/24hr';
        $body['symbol']                         = $currency->code;
        $send['body']                           = $body; 
        $post                                   = $binance->callApi($send);

        $amount                                 = $post->lastPrice * $request->input('amount');

        $user = Auth::user();
        $balance = Balance::where('user_id',$user->id)->where('currency_id',$request->input('code'))->orderBy('created_at', 'DESC')->first();
        $fee = $this->trxFee($amount);
        $tax = $fee['feeInCoin'];
        
        if($balance) {
            $blc = $balance->amount + $tax;
        } else {
            $blc = 0;
        }
        
        // if(round($request->amount,5) > round($blc,5)){
        //     echo round($request->amount,5) .' is biger than ' .round($blc,5); 
        // }else{
        //     echo round($request->amount,5).' is smaller than '.round($blc,5);
        // }
        // die;
        // dd(round($request->amount,5));
        if (!$request->input('amount')) {
            return redirect('/withdraw')->withInput()->with('error','Please input withdraw amount');
        } else if (round($request->amount,5) > round($blc,5)) {
            return redirect('/withdraw')->withInput()->with('error','Not enough balance');
        } else {
           
            $fee = $this->trxFee($amount,$currency->code);
            // $this->trxFee()
            $newpin                                 = random_int(100000, 999999);
            $pin                                    = NEW UserPin;
            $pin->user_id                           = $user->id;
            $pin->pin                               = Hash::make($newpin);
            $pin->save();

            $data = [
                'name'      => $user->name, 
                'pin'       => $newpin,
            ];

            Mail::send('emails.withdrawpin', $data, function($message)use($data, $user) {
                $message->to($user->email)
                        ->subject('Withdraw Confirmation');
            });

            $datas = [
                'name'  => auth()->user()->name, 
                'email'   => auth()->user()->email,
            ];
            !isset(auth()->user()->adminGmail)? $user->adminGmail = env('MAIL_ADMIN_ADDRESS_GMAIL'):'';
            Mail::send('emails.admin_withdraw', $datas, function($message)use($datas, $user) {
                $message->to($user->adminGmail)
                ->subject('User Request Withdraw / Convert Coin');
            });
            
            !isset(auth()->user()->adminLautan)? $user->adminLautan = env('MAIL_ADMIN_ADDRESS_LAUTAN'):'';
            Mail::send('emails.admin_withdraw', $datas, function($message)use($datas, $user) {
                $message->to($user->adminLautan)
                        ->subject('User Request Withdraw / Convert Coin');
            });


            $request->session()->put('type', $request->input('type'));
            $request->session()->put('amount', $request->input('amount'));
            $request->session()->put('amountIdr', $amount);
            $request->session()->put('code', $currency->id);
            $request->session()->put('coin', $currency->code);
            $request->session()->put('bankCode', $bank_acc->bank_code);
            $request->session()->put('bankName', $bank->name);
            $request->session()->put('bankAccount', $bank_acc->bank_account);
            $request->session()->put('bankNumber', $bank_acc->account_number);
            $request->session()->put('bankID', $bank_acc->id);
            $request->session()->put('feeTransaction', $fee);
            // $request->session()->put('pin', $newpin);
            
            $user = Auth::user();
            DB::beginTransaction();
            try {
                
            if($bank_acc->isReg == 0){
                
                $sendme = new SendMe();	
                $sendme->enableProd();
                $reg = $sendme->register([	
                    "beneficiary_account" 		=> $bank_acc->account_number,
                    "beneficiary_account_name" 	=> $bank_acc->bank_account,
                    "beneficiary_va_name" 		=> $bank_acc->bank_account,
                    "beneficiary_bank_code" 	=> $bank_acc->bank_code,
                    "beneficiary_bank_branch" 	=> $bank->name,
                    "beneficiary_region_code" 	=> "0102",
                    "beneficiary_country_code" 	=> "ID",
                    "beneficiary_purpose_code" 	=> "1",
                ]);
                $reg2 = $sendme->confirm([
                    // "virtual_account"            => "9920001189", //testing
                    "virtual_account"            => "9847090569", //production
                    "beneficiary_account" 		=> $bank_acc->account_number,
                    "beneficiary_account_name" 	=> $bank_acc->bank_account,
                    "beneficiary_va_name" 		=> $bank_acc->bank_account,
                    "beneficiary_bank_code" 	=> $bank_acc->bank_code,
                    "beneficiary_bank_branch" 	=> $bank->name,
                    "beneficiary_region_code" 	=> "0102",
                    "beneficiary_country_code" 	=> "ID",
                    "beneficiary_purpose_code" 	=> "1",
                    "bank_account_number"=> $bank_acc->account_number,
                    "bank_account_name"=> $bank_acc->bank_account,
                    "confirm" => '2',
                ]);
                // dd($reg2);
                $ba = db::table('user_bank')
                    ->where('id',$bank_acc->id)
                    ->update([
                            'bva' => $reg2['beneficiary_virtual_account'],
                            'ban' => $reg2['beneficiary_account_name'],
                            'isReg' => 1
                        ]);

                // inject
                // $ba = db::table('user_bank')->where('id',$bank_acc->id)->update([
                //             'bva' => 1190007016593,
                //             'ban' => 'IGede Miarta Yasa',
                //             'isReg' => 1
                //         ]);
                if($user->trx_fp == null || empty($user->trx_fp) || $user->trx_fp == 0){
                    $user->trx_fp =   ($user->id * 100000) + 1;
                }else{
                    $user->trx_fp  = $user->trx_fp + 1;
                }
                ;
                $user->save();
                DB::commit();
            }else{
                if($user->trx_fp == null || empty($user->trx_fp) || $user->trx_fp == 0){
                    $user->trx_fp =   ($user->id * 100000) + 1;
                }else{
                    $user->trx_fp  = $user->trx_fp + 1;
                }
                ;
                $user->save();
                DB::commit();
            }
            

            } catch (ClientException $e) {
                DB::rollback();
                Log::info($e->getMessage());
                return redirect('/withdraw')->withInput()->with('error','Convert error, please try again later');
            }
            
            
            
            return redirect('/withdraw/info');
            
            // return json_encode($reg2);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendWallet(Request $request){
        if ($request->total_bnb == 0) {
            return response()->json(['status' => 401, 'message' => 'Not enough balance']);
        }
        $user = Auth::user();
        $balance = Balance::where('user_id',$user->id)->where('currency_id',4)->where('active',1)->orderBy('created_at', 'DESC')->first();
        if($balance){
            $blc = $balance->amount;
        }else{
            $blc = 0;
        }
        // create Balance
        try {
            $blc = Balance::create([
                'active' => 1,
                'user_id' => $user->id,
                'currency_id' => 4,
                'code' => 'BNBBIDR',
                'amount' => $request->total_bnb + $blc,
                'amount_idr' => null
            ]);
            $user = User::where('user_agent_referal',$user->as_agent_referal)->get();{
                foreach($user as $u){
                    AgentFee::where('user_id',$u->id)->where('status',1)->update([
                        'status' => 0
                    ]);
                }
            }
            return response()->json(['status' =>201, 'message' => 'Successfully send to wallet']);
        } catch (QueryException $e) {
            return response()->json(['status' =>500, 'message' => 'Failed send to wallet']);
        }
        
    }
    
    public function store(Request $request)
    {
        // dd($request->all());
        // dd( $request->input('amount'));
        $user = Auth::user();
        $balance = Balance::where('user_id',$user->id)->where('currency_id',$request->input('code'))->orderBy('created_at', 'DESC')->first();
        $imp_fee = $this->trxFee($request->input('amount'));
        $tax = $imp_fee['totalFee'];

        if($balance) {
            $blc = $balance->amount;
        } else {
            $blc = 0;
        }

        $pin = UserPin::where('user_id',$user->id)->orderBy('id','DESC')->first();
        if (!$request->input('amount')) {
            return redirect('/withdraw')->withInput()->with('error','Please input convert amount');
        } elseif ($request->input('amount') > $blc + $tax) {
            return redirect('/withdraw')->withInput()->with('error','Not enough balance');
        } elseif (!$request->input('pin')) {
            return redirect('/withdraw/info')->withInput()->with('error','Please input PIN');
        } elseif (!Hash::check($request->input('pin'),$pin->pin)) {
            return redirect('/withdraw/info')->withInput()->with('error','Wrong PIN');
        } else {
            DB::beginTransaction();
            try {
                // return redirect('/withdraw')->withInput()->with('error','Terjadi Kesalahan, coba beberapa saat lagi');
                // $user->tokocryptoKey              = $request->input('apikey');
                // $user->tokocryptoSecret           = $request->input('secretkey');
                // $user->save();
                // return redirect('/tokocrypto')->with('success','Data berhasil di simpan');
                $currency                               = Currency::where('active',1)->where('id',$request->input('code'))->first();
                $row                                    = NEW Transaction;
                $row->user_id                           = $user->id;
                $trx_id                                 = 'W'.$user->id.date('ymdhis');
                $row->trx_id                            = $trx_id;
                $row->amount                            = $request->input('amount');
                $row->type                              = 'withdraw';
                // $row->sender                            = $request->input('sender');
                // $row->receiver                          = $currency->address;
                $row->currency_id                       = $currency->id;
                $row->code                              = $currency->code;
                $row->bankID                            = $request->session()->get('bankID');
                $row->save();
                
                $binance                                = NEW Binance;
                $send['url']                            = '/ticker/24hr';
                $body['symbol']                         = $currency->code;
                $send['body']                           = $body; 
                $post                                   = $binance->callApi($send);

                $amount                                 = $post->lastPrice * $request->input('amount');

                $fee = $this->trxFee($amount);
                $lastBalance        = Balance::where('user_id',$user->id)->where('currency_id',$currency->id)->where('active',1)->orderBy('id','DESC')->first();
                if($lastBalance) {
                    $trxBalance = $lastBalance->amount - $row->amount - $fee['feeInCoin'];
    
                } else {
                    $trxBalance = 0;
                }

                $newBalance                             = NEW Balance;
                $newBalance->user_id                    = $row->user_id;
                $newBalance->currency_id                = $row->currency_id;
                $newBalance->code                       = $row->code;
                $newBalance->amount                     = $trxBalance;
                $newBalance->save();

                // $amount                                 = $currency->price * $request->input('amount');
                
                if($request->session()->get('type') == 'bank') {
                    $ipaymu                                 = NEW Ipaymu;
                    $send['url']                            = env('IPAYMU_URL').'/api/v2/withdrawbank';
                    $send['va']                             = env('IPAYMU_VA');
                    $send['secret']                         = env('IPAYMU_KEY');
                    $send['method']                         = 'POST';
                    $body['bankCode']                       = $request->session()->get('bankCode');
                    $body['bankName']                       = $request->session()->get('bankName');
                    $body['bankNumber']                     = $request->session()->get('bankNumber');
                    $body['bankAccount']                    = $request->session()->get('bankAccount');
                    $body['amount']                         = $amount;
                    $body['referenceId']                    = $row->id;
                    $body['notes']                          = 'Convert Lautan Trx ID'.$row->id;
                    $send['body']                           = json_encode($body, JSON_UNESCAPED_SLASHES);
                    $post                                   = $ipaymu->callApi($send);
                    if($post AND $post->Status == 200) {
                        $row->ipaymuId                      = $post->Data->TransactionId;
                        $row->save();
                        DB::commit();
                        return redirect('/withdraw')->with('success','Convert Success');
                    } else {
                        DB::rollback();
                        return redirect('/withdraw')->withInput()->with('error','Convert error, please try again later');
                    }
                }elseif($request->session()->get('type') == 'bankk'){
                    // $virtual_ac                  = "9920001189";
                    // $faspayKey                   = "c2b21b0e-e988-4fc8-ae31-39d7177a7186";
                    // $faspaySecret                = "19c887a3-49af-4087-8e82-58a1875b1452";
                    // $appKey                      = "055061d5-626a-420b-8e1a-dbd6772fd5f6";
                    // $appSecret                   = "60d5ce04-b077-44c5-b5b8-6ceeeab11e18";
                    // $clientKey                   = "571f97de-3216-4598-a80c-240e6d571000";
                    // $clientSecret                = "3155e4cb-58af-46c7-b1df-86cd54809672";
                    // $iv                          = "faspay2018xAuth@#";

                    $b_ac = db::table('user_bank')->where('id',$request->session()->get('bankID'))->first();
                    $sendme = new SendMe();	
                    $sendme->enableProd();
                    
                    $reg = $sendme->transfer([	//lautan transfer ke user
                        "virtual_account" => "9847090569",
                        "beneficiary_virtual_account" => $b_ac->bva,
                        "beneficiary_account" =>$b_ac->account_number,
                        "beneficiary_name" => $b_ac->ban,
                        "beneficiary_bank_code" => $b_ac->bank_code,
                        "beneficiary_region_code" => "0102",
                        "beneficiary_country_code" => "ID",
                        "beneficiary_purpose_code" => "1",
                        "beneficiary_email" => $user->email,
                        "trx_no" => $user->trx_fp,
                        "trx_date" => Date("Y-m-d H:i:s"),
                        "instruct_date" => "",
                        "trx_amount" => round($amount,0) * 100,
                        "trx_desc" => "Withdrawal",
                        "callback_url" => "https://lautan.io/api/withdraw/sendme_notify"
                    ]); 

                    $reg['status'] = 1; //iject

                    // dd($reg);
                    if($reg['status'] == 1) {
                        // $row->ipaymuId         = $post->Data->TransactionId;
                        // $row->save();
                        DB::commit();
                        DB::table('sendme')->insert([
                            'user_id' => $user->id,
                            'currency_code' => $request->input('code'),
                            'currency_amount' => $request->input('amount'),
                            'trx_id' => $reg['trx_id'],
                            'trx_no' => $reg['trx_no'],
                            'trx_date' => $reg['trx_date'],
                            'trx_amount' => round($amount,0),
                            'status' => $reg['status'],
                            'tid' => $row->id
                        ]);

                        // update transaction 
                        DB::table('transactions')->where('trx_id',$trx_id)
                        ->update([
                            'amount_idr' => $amount,
                            'fee_tko' => $fee['tkoFee'],
                            'fee_agent' => $fee['agentCashIDR'],
                            'fee_agent_to_lautan' =>$fee['agnetLeftOver'],
                            'fee_lautan' => $fee['lautanFee'],
                            'fee_lautan_plus_agent'=>$fee['lautanCash'],
                            'fee_platform' => $fee['platformFee'],
                            'fee_trx_tax' => $fee['transTax'],
                            'fee_total' => $fee['totalFee'],
                            'fee_in_coin' => $fee['feeInCoin'],
                        ]);

                        if($fee['hv_agen']){
                            
                            if($fee['agen']){
                                DB::table('agent_fees')->insert([
                                    'user_id' => auth()->user()->id,
                                    'amount_idr' => $fee['agentCashIDR'],
                                    'amount_bnb' => $fee['agentCashBNB'],
                                    'status'=>true
                                ]);
                            }
                            // if user have agent manager
                            if($fee['manager']){
                                DB::table('agent_fees')->insert([
                                    'user_id' => $fee['agen_manager_id'],
                                    'amount_idr' => $fee['agentManagerCashIDR'],
                                    'amount_bnb' => $fee['agentManagerCashBNB'],
                                    'status'=>true
                                ]);
                            }

                            if($fee['director']){
                                DB::table('agent_fees')->insert([
                                    'user_id' => $fee['manager_director_id'],
                                    'amount_idr' => $fee['agentDirectorCashIDR'],
                                    'amount_bnb' => $fee['agentDirectorCashBNB'], 
                                    'status'=>true
                                ]);
                            }
                        
                        }
                        $request->session()->put('trxFee', $fee);

                        // return json_encode($reg);
                        return redirect('/withdraw')->with('success','Convert Success');
                        
                    }elseif(round($amount,0) < 10000){
                        DB::rollback();
                        return redirect('/withdraw')->withInput()->with('error','Convert error, amount below the minimum withdrawal. Amount must be more than Rp. 10.000');
                    }
                    else {
                        DB::rollback();
                        return redirect('/withdraw')->withInput()->with('error','Convert error, please try again later');
                    }
                    // dd($sendme);
                }elseif($request->session()->get('type') == 'idcash'){
                    // $va                         = env('IPAYMU_VA');
                    // $api                        = env('IPAYMU_KEY');
                    $va                         = $user->idcashVa;
                    $api                        = $user->idcashKey;
                    $integration_key            = Idcash::encryptQR($va . '|' . $api, env('IDCASH_KEY'));
                    // register to idcash ambil va dan key
                    $idcash                     = NEW Idcash;
                    $send['url']                = env('IDCASH_URL').'reload';
                    $send['method']             = 'POST';
                    $body['amount']             = $amount;
                    $body['request']            = 'reload';
                    $send['va']                 = $user->idcashVa;
                    $send['secret']             = $user->idcashKey;
                    $send['body']               = $body;
                    $post                       = $idcash->callApi($send);
                    dd($post);
                    if($post AND $post->Status == 201) {
                        // $row->ipaymuId          = $post->Data->TransactionId;
                        // $row->save();
                        DB::commit();
                        return redirect('/withdraw')->with('success','Withdraw Success');
                    } else {
                        DB::rollback();
                        return redirect('/withdraw')->withInput()->with('error','Withdraw error, please try again later');
                    }
                }
            } catch (ClientException $e) {
                DB::rollback();
                Log::info($e->getMessage());
                return redirect('/withdraw')->withInput()->with('error','Convert error, please try again later'.$e->getMessage() );
            }
        }
    }
    public function trxFee($amount, $currency = 'BNBBIDR') {
        $fee['tkoFee'] = round($amount * (0.10 / 100), 2);
        $fee['grossFee'] = round( $amount * (1.90 / 100), 2);
        $fee['agentFee'] = round($amount * (0.75 / 100), 2);
        $fee['lautanFee'] = round($amount * (1.15 / 100), 2);
        $fee['netFee'] = round( $amount * (2 / 100), 2);
        $fee['platformFee'] = 5000;
        $fee['transTax'] = round($amount * (0.26 / 100), 2);
        $fee['totalFee'] = round($fee['tkoFee'] + $fee['agentFee'] + $fee['lautanFee'] + $fee['platformFee'] + $fee['transTax']);
        
        $binance                                = NEW Binance;
        $send['url']                            = '/ticker/24hr';
        $body['symbol']                         = $currency;
        $send['body']                           = $body; 
        $post                                   = $binance->callApi($send);

        $fee['feeInCoin']                       = round($fee['totalFee'] / $post->lastPrice, 8);
        // $fee['feeInCoin'] = round($fee['totalFee'] / $amount,8);
        // chek if user have agent

        $agens = AgentRole::find(3);
        $agen_tax = $agens->fees;

        $agensM = AgentRole::find(2);
        $manager_tax = $agensM->fees;

        $agenD = AgentRole::find(1);
        $dir_tax = $agenD->fees;

        $agentRef = auth()->user()->user_agent_referal;
        if($agentRef != null || $agentRef != ''){
            $fee['hv_agen'] = true;
            $user = User::where('as_agent_referal',$agentRef)->first();
            
            if($user->agent_role_id == 3){
                $fee['agen'] = true;
                $agentFee = $fee['agentFee'] * $agen_tax/100;
                
                $agentManager = $user->user_agent_referal;
                if($agentManager != null || $agentManager != ''){
                    $fee['manager'] = true;
                    $manager = User::where('as_agent_referal',$agentManager)->first();
                    $fee['agen_manager_id'] = $user->id;
                    
                    $managerFee = $fee['agentFee'] * $manager_tax/100;
                    
                    $managerDirector = $manager->user_agent_referal;
                    if ($managerDirector != null || $managerDirector != '') {
                        $fee['director'] = true;
                        $fee['manager_director_id'] = $manager->id;
                        
                        $directorFee = $fee['agentFee'] * $dir_tax/100;

                        $leftover = $fee['agentFee'] - $agentFee - $managerFee - $directorFee;

                    }else{
                        $directorFee = 0;
                        $fee['director'] = false;
                        $leftover = $fee['agentFee'] - $agentFee - $managerFee - $directorFee;
                    }
                }else{
                    $fee['manager'] = false;
                    $managerFee = 0;
                    $leftover = $fee['agentFee'] - $agentFee - $managerFee;
                }
            }elseif($user->agent_role_id == 2){
                $fee['manager'] = true;
                $manager = User::where('as_agent_referal',$agentRef)->first();
                $fee['agen_manager_id'] = $user->id;
                
                $managerFee = $fee['agentFee'] * $manager_tax/100;
                
                $managerDirector = $manager->user_agent_referal;
                if ($managerDirector != null || $managerDirector != '') {
                    $fee['director'] = true;
                    $fee['manager_director_id'] = $manager->id;
                    
                    $directorFee = $fee['agentFee'] * $dir_tax/100;

                    $leftover = $fee['agentFee'] - $managerFee - $directorFee;

                }else{
                    $directorFee = 0;
                    $fee['director'] = false;
                    $leftover = $fee['agentFee'] - $managerFee - $directorFee;
                }
            }elseif($user->agent_role_id == 1){
                $directorFee = $fee['agentFee'] * $dir_tax/100;
                $leftover = $fee['agentFee'] - $directorFee;
            }
        }else{
            $agentFee = 0;
            $managerFee = 0;
            $directorFee = 0;
            $leftover = $fee['agentFee'];

            $fee['agen'] = 0;
        }


        $fee['agentCashIDR']            = $agentFee + $managerFee + $directorFee;
        $fee['agentCashBNB']            = $agentFee != 0 ? round($agentFee / $post->lastPrice,8) : 0;        
        $fee['agentManagerCashIDR']     = $managerFee;
        $fee['agentManagerCashBNB']     = $managerFee != 0 ? round($managerFee / $post->lastPrice,8) : 0;
        $fee['agentDirectorCashIDR']    = $directorFee;
        $fee['agentDirectorCashBNB']    = $directorFee != 0 ? round($directorFee / $post->lastPrice,8) : 0;
        
        $fee['agnetLeftOver'] = $leftover;
        $fee['lautanCash'] = $leftover + $fee['lautanFee'];
        return $fee;
    }
    
    public function sendme_notify(Request $request){
        // dd($request->all());
        // $user_id 	='bot34465';
        // $passw		='p@ssw0rd';
        $user_id 	='bot34465';
        $passw		='gL@i9ckU';
        
        $token = csrf_token();
        // $data = array(
        //     "request"=> $request->request,
        //     "virtual_account"=> $request->virtual_account,
        //     "va_name"=> $request->va_name,
        //     "beneficiary_virtual_account"=> $request->beneficiary_virtual_account,
        //     "beneficiary_account"=> $request->beneficiary_account,
        //     "beneficiary_name"=> $request->beneficiary_name,
        //     "trx_id"=> $request->trx_id,
        //     "trx_date"=> $request->trx_date,
        //     "trx_amount"=> $request->trx_amount,
        //     "trx_desc"=> $request->trx_desc,
        //     "trx_status"=> $request->trx_status,
        //     "trx_status_date"=> $request->trx_status_date,
        //     "trx_reff"=> $request->trx_reff,
        //     "trx_no"=> $request->trx_no,
        //     "bank_code"=> $request->bank_code,
        //     "bank_name"=> $request->bank_name,
        //     "bank_response_code"=> $request->bank_response_code,
        //     "bank_response_msg"=> $request->bank_response_msg,
        // );
        
        // return response()->json(["message"=>$request->input('request')]);
        
        $data =  '{
            "request": "'.$request->input("request").'",
            "virtual_account": "'.$request->virtual_account.'",
            "va_name": "'.$request->va_name.'",
            "beneficiary_virtual_account": "'.$request->beneficiary_virtual_account.'",
            "beneficiary_account": "'.$request->beneficiary_account.'",
            "beneficiary_name": "'.$request->beneficiary_name.'",
            "trx_id": "'.$request->trx_id.'",
            "trx_date": "'.$request->trx_date.'",
            "trx_amount": "'.$request->trx_amount.'",
            "trx_desc": "'.$request->trx_desc.'",
            "trx_status": "'.$request->trx_status.'",
            "trx_status_date": "'.$request->trx_status_date.'",
            "trx_reff": "'.$request->trx_reff.'",
            "trx_no": "'.$request->trx_no.'",
            "bank_code": "'.$request->bank_code.'",
            "bank_name": "'.$request->bank_name.'",
            "bank_response_code": "'.$request->bank_response_code.'",
            "bank_response_msg": "'.$request->bank_response_msg.'"
        }';

        // $data =  '{ 
        //     "request":"Notification",
        //     "virtual_account":"9920001189",
        //     "va_name":"Sandbox 3",
        //     "beneficiary_virtual_account":"9920020792",
        //     "beneficiary_account":"1060045760",
        //     "beneficiary_name":"BCA Dummy",
        //     "trx_id":"591289",
        //     "trx_date":"2022-01-26 14:56:53",
        //     "trx_amount":"5333248",
        //     "trx_desc":"Withdrawal",
        //     "trx_status":"2",
        //     "trx_status_date":"2022-01-26 14:57:24.923868",
        //     "trx_reff":"xLRSR7RpWT-W7a0Uo7AT_1643183826577244",
        //     "trx_no":"260054",
        //     "bank_code":"014",
        //     "bank_name":"BANK BCA",
        //     "bank_response_code":"2",
        //     "bank_response_msg":"Transaksi Berhasil"
        // }';
        // $data_notif = file_get_contents('php://input');
        
        // $data = json_decode($data_notif);
        
        // echo "tes";
        // dd($data_notif);
        //signature
        
        // $param["production"]["virtual_account"] 	= "9847090569";
        // $param["production"]["faspay_key"]			= "c2b21b0e-e988-4fc8-ae31-39d7177a7186";
        // $param["production"]["faspay_secret"] 		= "19c887a3-49af-4087-8e82-58a1875b1452";
        // $param["production"]["app_key"]				= "6e7a0e43-6b83-4891-8f7b-b534194c0cfa";
        // $param["production"]["app_secret"]			= "84dce914-e5e2-441e-9f3d-68febf898280";
        // $param["production"]["client_key"]			= "d563f2bb-a901-4b81-8fd3-305770b284a8";
        // $param["production"]["client_secret"] 		= "c0869c7e-1e16-4177-b48c-0f1db9540c9e";
        // $param["production"]["iv"]					= "faspay2018xAuth@#";

        //prod
        $client_id = "d563f2bb-a901-4b81-8fd3-305770b284a8";
        $client_secret = "c0869c7e-1e16-4177-b48c-0f1db9540c9e";
        $app_key = "6e7a0e43-6b83-4891-8f7b-b534194c0cfa";
        $app_secret = "84dce914-e5e2-441e-9f3d-68febf898280";

        //dev
        //  $client_id = "571f97de-3216-4598-a80c-240e6d571000";
        //         $client_secret = "3155e4cb-58af-46c7-b1df-86cd54809672";
        //         $app_key = "055061d5-626a-420b-8e1a-dbd6772fd5f6";
        //         $app_secret = "60d5ce04-b077-44c5-b5b8-6ceeeab11e18";
                
        
        $strReplace = trim(preg_replace("/\r|\n|\t|\ /", "", $data));
        $requestBody = hash("sha256", $strReplace);
        
        $datas[] = $app_key;
        $datas[] = "POST";
        $datas[] = base64_encode($client_id.":".$client_secret);
        $datas[] = strtoupper($requestBody);
        
        $strToSign = implode(":", $datas);
        // print_r($strToSign);
        // exit();
        $signature = hash_hmac("sha256", $strToSign, $app_secret);
        // $signature = sha1(md5(($user_id.$passw.$data->bill_no.$data->payment_status_code)));
        
        // return response()->json(["message"=>$signature]);
        
        // ======= Make Signature Validation ===== 
        if ($request->input('signature') == $signature) {
        
        	/* ====== Update Transaction Status ==== */
        	if ($request->bank_response_code == '2'){
        
        		/* === update your transaction status === */
                 
                $sm = DB::table('sendme')->where("trx_no", $request->trx_no)->first();
                // $sm = DB::select('SELECT * FROM sendme WHERE trx_no = "'.$request->trx_no.'"');
                // return response()->json(["message"=>$sm->id]);
                
                        
                        DB::beginTransaction();
                        try {
                        
                                
                        //         // $send = DB::table('sendme')->where('trx_no',$request->trx_no)->first();
                                
                                $currency                               = Currency::where('active',1)->where('id',$sm->currency_code)->first();
                                $row                                    = Transaction::where('id', $sm->tid)->first();
                                $row->status                            = '1';
                                $row->save();
                                // return response()->json(["message"=>$currency]);
                        //         // DB::table('transactions')->where('id','=',$sm->tid)
                        //         // ->update([
                        //         //     'status' => '1'
                        //         // ]);
                                
                                
                                // $lastBalance        = Balance::where('user_id',$sm->user_id)->where('currency_id',$currency->id)->where('active',1)->orderBy('id','DESC')->first();
                                // if($lastBalance) {
                                //     $balance = $lastBalance->amount - $row->amount;
                                // } else {
                                //     $balance = 0;
                                // }
                
                                // $newBalance                             = NEW Balance;
                                // $newBalance->user_id                    = $row->user_id;
                                // $newBalance->currency_id                = $row->currency_id;
                                // $newBalance->code                       = $row->code;
                                // $newBalance->amount                     = $balance;
                                // $newBalance->save();
                                
                                DB::table('sendme')->where('trx_no',$request->trx_no)
                                ->update([
                                    'status' => $request->bank_response_code
                                ]);
                                
                                
                                DB::commit();
                    //         	$data = array( 
                				// "response"=>"Notification",
                    //           "virtual_account"=>$data->virtual_account,
                    //           "beneficiary_virtual_account"=>$data->beneficiary_virtual_account,
                    //           "bank_code"=>$data->bank_code,
                    //           "bank_name"=>$data->bank_name,
                    //           "response_code"=>"00",
                    //           "response_desc"=>"Success"
                    //     		);
                        
                        // 		$response = json_decode($data);
                        
                        // 		echo $response;
                        
                                $user = db::table('users')->where('id',$sm->user_id)->first();
                        		
                        		 $data = [
                                        'name'  => $user->name,
                                        'amount' => $request->trx_amount,
                                        'fee' => 0,
                                        'remain' => $request->trx_amount - 0,
                                        'bname' => $request->bank_name,
                                        'bacc' => $request->beneficiary_account,
                                        'time' => $request->trx_date,
                                        'trx' => $row->trx_id
                                        
                                ];
                        
                                Mail::send('emails.withdrawok', $data, function($message)use($data, $user) {
                                    $message->to($user->email)
                                            ->subject('Convert Success');
                                });
                                
                                !isset(auth()->user()->adminGmail)? $user->adminGmail = env('MAIL_ADMIN_ADDRESS_GMAIL'):'';
                                Mail::send('emails.admin_withdraw_success', $datas, function($message)use($datas, $user) {
                                    $message->to($user->adminGmail)
                                    ->subject('Convert Success');
                                });
                                
                                !isset(auth()->user()->adminLautan)? $user->adminLautan = env('MAIL_ADMIN_ADDRESS_LAUTAN'):'';
                                Mail::send('emails.admin_withdraw_success', $datas, function($message)use($datas, $user) {
                                    $message->to($user->adminLautan)
                                            ->subject('Convert Success');
                                });


                        
                                return response()->json([
                                    "response"=>"Notification",
                                    "virtual_account"=>$request->virtual_account,
                                    "beneficiary_virtual_account"=>$request->beneficiary_virtual_account,
                                    "bank_code"=>$request->bank_code,
                                    "bank_name"=>$request->bank_name,
                                    "response_code"=>"00",
                                    "response_desc"=>"Success"
                                ]);
                                
                               
                		
                        } catch (ClientException $e) {
                            DB::rollback();
                            Log::info($e->getMessage());
                            return redirect('/withdraw')->withInput()->with('error','Convert error, please try again later');
                        }
        		
               
        
        		/* === give response to faspay === */
        	
        	}else{
         return response()->json(["message"=>"Bank Response Invalid"]);
        }
        	
        }else{
         return response()->json(["message"=>"Signature Not match"]);
        }
    }

    private function encryptAES256($string, $key){
		$plaintext 	= $string;
		$method 	= "aes-256-cbc";
		$password 	= substr(hash('sha256', $key, true), 0, 32);            
		$iv 		= substr(md5($key."faspay2018xAuth@#"), -16);
		
		return base64_encode(openssl_encrypt($plaintext, $method, $password, OPENSSL_RAW_DATA, $iv));
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function bank(){
        return view('withdraw.bankacc');
    }
    public function add_bank(Request $request){
        $coin = isset($request->coin) ? '?coin='. $request->coin : '/';
        DB::table('user_bank')->insert([
            'user_id' => Auth::user()->id,
                            'bank_code' => $request->bank_code,
                            'bank_account' => $request->bank_account,
                            'account_number' => $request->account_number,
                            'isReg' => 0
                        ]);
        return redirect('/withdraw'.$coin)->withInput()->with('success','Add bank account success!!');
    }
    public function edit_bank(Request $request,$id){
        DB::table('user_bank')->where('id',$id)->update([
                            'bank_code' => $request->bank_code,
                            'bank_account' => $request->bank_account,
                            'account_number' => $request->account_number,
                            'isReg' => 0
                        ]);
        return redirect('/withdraw')->withInput()->with('success','Edit bank account success!!');
    }
    
    public function del_bank($id){
        
        $b = DB::table('user_bank')->where('id',$id)->delete();
        return redirect('/withdraw')->withInput()->with('success','Bank account success deleted!!');
    }
    
    public function testemail(Request $request){
            
            $user = db::table('users')->where('email','risfandibayu19@gmail.com')->first();
                
            $data = [
                'name'  => $user->name, 
            ];

            Mail::send('emails.withdrawreqrej', $data, function($message)use($data, $user) {
                $message->to($user->email)
                        ->subject('Convert Success');
            });
            
            dd(date('ymdHis'));
            // echo "tes";
    }
}

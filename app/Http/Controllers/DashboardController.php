<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libs\Binance;
use App\Models\Currency;
use App\Models\custom_variabel;
use App\Models\Webpages;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu               = 'dashboard';
        $assets             = Currency::where('active',1)->orderBy('name')->get();
        $row                = Currency::where('active',1)->orderBy('name')->limit(3)->get();
        $currency           = Currency::where('active',1)->orderBy('price','DESC')->limit(3)->get();
        $coin               = [];
        foreach ($currency as $rows) {
            $binance                                = NEW Binance;
            $send['url']                            = '/ticker/24hr';
            $body['symbol']                         = $rows->code;
            $send['body']                           = $body; 
            $post                                   = $binance->callApi($send);
            if($post) {
                $list['id']                         = $rows->id;
                $list['image']                      = $rows->image;
                $list['code']                       = $rows->code;
                $list['name']                       = $rows->name;
                $list['price']                      = $post->lastPrice;
                $list['change']                     = $post->priceChangePercent;
                $list['highPrice']                  = $post->highPrice;
                $list['lowPrice']                   = $post->lowPrice;
                $list['marketPrice']                = $post->quoteVolume;
                $coin[]                             = $list;
            }   
        }
        $market                                     = json_decode(json_encode($coin), FALSE);
        return view('dashboard.index')->with(compact('menu','row','market','assets'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function landing()
    // {
    //     $check = custom_variabel::where('type','web')->where('code','comingsoon')->first();
    //     if($check){
    //         if($check->value == 'off'){

    //             //getsetting homepage version
    //             $version = custom_variabel::where('type','web')->where('code','homepage')->first();

    //             $pages = Webpages::where('status', 1)->where('slug', 'homepage-'.$version->value)->first();
    //             if($pages){

    //                 //header
    //                 $header = Webpages::where('status', 1)->where('slug', '#header#')->first();

    //                 //getsetting footer version
    //                 $version = custom_variabel::where('type','homepage')->where('code','footer')->first();
    //                 $footer = Webpages::where('status', 1)->where('slug', 'footer-'.$version->value)->first();

    //                 #market#
    //                 $pages = $pages->content;
    //                 $pages = str_replace('#market#', $this->marketTable(), $pages);

    //                 #cryptcash#
    //                 $pages = str_replace('#cryptcash#', $this->cryptCash(), $pages);

    //                 return view('landingpage/pages/index', compact('pages','header','footer'));
    //             } else {
    //                 return view('landingpage/pages/404');
    //             }
    //         } else {
    //             return view('comingsoon');
    //         }
    //     } else {
    //         return view('comingsoon');
    //     }
    // }

    // public function page($slug)
    // {
    //     $check = custom_variabel::where('type','web')->where('code','comingsoon')->first();
    //     if($check){
    //         if($check->value == 'off'){
    //             $pages = Webpages::where('status', 1)->where('slug', $slug)->first();
    //             if($pages){

    //                 $header = Webpages::where('status', 1)->where('slug', '#headerpage#')->first();

    //                 //getsetting footer version
    //                 $version = custom_variabel::where('type','pages')->where('code','footer')->first();
    //                 $footer = Webpages::where('status', 1)->where('slug', 'footer-'.$version->value)->first();

    //                 #market#
    //                 $pages = $pages->content;
    //                 $pages = str_replace('#market#', $this->marketTable(), $pages);

    //                 #cryptcash#
    //                 $pages = str_replace('#cryptcash#', $this->cryptCash(), $pages);

    //                 return view('landingpage/pages/pages', compact('pages','header','footer'));
    //             } else {
    //                 return view('landingpage/pages/404');
    //             }
    //         } else {
    //             return view('comingsoon');
    //         }
    //     } else {
    //         return view('comingsoon');
    //     }
    // }
    public function landing(){
        return view('land.index');
    }

    public function marketTable()
    {
        $gainer = Currency::where('active',1)->orderBy('change','DESC')->limit(5)->get();
        $loser = Currency::where('active',1)->orderBy('change','ASC')->limit(5)->get();

        $section = '<section class="clients-section home-12" style="margin-top:50px">
            <div class="container">
                <div class="sec-titlex"><h2>Market</h2></div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="color:#14183E;font-size:18px;margin-bottom:5px">Top Gainer</p>
                        <table class="table table-condensed table-hover" style="border-bottom:1px solid #cecece">
                            <tr style="background-color: #2F80ED;color:white">
                                <th>Currency</th>
                                <th style="text-align: right">Price</th>
                                <th style="text-align: right">24h %</th>
                                <th style="text-align: right">Volume (24h)</th>
                            </tr>';

        foreach($gainer as $row)
        {
            $section .= '<tr style="color: #333333">
                <td>
                    <div style="padding:5px;width:50px;float:left"><img src="'.$row->image.'" style="height:30px"></div>
                    <div style="float:left;margin:-8px 0px -10px 10px;">'.$row->name.'<div style="margin-top:-5px">'.$row->code.'</div></div>
                </td>
                <td style="text-align: right">'.number_format($row->price).'</td>
                <td style="text-align: right">';

            if($row->change < 0) $section .= '<span style="color: red">'.$row->change.'%</span>';
            else $section .= '<span style="color: green">'.$row->change.'%</span>';

            $section .= '</td><td style="text-align: right">'.number_format($row->marketPrice).'</td></tr>';
        }

        $section .= '</table>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <p style="color:#14183E;font-size:18px;margin-bottom:5px">Top Loser</p>
                        <table class="table table-condensed table-hover" style="border-bottom:1px solid #cecece">
                            <tr style="background-color: #2F80ED;color:white">
                                <th>Currency</th>
                                <th style="text-align: right">Price</th>
                                <th style="text-align: right">24h %</th>
                                <th style="text-align: right">Volume (24h)</th>
                            </tr>';

        foreach($loser as $row)
        {
            $section .= '<tr style="color: #333333">
                    <td>
                    <div style="padding:5px;width:50px;float:left"><img src="'.$row->image.'" style="height:30px"></div>
                    <div style="float:left;margin:-8px 0px -10px 10px;">'.$row->name.'<div style="margin-top:-5px">'.$row->code.'</div></div>
                </td>
                <td style="text-align:right;">'.number_format($row->price).'</td>
                <td style="text-align:right;">';

            if($row->change < 0) $section .= '<span style="color: red">'.$row->change.'%</span>';
            else $section .= '<span style="color: green">'.$row->change.'%</span>';
            
            $section .= '</td><td style="text-align: right">'.number_format($row->marketPrice).'</td></tr>';
        }
        
        $section .= '</table>
                    </div>
                </div>
            </div>
        </section>';

        return $section;
    }

    public function cryptCash()
    {
        $data = Currency::where('active',1)->get();

        $section = '<section class="clients-section home-12">
            <div class="container">
                <div class="sec-titlex"><h2>Laut CryptCash</h2></div>
                <div style="margin-bottom:50px">Integration blockchain technology, simple, and secure for you to <br>offer the forefront of digital asset Interchange transaction.</div>
                <div class="row">
                    <div class="col-md-12">
                        
                        <div>
                            <div class="row">
                                <div class="col-md-4" id="idrleft">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" value="100000" id="idrleft_price" style="width:100%">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control select2-single" id="idrleft_select" style="width:100%">
                                                    <option value="IDR">IDR</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="curleft" style="display:none">
                                    <div class="row">
                                        <div class="col-md-6">';

                                            $price = round(100000 / $data[0]->price,5);

                                            $section .= '<div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" value="'.$price.'" id="curleft_price" style="width:100%">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control select2-single" id="curleft_select" style="width:100%">';
                                                    
                                                foreach($data as $row){ $section .= '<option value="'.$row->code.'">'.$row->code.'</option>'; }
                                            
                                            $section .= '</select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control select2-single" id="changebox">
                                            <option value="BUY">BUY</option>
                                            <option value="SELL">SELL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4" id="curright">
                                    <div class="row">
                                        <div class="col-md-6">';
                                            
                                        $price = round(100000 / $data[0]->price,5);
                                            
                                        $section .= '<div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" value="'.$price.'" id="curright_price" style="width:100%">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control select2-single" id="curright_select" style="width:100%">';
                                                    
                                                foreach($data as $row)
                                                {
                                                    $section .= '<option value="'.$row->code.'">'.$row->code.'</option>';
                                                }

                                        $section .= '</select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="idrright" style="display:none">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="0" value="100000" id="idrright_price" style="width:100%">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control select2-single" id="idrright_select" style="width:100%">
                                                    <option value="IDR">IDR</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2" id="btnnya">
                                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-signin">BUY</button>
                                </div>
                            </div>
                        </div>
        
                    </div>
                </div>
            </div>
        </section>';

        return $section;
    }

    public function calculate(Request $request)
    {
        $change = $request->change;
        $changebox = $request->changebox;

        $idrleft_price = $request->idrleft_price;
        $idrleft_select = $request->idrleft_select;
        $curright_price = $request->curright_price;
        $curright_select = $request->curright_select;

        $idrright_price = $request->idrright_price;
        $idrright_select = $request->idrright_select;
        $curleft_price = $request->curleft_price;
        $curleft_select = $request->curleft_select;

        if($changebox == 'BUY')
        {
            if($change == 'idrleft_price' or $change == 'idrleft_select')
            {
                $getprice = Currency::where('active',1)->where('code', $curright_select)->get();
                    $coin     = [];
                    foreach ($getprice as $rows) {
                        $binance                                = NEW Binance;
                        $send['url']                            = '/ticker/24hr';
                        $body['symbol']                         = $rows->code;
                        $send['body']                           = $body; 
                        $post                                   = $binance->callApi($send);
                        if($post) {
                            $list['id']                         = $rows->id;
                            $list['image']                      = $rows->image;
                            $list['code']                       = $rows->code;
                            $list['name']                       = $rows->name;
                            $list['price']                      = $post->lastPrice;
                            $list['change']                     = $post->priceChangePercent;
                            $list['highPrice']                  = $post->highPrice;
                            $list['lowPrice']                   = $post->lowPrice;
                            $list['marketPrice']                = $post->quoteVolume;
                            $coin[]                             = $list;
                        }   
                    }
                    $pricing = round($idrleft_price / $coin[0]['price'],5);
                    return $pricing;
            }
            else if($change == 'curright_price' or $change == 'curright_select')
            {
                $getprice = Currency::where('active',1)->where('code', $curright_select)->get();
                 $coin               = [];
                foreach ($getprice as $rows) {
                    $binance                                = NEW Binance;
                    $send['url']                            = '/ticker/24hr';
                    $body['symbol']                         = $rows->code;
                    $send['body']                           = $body; 
                    $post                                   = $binance->callApi($send);
                    if($post) {
                        $list['id']                         = $rows->id;
                        $list['image']                      = $rows->image;
                        $list['code']                       = $rows->code;
                        $list['name']                       = $rows->name;
                        $list['price']                      = $post->lastPrice;
                        $list['change']                     = $post->priceChangePercent;
                        $list['highPrice']                  = $post->highPrice;
                        $list['lowPrice']                   = $post->lowPrice;
                        $list['marketPrice']                = $post->quoteVolume;
                        $coin[]                             = $list;
                    }   
                }
                $pricing = number_format($curright_price * $coin[0]['price']);
                return $pricing;
            }

        }
        else
        {
            if($change == 'idrright_price' or $change == 'idrright_select')
            {
                $getprice = Currency::where('active',1)->where('code', $curleft_select)->get();
                 $coin               = [];
                foreach ($getprice as $rows) {
                    $binance                                = NEW Binance;
                    $send['url']                            = '/ticker/24hr';
                    $body['symbol']                         = $rows->code;
                    $send['body']                           = $body; 
                    $post                                   = $binance->callApi($send);
                    if($post) {
                        $list['id']                         = $rows->id;
                        $list['image']                      = $rows->image;
                        $list['code']                       = $rows->code;
                        $list['name']                       = $rows->name;
                        $list['price']                      = $post->lastPrice;
                        $list['change']                     = $post->priceChangePercent;
                        $list['highPrice']                  = $post->highPrice;
                        $list['lowPrice']                   = $post->lowPrice;
                        $list['marketPrice']                = $post->quoteVolume;
                        $coin[]                             = $list;
                    }   
                }
                $pricing = round($idrright_price / $coin[0]['price'],5);
                return $pricing;
            }
            else if($change == 'curleft_price' or $change == 'curleft_select')
            {
                $getprice = Currency::where('active',1)->where('code', $curleft_select)->get();
                 $coin               = [];
                foreach ($getprice as $rows) {
                    $binance                                = NEW Binance;
                    $send['url']                            = '/ticker/24hr';
                    $body['symbol']                         = $rows->code;
                    $send['body']                           = $body; 
                    $post                                   = $binance->callApi($send);
                    if($post) {
                        $list['id']                         = $rows->id;
                        $list['image']                      = $rows->image;
                        $list['code']                       = $rows->code;
                        $list['name']                       = $rows->name;
                        $list['price']                      = $post->lastPrice;
                        $list['change']                     = $post->priceChangePercent;
                        $list['highPrice']                  = $post->highPrice;
                        $list['lowPrice']                   = $post->lowPrice;
                        $list['marketPrice']                = $post->quoteVolume;
                        $coin[]                             = $list;
                    }   
                }
                $pricing = number_format($curleft_price * $coin[0]['price']);
                return $pricing;
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

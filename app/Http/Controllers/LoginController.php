<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=> 401,'errors' => $validator->errors()]);
        }
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['status'=> 402,'errors' => 'User not found']);
        }
        if ($user->active == 0) {
            return response()->json(['status'=> 402,'errors' => 'Your account is not active, check your email for activation link']);
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return response()->json(['status'=> 200,'message' => 'Login Successful']);
        }else{
            return response()->json(['status'=> 402,'errors' => 'Invalid email or password']);
        }

        // $rules = array(
        //     'email'         => 'required',
        //     'password'      => 'required|min:8',
        // );
        // $validator = Validator::make($request->all(), $rules);

        // // process validasi
        // if ($validator->fails()) {
        //     return redirect('/login')->withErrors($validator)->withInput($request->except('password'));
        // } else {

        //     $userdata = array(
        //         'email'       => strtolower($request->input('email')),
        //         'password'    => $request->input('password'),
        //         'active'      => 1
        //     );
            
        //     if (Auth::attempt($userdata)) {
        //         return redirect('/dashboard');
        //     } else {        
        //         return redirect('/login')->withInput()->with('error','Username atau password salah!');
        //     }
            
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

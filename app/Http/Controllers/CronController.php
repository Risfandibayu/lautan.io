<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libs\TKO;
use App\Libs\Binance;
use App\Libs\Bscscan;
use App\Libs\Etherscan;
use App\Models\TkoHistory;
use App\Models\Transaction;
use App\Models\Balance;
class CronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $last                                   = TkoHistory::orderBy('id','DESC')->first();
        $lastId                                 = null;
        // if($last) {
        //     $lastId = $last->tko_id;
        // }

        $tko                                    = NEW TKO;
        $send['url']                            = '/open/v1/deposits';
        // $body                                   = [];
        $body['fromId']                         = $lastId;
        // $send['body']                           = json_encode($body, JSON_UNESCAPED_SLASHES);
        $send['body']                           = $body; 
        $post                                   = $tko->callApi($send);

        if($post->code == 0) {
            foreach($post->data->list as $row) {
                if(!TkoHistory::where('tko_id',$row->id)->first()) {
                    $history                    = NEW TkoHistory;
                    $history->tko_id            = $row->id;
                    $history->asset             = $row->asset;
                    $history->network           = $row->network;
                    $history->address           = $row->address;
                    $history->addressTag        = $row->addressTag;
                    $history->txId              = $row->txId;
                    $history->amount            = $row->amount;
                    $history->transferType      = $row->transferType;
                    $history->status            = $row->status;
                    $history->insertTime        = $row->insertTime;
                    $history->save();
                }
            }
        }
        // dd($post);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bscscan()
    {
        $row                                            = TkoHistory::where('active',1)->orderBy('id')->limit(5)->get();
        if(count($row)) {
            foreach($row as $rows) {
                $tko                                    = NEW Bscscan;
                $body['module']                         = 'proxy';
                $body['action']                         = 'eth_getTransactionByHash';
                $body['txhash']                         = $rows->txId;
                $send['body']                           = $body; 
                $post                                   = $tko->callApi($send);

                if($post->result) {
                    if($post->result->blockHash && $post->result->blockNumber) {
                        $search = strtolower($post->result->from);
                        // $trx = Transaction::where('sender',$post->result->from)->where('status',0)->orderBy('id','DESC')->first();
                        $trx = Transaction::whereRaw("LOWER(sender) = ?",[$search])->where('status',0)->orderBy('id','DESC')->first();
                        if($trx) {
                            $rows->active       = 0;
                            $rows->save();

                            $trx->success_at    = DATE('Y-m-d H:i:s');
                            $trx->status        = 1;
                            $trx->save();

                            $lastBalance        = Balance::where('user_id',$trx->user_id)->where('currency_id',$trx->currency_id)->where('active',1)->orderBy('id','DESC')->first();
                            if($lastBalance) {
                                $balance = $lastBalance->amount + $trx->amount;
                            } else {
                                $balance = $trx->amount;
                            }

                            $newBalance                 = NEW Balance;
                            $newBalance->user_id        = $trx->user_id;
                            $newBalance->currency_id    = $trx->currency_id;
                            $newBalance->code           = $trx->code;
                            $newBalance->amount         = $balance;
                            $newBalance->save();
                        }
                    }
                }
            }
        }

        
        // dd($post);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function etherscan()
    {
        $row                                            = TkoHistory::where('active',1)->orderBy('id')->limit(5)->get();
        if(count($row)) {
            foreach($row as $rows) {
                $tko                                    = NEW Etherscan;
                $body['module']                         = 'proxy';
                $body['action']                         = 'eth_getTransactionByHash';
                $body['txhash']                         = $rows->txId;
                $send['body']                           = $body; 
                $post                                   = $tko->callApi($send);

                if($post->result) {
                    if($post->result->blockHash && $post->result->blockNumber) {
                        $search = strtolower($post->result->from);
                        // $trx = Transaction::where('sender',$post->result->from)->where('status',0)->orderBy('id','DESC')->first();
                        $trx = Transaction::whereRaw("LOWER(sender) = ?",[$search])->where('status',0)->orderBy('id','DESC')->first();
                        if($trx) {
                            $rows->active       = 0;
                            $rows->save();

                            $trx->success_at    = DATE('Y-m-d H:i:s');
                            $trx->status        = 1;
                            $trx->save();

                            $lastBalance        = Balance::where('user_id',$trx->user_id)->where('currency_id',$trx->currency_id)->where('active',1)->orderBy('id','DESC')->first();
                            if($lastBalance) {
                                $balance = $lastBalance->amount + $trx->amount;
                            } else {
                                $balance = $trx->amount;
                            }

                            $newBalance                 = NEW Balance;
                            $newBalance->user_id        = $trx->user_id;
                            $newBalance->currency_id    = $trx->currency_id;
                            $newBalance->code           = $trx->code;
                            $newBalance->amount         = $balance;
                            $newBalance->save();
                        }
                    }
                }
            }
        }

        
        // dd($post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $binance                                = NEW Binance;
        $send['url']                            = '/ticker/24hr';
        $body                                   = [];
        // $body['symbol']                         = 'BNB';
        // $body['interval']                         = 1;
        // $send['body']                           = json_encode($body, JSON_UNESCAPED_SLASHES);
        $send['body']                           = $body; 
        $post                                   = $binance->callApi($send);
        dd($post);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

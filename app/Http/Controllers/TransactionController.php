<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Libs\UploadImage;
use File;
use Validator;
use DB;
use Mail;
use App\Models\Balance;
use Illuminate\Support\Str;
use App\Models\Transaction;
use DateTime;
use App\Libs\Binance;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu           = 'transaction';
        $user           = Auth::user();
        $row            = Transaction::where('active',1)
                        ->when($user->type == 'user', function($qkey) use ($user) {
                            return $qkey->where("user_id",$user->id);
                        })->orderBy('updated_at','DESC')->get();
        return view('transaction.index')->with(compact('menu','row'));
    }

    public function index_adm()
    {
        $menu           = 'transaction';
        $user           = Auth::user();
        $row            = Transaction::where('active',1)
                        ->orderBy('updated_at','DESC')->get();
        return view('transaction.index_adm')->with(compact('menu','row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if (!$request->input('apikey')) {
            return redirect('/tokocrypto')->withInput()->with('error','Masukan api key');
        } else if (!$request->input('secretkey')) {
            return redirect('/tokocrypto')->withInput()->with('error','Masukan secret key');
        } else {
            $user->tokocryptoKey              = $request->input('apikey');
            $user->tokocryptoSecret           = $request->input('secretkey');
            $user->save();
            return redirect('/tokocrypto')->with('success','Data berhasil di simpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request)
    {
        $row                = Transaction::where('id',$request->input('id'))->first();
        if($row) {
            if(!$request->input('txhash')) {
                return redirect('/transaction')->withInput()->with('error','Please input TX ID');
            } else {
                date_default_timezone_set('Asia/Bangkok');
                $row->created_at            = date("Y-m-d H:i:s");
                // $row->created_at            = now();
                $row->txhash                = $request->input('txhash');

                if ($request->hasFile('image')) {
                    $fileProfile                    = $request->file('image');
                    $uploadImage                    = New UploadImage;
                    $filenameProfile                = 'txhash_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
                    $filepathProfile                = 'txhash/' . $filenameProfile;
                    $uploadProfile                  = $uploadImage->upload($filepathProfile, $fileProfile);
                    $row->tximage                   = $uploadProfile;
                }

                $row->save();

                return redirect('/transaction')->with('success','Data has been update');
            }
        } else {
            abort(404);
        }
    }
    
     public function wconfirm(Request $request)
    {
        $row                = Transaction::where('id',$request->input('id'))->first();
        if($row) {
            
                date_default_timezone_set('Asia/Bangkok');
                $row->created_at            = date("Y-m-d H:i:s");
                // $row->created_at            = now();

                if ($request->hasFile('cwimg1')) {
                    $fileProfile                    = $request->file('cwimg1');
                    $uploadImage                    = New UploadImage;
                    $filenameProfile                = 'cwimg1_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
                    $filepathProfile                = 'cwimg/' . $filenameProfile;
                    $uploadProfile                  = $uploadImage->upload($filepathProfile, $fileProfile);
                    $row->cwimg1                   = $uploadProfile;
                }
                if ($request->hasFile('cwimg2')) {
                    $fileProfile                    = $request->file('cwimg2');
                    $uploadImage                    = New UploadImage;
                    $filenameProfile                = 'cwimg2_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
                    $filepathProfile                = 'cwimg/' . $filenameProfile;
                    $uploadProfile                  = $uploadImage->upload($filepathProfile, $fileProfile);
                    $row->cwimg2                    = $uploadProfile;
                }

                $row->save();

                return redirect('/transaction')->with('success','Data has been update');
            
        } else {
            abort(404);
        }
    }
    

    public function adm_confirm(Request $request,$id)
    {
        // dd($id);
        $menu           = 'transaction';
        $trx =  DB::table('transactions')->select('transactions.*','transactions.id as trid','users.*','transactions.created_at as time')
        ->join('users', 'users.id', '=', 'transactions.user_id')
        ->where('transactions.id',$id)
        ->first();
        return view('transaction.confirm')->with(compact('menu','id','trx'));
    }
    public function wadm_confirm(Request $request,$id)
    {
        // dd($id);
        $menu           = 'transaction';
        $trx =  DB::table('transactions')->select('transactions.*','transactions.id as trid','users.*','transactions.created_at as time')
        ->join('users', 'users.id', '=', 'transactions.user_id')
        ->where('transactions.id',$id)
        ->first();
        $bank = DB::table('user_bank')->select('user_bank.*','banks.name as bn')
        ->join('banks','banks.code','=','user_bank.bank_code')
        ->where('user_bank.id',$trx->bankID)->first();
        
            $binance                                = NEW Binance;
            $send['url']                            = '/ticker/24hr';
            $body['symbol']                         = $trx->code;
            $send['body']                           = $body; 
            $post                                   = $binance->callApi($send);
        return view('transaction.wconfirm')->with(compact('menu','id','trx','post','bank'));
    }

    public function trx_confirm(Request $request)
    {
        // dd($request->id);
        $row = Transaction::where('id', $request->id)->first();
        $row->status      = '1';
        $row->save();
        
        $balance = Balance::where('user_id',$row->user_id)->where('code',$row->code)->orderBy('created_at', 'DESC')->first();
        if($balance) {
            $blc = $balance->amount;
        } else {
            $blc = 0;
        }
        
        
        
        $newBalance                             = NEW Balance;
                $newBalance->user_id                    = $row->user_id;
                $newBalance->currency_id                = $row->currency_id;
                $newBalance->code                       = $row->code;
                $newBalance->amount                     = $row->amount + $blc;
                $newBalance->save();

        
        return redirect('/transaction/adm')->with('success','Data has been update');
    }
    public function wtrx_confirm(Request $request)
    {
        // dd($request->id);
        $row = Transaction::where('id', $request->id)->first();
        $row->status      = '1';
        $row->save();
        
        $balance = Balance::where('user_id',$row->user_id)->where('code',$row->code)->orderBy('created_at', 'DESC')->first();
        if($balance) {
            $blc = $balance->amount;
        } else {
            $blc = 0;
        }
        
        $binance                                = NEW Binance;
            $send['url']                            = '/ticker/24hr';
            $body['symbol']                         = $row->code;
            $send['body']                           = $body; 
            $post                                   = $binance->callApi($send);
            
        $newBalance                             = NEW Balance;
                $newBalance->user_id                    = $row->user_id;
                $newBalance->currency_id                = $row->currency_id;
                $newBalance->code                       = $row->code;
                $newBalance->amount                     = $blc - $row->amount ;
                $newBalance->save();
                
        
        $user = db::table('users')->where('id',$row->user_id)->first();
        $bank = DB::table('user_bank')->select('user_bank.*','banks.name as bn')
        ->join('banks','banks.code','=','user_bank.bank_code')
        ->where('user_bank.id',$row->bankID)->first();
        $data = [
                                        'name'  => $user->name,
                                        'amount' => $post->lastPrice * $row->amount,
                                        'fee' => 0,
                                        'remain' => ($post->lastPrice * $row->amount) - 0,
                                        'bname' => $bank->bn,
                                        'bacc' => $bank->account_number,
                                        'time' => $row->updated_at,
                                        'trx' => $row->trx_id
                                        
                                    ];
                        
                        
                                    Mail::send('emails.withdrawreqok', $data, function($message)use($data, $user) {
                                        $message->to($user->email)
                                                ->subject('Withdraw Confirmation Request Success');
                                    });


        
        return redirect('/transaction/adm')->with('success','Data has been update');
    }
    
    public function trx_reject(Request $request){
        $row = Transaction::where('id', $request->id)->first();
        $row->status      = '2';
        $row->save();
        
        
        return redirect('/transaction/adm')->with('success','Data has been update');
    }
    public function wtrx_reject(Request $request){
        $row = Transaction::where('id', $request->id)->first();
        $row->status      = '2';
        $row->save();
        
         
        $binance                                = NEW Binance;
            $send['url']                            = '/ticker/24hr';
            $body['symbol']                         = $row->code;
            $send['body']                           = $body; 
            $post                                   = $binance->callApi($send);
        
         $user = db::table('users')->where('id',$row->user_id)->first();
        $bank = DB::table('user_bank')->select('user_bank.*','banks.name as bn')
        ->join('banks','banks.code','=','user_bank.bank_code')
        ->where('user_bank.id',$row->bankID)->first();
        $data = [
                                        'name'  => $user->name,
                                        'amount' => $post->lastPrice * $row->amount,
                                        'fee' => 0,
                                        'remain' => ($post->lastPrice * $row->amount) - 0,
                                        'bname' => $bank->bn,
                                        'bacc' => $bank->account_number,
                                        'time' => $row->updated_at,
                                        'trx' => $row->trx_id
                                        
                                    ];
                        
                        
                                    Mail::send('emails.withdrawreqrej', $data, function($message)use($data, $user) {
                                        $message->to($user->email)
                                                ->subject('Withdraw Confirmation Request Rejected');
                                    });
                                    
        return redirect('/transaction/adm')->with('success','Data has been update');
    }
    
    public function cfaspay(Request $request){
        $request->session()->put('email', $request->input('email'));
        return view('faspay');
    }

    public function faspay(Request $request){
//         $faspayer = new LaravelFaspay();
//         $paymentChannel['pg_code'] = "802";
//   $paymentChannel['pg_name'] = "Mandiri Virtual Account";
    
//   // Create an array for ordered Item
//   $item = Array();
//   $order["product"] = "Item #1"; // Product Name
//   $order["qty"] = 1; // QTY
//   $order["amount"] = 100000; // Price. Just the real price, without any '.'(dot) or ',' (comma).
//   $order["paymentplan"] = 1; // Payment Plan. See References below.
//   $order["merchant_id"] = $faspayer->getConfig()->getUser()->getMerchantId(); // Merchant ID
//   $order["tenor"] = 00; // Tenor. See References below.
//   array_push($item, $order); // Push order,
//   // Loop or push again for more than 1 items.

//   // Create Bill Data
//   $billData["billno"] = "1232123"; // Bill Number
//   $billData["billdesc"] = "Pembayaran RHI"; // Billing Description
//   $billData["billexp"] = 2; // Expired Day Interval (in total days)
//   $billData["billtotal"] = 10000; // Bill Total, Just the real bill, without any '.'(dot) or ',' (comma).
//   $billData["paytype"] = 3; // Pay Type. See References below.

//     // Create User Data
//   $userData["phone"] = "087123123123"; // Phone Number
//   $userData["email"] = "arka.progammer@gmail.com"; // Email
//   $userData["terminal"] = 21; // Terminal
//   $userData["custno"] = "12345"; // Customer No
//   $userData["custname"] = "thearka"; // Customer Name

//   // Proccess the request and return the result.
//   return $faspayer->createPayment($item, $paymentChannel, $billData, $userData);
            $signature = sha1(md5(("bot34465gL@i9ckU".date('Ymdhis'))));
            $data = array(
                "request"=>"Post Data Transaction",
              "merchant_id"=>"34465",
              "merchant"=>"PT Lautan Harum Mewangi",
              "bill_no"=> date('Ymdhis'),
              "bill_reff"=>"LHW".date('Ymdhis'),
              "bill_date"=> date("Y-m-d H:i:s"),
              "bill_expired"=>date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s") . ' +1 day')),
              "bill_desc"=>"Pembayaran #".date('Ymdhis'),
              "bill_currency"=>"IDR",
              "bill_gross"=>"0",
              "bill_miscfee"=>"0",
              "bill_total"=>"10000000",
              
              
              "cust_no"=>"1",
              "cust_name"=>$request->name,
              "payment_channel"=>"402",
              "pay_type"=>"1",
              "bank_userid"=>"",
              "msisdn"=>$request->phone,
              "email"=>$request->email,
              "terminal"=>"10",
              
              
              "billing_name"=>"0",
              "billing_lastname"=>"0",
              "billing_address"=>"jalan pintu air raya",
              "billing_address_city"=>"Jakarta Pusat",
              "billing_address_region"=>"DKI Jakarta",
              "billing_address_state"=>"Indonesia",
              "billing_address_poscode"=>"10710",
              "billing_msisdn"=>"",
              "billing_address_country_code"=>"ID",
              "receiver_name_for_shipping"=>"Nur Auliya",
              "shipping_lastname"=>"",
              "shipping_address"=>"jalan pintu air raya",
              "shipping_address_city"=>"Jakarta Pusat",
              "shipping_address_region"=>"DKI Jakarta",
              "shipping_address_state"=>"Indonesia",
              "shipping_address_poscode"=>"10710",
              "shipping_msisdn"=>"",
              "shipping_address_country_code"=>"ID",
              "item"=>
                array(
                  "product"=>"Laut CryptCash Member Subscription",
                  "qty"=>"1",
                  "amount"=>"10000000",
                  "payment_plan"=>"01",
                  "merchant_id"=>"34465",
                  "tenor"=>"00"
                )
              ,
              "reserve1"=>"",
              "reserve2"=>"",
              "signature"=>$signature
            );
            // $data = array(
            //     'name' => 'Ngelapak',
            //     'email' => 'customer@ngelapak.co.id',
            //     'phone' => '08113443154',
            //     'password' => 'Ngelapak2021!',
            // );
            $body = json_encode($data, JSON_UNESCAPED_SLASHES);
            $headers = array(
                'Content-Type: application/json',
            );
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
            // CURLOPT_URL => 'https://debit-sandbox.faspay.co.id/cvr/300011/10',
            CURLOPT_URL => 'https://web.faspay.co.id/cvr/300011/10',
            // CURLOPT_URL => 'https://my.ipaymu.com/api/v2/register',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => $headers,
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            $result = json_decode($response);
            DB::table('subcribe')->insert([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'trx_id' => $result->trx_id,
                'trx_date' => date("Y-m-d H:i:s"),
                'bill_no' =>$result->bill_no,
                'status' => 0,
            ]);
            
            // echo $response;
            
            // dd($response);
            // echo date("Y-m-d H:i:s");
            // echo date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s") . ' +1 day'));
            // dd($result[0]);
            return redirect($result->redirect_url);
    }
    public function faspay_notify(Request $request){
        // $request = $data;
        $user_id 	='bot34465';
        $passw		='gL@i9ckU';
        
        
        $signature = sha1(md5(($user_id.$passw.$request->bill_no.$request->payment_status_code)));
        
        // ======= Make Signature Validation ===== 
        if ($request->signature == $signature) {
        
        	/* ====== Update Transaction Status ==== */
        	if ($request->payment_status_code == '2'){
        
        		/* === update your transaction status === */
        		DB::table('subcribe')->where('trx_id', $request->trx_id)->update([
        		'status' => 1,
        		]);
        
        		/* === give response to faspay === */
        		return response()->json([
        				"response"=>"Payment Notification",
        				"trx_id"=>$request->trx_id,
        				"merchant_id"=> $request->merchant_id,
        				"merchant"=> $request->merchant,
        				"bill_no"=> $request->bill_no,
        				"response_code"=>"00",
        				"response_desc"=>"Payment Sukses",
        				"response_date"=> date('Y-m-d H:is')
        		]);
        
        // 		$response = json_encode($data);
        
        // 		echo $response;
        	}
        }
    
    }
    
    
    public function faspay_callback(Request $request){
        DB::table('subcribe')->where('trx_id', $request->trx_id)->update([
        		'status' => 2,
        		]);
        echo "Sukses";

    }
    
    public function test_email(){
        
        Mail::send('emails.withdrawok', $data, function($message)use($data, $user) {
                                        $message->to($user->email)
                                                ->subject('Withdraw Success');
                                    });
    }
}

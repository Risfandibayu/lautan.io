<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Currency;
use App\Models\Balance;
use Illuminate\Support\Facades\Auth;

class DepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if(!Auth::user()->tokocryptoKey OR !Auth::user()->tokocryptoSecret) {
        //     return redirect('/tokocrypto')->with('warning','Silahkan hubungkan akun Tokocrypto Anda terlebih dahulu sebelum dapat mengakses menu Penarikan');
        // if(Auth::user()->status != 3) {
        //     return redirect('/verification')->with('warning','Silahkan lakukan verifikasi terlebih dahulu sebelum dapat mengakses menu Penarikan');
        // } else {
            // dd(session()->all());
            $menu = 'balance';
            $coin                = Currency::where('active',1)->orderBy('name')->get();
            return view('deposit.index')->with(compact('menu','coin'));
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $txs = Transaction::where('sender',$request->input('sender'))->first();
        if ($request->input('amount') <= 0) {
            return redirect('/deposit?code='.$request->input('code'))->withInput()->with('error','Please input top up amount');
        }else if($txs){
            return redirect('/deposit?code='.$request->input('code'))->withInput()->with('error','Sender Address Invalid');
        }else if (!$request->input('sender')) {
            return redirect('/deposit?code='.$request->input('code'))->withInput()->with('error','Please input sender wallet address');
        } else {
            $currency           = Currency::where('active',1)->where('code',$request->input('code'))->first();
            $row                = NEW Transaction;
            $row->trx_id        = 'D'.$user->id.date('ymdhis');
            $row->user_id       = $user->id;
            $row->amount        = $request->input('amount');
            $row->sender        = $request->input('sender');
            $row->receiver      = $currency->address;
            $row->currency_id   = $currency->id;
            $row->code          = $currency->code;
            $row->save();

            $request->session()->put('sender',$request->sender);
            $request->session()->put('currency_address',$currency->address);
            $request->session()->put('amount',$request->amount);


            return redirect('/deposit')->with('success',"Top Up request created. Please log in to your hot wallet account and sent your digital asset to this address below :");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

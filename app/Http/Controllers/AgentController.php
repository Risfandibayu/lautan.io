<?php

namespace App\Http\Controllers;

use App\Libs\UploadImage;
use App\Models\AgentFee;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

// use Auth;

use App\Models\User;
use App\Models\AgentRole;
use App\Models\Bank;
use App\Models\Partner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu               = 'agent';
        $row                = User::where('active',1)->where('type','user')->where('agentStatus',3)->orderBy('name')->get();
        return view('agent.index')->with(compact('menu','row'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reqlist()
    {
        $menu               = 'agent';
        $row                = User::where('active',1)->where('type','user')->where('agentStatus',1)->orderBy('name')->get();
        return view('agent.req')->with(compact('menu','row'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function landing()
    {
        $menu               = 'agent';
        $user                       = Auth::user();
        if($user->agentStatus == 0) {
            return view('agent.landing')->with(compact('menu'));
        } else  {
            return redirect('/agent/done');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function req(Request $request)
    {
        if(Auth::user()->status != 3) {
            return redirect('/verification')->with('warning','Please verify first before you can become an agent');
        } else {
            $row                    = Auth::user();
            $row->agentStatus       = 1;

            if ($request->hasFile('fileKuisioner')) {
                $file                    = $request->file('fileKuisioner');
                $uploadImage            = New UploadImage;
                $filenameProfile        = $row->id . '_nationalid_' . Str::random(6) . '.' . $file->getClientOriginalExtension();
                $filepathProfile        = 'verification/' . $filenameProfile;
                $uploadProfile          = $uploadImage->upload($filepathProfile, $file);
                //Log::info($uploadProfile);
                //$image_resize                   = Image::make($file->getRealPath())->widen(500);              
                //$image_resize->save($destinationPath.'/'.$filename);
                //$uploadSuccess                  = $file->move($destinationPath, $filename);
                $row->fileKuisioner   = $uploadProfile;
            }
            $row->save();

            $data = [
                'name'  => $row->name, 
                'url'   => 'https://lautan.io'
            ];
            Mail::send('emails.agent', $data, function($message)use($data, $row) {
                $message->to($row->email)
                        ->subject('Partner Questionnaire');
            });

            !isset(auth()->user()->adminGmail)? $row->adminGmail = env('MAIL_ADMIN_ADDRESS_GMAIL'):'';
            Mail::send('emails.kyc_admin', $data, function($message)use($data, $row) {
                $message->to($row->adminGmail)
                ->subject('Partner Request');
            });
            
            !isset(auth()->user()->adminLautan)? $row->adminLautan = env('MAIL_ADMIN_ADDRESS_LAUTAN'):'';
            Mail::send('emails.kyc_admin', $data, function($message)use($data, $row) {
                $message->to($row->adminLautan)
                        ->subject('Partner Request');
            });


            return redirect('/agent/done');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function done()
    {
        $agent_role = AgentRole::find(auth()->user()->agent_role_id);
        if ($agent_role) {
            $role = $agent_role->name;
        }else{
            $role = '';
        }
        $menu               = 'agent';
        $referal = auth()->user()->as_agent_referal != ""? auth()->user()->as_agent_referal:null;
        // dd($referal);
        $partner = User::userAgentFee($referal);
        // $director = User::
        // dd($partner);
        $p2 = User::where('user_agent_referal', $referal)->get();
        $referal != null ? $count = $p2->count(): $count = -1;

        if(Auth::user()->agentStatus == 3  && $count < 0 && auth()->user()->as_agent_referal != null ) {
            $status = true;
        }else {
            $status = false;
        }
  
        // die;
        return view('agent.done')->with(compact('menu','partner','role','referal','status'));
    }

    

    public function transaction($id){
        $menu               = 'agent';
        $transaction        = AgentFee::where('user_id', $id)->get();
        // dd($transaction);
        $user = User::find($id);
        return view('agent.transaction')->with(compact('menu','transaction','user'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu               = 'user';
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            return view('user.show')->with(compact('menu','row'));
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu               = 'user';
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $bank = Bank::where('active',1)->orderBy('name')->get();
            return view('user.edit')->with(compact('menu','row','bank'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row               = User::where('active',1)->where('id',$id)->first();
        if($row) {
            if(!$request->input('name')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input name');
            } elseif (!$request->input('email') or !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input a valid email');
            } elseif (User::where('email', strtolower($request->input('email')))->where('id','!=',$id)->first()) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Email already registered');
            } elseif (!$request->input('phone') or !is_numeric($request->input('phone'))) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please enter a valid phone number');
            } elseif (User::where('phone', $request->input('phone'))->where('id','!=',$id)->first()) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Phone number already registered');
            } elseif (!$request->input('nationalId')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input national id');
            } elseif (!$request->input('address')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input address');
            } elseif (!$request->input('bankCode')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input bank');
            } elseif (!$request->input('bankAccount')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input bank account name');
            } elseif (!$request->input('bankNumber')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input bank account number');
            } else {
                $bank                       = Bank::where('code',$request->input('bankCode'))->first();
                $row->name                  = $request->input('name');
                $row->email                 = strtolower($request->input('email'));
                $row->phone                 = $request->input('phone');
                $row->address               = $request->input('address');
                $row->nationalId            = $request->input('nationalId');
                $row->bankCode              = $request->input('bankCode');
                $row->bankName              = $bank->name;
                $row->bankAccount           = $request->input('bankAccount');
                $row->bankNumber            = $request->input('bankNumber');
                if($request->input('password')) {
                    $row->password  = Hash::make($request->input('password'));
                }
                $row->save();

                return redirect('/user')->with('success','Data has been update');
            }
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $id                 = $request->input('id');
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $row->agentStatus    = 0;
            $row->save();
            return redirect('/agent')->with('success', 'Agent has been deactivate');
        } else {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verification($id)
    {
        $menu               = 'user';
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            return view('user.verification')->with(compact('menu','row'));
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request)
    {
        $id   = $request->input('id');
        $row  = User::where('active',1)->where('id',$id)->first();
        $role = $request->input('role');
        $refrenceCode = User::referenceCode($role);
        if($row) {
            $row->agentStatus       = 3; //status validasi admin 3 = approved
            $row->agent_role_id     = $role;
            $row->as_agent_referal = $refrenceCode;
            $row->save();
            // add refece code 
            
            
            $data = [
                'name'  => $row->name, 
                'reference' => $refrenceCode,
                'url'   => 'https://lautan.io'
            ];

            Mail::send('emails.agentapprove', $data, function($message)use($data, $row) {
                $message->to($row->email)
                        ->subject('Partner Approved');
            });

            return redirect('/agent')->with('success', 'Data has been update');
        } else {
            abort(404);
        }
    }

    public function referal(){
        $user_id = auth()->user()->id;
        $user = User::where('active',1)->find($user_id);
        $role = auth()->user()->agent_role_id;
        $refrenceCode = User::referenceCode($role);
        if($user){
            $user->as_agent_referal = $refrenceCode;
            $user->save();
        }
        return redirect('/agent/done')->with('success','Data has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request)
    {
        $id                 = $request->input('id');
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $row->agentStatus       = 2; //status validasi admin 2 = rejected
            $row->save();
            return redirect('/agentrequest')->with('success', 'Data has been update');
        } else {
            abort(404);
        }
    }
}

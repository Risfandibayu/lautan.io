<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('register');
    }

    public function referal($id){
        $user = User::where('as_agent_referal',$id)->first();
        if($user){
            return response()->json(['status'=>'success','data'=>true]);
        }else{
            return response()->json(['status'=>'erorr','data'=>false]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|min:10|max:20',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>401,'errors'=>$validator->errors()]);
        }

        $refer = $request->input('refer');
        $user = User::where('as_agent_referal',$refer)->first();
        if($user){
            $user_agent_referal = $user->as_agent_referal;
        }else{
            $user_agent_referal = null;
        }
        
        DB::beginTransaction();
        try {
            $row            = NEW User;
            $row->active    = 0;
            $row->name      = $request->input('name');
            $row->email     = strtolower($request->input('email'));
            $row->phonecode = $request->input('phonecode');
            $row->phone     = $request->input('phone');
            $row->password  = Hash::make($request->input('password'));
            $row->uuid      = strtoupper(Uuid::generate(4));
            $row->user_agent_referal = $user_agent_referal;
            $row->save();

            $data = [
                'name' => $row->name,
                'url' => url('register/'.$row->uuid)
            ];

            Mail::send('emails.register', $data, function($message)use($data, $row) {
                $message->to($row->email)
                        ->subject('Konfirmasi Pendaftaran');
            });
            DB::commit();
            return response()->json(['status'=>201,'message'=>'Registrasi berhasil, silahkan cek email Anda']);
        } catch (ClientException $e) {
            DB::rollback();
            return response()->json(['status'=>401,'message'=>'Registrasi gagal']);
        }




        // if(!$request->input('name')) {
        //     return redirect('/register')->withInput()->with('error','Please input your name');
        // } elseif (!$request->input('email') or !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
        //     return redirect('/register')->withInput()->with('error','Please input a valid email');
        // } elseif (User::where('email', strtolower($request->input('email')))->first()) {
        //     return redirect('/register')->withInput()->with('error','Email already registered');
        // } elseif (!$request->input('phone') or !is_numeric($request->input('phone'))) {
        //     return redirect('/register')->withInput()->with('error','Please enter a valid phone number');
        // // } elseif (User::where('phone', $request->input('phone'))->first()) {
        //     // return redirect('/register')->withInput()->with('error','Phone number already registered');
        // } elseif (!$request->input('password')) {
        //     return redirect('/register')->withInput()->with('error','Please input your password');
        // } elseif (!$request->input('repassword')) {
        //     return redirect('/register')->withInput()->with('error','Please input your re-password');
        // } elseif ($request->input('password') != $request->input('repassword')) {
        //     return redirect('/register')->withInput()->with('error','Password and re-password not match');
        // } else {
        //     // find refer
        //     $refer = $request->input('refer');
        //     $user = User::where('as_agent_referal',$refer)->first();
        //     if($user){
        //         $user_agent_referal = $user->as_agent_referal;
        //     }else{
        //         $user_agent_referal = null;
        //     }

        //     DB::beginTransaction();
        //     try {
        //         $row            = NEW User;
        //         $row->name      = $request->input('name');
        //         $row->email     = strtolower($request->input('email'));
        //         $row->phonecode = $request->input('phonecode');
        //         $row->phone     = $request->input('phone');
        //         $row->password  = Hash::make($request->input('password'));
        //         $row->uuid      = strtoupper(Uuid::generate(4));
        //         $row->user_agent_referal = $user_agent_referal;
        //         $row->save();

        //         $data = [
        //             'name' => $row->name,
        //             'url' => url('register/'.$row->uuid)
        //         ];

        //         Mail::send('emails.register', $data, function($message)use($data, $row) {
        //             $message->to($row->email)
        //                     ->subject('Konfirmasi Pendaftaran');
        //         });
        //         DB::commit();
        //         return redirect('/login')->with('success','Registrasi berhasil, silahkan cek email Anda');
        //     } catch (ClientException $e) {
        //         DB::rollback();
        //         return redirect('/register')->with('error','Terjadi kesalahan');
        //     }
            
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('uuid',$id)->first();
        if ($user) {
            $user->update(['active'=>1]);
            Auth::login($user);
            return redirect('/dashboard');
        }else{
            return redirect('/login');
        }
        // if($row = User::where('uuid',$id)->first()) {
        //     $row->update(['active'=>1]);
        //     Auth::login($row);
        //     return redirect('/dashboard');
        // } else {
        //     return redirect('/login')->with('error','User tidak ditemukan');
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

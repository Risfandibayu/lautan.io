<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Bank;
use App\Models\AgentRole;
use App\Models\Currency;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu               = 'user';
        $row                = User::where('active',1)->where('type','user')->orderBy('name')->get();
        return view('user.index')->with(compact('menu','row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu               = 'user';
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            return view('user.show')->with(compact('menu','row'));
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu               = 'user';
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $bank = Bank::where('active',1)->orderBy('name')->get();
            return view('user.edit')->with(compact('menu','row','bank'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            if(!$request->input('name')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input name');
            } elseif (!$request->input('email') or !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input a valid email');
            } elseif (User::where('email', strtolower($request->input('email')))->where('id','!=',$id)->first()) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Email already registered');
            } elseif (!$request->input('phone') or !is_numeric($request->input('phone'))) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please enter a valid phone number');
            } elseif (User::where('phone', $request->input('phone'))->where('id','!=',$id)->first()) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Phone number already registered');
            } elseif (!$request->input('nationalId')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input national id');
            } elseif (!$request->input('address')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input address');
            } elseif (!$request->input('bankCode')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input bank');
            } elseif (!$request->input('bankAccount')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input bank account name');
            } elseif (!$request->input('bankNumber')) {
                return redirect('/user/'.$id.'/edit')->withInput()->with('error','Please input bank account number');
            } else {
                $bank                       = Bank::where('code',$request->input('bankCode'))->first();
                $row->name                  = $request->input('name');
                $row->email                 = strtolower($request->input('email'));
                $row->phone                 = $request->input('phone');
                $row->address               = $request->input('address');
                $row->nationalId            = $request->input('nationalId');
                $row->bankCode              = $request->input('bankCode');
                $row->bankName              = $bank->name;
                $row->bankAccount           = $request->input('bankAccount');
                $row->bankNumber            = $request->input('bankNumber');
                if($request->input('password')) {
                    $row->password  = Hash::make($request->input('password'));
                }
                $row->save();

                return redirect('/user')->with('success','Data has been update');
            }
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $id                 = $request->input('id');
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $row->active    = 0;
            $row->save();
            return redirect('/user')->with('success', 'User has been deactivate');
        } else {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verification($id)
    {
        $menu               = 'user';
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $role                = AgentRole::where('active',1)->orderBy('level')->get();
            return view('user.verification')->with(compact('menu','row','role'));
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request)
    {
        $id                 = $request->input('id');
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $row->status    = $request->input('status');
            $row->save();
            return redirect('/user/verification/'.$id)->with('success', 'Data has been update');
        } else {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assets($id)
    {
        $menu               = 'user';
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $currency                = Currency::where('active',1)->orderBy('name')->get();
            return view('user.assets')->with(compact('menu','row','currency'));
        } else {
            abort(404);
        }
    }
}

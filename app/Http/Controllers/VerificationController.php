<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Libs\UploadImage;
use Hash;
use db;
use File;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Bank;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class VerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user                       = Auth::user();
        if($user->status == 1) {
            $menu = 'account';
            return view('verification.index')->with(compact('menu'));
        } else  {
            return redirect('/verification/done');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bank()
    {
        $user                       = Auth::user();
        if($user->status == 1) {
            $menu = 'account';
            $row = Bank::where('active',1)->orderBy('name')->get();
            return view('verification.bank')->with(compact('menu','row'));
        } else {
            return redirect('/verification/done');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function file()
    {
        $user                       = Auth::user();
        if($user->status == 1) {
            $menu = 'account';
            return view('verification.file')->with(compact('menu'));
        } else {
            return redirect('/verification/done');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function done()
    {
        $user                       = Auth::user();
        if($user->status == 1) {
            return redirect('/verification');
        } else {
            $menu = 'account';
            return view('verification.done')->with(compact('menu'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user                       = Auth::user();
        if($user->status == 1) {
            $user->name                 = $request->input('name');
            $user->phone                = $request->input('phone');
            $user->address              = $request->input('address');
            $user->nationalId           = $request->input('nationalId');
            $user->save();
            return redirect('/verification/file');
        } else {
            return redirect('/verification/done');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBank(Request $request)
    {
        $user                       = Auth::user();
        if($user->status == 1) {
            $bank                       = Bank::where('code',$request->input('bankCode'))->first();
            $user->bankCode             = $request->input('bankCode');
            $user->bankName             = $bank->name;
            $user->bankAccount          = $request->input('bankAccount');
            $user->bankNumber           = $request->input('bankNumber');
            $user->save();
            return redirect('/verification/file');
        } else {
            return redirect('/verification/done');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeFile(Request $request)
    {
        $user                       = Auth::user();
        if($user->status == 1) {
            // cek apakah folder untuk menyimpan data
            // kalau belum,BUATKAN!
            // $destinationPath                    = public_path() . '/uploads/verification';
            // upload logo

            if ($request->hasFile('fileNationalId')) {
                // if (!File::exists($destinationPath)) {
                //     File::makeDirectory($destinationPath, $mode = 0777, true, true);
                // }
                $fileProfile                    = $request->file('fileNationalId');
                // $filename                       = $user->id . '_' . Str::random(6) .'.'.$file->getClientOriginalExtension();
                // $uploadSuccess                  = $file->move($destinationPath, $filename);
                // $user->fileNationalId           = $filename;
                $uploadImage            = New UploadImage;
                $filenameProfile        = $user->id . '_nationalid_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
                $filepathProfile        = 'verification/' . $filenameProfile;
                $uploadProfile          = $uploadImage->upload($filepathProfile, $fileProfile);
                //Log::info($uploadProfile);
                //$image_resize                   = Image::make($file->getRealPath())->widen(500);              
                //$image_resize->save($destinationPath.'/'.$filename);
                //$uploadSuccess                  = $file->move($destinationPath, $filename);
                $user->fileNationalId   = $uploadProfile;
            }

            if ($request->hasFile('filePhoto')) {
                // if (!File::exists($destinationPath)) {
                //     File::makeDirectory($destinationPath, $mode = 0777, true, true);
                // }
                $fileProfile                    = $request->file('filePhoto');
                $uploadImage                    = New UploadImage;
                $filenameProfile                = $user->id . '_selfie_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
                $filepathProfile                = 'verification/' . $filenameProfile;
                $uploadProfile                  = $uploadImage->upload($filepathProfile, $fileProfile);
                // $filename                       = $user->id . '_' . Str::random(6).'.'.$file->getClientOriginalExtension();
                // $uploadSuccess                  = $file->move($destinationPath, $filename);
                $user->filePhoto                = $uploadProfile;
            }

            if ($request->hasFile('fileBank')) {
                // if (!File::exists($destinationPath)) {
                //     File::makeDirectory($destinationPath, $mode = 0777, true, true);
                // }
                // $file                           = $request->file('fileBank');
                // $filename                       = $user->id . '_' . Str::random(6).'.'.$file->getClientOriginalExtension();
                // $uploadSuccess                  = $file->move($destinationPath, $filename);
                $fileProfile                    = $request->file('fileBank');
                $uploadImage                    = New UploadImage;
                $filenameProfile                = $user->id . '_bank_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
                $filepathProfile                = 'verification/' . $filenameProfile;
                $uploadProfile                  = $uploadImage->upload($filepathProfile, $fileProfile);
                $user->fileBank                 = $uploadProfile;
            }
            $user->status = 2;
            $user->save();

            // send mail
            
            $data = [
                'name'  => auth()->user()->name, 
                'email'   => auth()->user()->email,
            ];
            !isset(auth()->user()->adminGmail)? $user->adminGmail = env('MAIL_ADMIN_ADDRESS_GMAIL'):'';
            Mail::send('emails.kyc_admin', $data, function($message)use($data, $user) {
                $message->to($user->adminGmail)
                ->subject('Registration KYC Upload');
            });
            
            !isset(auth()->user()->adminLautan)? $user->adminLautan = env('MAIL_ADMIN_ADDRESS_LAUTAN'):'';
            Mail::send('emails.kyc_admin', $data, function($message)use($data, $user) {
                $message->to($user->adminLautan)
                        ->subject('Registration KYC Upload');
            });
        }
        return redirect('/verification/done');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $menu               = 'verification';
        $row                = User::where('active',1)->where('type','user')->where('status','2')->orderBy('updated_at')->get();
        return view('verification.list')->with(compact('menu','row'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

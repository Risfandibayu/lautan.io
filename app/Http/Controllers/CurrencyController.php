<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Libs\UploadImage;
use File;
use Illuminate\Support\Str;
use Auth;
use Validator;
use Hash;
use App\Models\Currency;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu               = 'currency';
        $row                = Currency::orderBy('name')->get();
        return view('currency.index')->with(compact('menu','row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu               = 'currency';
        return view('currency.create')->with(compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->input('name')) {
            return redirect('/currency/')->withInput()->with('error','Please input name');
        } elseif (!$request->input('code')) {
            return redirect('/currency/')->withInput()->with('error','Please input code');
        // } elseif (!$request->input('address')) {
            // return redirect('/currency/')->withInput()->with('error','Please input address');
        } else {
            $row                        = NEW Currency;
            $row->name                  = $request->input('name');
            $row->code                  = $request->input('code');
            $row->address               = $request->input('address');

            if ($request->hasFile('icon')) {
                $fileProfile                    = $request->file('icon');
                $uploadImage                    = New UploadImage;
                $filenameProfile                = 'icon_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
                $filepathProfile                = 'currency/' . $filenameProfile;
                $uploadProfile                  = $uploadImage->upload($filepathProfile, $fileProfile);
                $row->image                     = $uploadProfile;
            }

            $row->save();

            return redirect('/currency')->with('success','Data has been add');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu               = 'currency';
        $row                = Currency::where('id',$id)->first();
        if($row) {
            return view('currency.show')->with(compact('menu','row'));
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu               = 'currency';
        $row                = Currency::where('id',$id)->first();
        if($row) {
            return view('currency.edit')->with(compact('menu','row'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row                = Currency::where('id',$id)->first();
        if($row) {
            if(!$request->input('name')) {
                return redirect('/currency/'.$id.'/edit')->withInput()->with('error','Please input name');
            } elseif (!$request->input('code')) {
                return redirect('/currency/'.$id.'/edit')->withInput()->with('error','Please input code');
            // } elseif (!$request->input('address')) {
                // return redirect('/currency/'.$id.'/edit')->withInput()->with('error','Please input address');
            } else {
                $row->name                  = $request->input('name');
                $row->code                  = $request->input('code');
                $row->address               = $request->input('address');

                if ($request->hasFile('icon')) {
                    $fileProfile                    = $request->file('icon');
                    $uploadImage                    = New UploadImage;
                    $filenameProfile                = 'icon_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
                    $filepathProfile                = 'currency/' . $filenameProfile;
                    $uploadProfile                  = $uploadImage->upload($filepathProfile, $fileProfile);
                    $row->image                     = $uploadProfile;
                }

                $row->save();

                return redirect('/currency')->with('success','Data has been update');
            }
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        // $id                 = $request->input('id');
        $row                = Currency::where('id',$id)->first();
        if($row) {
            $row->active    = 0;
            $row->save();
            return redirect('/currency')->with('success', 'Currency has been deactivate');
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request,$id)
    {
        // $id                 = $request->input('id');
        $row                = Currency::where('id',$id)->first();
        if($row) {
            $row->active    = 1;
            $row->save();
            return redirect('/currency')->with('success', 'Currency has been active');
        } else {
            abort(404);
        }
    }
}

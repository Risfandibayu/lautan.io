<?php
namespace App\Libs;

use Illuminate\Support\Facades\Config;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;
use DB;
use App\Models\Datalog;
Class Ipaymu {
    
    public function callApi($data)
    {
        // return $data['body'];
        $body         = $data['body'];
        $requestBody  = strtolower(hash('sha256', $body));
        $secret       = $data['secret'];
        $va           = $data['va'];
        $stringToSign = strtoupper($data['method']) . ':' . $va . ':' . $requestBody . ':' . $secret;
        $signature    = hash_hmac('sha256', $stringToSign, $secret);
        $timestamp    = Date('YmdHis');
        $url          = $data['url'];
        // $signature . ' - ' . $stringToSign . ' - ' . $requestBody;
        
        $headers = [
            'Content-Type' => 'application/json',
            'va'          => $va,
            'signature'   => $signature,
            'timestamp'   => $timestamp
        ];

        $client = new Client([
            'headers' => $headers
        ]);
        try {
            $response = $client->request(strtoupper($data['method']), $url, [
                'body' => $body,
            ]);
            // $data =  $response->getBody()->getContents();
            $data   = json_decode($response->getBody()->getContents());
            $status = 200;
            // $data['data']   = $response->getBody();
        } catch (RequestException $e) {
            // $data['status'] = 400;
            $data['Status']    = 400;
            $data['Data']      = $e->getResponse();
            $data['Message']   = $e->getMessage();
            $status            = 400;
            // $data['msg']    = 'invalid request';
        }
       
        // LOG RESPONSE
        $filename    = storage_path() . '/logs/ipaymu/' . date('Y-m-d') . '.log';
        $directory   = dirname($filename);
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
         
        $clog       = "TIME: ".date('Y-m-d H:i:s')." \n";
        $clog      .= 'IP: ' . request()->ip() . " \n";
        $clog      .= "URL: " . $url . " \n";
        $clog      .= "StringToSign: " . $stringToSign . " \n";
        $clog      .= "Request (Header): " . json_encode($headers, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "Request (Body): " . json_encode($body, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "Response Status: " . $status . " \n";
        $clog      .= "Response: " . json_encode($data, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "-----------------------------------------\n\n";
        $h          = file_put_contents($filename, $clog, FILE_APPEND);

        DB::beginTransaction();
        try {
            $datalog            = NEW Datalog;
            $datalog->channel   = 'ipaymu';
            $datalog->log       = $clog;
            $datalog->save();
            DB::commit();
        } catch (ClientException $e) {
            DB::rollback();
        }
        
        if($status == 200) {
            return $data;
        }
        // return $data;
        return false;
        // return response()->json($data, $data['status']);  
    }

    public function register($data)
    {
        $body['name']           = $data['name'];
        $body['email']          = $data['email'];
        $body['phone']          = $data['phone'];
        $body['password']       = $data['password'];
        // $data['url']            = 'sandbox.ipaymu.com/api/v2/register';
        $send['url']            =  '/api/v2/register';
        $send['method']         = 'POST';
        
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }

    public function verification($data)
    {
        // $body['website']        = $data['website'];
        $body['nationalId']     = $data['nationalId'];
        $body['province']       = $data['province'];
        $body['bankCode']       = $data['bankCode'];
        $body['bankNumber']     = $data['bankNumber'];
        $body['bankAccount']    = $data['bankAccount'];
        $body['account']        = $data['va'];
        // $data['url']            = 'sandbox.ipaymu.com/api/v2/register';
        $send['url']            =  '/api/v2/verify';
        $send['method']         = 'POST';
        // $send['key']            = $data['key'];
        // $send['va']             = $data['va'];
        
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }

    public function profile($data)
    {
        $body['va']             = $data['va'] ?? Config::get('constants.ipaymu.va');
        $send['url']            =  '/api/v2/profile';
        $send['method']         = 'POST';
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }


    public function getVa($data)
    {
        $body['name']           = $data['name'];
        $body['phone']          = $data['phone'];
        $body['email']          = $data['email'];
        $body['amount']         = $data['amount'];
        $body['notifyUrl']      = $data['notifyUrl'];
        $body['expired']        = '2';
        $body['expiredType']    = 'days';
        $body['comments']       = $data['comments'];
        $body['referenceId']    = $data['referenceId'];
        $body['va']             = $data['va'] ?? null;
        $vaType                 = $data['vaType'] ?? 'niagava';
        // $data['url']            = 'sandbox.ipaymu.com/api/v2/register';
        $send['url']            =  '/api/v2/payment/' . $vaType;
        $send['method']         = 'POST';
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }

    public function inquiryFee($data)
    {
        $body['account']        = $data['va'];
        $body['fee']            = $data['fee'];
        // $data['url']            = 'sandbox.ipaymu.com/api/v2/register';
        $send['url']            =  '/api/v2/getfee';
        $send['method']         = 'POST';
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }

    public function transferVa($data)
    {
        $body['sender']         = $data['sender'];
        $body['receiver']       = $data['receiver'];
        $body['amount']         = $data['amount'];
        $body['notes']          = $data['notes'] ?? null;
        $body['referenceId']    = $data['referenceId'] ?? null;
    
        $send['url']            =  '/api/v2/transferva';
        $send['method']         = 'POST';
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }

    public function withdraw($data)
    {
        $body['account']        = $data['va'];
        $body['amount']         = $data['amount'];
        $body['notes']          = $data['notes'] ?? null;
        // $body['referenceId']    = $data['referenceId'] ?? null;
    
        $send['url']            =  '/api/v2/withdraw';
        $send['method']         = 'POST';
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }

    public function reversal($data)
    {
        $body['va']             = $data['va'];
        $body['transactionId']  = $data['ipaymuId'];
        $body['amount']         = $data['amount'];
        $body['notes']          = $data['amount'] ?? null;
    
        $send['url']            =  '/api/v2/reversal';
        $send['method']         = 'POST';
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }

    public function balance($data)
    {
        $body['account']        = $data['va'];
        $send['url']            =  '/api/v2/balance';
        $send['method']         = 'POST';
        $send['body']           = json_encode($body, JSON_UNESCAPED_SLASHES);
        return $post = $this->callApi($send);
    }
    
}
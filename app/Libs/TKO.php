<?php
namespace App\Libs;

use Illuminate\Support\Facades\Config;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;
use DB;
use App\Models\Datalog;
Class TKO {
    
    public function callApi($data)
    {
        // return $data['body'];
        $body               = $data['body'];
        $secret             = env('TKO_SECRET');
        $apiKey             = env('TKO_APIKEY');
        $timestamp          = microtime(true);
        $body['timestamp']  = $timestamp;

        // dd($body);
        $signature          = hash_hmac('sha256', http_build_query($body), $secret);
        $body['signature']  = $signature;
        $url                = env('TKO_URL').$data['url'];
        
        $headers = [
            // 'Content-Type' => 'application/json',
            'X-MBX-APIKEY' => $apiKey,
        ];

        $client = new Client([
            'headers' => $headers
        ]);
        try {
            $response = $client->request('GET', $url, [
                'query' => http_build_query($body),
            ]);
            $data   = json_decode($response->getBody()->getContents());
            $status = 200;
        } catch (RequestException $e) {
            $data['Status']    = 400;
            $data['Data']      = $e->getResponse();
            $data['Message']   = $e->getMessage();
            $status            = 400;
        }
       
        //LOG RESPONSE
        $filename    = storage_path() . '/logs/tko/' . date('Y-m-d') . '.log';
        $directory   = dirname($filename);
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
         
        $clog       = "TIME: ".date('Y-m-d H:i:s')." \n";
        $clog      .= 'IP: ' . request()->ip() . " \n";
        $clog      .= "URL: " . $url . " \n";
        // $clog      .= "StringToSign: " . $stringToSign . " \n";
        $clog      .= "Request (Header): " . json_encode($headers, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "Request (Body): " . json_encode($body, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "Response Status: " . $status . " \n";
        $clog      .= "Response: " . json_encode($data, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "-----------------------------------------\n\n";
        $h          = file_put_contents($filename, $clog, FILE_APPEND);

        // DB::beginTransaction();
        // try {
        //     $datalog            = NEW Datalog;
        //     $datalog->channel   = 'tko';
        //     $datalog->log       = $clog;
        //     $datalog->save();
        //     DB::commit();
        // } catch (ClientException $e) {
        //     DB::rollback();
        // }

        
        if($status == 200) {
            return $data;
        }
        // return $data;
        return false;
        // return response()->json($data, $data['status']);  
    }
    
}
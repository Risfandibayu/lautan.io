<?php
namespace App\Libs;

use Illuminate\Support\Facades\Config;

// use AWS;
use File;
use Image;
use Storage;

Class UploadImage {

    public function upload($filePath, $file, $width = '800', $height = '600')
    {
        // $name = time() . $file->getClientOriginalName();
        // $filePath = $path . $name;
        // $image  = Image::canvas($width, $height);
        // $image  = Image::make($file)->resize($width, $height, function($constraint) {
        //     $constraint->aspectRatio();
        // });
        $path    = Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
        $url     = 'https://' . env('AWS_BUCKET') .'.s3.amazonaws.com/';

        if($path) {
            return $url . $filePath;
        }
        return false;

        $image  = Image::make($file)->widen(500);
        $image->insert($image, 'center');
        $image->stream();

        $path    = Storage::disk('s3')->put($filePath, $image->__toString(), 'public');
        // $path    = Storage::disk('s3')->put($filePath, $file->__toString(), 'public');
        $url     = 'https://' . env('AWS_BUCKET') .'.s3.amazonaws.com/';

        if($path) {
            return $url . $filePath;
        }
        return false;
    }

    public function uploadFile($filePath, $file)
    {

        $path    = Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
        $url     = 'https://' . env('AWS_BUCKET') .'.s3.amazonaws.com/';

        if($path) {
            return $url . $filePath;
        }
        return false;
    }

}

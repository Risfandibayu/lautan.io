<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    public function getActiveLabel()
    {
        switch ($this->active) {
            case '0':
                return '<span class="badge bg-warning">Not Active</span>';
            break;
            case '1':
                return '<span class="badge bg-success">Active</span>';
            break;
        }
    }
}

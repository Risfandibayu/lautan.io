<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'status',
        'name',
        'email',
        'password',
        'type',
        'phonecode',
        "address",
        "nationalId",
        "bankCode",
        "bankName",
        "bankAccount",
        "bankNumber",
        "ipaymuKey",
        "ipaymuVa",
        "tokocryptoKey",
        "fileNationalId",
        "fileBank",
        "filePhoto",
        "tokocryptoSecret",
        "avatar",
        "uuid",
        "idcashStatus",
        "idcashVa",
        "idcashKey",
        "agent_role_id",
        "agentStatus",
        "user_agent_referal",
        "as_agent_referal",
        "bfa",
        "trx_fp",
        "ban",

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function agent()
    {
        return $this->belongsTo('App\Models\AgentRole','agent_role_id');
    }

    public function getAvatar()
    {
        if($this->avatar) {
            return url('/public/uploads/avatar/'.$this->avatar);
        } else {
            return url('/public/assets/images/profile.png');
        }
    }

    public function getStatusLabel()
    {
        switch ($this->status) {
            case '1':
                return '<span class="badge bg-warning">Unverified</span>';
            break;
            case '2':
                return '<span class="badge bg-info">Under Verification</span>';
            break;
            case '3':
                return '<span class="badge bg-success">Verified</span>';
            break;
        }
    }
    public static function referenceCode($agent_role_id){
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 4;
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        if ($agent_role_id == 1) {
            $partner = User::where('agent_role_id',1)->get();
            $count = $partner->count();
            $code = 'D';
        }elseif ($agent_role_id == 2){
            $partner = User::where('agent_role_id',1)->get();
            $count = $partner->count();
            $code = 'M';
        }else {
            $partner = User::where('agent_role_id',1)->get();
            $count = $partner->count();
            $code = 'A';
        }
        $ref = $randomString.$code. sprintf("%04s", $count + 1);
        return $ref;
    }

    public static function userAgentFee($user_agent_referal){
        return DB::select("SELECT 
                        users.*,
                        COALESCE(tmp.idr_fee,0) AS idr_fee, 
                        COALESCE(tmp.bnb_fee,0) AS bnb_fee
                    FROM 
                        users
                    LEFT JOIN 
                        (
                            SELECT 
                                s.user_id, 
                                COALESCE(SUM(s.amount_idr),0) AS idr_fee, 
                                COALESCE(SUM(s.amount_bnb),0) AS bnb_fee 
                            FROM 
                                agent_fees as s 
                            WHERE 
                                s.status = 1 
                            GROUP BY s.user_id
                        ) as tmp 
                    ON 
                        users.id = tmp.user_id
                    WHERE 
                        users.user_agent_referal= '$user_agent_referal'
                    ORDER BY 
                        users.id 
                    ASC");
    }

}

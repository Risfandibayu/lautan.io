<?php

use App\Http\Controllers\AgentController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Libs\Binance;
use App\Models\Currency;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\WithdrawController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () { return view('landing'); });
Route::get('/offline', function () {    
return '404';
});
Route::get('/', 'DashboardController@landing');
Route::get('/about-us', function () {
return view('land.about-us');
});
Route::get('/user-agreement', function () {
return view('tnc');
});
Route::get('/contact-us', function () {
return view('land.contact');
});
Route::get('/privacy-policy', function () {
return view('prvpol');
});
Route::get('/career', function () {
return view('land.career');
});
Route::get('/products', function () {
     $currency           = Currency::where('active',1)->orderBy('name')->get();
        $coin               = [];
        foreach ($currency as $rows) {
            $binance                                = NEW Binance;
            $send['url']                            = '/ticker/24hr';
            $body['symbol']                         = $rows->code;
            $send['body']                           = $body; 
            $post                                   = $binance->callApi($send);
            if($post) {
                $list['id']                         = $rows->id;
                $list['image']                      = $rows->image;
                $list['code']                       = $rows->code;
                $list['name']                       = $rows->name;
                $list['price']                      = $post->lastPrice;
                $list['change']                     = $post->priceChangePercent;
                $list['highPrice']                  = $post->highPrice;
                $list['lowPrice']                   = $post->lowPrice;
                $list['marketPrice']                = $post->quoteVolume;
                $coin[]                             = $list;
            }   
        }
        $row                                        = json_decode(json_encode($coin), FALSE);
return view('land.products')->with(compact('row'));
});
Route::get('/partnership', function () {
return view('land.partnership');
});
Route::get('/news', function () {
return view('land.news');
});
Route::get('/page/{slug}', 'DashboardController@page');
Route::get('/calculate', 'DashboardController@calculate');



Route::resource('login', 'LoginController');
Route::get('/login', function () {
    return view('login');
})->name('login');
Route::resource('register', 'RegisterController');
Route::get('/reg/{id}', 'RegisterController@referal');
// Route::post('/referal')
Route::resource('cron', 'CronController');
Route::get('bscscan', 'CronController@bscscan');
Route::get('markets', 'CronController@create');

Route::post('cfaspay', 'TransactionController@cfaspay');
Route::post('faspay', 'TransactionController@faspay');
// Route::post('faspay_notify', 'TransactionController@faspay_notify');
// Route::post('faspay_notify', 'TransactionController@faspay_notify');
Route::get('/faspay/thankyou', 'TransactionController@faspay_callback');

Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post'); 
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/market', 'MarketController@index');
    Route::resource('balance', 'BalanceController');
    Route::resource('buy', 'BuyController');
    Route::resource('deposit', 'DepositController');

    Route::get('withdraw/getCryptoCurrency', 'WithdrawController@getCryptoCurrency');
    Route::get('withdraw/getCryptoIDR', 'WithdrawController@getCryptoIDR');
    Route::get('bank', 'WithdrawController@bank');
    Route::post('add_bank', 'WithdrawController@add_bank');
    Route::post('edit_bank/{id}', 'WithdrawController@edit_bank');
    Route::post('del_bank/{id}', 'WithdrawController@del_bank');
    Route::get('withdraw/info', 'WithdrawController@getInfo');
    Route::post('withdraw/info', 'WithdrawController@postInfo');
    Route::post('/withdraw/sendme_notify', 'WithdrawController@sendme_notify');
    Route::get('/testemail', 'WithdrawController@testemail');
    Route::resource('withdraw', 'WithdrawController');

    Route::post('transaction/confirm', 'TransactionController@confirm');
    Route::post('transaction/wconfirm', 'TransactionController@wconfirm');
    Route::get('transaction/depo_confirm/{id}', 'TransactionController@adm_confirm');
    Route::get('transaction/wd_confirm/{id}', 'TransactionController@wadm_confirm');
    Route::post('transaction/dp_update', 'TransactionController@trx_confirm');
    Route::post('transaction/dp_reject', 'TransactionController@trx_reject');
    Route::post('transaction/wd_update', 'TransactionController@wtrx_confirm');
    Route::post('transaction/wd_reject', 'TransactionController@wtrx_reject');
    Route::get('transaction/adm', 'TransactionController@index_adm');
    Route::resource('transaction', 'TransactionController');
    Route::resource('account', 'AccountController');
    Route::resource('tokocrypto', 'TokocryptoController');

    Route::post('/sendwallet',[WithdrawController::class,'sendWallet']);

    Route::get('currency/activate/{id}', 'CurrencyController@activate');
    Route::resource('currency', 'CurrencyController');

    Route::get('verification/list', 'VerificationController@list');
    Route::get('verification/bank', 'VerificationController@bank');
    Route::post('verification/bank', 'VerificationController@storeBank');
    Route::get('verification/file', 'VerificationController@file');
    Route::post('verification/file', 'VerificationController@storeFile');
    Route::get('verification/done', 'VerificationController@done');
    Route::resource('verification', 'VerificationController');

    Route::post('user/verification', 'UserController@approve');
    Route::get('user/verification/{id}', 'UserController@verification');
    Route::get('user/assets/{id}', 'UserController@assets');
    Route::resource('user', 'UserController');

    Route::resource('admin', 'AdminController');
    Route::resource('agentrole', 'AgentRoleController');
    Route::get('agentrequest', 'AgentController@reqlist');
    Route::post('agent/req', 'AgentController@req')->name('agent.req');
    Route::post('agent/approve', 'AgentController@approve');
    Route::post('agent/reject', 'AgentController@reject');
    Route::get('agent/done', 'AgentController@done')->middleware('cekAgentStatus');
    Route::get('agent/detail-transaction/{id}', 'AgentController@transaction')->middleware('cekAgentStatus');
    Route::get('agent/landing', 'AgentController@landing');
    Route::resource('agent', 'AgentController');
    Route::get('add_referal',[AgentController::class,'referal']);


    Route::group(['prefix' => 'webadmin'], function () {
        Route::get('media', 'WebadminController@listMedia')->name('webadmin.media');
        Route::post('media/upload', 'WebadminController@uploadMedia')->name('webadmin.uploadmedia');

        Route::get('pages', 'WebadminController@listPages')->name('webadmin.pages');
        Route::post('pages/add', 'WebadminController@addPages')->name('webadmin.addpages');
        Route::get('pages/edit/{id}', 'WebadminController@detailPages');
        Route::post('pages/edit', 'WebadminController@editPages')->name('webadmin.editpages');
        Route::get('pages/noactive/{id}', 'WebadminController@noactivePages');
        Route::get('pages/active/{id}', 'WebadminController@activePages');

        Route::get('customvar', 'WebadminController@customvar')->name('webadmin.customvar');
        Route::post('customvar/add', 'WebadminController@addCustomvar')->name('webadmin.addcustomvar');
        Route::get('customvar/edit/{id}', 'WebadminController@detailCustomvar');
        Route::post('customvar/edit', 'WebadminController@editCustomvar')->name('webadmin.editcustomvar');
    });

    Route::post('datalog/search', 'DatalogController@postSearch');
    Route::get('datalog/search', 'DatalogController@getSearch');
    Route::resource('datalog', 'DatalogController');

    Route::post('idcash/activate', 'AccountController@idcash');
    Route::post('idcash/connect', 'AccountController@idcashConnect');
    Route::get('idcash/done', 'AccountController@idcashDone');

    Route::resource('password', 'PasswordController');
    Route::get('/logout', function () { Auth::logout(); return redirect('/'); });
});

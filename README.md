```
.__                 __                     .__        
|  | _____   __ ___/  |______    ____      |__| ____  
|  | \__  \ |  |  \   __\__  \  /    \     |  |/  _ \ 
|  |__/ __ \|  |  /|  |  / __ \|   |  \    |  (  <_> )
|____(____  /____/ |__| (____  /___|  / /\ |__|\____/ 
          \/                 \/     \/  \/            
```

## Usage


#### Branch Issue/Hotfix
2. Untuk menghubungkan gitlab issue secara otomatis, buat branch untuk perbaikan tersebut dengan cara membuat branch sesuai id issue yang akan ditangani, contoh: `EX-111-staging`

```bash
$ git checkout staging
$ git checkout -b EX-111-staging
```
...coding
#### Review /Merge Branch
3. Untuk mengirimkan code dari branch 1-login-issue ke branch staging, gunakan perintah sebagai berikut:
```bash
$ git add .
$ git commit -am "EX-111 test jira integration"
$ git push origin EX-111-staging -o merge_request.create -o merge_request.target=staging
$ git branch -d EX-111-staging
```
Pada langkah ini, issue otomatis akan diedit menjadi `WIP Nama Issue` dan akan diclose setelah merge

4. Untuk menyetujui merge, gunakan perintah sebagai berikut:
```bash
$ git checkout staging
$ git merge EX-111-staging
```
> Pada langkah ini, code otomatis dikirimkan ke server staging. Bisa dicek live di https://staging.$domain

5. Untuk mengirimkan dari branch staging ke branch master, gunakan perintah sebagai berikut:
```bash
$ git checkout staging
$ git add .
$ git commit -am "EX-111 test jira integration"
$ git push -o merge_request.create
```
> Lebih lanjut mohon pelajari https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging
6. Untuk menyetujui merge request dari branch staging ke branch master, dilakukan melalui gitlab karena deployment ke server production harus menggunakan tombol deploy manual di `pipeline`

> Pada langkah ini, code otomatis dikirimkan ke server production setelah tombol deploy dijalankan. Bisa dicek live di https://$domains

> <span style="color:red">Branch master tidak boleh dicheckout untuk diedit karena menggunakan mekanisme merge request.</span>
## Deployment
Setelah staging selesai, saatnya rilis versi sekaligus release production.
Release di sini https://gitlab.ipaymu.com/lautan/exlaut/-/releases/new

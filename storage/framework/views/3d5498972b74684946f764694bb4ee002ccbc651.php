
<?php $__env->startSection('content'); ?>
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div class="container-xl px-4 mt-5">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-9">
                            <!-- Social registration form-->
                            <div class="card my-5">
                                <div class="card-body p-5 text-center">
                                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/logo-nav.png"
                                        style="height:50px;width:auto;"></a>
                                </div>
                                <hr class="my-0" />
                                <div class="card-body p-5">
                                    <div class="success-alert d-none">
                                        <div class="alert alert-success alert-icon" role="alert">
                                            <div class="alert-icon-aside">
                                                <i data-feather="check-circle"></i>
                                            </div>
                                            <div class="alert-icon-content" id="success-text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="error-alert d-none">
                                        <div class="alert alert-danger alert-icon" role="alert">
                                            <div class="alert-icon-aside">
                                                <i data-feather="alert-octagon"></i>
                                            </div>
                                            <div class="alert-icon-content" id="error-text">
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Register form-->
                                    <form id="register">
                                        <!-- Form Row-->
                                        <div class="row gx-3">
                                            <div class="col-md-6">
                                                <!-- Form Group (first name)-->
                                                <div class="mb-3">
                                                    <label class="text-gray-600 small">Name</label>
                                                    <input class="form-control form-control-solid name" type="text"
                                                        placeholder="Please input your name" aria-label="Name"
                                                        name="name" />
                                                    <span class="err-text text-danger d-none err_name"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <!-- Form Group (last name)-->
                                                <div class="mb-3">
                                                    <label class="text-gray-600 small">Phone</label>
                                                    <div class="row">
                                                        <?php
                                                        use Illuminate\Support\Facades\DB;
                                                        $country = db::table('country')
                                                            ->select('*')
                                                            ->get();
                                                        
                                                        ?>
                                                        <div class="col-md-4">
                                                            <select id="phonecode" value="<?php echo e(old('phonecode')); ?>"
                                                                onchange="displayCountryCode()"
                                                                class="form-control form-control-solid phonecode"
                                                                name="phonecode">
                                                                <option value="+62" hidden selected>+62</option>
                                                                <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $co): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="+<?php echo e($co->phonecode); ?>">
                                                                        <?php echo e($co->nicename); ?> (
                                                                        +<?php echo e($co->phonecode); ?> )
                                                                    </option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input class="form-control form-control-solid phone"
                                                                type="number" placeholder="Please input your phone number"
                                                                aria-label="Phone" name="phone" />
                                                            <span class="err-text text-danger d-none err_phone">test
                                                                error</span>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Group (email address)-->
                                        <div class="mb-3">
                                            <label class="text-gray-600 small">Email address</label>
                                            <input class="form-control form-control-solid email" type="email"
                                                placeholder="Please input your email address" aria-label="Email Address"
                                                name="email" />
                                            <span class="err-text text-danger d-none err_email">test error</span>
                                        </div>
                                        <!-- Form Row-->
                                        <div class="row gx-3">
                                            <div class="col-md-6">
                                                <!-- Form Group (choose password)-->
                                                <div class="mb-3">
                                                    <label class="text-gray-600 small">Password</label>
                                                    <input class="form-control form-control-solid password" type="password"
                                                        placeholder="" aria-label="Password" name="password" />
                                                    <span class="err-text text-danger d-none err_password">test
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <!-- Form Group (confirm password)-->
                                                <div class="mb-3">
                                                    <label class="text-gray-600 small">Confirm Password</label>
                                                    <input class="form-control form-control-solid" type="password"
                                                        placeholder="" aria-label="Confirm Password"
                                                        name="password_confirmation" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="mb-3">
                                            <label class="text-gray-600 small">Referal</label>
                                            <input class="form-control form-control-solid referal" type="text"
                                                placeholder="Referal Code (Optional)" aria-label="Referal" name="refer"
                                                value="<?php echo e(isset($_GET['ref']) ? $_GET['ref'] : ''); ?>"
                                                <?php echo e(isset($_GET['ref']) ? 'readonly' : ''); ?> />
                                        </div>
                                        <!-- Form Group (form submission)-->
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="form-check">
                                                <input class="form-check-input" id="checkTerms" type="checkbox" value=""
                                                    required />
                                                <label class="form-check-label" for="checkTerms">
                                                    I accept the
                                                    <a id="term" href="<?php echo e(url('/user-agreement')); ?>"
                                                        target="
                                                                                                                                _blank">User
                                                        &amp;
                                                        Aggrement</a>
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-primary" id="btn-submit">Create
                                                Account</button>
                                        </div>
                                    </form>
                                </div>
                                <hr class="my-0" />
                                <div class="card-body px-5 py-4">
                                    <div class="small text-center">
                                        Have an account?
                                        <a href="<?php echo e(url('/login')); ?>">Sign in!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <footer class="footer-admin mt-auto footer-dark">
                <div class="container-xl px-4">
                    <div class="row">
                        <div class="col-md-6 small">Copyright © Exlaut <?php echo e(date('Y')); ?></div>
                        <div class="col-md-6 text-md-end small">
                            <a href="#!">Privacy Policy</a>
                            ·
                            <a href="#!">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script>
        function displayCountryCode() {
            var countrycode = document.getElementById("phonecode");
            countrycode.options[countrycode.selectedIndex].text = countrycode.value;
            // console.log(countrycode.value);
        }
        $(document).ready(function() {
            $('#register').on('submit', function(e) {
                e.preventDefault();
                clear_error();
                disable_submit();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "<?php echo e(url('register')); ?>",
                    type: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 401) {
                            $.each(data.errors, function(key, value) {
                                $('.' + key).addClass('is-invalid');
                                $('.err_' + key).removeClass('d-none');
                                $('.err_' + key).html(value);
                            });
                            undisable_submit();
                        } else if (data.status == 201) {
                            $('.success-alert').removeClass('d-none');
                            $('#success-text').html(data.message);
                            undisable_submit();
                        } else {
                            $('.error-alert').removeClass('d-none');
                            $('#error-text').html(data.message);
                            undisable_submit();
                        }
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            });

            function disable_submit() {
                $('#btn-submit').attr('disabled', true);
                $('#btn-submit').html('Processing...');
            }

            function undisable_submit() {
                $('#btn-submit').attr('disabled', false);
                $('#btn-submit').html('Create Account');
            }

            function clear_error() {
                $('.err_name').addClass('d-none');
                $('.err_email').addClass('d-none');
                $('.err_phone').addClass('d-none');
                $('.err_password').addClass('d-none');
                $('.err_phonecode').addClass('d-none');
                $('.name').removeClass('is-invalid');
                $('.email').removeClass('is-invalid');
                $('.phone').removeClass('is-invalid');
                $('.password').removeClass('is-invalid');
            }

            $('.referal').on('keyup', function() {
                var referal = $(this).val();
                console.log(referal);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: `<?php echo e(url('reg')); ?>/${referal}`,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 'success') {
                            $('.referal').removeClass('is-invalid');
                            $('.referal').addClass('is-valid');
                        } else {
                            $('.referal').removeClass('is-valid');
                            $('.referal').addClass('is-invalid');
                        }
                    }
                })
            })
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/register.blade.php ENDPATH**/ ?>
<?php echo $__env->make('emails.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;"
    align="left">
    Partner Approved.!
    <br><br>
    Thank you for submitting your Lautan.io Partnership Program application. We are pleased to inform you that your
    application has been approved.
    <br><br>
    You are now able to start introducing clients to Lautan.io through your dedicated tracking links available in your
    Partner Portal.
    <br><br>
    Please find below your login details for your Lautan.io Partner Portal.
    <br><br>
    Partner Login <br>
    <a href="<?php echo e(url('login')); ?>">https://lautan.io/login</a>
    <br><br>
    Review your tracking links, acquired clients and earned commission.
    <br><br>
    Let’s Connect!
    <br><br>
    Should you need further assistance, please contact the Client Support Team on support@lautan.io
    <br><br>
    Kind Regards,<br>
    Client Support Team<br>
    <br><br><br>
    <hr>
    <br><br><br>
    Partner Disetujui.!
    <br><br>
    Terima kasih telah mengirimkan aplikasi Program Partnership Lautan.io Anda. Dengan senang hati kami informasikan
    bahwa permohonan Anda telah disetujui.
    <br><br>
    Anda sekarang dapat mulai memperkenalkan klien ke Lautan.io melalui tautan pelacakan khusus yang tersedia di Portal
    Partner Anda.
    <br><br>
    Temukan detail login Anda untuk Portal Partner Lautan.io dibawah ini.
    <br><br>
    Login Mitra<br>
    <a href="<?php echo e(url('login')); ?>">https://lautan.io/login</a>
    <br><br>
    Tinjau tautan pelacakan Anda, dapatkan klien, dan dapatkan komisi.
    <br><br>
    Mari Terhubung!
    <br><br>
    Jika Anda memerlukan bantuan lebih lanjut, silakan hubungi Client Support Team di support@lautan.io
    <br><br>
    Salam Hormat,

    <br>
    Client Support Team
    <br>

</p>
<?php echo $__env->make('emails.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/emails/agentapprove.blade.php ENDPATH**/ ?>
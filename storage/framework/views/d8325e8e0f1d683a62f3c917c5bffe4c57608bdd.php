<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Lautan | subcription</title>
        <link href="<?php echo e(asset('public')); ?>/assets/css/styles.css" rel="stylesheet" />
        <link rel="shortcut icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <script data-search-pseudo-elements="" defer="" src="<?php echo e(asset('public')); ?>/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    </head>
    <body>
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4 mt-5">
                        <div class="row justify-content-center">
                        <div class="col-xl-12 col-lg-12">
                                <!-- Social registration form-->
                                <div class="card my-5">
                                    <div class="card-body p-5 text-center">
                                    <img src="<?php echo e(asset('public')); ?>/landing-assets/images/logo.png" style="height:50px;width:auto;"></a>
                                    </div>
                                    <hr class="my-0" />
                                    <div class="card-body p-5">
                                    <?php if(session('error')): ?>
                                    <div class="alert alert-danger alert-icon" role="alert">
                                        <div class="alert-icon-aside">
                                            <i data-feather="alert-triangle"></i>
                                        </div>
                                        <div class="alert-icon-content">
                                            <?php echo e(session('error')); ?>

                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if(session('success')): ?>
                                    <div class="alert alert-success alert-icon" role="alert">
                                        <div class="alert-icon-aside">
                                            <i data-feather="check-circle"></i>
                                        </div>
                                        <div class="alert-icon-content">
                                            <?php echo e(session('success')); ?>

                                        </div>
                                    </div>
                                    <?php endif; ?>
                                        <!-- Register form-->
                                        <!-- Form Row-->
                                        
                                        <div class="row">
                                            <div class="col-md-4 order-md-2 mb-4">
                                              <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                <span class="text-muted">Your cart</span>
                                                <span class="badge badge-secondary badge-pill">3</span>
                                              </h4>
                                              <ul class="list-group mb-3">
                                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                  <div>
                                                    <h6 class="my-0">Laut CryptCash Member</h6>
                                                    <small class="text-muted">Laut CryptCash Member Subscription</small>
                                                  </div>
                                                  <span class="text-muted">Rp. 100.000</span>
                                                </li>
                                                
                                                <li class="list-group-item d-flex justify-content-between">
                                                  <span>Total</span>
                                                  <strong>Rp. 100.000</strong>
                                                </li>
                                              </ul>
                                    
                                             
                                            </div>
                                            <div class="col-md-8 order-md-1">
                                              
                                              <!--<form class="needs-validation" novalidate>-->
                                              <form method="POST" action="<?php echo e(url('faspay')); ?>">
                                                 <?php echo csrf_field(); ?>
                                                 <h4 class="mb-3">User Info</h4>
                                                 <div class="mb-3">
                                                  <label for="address">Name</label>
                                                  <input type="text" class="form-control" id="name" name="name" placeholder="your name" required>
                                                  <div class="invalid-feedback">
                                                    Please enter your name.
                                                  </div>
                                                </div>
                                                 <div class="mb-3">
                                                  <label for="email">Email</label>
                                                  <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" value="<?php echo e(session('email')); ?>">
                                                  <!--<?php echo e(session('amount')); ?>-->
                                                  <div class="invalid-feedback">
                                                    Please enter a valid email address for shipping updates.
                                                  </div>
                                                </div>
                                                <div class="mb-3">
                                                  <label for="address">Phone Number</label>
                                                  <input type="text" class="form-control" id="phone" name="phone" placeholder="your phone number" required>
                                                  <div class="invalid-feedback">
                                                    Please enter your phone number.
                                                  </div>
                                                </div>
                                                <hr>
                                                <h4 class="mb-3">Billing address</h4>
                                                
                                    
                                    
                                                
                                    
                                                <div class="mb-3">
                                                  <label for="address">Address</label>
                                                  <input type="text" class="form-control" id="address" name="address" placeholder="Your address" required>
                                                  <div class="invalid-feedback">
                                                    Please enter your address.
                                                  </div>
                                                </div>
                                    
                                               
                                    
                                                <div class="row">
                                                  <div class="col-md-5 mb-3">
                                                    <label for="address">City</label>
                                                      <input type="text" class="form-control" id="city" name="city" placeholder="Your city" required>
                                                      <div class="invalid-feedback">
                                                        Please enter your city.
                                                      </div>
                                                  </div>
                                                  <div class="col-md-4 mb-3">
                                                        <label for="address">Region</label>
                                                      <input type="text" class="form-control" id="region" name="region" placeholder="Your region" required>
                                                      <div class="invalid-feedback">
                                                        Please enter your region.
                                                      </div>
                                                  </div>
                                                  <div class="col-md-3 mb-3">
                                                    <label for="address">State</label>
                                                  <input type="text" class="form-control" id="state" name="state" placeholder="Your state" required>
                                                  <div class="invalid-feedback">
                                                    Please enter your state.
                                                  </div>
                                                  </div>
                                                </div>
                                                 <div class="mb-3">
                                                  <label for="address">Pos Code</label>
                                                  <input type="text" class="form-control" id="poscode" name="poscode" placeholder="Your pos code" required>
                                                  <div class="invalid-feedback">
                                                    Please enter your pos code.
                                                  </div>
                                                </div>
                                                <hr>
                                                <h4 class="mb-3">Shipping address</h4>
                                                
                                    
                                    
                                                
                                                <div class="mb-3">
                                                  <label for="address">Receiver name for shipping</label>
                                                  <input type="text" class="form-control" id="shipname" name="shipname" placeholder="receiver name" required>
                                                  <div class="invalid-feedback">
                                                    Please enter shipping address.
                                                  </div>
                                                </div>
                                                <div class="mb-3">
                                                  <label for="address">Address</label>
                                                  <input type="text" class="form-control" id="shipaddress" name="shipaddress" placeholder="shipping address" required>
                                                  <div class="invalid-feedback">
                                                    Please enter shipping address.
                                                  </div>
                                                </div>
                                    
                                               
                                    
                                                <div class="row">
                                                  <div class="col-md-5 mb-3">
                                                    <label for="address">City</label>
                                                      <input type="text" class="form-control" id="shipcity" name="shipcity" placeholder="shipping city" required>
                                                      <div class="invalid-feedback">
                                                        Please enter shipping city.
                                                      </div>
                                                  </div>
                                                  <div class="col-md-4 mb-3">
                                                        <label for="address">Region</label>
                                                      <input type="text" class="form-control" id="shipregion" name="shipregion" placeholder="shipping region" required>
                                                      <div class="invalid-feedback">
                                                        Please enter shipping region.
                                                      </div>
                                                  </div>
                                                  <div class="col-md-3 mb-3">
                                                    <label for="address">State</label>
                                                  <input type="text" class="form-control" id="shipstate" name="shipstate" placeholder="shipping state" required>
                                                  <div class="invalid-feedback">
                                                    Please enter shipping state.
                                                  </div>
                                                  </div>
                                                </div>
                                                 <div class="mb-3">
                                                  <label for="address">Pos Code</label>
                                                  <input type="text" class="form-control" id="shipposcode" name="shipposcode" placeholder="shipping pos code" required>
                                                  <div class="invalid-feedback">
                                                    Please enter shipping pos code.
                                                  </div>
                                                </div>
                                    
                                                <hr class="mb-4">
                                                <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
                                              </form>
                                            </div>
                                          </div>
                                            
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer-admin mt-auto footer-dark">
                    <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">Copyright © Lautan.io <?php echo e(date("Y")); ?></div>
                            <div class="col-md-6 text-md-end small">
                                <a href="#!">Privacy Policy</a>
                                ·
                                <a href="#!">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="<?php echo e(asset('public')); ?>/assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo e(asset('public')); ?>/assets/js/scripts.js"></script>
</body>

</html>
<?php /**PATH /home/lautanio/dev.lautan.io/resources/views/faspay.blade.php ENDPATH**/ ?>
<?php echo $__env->make('emails.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
    Hi, <?php echo e($name); ?> <br><br>
    Withdrawal request has been successful. Withdrawal details : <br>
</p>
<table style="color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em;">
        <tr>
            <td style="width: 150px;">Amount</td>
            <td>: <?php echo e(number_format($amount,0,",",".")); ?> IDR</td>
        </tr>
        <tr>
            <td>Fee</td>
            <td>: <?php echo e(number_format($fee,0,",",".")); ?> IDR</td>
        </tr>
        <tr>
            <td>Remain</td>
            <td>: <?php echo e(number_format($remain,0,",",".")); ?> IDR</td>
        </tr>
        <tr>
            <td>Bank</td>
            <td>: <?php echo e($bname); ?></td>
        </tr>
        <tr>
            <td>Account</td>
            <td>: <?php echo e($bacc); ?></td>
        </tr>
        <tr>
            <td>Time</td>
            <td>: <?php echo e($time); ?></td>
        </tr>
        <tr>
            <td>TRX ID</td>
            <td>: <?php echo e($trx); ?></td>
        </tr>
    </table>
<br>
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
    Your withdrawal has been successful. Please check the balance in your bank account. <br><br>
    
    Thank you for using Lautan.io
</p>
<?php echo $__env->make('emails.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/public_html/resources/views/emails/withdrawreqok.blade.php ENDPATH**/ ?>
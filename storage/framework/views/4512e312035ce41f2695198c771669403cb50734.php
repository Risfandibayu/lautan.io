
<?php $__env->startSection('content'); ?>
    <!-- <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
                                                                                                                                                                            <div class="container-xl px-4">
                                                                                                                                                                                <div class="page-header-content">
                                                                                                                                                                                    <div class="row align-items-center justify-content-between pt-3">
                                                                                                                                                                                        <div class="col-auto mb-3">
                                                                                                                                                                                            <h1 class="page-header-title">
                                                                                                                                                                                                <div class="page-header-icon"><i data-feather="download"></i></div>
                                                                                                                                                                                                Penarikan
                                                                                                                                                                                            </h1>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                            </div>
                                                                                                                                                                        </header>
                                                                                                                                                                        <!-- Main page content-->
    <div class="container-xl mt-4">
        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                <?php if(session('warning')): ?>
                    <div class="alert alert-warning alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('warning')); ?>

                        </div>
                    </div>
                <?php endif; ?>

                <?php if(session('error')): ?>
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('error')); ?>

                        </div>
                    </div>
                <?php endif; ?>

                <?php if(session('success')): ?>
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('success')); ?>

                        </div>
                    </div>
                <?php endif; ?>
                <div class="card mb-4">
                    <div class="card-header">Withdraw Confirmation</div>
                    <div class="card-body">
                        <form method="POST" action="<?php echo e(url('withdraw')); ?>">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" name="code" value="<?php echo e(session('code')); ?>">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Assets</label>
                                    <input class="form-control form-control-solid" type="text" name="coin"
                                        value="<?php echo e(session('coin')); ?>" required readonly />
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Amount</label>
                                    <input class="form-control form-control-solid" type="text" name="amount"
                                        value="<?php echo e(session('amount')); ?>" required readonly />
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Amount IDR</label>
                                    <input class="form-control form-control-solid " type="text" name="amountIdr"
                                        value="<?php echo e(number_format(session('amountIdr'))); ?>" required readonly />
                                    <?php if(session('amountIdr') < 10000): ?>
                                        <span style="color:red">
                                            <small>Amount below the minimum withdrawal. Amount must be more than
                                                10.000</small>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Transaction Fee & Tax</label>

                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-solid"
                                            value="<?php echo e('IDR ' . number_format(session('feeTransaction.totalFee'))); ?>">
                                        <span class="input-group-text form-control-solid"> ≈ </span>
                                        <input type="text" class="form-control form-control-solid"
                                            value="<?php echo e(session('feeTransaction.feeInCoin') . ' ' . session('coin')); ?>">
                                    </div>
                                </div>
                                <h6 class="small mt-2">Withdraw to</h6>
                                <hr>
                                <?php if(session('type') == 'bank'): ?>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Name</label>
                                        <input class="form-control form-control-solid" type="text" name="bankName"
                                            value="<?php echo e(session('bankName')); ?>" required disabled />
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Account Name</label>
                                        <input class="form-control form-control-solid" type="text" name="bankAccount"
                                            value="<?php echo e(session('bankAccount')); ?>" required disabled />
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Account Number</label>
                                        <input class="form-control form-control-solid" type="text" name="bankNumber"
                                            value="<?php echo e(session('bankNumber')); ?>" required disabled />
                                    </div>
                                <?php elseif(session('type') == 'bankk'): ?>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Name</label>
                                        <input class="form-control form-control-solid" type="text" name="bankName"
                                            value="<?php echo e(session('bankName')); ?>" required disabled />
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Account Name</label>
                                        <input class="form-control form-control-solid" type="text" name="bankAccount"
                                            value="<?php echo e(session('bankAccount')); ?>" required disabled />
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Account Number</label>
                                        <input class="form-control form-control-solid" type="text" name="bankNumber"
                                            value="<?php echo e(session('bankNumber')); ?>" required disabled />
                                    </div>
                                <?php else: ?>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">IDCash</label>
                                        <input class="form-control form-control-solid" type="text" name="bankNumber"
                                            value="<?php echo e(Auth::user()->idcashVa); ?>" required disabled />
                                    </div>
                                <?php endif; ?>


                                <?php if(session('amountIdr') < 10000): ?>
                                    <hr>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-primary float-end" href="<?php echo e(url('withdraw')); ?>">Back</a>
                                    </div>
                                <?php else: ?>
                                    <h6 class="small mt-2">Confirmation</h6>
                                    <hr>

                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Please enter the pin we sent via email to continue the
                                            transaction</label>
                                        <input class="form-control form-control-solid" type="password" name="pin" value=""
                                            required />
                                    </div>
                                    <hr class="my-4" />
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <button class="btn btn-primary float-end" type="submit">Submit</button>
                                    </div>
                                <?php endif; ?>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/withdraw/info.blade.php ENDPATH**/ ?>
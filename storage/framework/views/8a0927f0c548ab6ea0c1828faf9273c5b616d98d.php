
<?php $__env->startSection('content'); ?>
    <div class="container-xl mt-4">
        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                <?php if(session('warning')): ?>
                    <div class="alert alert-warning alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('warning')); ?>

                        </div>
                    </div>
                <?php endif; ?>

                <?php if(session('error')): ?>
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('error')); ?>

                        </div>
                    </div>
                <?php endif; ?>

                <?php if(session('success')): ?>
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('success')); ?>

                        </div>
                    </div>
                <?php endif; ?>
                <div class="card mb-4">
                    <div class="card-header">Withdraw</div>
                    <div class="card-body">
                        <form method="POST" action="<?php echo e(url('withdraw/info')); ?>">
                            <?php echo csrf_field(); ?>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Assets</label>
                                    <select class="select2 code" name="code" required>
                                        <option disabled>Select</option>
                                        <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($rows->id); ?>"
                                                <?php if($rows->code == request('coin')): ?> selected <?php endif; ?>><?php echo e($rows->name); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Amount</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="" name="amount" id="amont">
                                        <span class="input-group-text readonly-color"><i
                                                class="fas fa-exchange-alt"></i></span>
                                        <input type="text" class="form-control" readonly id="idrvalue" name="idrvalue">
                                    </div>

                                </div>

                                <h6 class="small mt-2">Withdraw to</h6>
                                <hr>
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="type" id="type" value="bankk"
                                            onchange="handleChange(this);" checked>
                                        <label class="form-check-label" for="type">
                                            Bank Account
                                        </label>

                                        <?php
                                        use Illuminate\Support\Facades\DB;
                                        $b = db::table('user_bank')
                                            ->select('user_bank.*', 'user_bank.id as bid', 'banks.*')
                                            ->join('banks', 'banks.code', '=', 'user_bank.bank_code')
                                            ->where('user_id', Auth::user()->id)
                                            ->get();
                                        ?>


                                        <?php if(count($b) >= 3): ?>
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#edit_bank"
                                                style="float: right;" class="btn btn-sm btn-outline-warning">Edit</a>
                                            <div class="reveal-if-active">
                                                <select class="require-if-active form-control form-control-solid select2"
                                                    data-require-pair="#type" name="bank_acc" required>-->
                                                    <option value="" selected disabled>Choose Bank Account</option>
                                                    <?php $__currentLoopData = $b; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($rows->bid); ?>"><?php echo e($rows->name); ?> |
                                                            <?php echo e($rows->bank_account); ?> | <?php echo e($rows->account_number); ?>

                                                        </option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        <?php elseif(count($b) == null): ?>
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#add_bank"
                                                style="float: right;" class="btn btn-sm btn-outline-primary">Add</a>
                                            <div class="reveal-if-active">
                                                <select class="require-if-active form-control form-control-solid select2"
                                                    data-require-pair="#type" name="bank_acc" required>-->
                                                    <option value="" selected disabled>Add Bank Account</option>
                                                </select>
                                            </div>
                                        <?php else: ?>
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#edit_bank"
                                                style="float: right;" class="btn btn-sm btn-outline-warning">Edit</a>
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#add_bank"
                                                style="float: right;" class="btn btn-sm btn-outline-primary">Add</a>
                                            <div class="reveal-if-active">
                                                <select class="require-if-active form-control form-control-solid select2"
                                                    data-require-pair="#type" name="bank_acc" required>-->
                                                    <option value="" selected disabled>Choose Bank Account</option>
                                                    <?php $__currentLoopData = $b; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($rows->bid); ?>"><?php echo e($rows->name); ?> |
                                                            <?php echo e($rows->bank_account); ?> | <?php echo e($rows->account_number); ?>

                                                        </option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        <?php endif; ?>



                                    </div>
                                    <br>
                                    
                                </div>
                                <hr class="my-4" />
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary float-end" type="submit">Next</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_bank" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Bank Account</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="POST" action="<?php echo e(url('add_bank')); ?>" tabindex="-1" aria-hidden="true">
                    <?php echo csrf_field(); ?>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label class="small mb-1">Bank Name</label>
                                <select class="form-control form-control-solid" name="bank_code">
                                    <option>Pilih</option>
                                    <?php $__currentLoopData = $bank; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($rows->code); ?>"
                                            <?php if(Auth::user()->bankCode == $rows->code): ?> selected <?php endif; ?>><?php echo e($rows->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="small mb-1">Bank Account Name</label>
                                <input class="form-control form-control-solid" type="text"
                                    placeholder="Input your Bank Account Name" name="bank_account" value="" required />
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="small mb-1">Bank Account Number</label>
                                <input class="form-control form-control-solid" type="text" placeholder="Bank Account Number"
                                    name="account_number" value="" required />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_bank" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Bank Account</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Bank Name</th>
                                    <th scope="col">Bank Account Name</th>
                                    <th scope="col">Bank Account Number</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $b; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ban): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <th scope="row"><?php echo e($ban->name); ?></th>
                                        <td><?php echo e($ban->bank_account); ?></td>
                                        <td><?php echo e($ban->account_number); ?></td>
                                        <td>
                                            <form method="POST" action="<?php echo e(url('del_bank')); ?>/<?php echo e($ban->bid); ?>"
                                                tabindex="-1" aria-hidden="true">
                                                <?php echo csrf_field(); ?>
                                                <a class="btn btn-sm btn-outline-warning" data-bs-dismiss="modal"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#edit_bank<?php echo e($ban->bid); ?>">edit</a>


                                                <button type="submit" class="btn btn-sm btn-outline-danger ">delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <?php $__currentLoopData = $b; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ban): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="modal fade" id="edit_bank<?php echo e($ban->bid); ?>" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Bank Account</h5>
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#edit_bank"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="POST" action="<?php echo e(url('edit_bank')); ?>/<?php echo e($ban->bid); ?>" tabindex="-1"
                        aria-hidden="true">
                        <?php echo csrf_field(); ?>
                        <div class="modal-body">
                            <div class="row">

                                <?php
                                $ba = db::table('user_bank')
                                    ->where('id', $ban->bid)
                                    ->first();
                                ?>

                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Bank Name</label>
                                    <select class="form-control form-control-solid" name="bank_code">-->
                                        <option>Pilih</option>
                                        <?php $__currentLoopData = $bank; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($rows->code); ?>"
                                                <?php if($ba->bank_code == $rows->code): ?> selected <?php endif; ?>><?php echo e($rows->name); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Bank Account Name</label>
                                    <input class="form-control form-control-solid" type="text"
                                        value="<?php echo e($ba->bank_account); ?>" placeholder="Input your Bank Account Name"
                                        name="bank_account" value="" required />
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Bank Account Number</label>
                                    <input class="form-control form-control-solid" type="text"
                                        value="<?php echo e($ba->account_number); ?>" placeholder="Bank Account Number"
                                        name="account_number" value="" required />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <div class="modal fade" id="idcashqModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">IDCash Activation</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Do you already have an idcash account?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#idcashModal">No</button>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#idcashfModal">Yes</button>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="<?php echo e(url('idcash/connect')); ?>" class="modal fade" id="idcashfModal" tabindex="-1"
        aria-hidden="true">
        <?php echo csrf_field(); ?>
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">IDCash Connect</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">IDCash VA Number</label>
                            <input class="form-control form-control-solid" type="text"
                                placeholder="Input your IDCash VA number" name="idcashVa" value="" required />
                        </div>
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">IDCash Card Number</label>
                            <input class="form-control form-control-solid" type="text"
                                placeholder="Input your IDCash Card number" name="idcashCard" value="" required />
                        </div>
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">IDCash Card PIN</label>
                            <input class="form-control form-control-solid" type="text"
                                placeholder="Input your IDCash Card PIN" name="idcashPin" value="" required />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Connect IDCash Account Now</button>
                </div>
            </div>
        </div>
    </form>

    <!-- Modal -->
    <form method="POST" action="<?php echo e(url('idcash/activate')); ?>" class="modal fade" id="idcashModal" tabindex="-1"
        aria-hidden="true">
        <?php echo csrf_field(); ?>
        <div class="modal-dialog modal-xl modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Terms and Conditions</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>
                        These terms and conditions must be read, understood and approved by the User before registering.
                        These terms and conditions elaborate the Privacy Policy and the Terms & Conditions that must be
                        approved by the User. The “idcash” is owned by PT Artha Total Mandiri (as application provider) and
                        by registering through the idcash Application, the User has understood, accepted and agreed to the
                        Terms & Conditions which have been set. <br><br>
                        PT Artha Total Mandiri ("Company") has the right to change these Terms & Conditions including but
                        not limited to the Update of the idcash application at any time without prior notice to the User.
                        Changes to the Terms & Conditions apply from the time they are displayed on the idcash Application.
                        If needed, the Company may at any time stop the services provided in the idcash Application (either
                        temporary or permanently) for any reason. User hereby agrees to and exempts the Company from all
                        forms of claims for the exercise of company rights under these provisions.<br><br>
                        The User can only access services in accordance with legal usage methods. The Company has the right
                        not to allow the User to use the service when the User uses an incompatible or invalid Application
                        and / or Software or for purposes other than those referred to in this Application.<br><br><br>
                        By using these applications and services, the User agrees that:<br>
                    <ul>
                        <li>The User will use the services only for legitimate and intended purposes.</li>
                        <li>The User will not use the service to send or store material that is unlawful or for fraud.</li>
                        <li>The User will not use the service to cause interference and inconvenience to other parties.</li>
                        <li>The User will not intentionally or unintentionally do something that causes harm to the other
                            party.</li>
                        <li>The User will not try to do anything that harms the service and the Company in any way.</li>
                        <li>The User will not copy or distribute content without written permission from the Company.</li>
                        <li>The User will only use this service for their own use.</li>
                        <li>The User will maintain the confidentiality of PINs and User Accounts.</li>
                        <li>The User agrees to provide accurate and up-to-date information on the use of the service.</li>
                        <li>The User agrees that the Company can rely on the information that the User provides, and that
                            all data / identities / information that the User provides to the Company are true and accurate.
                            If the information that the User provides is incorrect then the Company has the right to
                            terminate the agreement and use of the service at any time without prior notice.</li>
                        <li>The User will only access or use account data themselves, not one belonging to other parties.
                        </li>
                        <li>The User agree not to use any means to deceive the Company.</li>
                        <li>The User agrees that the User is fully responsible for all losses, damage and losses suffered by
                            the User, other parties or the Company caused by violations of these Terms & Conditions.</li>
                    </ul><br>
                    <h5>IDCash Application</h5>
                    <ol>
                        <li>idcash application is an application that is prepared to simplify Users in recording payment
                            transactions made using the idcash Card as well as the idcash Card management.</li>
                        <li>The User Account can only be activated if the User agrees to these Terms and Conditions and
                            performs a self-photograph (selfie), photo of their identity card, and some other data as
                            contained in the idcash Application. The User are required to fill in several additional fields
                            related to the user's personal data.</li>
                        <li>By carrying out the steps as outlined in number 2, the User declares and guarantees that the
                            data entered into the idcash Application is true and can be legally justified.</li>
                        <li>The User understands, accepts and agrees that:<br>
                            a. The Company has the right to reject or approve verification of User Accounts in accordance
                            with the specified standards.<br>
                            b. The Company has the right to request verification of User Accounts and User transactions if
                            the system detects suspicious and / or improper transaction activities according to the Company.
                        </li>
                        <li>The user is aware and agrees that the misuse of the idcash account by other parties as a result
                            of negligence is the responsibility of the User.</li>
                        <li>To improve service to the User, the Company has the right to make an Update of the idcash
                            Application without prior notice to the User.</li>
                        <li>The company can carry out legal processes in accordance with the prevailing laws and regulations
                            in Indonesia for all forms of misuse, falsification of data including but not limited to data
                            theft of idcash Applications and idcash Cards.</li>
                    </ol><br>
                    <h5>Service Fee</h5><br>
                    By conducting transactions using the idcash Card, you agree to be charged the following fees:<br>
                    <ion-list lines="full">
                        <ion-item>
                            <p>Cash withdrawal through ATM*<br>Rp. 7.500,- per transaction</p>
                        </ion-item>
                        <ion-item>
                            <p>Monthly Fee<br>Rp. 10.000,-</p>
                        </ion-item>
                        <ion-item>
                            <p>Card Administration Fee*<br>Rp. 35.000,-</p>
                        </ion-item>
                        <ion-item>
                            <p>No Transaction >3 months<br>Rp. 5.000,-</p>
                        </ion-item>
                        <ion-item>
                            <p>Card Administration Fee*<br>Rp. 35.000,-</p>
                        </ion-item>
                        <ion-item>
                            <p>Inactive Account >9 months<br>Rp. 15.000,- per transaction</p>
                        </ion-item>
                        <ion-item>
                            <p>No Transaction >3 months<br>Rp. 5.000,- per transaction</p>
                        </ion-item>
                        <ion-item>
                            <p>Minimum Required Balance<br>Rp. 35.000,-</p>
                        </ion-item>
                        <ion-item>
                            <p>Inactive Account >9 months<br>Rp. 15.000,- per transaction</p>
                        </ion-item>
                        <ion-item>
                            <p>Minimum Required Balance<br>Rp. 35.000,-</p>
                        </ion-item>
                    </ion-list><br>
                    <h5>Privacy</h5><br>
                    <ol>
                        <li>In the account registration process on the idcash application, the personal data that is
                            inputted by the User includes:
                            <ol type="a">
                                <li>E-mail, which will be used to communicate with the User in terms of transactions,
                                    newsletters, promotional program information, or other matters related to platform
                                    development.</li>
                                <li>Demographic information such as gender, date of birth, occupation, etc.</li>
                            </ol>
                        </li>
                        <li>Information Sharing
                            <ol type="a">
                                <li>The Company has the right to share the User’s identity information in the matter of
                                    investigating legal cases, Government requests, legal processes and investigations that
                                    are in accordance with the requests stipulated in the applicable legal provisions and
                                    laws.</li>
                                <li>The User agrees to the use of the User data for the services provided by the Company and
                                    its business partners.</li>
                                <li>The User agrees to the company to process data, including but not limited to disclosing,
                                    collecting, using, storing, transferring and sharing data.</li>
                            </ol>
                        </li>
                        <li>The Company has an idcash website that can have links to other third party websites. The company
                            is not responsible for the privacy actions of third parties including the contents of the
                            website. All consequences that occur related to the activities of the User who visit the website
                            are the responsibility of the User.</li>
                    </ol>
                    <br>
                    <h5>Blocking</h5><br>
                    The company has the right to block the idcash Application Account if it is indicated or suspected that
                    there is an out-of-the-ordinary transaction as stipulated in the Company provisions, or fraudulent
                    transactions and / or identified as being included in the List of Terrorists registered with the
                    applicable Government institution. Decisions made by the Company in blocking idcash Application Accounts
                    permanently or temporarily are valid and absolute on applicable Company policies. The User acknowledges
                    and agrees that the Company has the right of the Company's authority to permanently or temporarily block
                    idcash Application Accounts and / or idcash Cards. The User and the Company do not have an obligation to
                    notify the User of the policies taken.
                    <br><br>
                    <h5>Message and Notification</h5><br>
                    The User knows and agrees that based on the data the User gives to the Company, the Company can send to
                    the User notifications, messages and / or electronic forms in the form of promotions or non-promotions
                    based on location and other information.<br>
                    <br>
                    <h5>Copyright</h5><br>
                    The entire content of the idcash Application, idcash Card and idcash website such as text, graphics,
                    logos, button icons, images, sounds, digital files, data compilation and software are the property of
                    the Company. Compilation of all contents of the idcash application, idcash Card, idcash website and its
                    services exclusively belongs to the Company and is protected by Indonesian Law and International
                    Copyright.<br>
                    <h5>Limitation of Liability</h5><br>
                    In certain which permitted by law, we (these includes our employees, wholesale agents and sub agents)
                    are not liable whether in contract, tort (including negligence), or any other reason to anyone else for
                    direct, indirect or consequential damage, loss of profits, loss of data or revenue, loss of use, missed
                    opportunities for business, denial of service or access to our website and application or the service
                    and based on any type of liability including breach.<br><br>
                    We provide application and website and we do not represent or warrant that the reliability, timeliness,
                    quality, suitability, availability, accuracy, completeness or security. Applications sometimes have
                    limitations, delays, and other issues contained in the use of internet and electronic communications.
                    Any delay, damage or loss, failure or delivery is not our responsibility for this.<br>
                    We do not have any responsibility to oversee your access or use of the application but we ensure uses of
                    the application and to ascertain compliances with this terms of use.<br><br>
                    <h5>DISCLAIMER</h5><br>
                    YOU HEREBY AGREE THAT YOUR USE OF THE APPLICATION AND ANY OTHER SERVICES OR MATERIAL WE PROVIDE IN
                    CONNECTION WITH YOUR APPLICATION IS AT YOUR SOLE RISK. WE EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES
                    INCLUDING (WITHOUT LIMITATION) WARRANTIES OF MERCHANTABILITY, FITNESS FOR PURPOSE, SAFETY, REALIABILITY,
                    DURABILITY, TITLE AND NON-INFRINGEMENT. IF YOU DOWNLOAD ANY MATERIALS FROM THE SITE AND APPLICATION, YOU
                    DO SO AT YOUR OWN DISCRETION AND RISK AND WE WILL NOT BE RESPONSIBLE WITH COMPUTER SYSTEM OR LOSS OF
                    DATA RESULT.<br><br>
                    <h5>Contact us</h5><br>
                    You may contact us by electronic mail to cs@idcash.id or by phone at (021)-51402015. All your
                    correspondences will be noted, recorder and stored for our records.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Agree & Activate Now</button>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        function handleChange(src) {
            if (src.value == 'bank') {
                $("#bank").show();
            } else {
                $("#bank").hide();
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style>
        .reveal-if-active {
            opacity: 0;
            max-height: 0;
            overflow: hidden;
            font-size: 16px;
            transform: scale(0.8);
            transition: 0.5s;
        }

        .reveal-if-active label {
            display: block;
            margin: 0 0 3px 0;
        }

        .reveal-if-active input[type=text] {
            width: 100%;
        }

        input[type="radio"]:checked~.reveal-if-active,
        input[type="checkbox"]:checked~.reveal-if-active {
            opacity: 1;
            max-height: 2000px;
            padding: 10px 20px;
            transform: scale(1);
            overflow: visible;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script>
        $(document).ready(function() {
            firstReload();

            function firstReload() {
                var code = $('.code').val();
                $('#amont').val(1);
                var amont = 1;
                console.log(code);
                console.log(amont);
                hitung(code, amont);
            }
            $('#amont').on('keyup', function() {
                var amont = $(this).val();
                var code = $('.code').val();
                console.log(code);
                console.log(amont);
                hitung(code, amont);
            });
            $('.code').on('change', function() {
                var code = $(this).val();
                var amont = $('#amont').val();
                console.log(code);
                console.log(amont);
                hitung(code, amont);
            });

            function hitung(code, amont = 1) {
                $.ajax({
                    method: 'GET',
                    url: "<?php echo e(url('withdraw/getCryptoCurrency')); ?>" + '?code=' + code + '&amont=' + amont,
                    success: function(rs) {
                        if (rs.status == 200) {
                            $('#idrvalue').val(rupiah(rs.data.idrvalue));
                        }
                    }
                })
            }

            const rupiah = (number) => {
                return new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR"
                }).format(number);
            }
        });
    </script>
    <script>
        var FormStuff = {

            init: function() {
                this.applyConditionalRequired();
                this.bindUIActions();
            },

            bindUIActions: function() {
                $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
            },

            applyConditionalRequired: function() {

                $(".require-if-active").each(function() {
                    var el = $(this);
                    if ($(el.data("require-pair")).is(":checked")) {
                        el.prop("required", true);
                    } else {
                        el.prop("required", false);
                    }
                });

            }

        };

        FormStuff.init();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/withdraw/index.blade.php ENDPATH**/ ?>
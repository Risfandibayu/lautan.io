<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Exlaut</title>
        <link href="<?php echo e(asset('public')); ?>/assets/css/styles.css" rel="stylesheet" />
        <link rel="shortcut icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <script data-search-pseudo-elements="" defer="" src="<?php echo e(asset('public')); ?>/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    </head>
    <body class="">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4 mt-5">
                        <div class="row justify-content-center">
                            <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11">
                                <!-- Social login form-->
                                <div class="card my-5">
                                    <div class="card-body p-5 text-center">
                                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/logo-nav.png" style="height:50px;    width: auto;"></a>
                                    </div>
                                    <hr class="my-0" />
                                    <div class="card-body p-5">
                                        <?php if(session('error')): ?>
                                        <div class="alert alert-danger alert-icon" role="alert">
                                            <div class="alert-icon-aside">
                                                <i data-feather="alert-triangle"></i>
                                            </div>
                                            <div class="alert-icon-content">
                                                <?php echo e(session('error')); ?>

                                            </div>
                                        </div>
                                        <?php endif; ?>

                                        <?php if(session('success')): ?>
                                        <div class="alert alert-success alert-icon" role="alert">
                                            <div class="alert-icon-aside">
                                                <i data-feather="check-circle"></i>
                                            </div>
                                            <div class="alert-icon-content">
                                                <?php echo e(session('success')); ?>

                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <!-- Login form-->
                                        <form method="POST" action="<?php echo e(route('login')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <!-- Form Group (email address)-->
                                            <div class="mb-3">
                                                <label class="text-gray-600 small" for="emailExample">Email address</label>
                                                <input class="form-control form-control-solid" type="text" placeholder="" aria-label="Email Address" name="email" value="<?php echo e(old('email')); ?>" />
                                            </div>
                                            <!-- Form Group (password)-->
                                            <div class="mb-3">
                                                <label class="text-gray-600 small" for="passwordExample">Password</label>
                                                <input class="form-control form-control-solid" type="password" placeholder="" aria-label="Password" name="password" />
                                            </div>
                                            <!-- Form Group (forgot password link)-->
                                            <!-- <div class="mb-3"><a class="small" href="/password/fotgot">Forgot your password?</a></div> -->
                                            <!-- Form Group (login box)-->
                                            <div class="d-flex align-items-center justify-content-between mb-0">
                                                <button type="submit" class="btn btn-primary btn-block">Login</button>
                                            </div>
                                        </form>
                                    </div>
                                    <hr class="my-0" />
                                    <div class="card-body px-5 py-4">
                                        <div class="small text-center">
                                            New user?
                                            <a href="<?php echo e(url('register')); ?>">Create an account!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer-admin mt-auto footer-dark">
                    <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">Copyright © Exlaut <?php echo e(date("Y")); ?></div>
                            <div class="col-md-6 text-md-end small">
                                <a href="#!">Privacy Policy</a>
                                ·
                                <a href="#!">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="<?php echo e(asset('public')); ?>/assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo e(asset('public')); ?>/assets/js/scripts.js"></script>
</body>

</html>
<?php /**PATH /home/lautanio/dev.lautan.io/resources/views/login.blade.php ENDPATH**/ ?>
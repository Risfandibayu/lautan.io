<?php $__env->startSection('style'); ?>
<style>
    img,
svg {
  width: 25px;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="server"></i></div>
                        Data Log
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3"><a href="/user/create" class="btn btn-sm btn-light"><i data-feather="plus"></i>&nbsp; Tambah User</a></div> -->
            </div>
        </div>
    </div>
</header>

<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="card mb-3">
        <div class="card-body">
            
            <form class="row" id="searchForm" method="POST" action="/datalog/search" autocomplete="off">
                <?php echo e(csrf_field()); ?>

                <input type="hidden" name="download" id="downloadVal" value="0">
                <input type="hidden" name="type" value="1">
                <div class="form-group col-md-3">
                    <label for="codeid">Search</label><br>
                    <input type="text" style="width:250px;" class="form-control" name="search" placeholder="" value="<?php if(session('kk_search')): ?><?php echo e(session('kk_search')); ?><?php endif; ?>">
                </div>
                <div class="form-group col-md-3">
                    <label for="codeid">Start Date</label>
                    <div class="input-icon">
                        <input class="form-control datepicker" name="start_date" id="start_date" type="text" autocomplete="off" value="<?php if(session('kk_start_date')): ?><?php echo e(session('kk_start_date')); ?><?php else: ?><?php echo e(DATE('Y-m-d')); ?><?php endif; ?>">
                        <div class="input-icon-addon"><i class="fe fe-calendar" aria-hidden="true"></i></div>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="codeid">End Date</label>
                    <div class="input-icon">
                        <input class="form-control datepicker" name="end_date" id="end_date" type="text" autocomplete="off" value="<?php if(session('kk_end_date')): ?><?php echo e(session('kk_end_date')); ?><?php else: ?><?php echo e(DATE('Y-m-d')); ?><?php endif; ?>">
                        <div class="input-icon-addon"><i class="fe fe-calendar" aria-hidden="true"></i></div>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="codeid">Channel</label>
                    <select class="form-control" name="type">
                        <option value="">Semua Channel</option>
                        <option value="ipaymu" <?php if(session('kk_status') == 'ipaymu'): ?> selected <?php endif; ?>>ipaymu</option>
                        <option value="tko" <?php if(session('kk_status') == 'tko'): ?> selected <?php endif; ?>>tokocrypto</option>
                        <option value="bscscan" <?php if(session('kk_status') == 'bscscan'): ?> selected <?php endif; ?>>bscscan</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                <button type="submit" class="btn btn-primary mt-3"><i data-feather="search"></i>&nbsp;&nbsp;Search</a></button>
                </div>
            </form>
                
        </div>
    </div>

    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <!-- <th scope="col">ID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Channel</th> -->
                        <th scope="col">Log</th>
                        <!-- <th scope="col" style="width:130px;"></th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <!-- <td class="align-middle"><?php echo e($rows->id); ?></td>
                        <td class="align-middle"><?php echo e($rows->created_at); ?></td>
                        <td class="align-middle"><?php echo e($rows->channel); ?></td> -->
                        <td class="align-middle">
                            <a href="/datalog/<?php echo e($rows->id); ?>" class="btn btn-light btn-sm"><i data-feather="eye"></i>&nbsp;&nbsp;View Log</a><br><br>
                            CHANNEL: <?php echo e($rows->channel); ?><br>
                            <?php echo nl2br($rows->log); ?>

                        </td>
                        <td class="text-right align-middle">
                        <!-- <div class="btn-group">
                            <a href="/datalog/<?php echo e($rows->id); ?>" class="btn btn-light btn-sm"><i data-feather="eye"></i></a>
                        </div> -->
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <?php echo e($row->links()); ?>

        </div>
    </div>
</div>

    
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script type="text/javascript">
    function downloads()
    {
        $('#downloadVal').val('1');
        $('#searchForm').submit();
    }

    $('form').submit(function() {
        var startDate = new Date($("#start_date").val());
        var endDate = new Date($("#end_date").val());
        if (startDate > endDate) {
            alert("Mohon cek kembali pemilihan tanggal. Tanggal Akhir harus lebih besar dari Tanggal Mulai");
            return false;
        } else {
            return true;
        }
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/datalog/index.blade.php ENDPATH**/ ?>
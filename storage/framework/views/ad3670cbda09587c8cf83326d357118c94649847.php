<?php echo $__env->make('emails.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
    Hi, <?php echo e($name); ?> <br><br>
    Pendaftaran Anda berhasil, silahkan klik link dibawah ini untuk masuk ke akun Anda. <bt><br>

    <a href="<?php echo e($url); ?>"><?php echo e($url); ?></a>
</p>
<?php echo $__env->make('emails.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/emails/register.blade.php ENDPATH**/ ?>
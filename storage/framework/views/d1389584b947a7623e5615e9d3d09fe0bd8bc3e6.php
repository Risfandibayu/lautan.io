
<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="file"></i></div>
                        Transaction Confirmation
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="row">

        <div class="col-xl-6">
            <div class="card mb-4">
                            <div class="card-header">Data Transfer</div>
                            <div class="card-body">
                                <p class="small text-muted mb-0">Coin</p>
                                <h5><?php echo e($trx->code); ?></h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Amount</p>
                                <h5><?php echo e($trx->amount); ?></h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Time</p>
                                <h5><?php echo e($trx->time); ?></h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Bukti Transfer</p>
                                <img src="<?php echo e($trx->tximage); ?>" class="img-fluid" style="width:200px;">
                                <hr class="my-3" />
                                
                            </div>
                        </div>
            </div>
        <div class="col-xl-6">
        <div class="card mb-4">
                        <div class="card-header">Detail User</div>
                        <div class="card-body">
                            <p class="small text-muted mb-0">Nama</p>
                            <h5><?php echo e($trx->name); ?></h5>
                            <hr class="my-3" />
                            <p class="small text-muted mb-0">Email</p>
                            <h5><?php echo e($trx->email); ?></h5>
                            <hr class="my-3" />
                            <p class="small text-muted mb-0">No. Telepon</p>
                            <h5><?php echo e($trx->phone); ?></h5>
                            <hr class="my-3" />
                        </div>
                    </div>
        </div>
        
            
            
                
            
        <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#alertModal" data-id="<?php echo e($trx->trid); ?>" data-status="3" data-name="Approve verifikasi user <?php echo e($trx->name); ?>?">Approve</button>
                        <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#alertModal" data-id="<?php echo e($trx->trid); ?>" data-status="4" data-name="Reject verifikasi user <?php echo e($trx->name); ?>?">Reject</button>
                    </div>
                </div>
            
            </div>


            <form method="POST" action="<?php echo e(action('TransactionController@trx_confirm')); ?>" class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
                <?php echo csrf_field(); ?>
                <input name="id" type="hidden" id="user-id" value="<?php echo e($trx->trid); ?>">
                
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center" id="user-name">
                        Konfirmasi Bukti Pembayaran 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-success">Ya</button>
                    </div>
                    </div>
                </div>
            </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/transaction/confirm.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>

<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        
                        Edit Custom Variable
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main page content-->
<div class="container-xl">
    <?php if(session('error')): ?>
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('error')); ?>

        </div>
    </div>
    <?php endif; ?>

    <?php if(session('success')): ?>
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('success')); ?>

        </div>
    </div>
    <?php endif; ?>


    <div class="card">
        <div class="card-body">

            <div id="form">
                <form method="POST" action="<?php echo e(route('webadmin.editcustomvar')); ?>">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" class="form-control" name="id" value="<?php echo e($data->id); ?>">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Type</label>
                            <input class="form-control" type="text" name="type" required value="<?php echo e($data->type); ?>" />
                        </div>
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Code</label>
                            <input class="form-control" type="text" name="code" required value="<?php echo e($data->code); ?>" />
                        </div>
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Value</label>
                            <input class="form-control" type="text" name="value" required value="<?php echo e($data->value); ?>" />
                        </div>	
                        <hr class="my-4" />
                        <div class="col-md-6">
                            <button class="btn btn-danger btn-sm" onclick="btnaction()">Cancel</button>
                            <button class="btn btn-primary btn-sm" type="submit">Update Custom Variable</button>
                        </div>
                    </div>
                    
                </form>
            </div>

        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Risfandi Bayu\Downloads\Compressed\exlaut-staging\exlaut-staging\resources\views/webadmin/customvaredit.blade.php ENDPATH**/ ?>
<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
    });
} );
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="activity"></i></div>
                        Markets
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl">

    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Coin</th>
                        <th scope="col">Name</th>
                        <th scope="col" class="text-end">Last Price</th>
                        <th scope="col" class="text-end">24H Change</th>
                        <th scope="col" class="text-end">24H High</th>
                        <th scope="col" class="text-end">24H Low</th>
                        <th scope="col" class="text-end">24H Volume</th>
                        <!-- <th scope="col"></th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php 
                        if($rows->change > 0) {
                            $class = 'text-success';
                        } elseif($rows->change < 0) {
                            $class = 'text-danger';
                        } else {
                            $class = '';
                        }
                    ?>
                    <tr>
                        <td><img src="<?php echo e($rows->image); ?>" width="20px"></td>
                        <td><?php echo e($rows->code); ?></td>
                        <td><?php echo e($rows->name); ?></td>
                        <td class="<?php echo e($class); ?> text-end"><?php echo e($rows->price); ?></td>
                        <td class="<?php echo e($class); ?> text-end"><?php echo e($rows->change); ?>%</td>
                        <td class="text-end"><?php echo e($rows->highPrice); ?></td>
                        <td class="text-end"><?php echo e($rows->lowPrice); ?></td>
                        <td class="text-end"><?php echo e($rows->marketPrice); ?></td>
                        <!-- <td><a href="/buy" class="btn btn-sm btn-success"><i data-feather="shopping-bag"></i>&nbsp; Beli</a></td> -->
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\dev.lautan.io\resources\views/market/index.blade.php ENDPATH**/ ?>
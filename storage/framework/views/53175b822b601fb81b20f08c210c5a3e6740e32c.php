
<?php $__env->startSection('content'); ?>
    <!-- Main page content-->
    <div class="container-xl mt-4">
        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                <?php if(session('success')): ?>
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('success')); ?>

                        </div>
                    </div>
                <?php endif; ?>
                <div class="card mb-4">
                    <!-- <div class="card-header"></div> -->
                    <div class="card-body text-center">
                        <img src="https://idcash.id/landing/img/idcash-logo/logo_hd.png" width="500px">
                        <p><br><br>Please download IDCash Apps to manage your IDCash account, IDCash balance and IDCash
                            card<br></p>
                        <a href="https://apps.apple.com/us/app/idcash/id1489893894" target="_blank"><img
                                src="https://idcash.s3.amazonaws.com/App-Store-buttons_Apple.png" width="500px"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.idcash" target="_blank"><img
                                src="https://idcash.s3.amazonaws.com/google-play-badge-300x89.png" width="500px"></a>
                    </div>
                    <div class="card-footer" style="background:#fff;">
                        <a class="btn btn-sm btn-primary float-end" href="<?php echo e(url('withdraw')); ?>">Continue Withdraw</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/account/idcash.blade.php ENDPATH**/ ?>
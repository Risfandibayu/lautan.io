<?php $__env->startSection('content'); ?>

<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        
                        List Media
                    </h1>
                </div>
                <div class="col-auto mb-3">
                    <button class="btn btn-success btn-sm btnmedia" onclick="btnaction()">Add Media</button>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main page content-->
<div class="container-xl">
    <?php if(session('error')): ?>
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('error')); ?>

        </div>
    </div>
    <?php endif; ?>

    <?php if(session('success')): ?>
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('success')); ?>

        </div>
    </div>
    <?php endif; ?>


    <div class="card">
        <div class="card-body">

            <div id="form" style="display: none">
                <form method="POST" action="<?php echo e(route('webadmin.uploadmedia')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Upload Images</label>
                            <input class="form-control" type="file" name="files" required />
                        </div>
                        <hr class="my-4" />
                        <div class="col-md-6">
                            <button class="btn btn-danger btn-sm" onclick="btnaction()">Cancel</button>
                            <button class="btn btn-primary btn-sm" type="submit">Save Media</button>
                        </div>
                    </div>
                    
                </form>
            </div>
            
            <div id="data">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Images</th>
                            <th scope="col">Images URL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__empty_1 = true; $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <tr>
                            <td><?php echo e($row->id); ?></td>
                            <td><img src="<?php echo e($row->url); ?>" width="150px"></td>
                            <td><?php echo e($row->url); ?></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <tr><td colspan="3">Data Not Found</td></tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<script>
function btnaction() {
    $("#form").toggle();
    $("#data").toggle();
}
</script>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/webadmin/media.blade.php ENDPATH**/ ?>
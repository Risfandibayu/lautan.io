
<?php $__env->startSection('content'); ?>
<section class="banner-style-ten"   style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;padding-top:180px;">
    <div class="container" style="margin-top:-20px;">
        <div class="row align-items-center">
            <div class="row">

          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="footer-info"><br/>
              <p style="font-weight:400;text-transform: none;font-size: 15px;" align="justify">Platform that provides a secure way where you can convert your digital asset in Indonesia and make everything become handy.

Lautan.io offers integrated digital asset transactions direct to your pocket. Designed to make it easier for you to make transactions in Indonesia.</p>
              
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 footer-links">
            
              <!-- <h3 style="text-transform: none;font-size: 40px;">Ngelapak</h3> -->
              <!-- <h4>Alamat Kami</h4> -->
              <h3 style="font-size: 24px;font-weight:600">Our Address</h3>
              <p style="font-size: 15px;">
                  Lautan Harum Mewangi <br>
Jl. Lebak Sari No.8A, Kerobokan Kelod, Kuta Utara <br>
Kab. Badung, Bali <br>
80361, Indonesia <br>


<br>
                  <ul>
                    <!--<li>Denpasar, Bali</li>-->
                    <li><i class="fab fa-whatsapp"></i><a style="color:grey;" href="https://api.whatsapp.com/send?phone=+62816860871"> +62 816 860 871</a></li>
                    <li><i class="fa fa-envelope"></i><a style="color:grey;" href="mailto:hello@lautan.io"> hello@lautan.io</a></li>
                </ul>
              </p>
              <div class="social-links mt-3">
                <a href="https://instagram.com/ngelapak.co.id?utm_medium=copy_link" class="instagram" target="_blank" ><i class="bx bxl-instagram"></i></a>
                  <a href="https://business.facebook.com/latest/home?nav_ref=bm_home_redirect&business_id=1073657656465527&asset_id=101427295405162" class="facebook" target="_blank" ><i class="bx bxl-facebook"></i></a>
                  <a href="https://www.linkedin.com/company/ngelapak-co-id" class="instagram" target="_blank" ><i class="bx bxl-linkedin"></i></a>
                  <a href="https://twitter.com/Ngelapak2021?s=08" class="twitter" target="_blank" ><i class="bx bxl-twitter"></i></a>
              </div>
            
          </div>

          

          

          <div class="col-lg-5 col-md-12 footer-newsletter">
            <!-- <div class="col-lg-4 col-md-6 footer-info"> -->
              <h3 style="font-size: 24px;font-family: 'Montserrat', sans-serif;font-weight:600">Find Us</h3>
              <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d494.65806635891215!2d112.6736403318811!3d-7.324155900000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fc58381ff9b7%3A0x194196ebbc065378!2sA%26A%20ROYAL%20RESIDENCE!5e0!3m2!1sid!2sid!4v1632986719031!5m2!1sid!2sid" width="100%" height="210" style="border:0;" allowfullscreen="" loading="lazy"></iframe>-->
             
              
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.158548765958!2d115.15564711411909!3d-8.676468290730593!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd24713734cfdc5%3A0x721c19997accf639!2sJl.%20Lb.%20Sari%20No.8%2C%20Kerobokan%2C%20Kec.%20Kuta%20Utara%2C%20Kabupaten%20Badung%2C%20Bali%2080361!5e0!3m2!1sid!2sid!4v1643273415647!5m2!1sid!2sid" width="100%" height="210" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <!-- </div> -->

          </div>

        </div>
        </div>
    </div>
</section>
    

<!-- partnership -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/public_html/resources/views/land/contact.blade.php ENDPATH**/ ?>
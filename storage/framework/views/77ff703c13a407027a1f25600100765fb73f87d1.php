<?php $__env->startSection('style'); ?>
<link href="<?php echo e(asset('public/Appway2')); ?>/css/flag-icon.css" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- <header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Akun
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header> -->
<!-- Main page content-->
<div class="container-xl mt-4">
    <div class="row">
        <div class="offset-xl-2 col-xl-8">
            <?php if(session('warning')): ?>
            <div class="alert alert-warning alert-icon" role="alert">
                <div class="alert-icon-aside">
                    <i data-feather="alert-triangle"></i>
                </div>
                <div class="alert-icon-content">
                    <?php echo e(session('warning')); ?>

                </div>
            </div>
            <?php endif; ?>

            <?php if(session('error')): ?>
            <div class="alert alert-danger alert-icon" role="alert">
                <div class="alert-icon-aside">
                    <i data-feather="alert-triangle"></i>
                </div>
                <div class="alert-icon-content">
                    <?php echo e(session('error')); ?>

                </div>
            </div>
            <?php endif; ?>

            <?php if(session('success')): ?>
            <div class="alert alert-success alert-icon" role="alert">
                <div class="alert-icon-aside">
                    <i data-feather="check-circle"></i>
                </div>
                <div class="alert-icon-content">
                    <?php echo e(session('success')); ?>

                </div>
            </div>
            <?php endif; ?>
            <div class="card mb-4">
                <div class="card-header">Account Verification</div>
                <div class="card-body">
                    <div class="step step-primary mb-5">
                        <div class="step-item active">
                            <a class="step-item-link" href="#">Account Information</a>
                        </div>
                        <!-- <div class="step-item">
                            <a class="step-item-link disabled" href="#">Bank Account</a>
                        </div> -->
                        <div class="step-item">
                            <a class="step-item-link disabled" href="#">Upload File</a>
                        </div>
                        <div class="step-item">
                            <a class="step-item-link disabled" href="#" tabindex="-1">Finish</a>
                        </div>
                    </div>
                    <form method="POST" action="/verification">
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label class="small mb-1">Name</label>
                                <input class="form-control form-control-solid" type="text" placeholder="Please insert your full name" name="name" value="<?php echo e(Auth::user()->name); ?>" required />
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="small mb-1">Email</label>
                                <input class="form-control form-control-solid" type="text" placeholder="Please insert your e-mail" name="email" value="<?php echo e(Auth::user()->email); ?>" required disabled />
                            </div>
                            
                            <div class="col-md-6 mb-3">
                                <label class="small mb-1">Phone</label>
                                <div class="row">
                                    
                                    <?php
                                    use Illuminate\Support\Facades\DB;
                                    $country = db::table('country')->select('*')->get();
                                    
                                    ?>
                                    
                                    
                                    <div class="col-md-3">
                                        <select class="form-control form-control-solid" name="country">
                                            <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $co): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="+<?php echo e($co->phonecode); ?>">+<?php echo e($co->phonecode); ?> - <?php echo e($co->nicename); ?> </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <input class="form-control form-control-solid" type="text" placeholder="Please insert your phone number" name="phone" value="<?php echo e(Auth::user()->phone); ?>" required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="small mb-1">National ID/Passport ID</label>
                                <input class="form-control form-control-solid" type="text" placeholder="Please insert your ID number" name="nationalId" value="<?php echo e(Auth::user()->nationalId); ?>" required />
                            </div>
                            <div class="mb-3">
                                <label class="small mb-1">Address</label>
                                <input class="form-control form-control-solid" type="text" placeholder="Please insert your detail address" name="address" value="<?php echo e(Auth::user()->address); ?>" required />
                            </div>
                            <hr class="my-4" />
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <button class="btn btn-primary float-end" type="submit">Next</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/verification/index.blade.php ENDPATH**/ ?>
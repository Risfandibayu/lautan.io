<?php echo $__env->make('emails.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
    Hi, <?php echo e($name); ?> <br><br>
    You have made a withdrawal request, please enter the pin below to continue the transaction. <bt><br>

    <strong><?php echo e($pin); ?></strong>
</p>
<?php echo $__env->make('emails.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/public_html/resources/views/emails/withdrawpin.blade.php ENDPATH**/ ?>

<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
    });
} );
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="clock"></i></div>
                        Transaction History
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl">
    <?php if(session('error')): ?>
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('error')); ?>

        </div>
    </div>
    <?php endif; ?>

    <?php if(session('success')): ?>
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('success')); ?>

        </div>
    </div>
    <?php endif; ?>
    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            <?php if(count($row)): ?>
            <table class="table" id='table'>
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">TRX ID</th>
                        <th scope="col">Coin</th>
                        <th scope="col">Type</th>
                        <th scope="col" class="text-end">Amount</th>
                        <th scope="col" class="text-center">Status</th>
                        <th scope="col">Transaction Date</th>
                        <th scope="col" width="250px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><img src="<?php echo e($rows->currency->image); ?>" width="20px"></td>
                        <td><?php echo e($rows->trx_id); ?></td>
                        <td><?php echo e($rows->code); ?></td>
                        <td class="<?php if($rows->type == 'deposit'): ?> text-success <?php elseif($rows->type == 'withdraw'): ?> text-warning <?php endif; ?>"><?php echo e($rows->getType()); ?></td>
                        <td class="text-end"><?php echo e($rows->amount); ?></td>
                        <td class="<?php if($rows->status == 1): ?> text-success <?php elseif($rows->status == 0): ?> text-dark <?php elseif($rows->status == 2): ?> text-danger <?php endif; ?> text-center">
                            
                            <?php if($rows->status == 1): ?>
                            Success
                            <?php elseif($rows->status == 0): ?>
                            Pending
                            <?php else: ?>
                            Reject
                            <?php endif; ?>
                            
                            </td>
                        <td><?php echo e($rows->created_at); ?></td>
                        <td class="text-right">
                        <?php if($rows->type == 'withdraw'): ?>
                            <?php if($rows->status == 0): ?>
                                <!--<a href="#" data-id="<?php echo e($rows->id); ?>" data-name="<?php echo e($rows->name); ?>" data-target="cw" class="btn-cw btn btn-sm btn-outline-primary"><i data-feather="check-circle"></i>&nbsp; Confirmation</a>-->
                                 <?php if($rows->cwimg1 == null): ?>
                                <a href="#" data-id="<?php echo e($rows->id); ?>" data-name="<?php echo e($rows->name); ?>" class="btn-cw btn btn-sm btn-outline-primary"><i data-feather="check-circle"></i>&nbsp; Confirmation</a>
                                <?php else: ?>
                                <span class="badge bg-warning">under verification process</span>
                                <?php endif; ?>
                            <?php else: ?>
                            
                            <?php endif; ?>
                        <?php else: ?>
                            <?php if($rows->status == 0): ?>
                                <?php if($rows->tximage == null): ?>
                                <a href="#" data-id="<?php echo e($rows->id); ?>" data-name="<?php echo e($rows->name); ?>" class="btn-update btn btn-sm btn-outline-primary"><i data-feather="check-circle"></i>&nbsp; Confirmation</a>
                                <?php else: ?>
                                <span class="badge bg-warning">under verification process</span>
                                <?php endif; ?>
                            <?php elseif($rows->status == 1): ?>
                                
                            <?php endif; ?>
                        <?php endif; ?>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            <?php else: ?> 
            <p class="text-center"><small>Transaction not found<small></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- Modal Confirm -->
<div class="modal fade" id="addItem" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="modal-dialog" role="document" method="POST" action="/transaction/confirm" enctype="multipart/form-data">
        <?php echo e(csrf_field()); ?>

        <input type="hidden" name="id" id="item-id">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="item-title"></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="small mb-1">TX ID</label>
                        <input type="text" class="form-control form-control-solid" name="txhash" value="" required>
                    </div>
                    <div class="form-group col-md-12 mt-3">
                        <label class="small mb-1">Transaction Screenshot</label>
                        <input class="form-control form-control-solid" name="image" type="file" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="save" value="1" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="cw" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="modal-dialog" role="document" method="POST" action="/transaction/wconfirm" enctype="multipart/form-data">
        <?php echo e(csrf_field()); ?>

        <input type="hidden" name="id" id="item-idc">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="item-titlec"></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="small mb-1">Withdrawal Input OTP screenshoot</label>
                        <input type="file" class="form-control form-control-solid" name="cwimg1" value="" required>
                    </div>
                    <div class="form-group col-md-12 mt-3">
                        <label class="small mb-1">OTP Code Screenshot (email)</label>
                        <input class="form-control form-control-solid" name="cwimg2" type="file" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="save" value="1" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script type="text/javascript">
    $('.btn-update').click(function(){
      $('#item-title').html('Transaction Confirmation');
      $('#item-id').val($(this).data("id"));
      $('#addItem').modal('show');        
    });
</script>
<script type="text/javascript">
    $('.btn-cw').click(function(){
      $('#item-titlec').html('Transaction Confirmation');
      $('#item-idc').val($(this).data("id"));
      $('#cw').modal('show');        
    });
</script>
<?php $__env->stopPush(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/transaction/index.blade.php ENDPATH**/ ?>
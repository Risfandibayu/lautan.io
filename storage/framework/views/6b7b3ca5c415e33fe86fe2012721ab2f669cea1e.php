<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Akun
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <nav class="nav nav-borders">
        <a class="nav-link ms-0" href="/account">Info Akun</a>
        <a class="nav-link active" href="/tokocrypto">Tokocrypto API</a>
        <a class="nav-link" href="/password">Ganti Password</a>
    </nav>
    <hr class="mt-0 mb-4" />
    <div class="row">
        <div class="col-xl-12">
            <div class="card mb-4">
                <!-- <div class="card-header">Tokocrypto API</div> -->
                <div class="card-body">
                    <?php if(session('warning')): ?>
                    <div class="alert alert-warning alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('warning')); ?>

                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(session('error')): ?>
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('error')); ?>

                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(session('success')): ?>
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('success')); ?>

                        </div>
                    </div>
                    <?php endif; ?>
                    <img src="<?php echo e(asset('public')); ?>/assets/images/logo-tokocrypto.png" width="130px">
                    <p class="small mb-3 mt-2">API Key dan Secret Key dibutuhkan untuk menghubungkan akun Tokocrypto Anda dengan platform Exlaut, Anda bisa mendapatkan API Key dan Secret Key dengan login ke akun Tokocrypto, kemudian buat API Key pada menu Manajemen API.</p>
                    <form method="POST" action="/tokocrypto">
                        <?php echo csrf_field(); ?>
                        <div class="mb-3">
                            <label class="small mb-1">API Key</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan tokocrypto api key" name="apikey" value="<?php echo e(Auth::user()->tokocryptoKey); ?>" required />
                        </div>
                        <div class="mb-3">
                            <label class="small mb-1">Secret Key</label>
                            <input class="form-control form-control-solid" type="password" placeholder="Masukan tokocrypto secret key" name="secretkey" value="<?php echo e(Auth::user()->tokocryptoSecret); ?>" required />
                        </div>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/account/tokocrypto.blade.php ENDPATH**/ ?>
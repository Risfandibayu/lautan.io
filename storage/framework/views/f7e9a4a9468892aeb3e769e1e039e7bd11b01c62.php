<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Lautan | subcription</title>
        <link href="<?php echo e(asset('public')); ?>/assets/css/styles.css" rel="stylesheet" />
        <link rel="shortcut icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <script data-search-pseudo-elements="" defer="" src="<?php echo e(asset('public')); ?>/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    </head>
    <body>
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4 mt-5">
                        <div class="row justify-content-center">
                        <div class="col-xl-12 col-lg-12">
                                <!-- Social registration form-->
                                <div class="card my-5">
                                    <div class="card-body p-5 text-center">
                                    <img src="<?php echo e(asset('public')); ?>/landing-assets/images/logo.png" style="height:50px;width:auto;"></a>
                                    </div>
                                    <hr class="my-0" />
                                    <div class="card-body p-5">
                                    <?php if(session('error')): ?>
                                    <div class="alert alert-danger alert-icon" role="alert">
                                        <div class="alert-icon-aside">
                                            <i data-feather="alert-triangle"></i>
                                        </div>
                                        <div class="alert-icon-content">
                                            <?php echo e(session('error')); ?>

                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if(session('success')): ?>
                                    <div class="alert alert-success alert-icon" role="alert">
                                        <div class="alert-icon-aside">
                                            <i data-feather="check-circle"></i>
                                        </div>
                                        <div class="alert-icon-content">
                                            <?php echo e(session('success')); ?>

                                        </div>
                                    </div>
                                    <?php endif; ?>
                                        <!-- Register form-->
                                        <!-- Form Row-->
                                        
                                        <div class="row">
                                            <div class="col-md-4 order-md-2 mb-4">
                                              <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                <span class="text-muted">Your cart</span>
                                                <span class="badge badge-secondary badge-pill">3</span>
                                              </h4>
                                              <ul class="list-group mb-3">
                                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                  <div>
                                                    <h6 class="my-0">Laut CryptCash Member</h6>
                                                    <small class="text-muted">Laut CryptCash Member Subscription</small>
                                                  </div>
                                                  <span class="text-muted">Rp. 100.000</span>
                                                </li>
                                                
                                                <li class="list-group-item d-flex justify-content-between">
                                                  <span>Total</span>
                                                  <strong>Rp. 100.000</strong>
                                                </li>
                                              </ul>
                                    
                                             
                                            </div>
                                            <div class="col-md-8 order-md-1">
                                              
                                              <!--<form class="needs-validation" novalidate>-->
                                              <form method="POST" action="<?php echo e(url('faspay')); ?>">
                                                 <?php echo csrf_field(); ?>
                                                 <h4 class="mb-3">Subcription</h4>
                                                 <div class="mb-3">
                                                  <label for="address">Name</label>
                                                  <input type="text" class="form-control" id="name" name="name" placeholder="your name" required>
                                                  <div class="invalid-feedback">
                                                    Please enter your name.
                                                  </div>
                                                </div>
                                                 <div class="mb-3">
                                                  <label for="email">Email</label>
                                                  <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" value="<?php echo e(session('email')); ?>">
                                                  <!--<?php echo e(session('amount')); ?>-->
                                                  <div class="invalid-feedback">
                                                    Please enter a valid email address for shipping updates.
                                                  </div>
                                                </div>
                                                <div class="mb-3">
                                                  <label for="address">Phone Number</label>
                                                  <input type="text" class="form-control" id="phone" name="phone" placeholder="your phone number" required>
                                                  <div class="invalid-feedback">
                                                    Please enter your phone number.
                                                  </div>
                                                </div>
                                                <hr>
                                                
                                                <!--<hr class="mb-4">-->
                                                <button class="btn btn-primary btn-lg btn-block" type="submit">Subcribe</button>
                                              </form>
                                            </div>
                                          </div>
                                            
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer-admin mt-auto footer-dark">
                    <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">Copyright © Lautan.io <?php echo e(date("Y")); ?></div>
                            <div class="col-md-6 text-md-end small">
                                <a href="#!">Privacy Policy</a>
                                ·
                                <a href="#!">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="<?php echo e(asset('public')); ?>/assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo e(asset('public')); ?>/assets/js/scripts.js"></script>
</body>

</html>
<?php /**PATH /home/lautanio/public_html/resources/views/faspay.blade.php ENDPATH**/ ?>
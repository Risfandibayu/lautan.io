
<?php $__env->startSection('content'); ?>
<section class="banner-style-ten" style="background-color:#F8FBFF;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%" >
        <div class="container">
            <div class="row align-items-center">
               
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h2>Partnership</h2>
                            </div>
                            <div class="text">Lautan.io offers an opportunity for everyone to receive additional income by participating in our partnership program. Grow your business with us and get a percentage of your client transactions! <br><br>Every customer can become a member of the partnership program, and all the links and marketing materials can be obtained directly into your Lautan.io partner dashboard. </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="https://lautan.s3.amazonaws.com/webcontent/media_qCHFcw.png" alt=""> </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="security-invisible"  style="background-color: #2F80ED;color: white;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%;">
        <div class="container">
            <div class="row align-items-center">
               
                <div class="col-lg-4 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInLeft" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="https://lautan.s3.amazonaws.com/webcontent/media_iSgbsz.png" alt=""> </figure>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex">
                                <h3>For websites and digital campaign</h3>
                            </div>
                            <div class="text">If you have a website, blog and forum, simply share a referral link on your platform and start receiving a percentage from each digital assets withdrawal that your referral makes. For your convenience, we provide all the digital campaign materials, such as: website banners can be found directly in your Lautan.io partner dashboard.
                                <br><br>
                            For any inquiries email to: marketing@Lautan.io.io </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>


    <section class="security-invisible" style="background-color:#F8FBFF;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%" >
        <div class="container">
            <div class="row align-items-center">
                
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h3>For individuals</h3>
                            </div>
                            <div class="text">If you want to receive partnership bonuses, you don’t have to create your own affiliate website. Simply copy the link from you’re your account page and share it with your friends or colleagues via an instant messenger, Telegram channel or any other resource that suits you better.<br><br>

                                Please note that spamming, advertising on illegal sites, as well as misleading advertising and any fraud is strictly prohibited.
                                <br><br>
                                Many of our partners have already appreciated all the advantages of Lautan.io’s partnership system and have become our reliable partners!</div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="https://lautan.s3.amazonaws.com/webcontent/media_Ds1MW8.png" alt=""> </figure>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <section class="security-invisible" style="background-color:#F8FBFF;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%" >
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInLeft" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="https://lautan.s3.amazonaws.com/webcontent/media_0H8o0w.png" alt=""> </figure>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h3>3 levels of the program</h3>
                            </div>
                            <div class="text">Lautan.io Partnership Program has 3 levels – after inviting customer, you will receive not only a percentage of their withdrawal of digital assets but also a percentage from your partnership team.
                                <br><br>
                                The detailed information can be found in your agreement that will be send by email after register as a partner.
                                <br><br>
                                The partnership program is not only profitable but also as transparent as possible – detailed statistics and the number of referrals for each level are available in the personal account of each participant in the “partner dashboard” tab.
                                <br><br>
                                In order to join our partnership program, you must register on the site or enter you personal account.</div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\dev.lautan.io\resources\views/land/partnership.blade.php ENDPATH**/ ?>
</td>
</tr>
<tr>
    
</table>
</td>
</tr>

<tr>
    <td style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word; background: #3282c3;">
        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0 auto; padding: 0; text-align: center; width: 570px;">
            <tr>
                <td class="content-cell" align="center" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 35px; word-break: break-word;">
                    <p class="sub align-center" style="box-sizing: border-box; color: #fff; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 1.5em; margin-top: 0;" align="center">© <?php echo DATE('Y'); ?> Lautan.io. All rights reserved.</p>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</td>
</tr>
</table>
</body>

</html>
<?php /**PATH /home/lautanio/dev.lautan.io/resources/views/emails/footer.blade.php ENDPATH**/ ?>
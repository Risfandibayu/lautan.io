<?php $__env->startSection('content'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/codemirror.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/codemirror.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/mode/python/python.min.js"></script>

<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        Update Pages
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main page content-->
<div class="container-xl">
    <?php if(session('error')): ?>
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('error')); ?>

        </div>
    </div>
    <?php endif; ?>

    <?php if(session('success')): ?>
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('success')); ?>

        </div>
    </div>
    <?php endif; ?>


    <div class="card">
        <div class="card-body">

            <div id="form">
                <form method="POST" action="<?php echo e(route('webadmin.editpages')); ?>">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" class="form-control" name="id" value="<?php echo e($data->id); ?>">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Title</label>
                            <input class="form-control" type="text" name="title" id="title" onkeyup="copyslug()" required  value="<?php echo e($data->title); ?>" />
                        </div>
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">slug</label>
                            <input class="form-control" type="text" name="slug" id="slug" required readonly  value="<?php echo e($data->slug); ?>" />
                        </div>
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Content</label>
                            <div style="border:1px solid #ced4da">
                                <textarea type="text" class="form-control" name="content" id="editor" rows="10"><?php echo e($data->content); ?></textarea>
                            </div>
                        </div>		
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Status</label>
                            <select class="form-control" name="status">
                                <option value="1" <?php if($data->status == 1): ?> selected="selected" <?php endif; ?>>Active</option>
                                <option value="0" <?php if($data->status == 0): ?> selected="selected" <?php endif; ?>>No Active</option>
                            </select>
                        </div>	
                        <hr class="my-4" />
                        <div class="col-md-6">
                            <a href="/webadmin/pages" class="btn btn-danger btn-sm">Cancel</a>
                            <button class="btn btn-primary btn-sm" type="submit">Update Pages</button>
                        </div>
                    </div>
                    
                </form>
            </div>

        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<script>
var code = $(".editor")[0];
var editor = CodeMirror.fromTextArea(code,{
    lineNumbers : true,
    mode: 'phyton',
});

function copyslug()
{
    var str = $("#title").val();
    str = str.replaceAll(" ", '-');
    str = str.toLowerCase();
    $("#slug").val(str);
}
</script>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH G:\exlaut-staging\resources\views/webadmin/pagesedit.blade.php ENDPATH**/ ?>

<?php $__env->startSection('style'); ?>
    <style>
        .shrbtn {
            padding: 5px;
            font-size: 40px;
            width: 50px;
            text-align: center;
            text-decoration: none;
            margin: 5px 2px;
        }

        .shrbtn:hover {
            opacity: 0.7;
        }

        /* whatsapp */
        .fa-whatsapp {
            background: #25d366;
            color: white;
        }

        /* link */
        .fa-external-link-square-alt {
            background: #1da1f2;
            color: white;
        }

        /* copy */
        .fa-copy {
            background: #ff944d;
            color: white;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="users"></i></div>
                            Partner
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main page content-->
    <?php if($status == false): ?>
        <div class="container-xl mt-4">
            <div class="row">
                <div class="offset-xl-2 col-xl-8">
                    <?php if(session('warning')): ?>
                        <div class="alert alert-warning alert-icon" role="alert">
                            <div class="alert-icon-aside">
                                <i data-feather="alert-triangle"></i>
                            </div>
                            <div class="alert-icon-content">
                                <?php echo e(session('warning')); ?>

                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(session('error')): ?>
                        <div class="alert alert-danger alert-icon" role="alert">
                            <div class="alert-icon-aside">
                                <i data-feather="alert-triangle"></i>
                            </div>
                            <div class="alert-icon-content">
                                <?php echo e(session('error')); ?>

                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(session('success')): ?>
                        <div class="alert alert-success alert-icon" role="alert">
                            <div class="alert-icon-aside">
                                <i data-feather="check-circle"></i>
                            </div>
                            <div class="alert-icon-content">
                                <?php echo e(session('success')); ?>

                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="card mb-4">
                        <div class="card-header">Partner Verification</div>
                        <div class="card-body">
                            <div class="text-center">

                                
                                <?php if(Auth::user()->agentStatus == 1): ?>
                                    <div class="logo-img mt-n5 mb-n5">
                                        <lottie-player src="https://assets10.lottiefiles.com/packages/lf20_omullrhw.json"
                                            background="transparent" speed="1" style="width: 300px; height: 300px;"
                                            loop autoplay></lottie-player>
                                    </div>
                                    <p class="small mb-3 mt-2">
                                        Our team will verify your data, the partner verification process will take a maximum
                                        of
                                        1x24 hours, please be patient we will inform you of your partner verification status
                                        via
                                        email.
                                    </p>
                                <?php elseif(Auth::user()->agentStatus == 2): ?>
                                    <div class="logo-img mt-n5 mb-n5">
                                        <lottie-player src="https://assets1.lottiefiles.com/packages/lf20_mbavm7f4.json"
                                            background="transparent" speed="1" style="width: 300px; height: 300px;"
                                            loop autoplay></lottie-player>
                                    </div>
                                    <p class="small mb-3 mt-2">
                                        Your partner request not approved, please contact us via email for more details
                                    </p>
                                <?php elseif(Auth::user()->agentStatus == 3): ?>
                                    <div class="logo-img mt-n5 mb-n5">
                                        <lottie-player src="https://assets10.lottiefiles.com/packages/lf20_jbrw3hcz.json"
                                            background="transparent" speed="1" style="width: 300px; height: 300px;"
                                            loop autoplay></lottie-player>
                                    </div>
                                    <div class="banner">
                                        <div class="alert alert-success  text-center" id="invite" role="alert"
                                            data-url=<?php echo e($role); ?>>
                                            <div class="text">
                                                Your are verified as <b><?php echo e($role); ?>,</b> invite
                                                your
                                                partner to join
                                                us!

                                                
                                            </div>
                                            <div class="icon">
                                                
                                                <?php if(auth()->user()->as_agent_referal != '' || auth()->user()->as_agent_referal != null): ?>
                                                    your referral code is <b><?php echo e($referal); ?></b>
                                                    <br>
                                                    Share this referral code to your partner
                                                    <div class="icons">
                                                        <a data-toggle="tooltip" data-placement="top"
                                                            title="Share to Whatsapp"
                                                            href="whatsapp://send?text=Hi, i invite you to convert digital assets crypto to idr, register with my referral for more info: <?php echo e(url('register') . '?ref=' . $referal); ?>"
                                                            data-action="share/whatsapp/share" target="_blank"><i
                                                                class="shrbtn fab fa-whatsapp"></i></a>

                                                        <a href="<?php echo e(url('register') . '?ref=' . $referal); ?>"
                                                            data-type="attribute" data-toggle="tooltip" data-placement="top"
                                                            title="Open Register" target="_blank"><i
                                                                class="shrbtn fas fa-external-link-square-alt"></i></a>
                                                        <a href="#" class="copy-btn ml-4" data-type="attribute"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Copy Referral Code" data-url="<?php echo e($referal); ?>"><i
                                                                class="shrbtn far fa-copy"></i></a>
                                                    </div>
                                                <?php else: ?>
                                                    <a href="<?php echo e(url('add_referal')); ?>" class="btn btn-primary">Get
                                                        Referal
                                                        Code</a>
                                                <?php endif; ?>


                                            </div>
                                        </div>

                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="container-xl px-4 mt-4">
            <div class="row mb-3">
                <div class="col-md-8">
                    <div class="h-100 card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <h3 class="mb-0">Partner</h3>

                                    <div class="d-flex">
                                        <div class="justify-content-end">
                                            <div class="icon">
                                                Your are verified as <b><?php echo e($role); ?>,</b> <br>
                                                your referral code is <?php echo '<b>' . $referal . '</b>'; ?>

                                                <br>
                                                Share this referral code to your partner
                                                <div class="icons">
                                                    <a data-toggle="tooltip" data-placement="top" title="Share to Whatsapp"
                                                        href="whatsapp://send?text=Hi, i invite you to convert digital assets crypto to idr, register with my referral for more info: <?php echo e(url('register') . '?ref=' . $referal); ?>"
                                                        data-action="share/whatsapp/share" target="_blank"><i
                                                            class="shrbtn fab fa-whatsapp"></i></a>

                                                    <a href="<?php echo e(url('register') . '?ref=' . $referal); ?>"
                                                        data-type="attribute" data-toggle="tooltip" data-placement="top"
                                                        title="Open Register" target="_blank"><i
                                                            class="shrbtn fas fa-external-link-square-alt"></i></a>
                                                    <a href="#" class="copy-btn ml-4" data-type="attribute"
                                                        data-toggle="tooltip" data-placement="top"
                                                        title="Copy Referral Code" data-url="<?php echo e($referal); ?>"><i
                                                            class="shrbtn far fa-copy"></i></a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="avatar avatar-lg"><img class="avatar-img img-fluid"
                                        src="<?php echo e(asset('public/assets/images/wallet.png')); ?>">
                                </div>
                                <div class="ms-3">
                                    <div class="fs-4 fw-500">Partner Fee</div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <h5 class="" style="font-size:14px;">Balance</h5>
                                </div>
                                <div class="col-md-6 text-end">
                                    <h4 class="total_idr" style="font-size:14px;">Rp 0</h4>
                                    <h5 class="total_bnb" style="font-size:14px;color:#ccc;">0 BNB
                                    </h5>

                                    </h4>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <a href="#" class="btn btn-sm btn-outline-primary withdraw_btn"><i
                                            data-feather="upload"></i>&nbsp;
                                        Send to wallet</a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <?php if(auth()->user()->agent_role_id == 1): ?>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Manager</th>
                                        <th scope="col" class="text-center">Partner</th>
                                        <th scope="col" class="text-end">Fee</th>
                                        <th scope="col" class="text-center">Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total = 0;
                                    $bnb = round(0, 8);
                                    $no = 1;
                                    $user_agen = '';
                                    ?>
                                    <?php $__currentLoopData = $partner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                        $user = App\Models\User::where('user_agent_referal', $rows->as_agent_referal)->get();
                                        $num = $user->count();
                                        ?>
                                        <tr>
                                            <?php
                                            if ($rows->avatar != null || $rows->avatar != '') {
                                                $avatar = url('/public/uploads/avatar/' . $this->avatar);
                                            } else {
                                                $avatar = url('/public/assets/images/profile.png');
                                            }
                                            switch ($rows->status) {
                                                case '1':
                                                    $status = '<span class="badge bg-warning">Unverified</span>';
                                                    break;
                                                case '2':
                                                    $status = '<span class="badge bg-info">Under Verification</span>';
                                                    break;
                                                case '3':
                                                    $status = '<span class="badge bg-success">Verified</span>';
                                                    break;
                                            }
                                            
                                            ?>
                                            <td class="align-middle">
                                                <img src="<?php echo e($avatar); ?>" class="avatar-img  ava-img">
                                            </td>

                                            <td class="align-middle">
                                                <?php echo e($rows->name); ?><br />
                                                <h5 class="" style="font-size:14px;color:#b3b3b3;">
                                                    <?php echo e($rows->email); ?>

                                                </h5>
                                                <?php echo $status; ?>

                                            </td>
                                            
                                            <?php if($rows->as_agent_referal != null || $rows->as_agent_referal != ''): ?>
                                                <td class="align-middle">
                                                    <table class="table table-bordered">
                                                        <?php
                                                        $users_agen = App\Models\User::where('user_agent_referal', $rows->as_agent_referal)->get();
                                                        ?>
                                                        <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php
                                                            if ($rows->avatar != null || $rows->avatar != '') {
                                                                $avatar2 = url('/public/uploads/avatar/' . $this->avatar);
                                                            } else {
                                                                $avatar2 = url('/public/assets/images/profile.png');
                                                            }
                                                            switch ($rows->status) {
                                                                case '1':
                                                                    $status2 = '<span class="badge bg-warning">Unverified</span>';
                                                                    break;
                                                                case '2':
                                                                    $status2 = '<span class="badge bg-info">Under Verification</span>';
                                                                    break;
                                                                case '3':
                                                                    $status2 = '<span class="badge bg-success">Verified</span>';
                                                                    break;
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td class=" align-middle mt-2 mb-2">
                                                                    <img src="<?php echo e($avatar2); ?>"
                                                                        class="avatar-img ava-img">
                                                                </td>
                                                                <td class="align-middle">
                                                                    <?php echo e($u->name); ?> <br>
                                                                    <h5 class=""
                                                                        style="font-size:14px;color:#b3b3b3;">
                                                                        <?php echo e($u->email); ?>

                                                                    </h5>
                                                                    <?php echo $status2; ?>

                                                                </td>
                                                                <?php if($u->as_agent_referal != null || $u->as_agent_referal != ''): ?>
                                                                    <?php
                                                                    $users = App\Models\User::where('user_agent_referal', $u->as_agent_referal)->get();
                                                                    ?>
                                                                    <td class="align-middle">
                                                                        <table class="table table-bordered">
                                                                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $uss): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php
                                                                                if ($uss->avatar != null || $uss->avatar != '') {
                                                                                    $avatar3 = url('/public/uploads/avatar/' . $this->avatar);
                                                                                } else {
                                                                                    $avatar3 = url('/public/assets/images/profile.png');
                                                                                }
                                                                                switch ($uss->status) {
                                                                                    case '1':
                                                                                        $status3 = '<span class="badge bg-warning">Unverified</span>';
                                                                                        break;
                                                                                    case '2':
                                                                                        $status3 = '<span class="badge bg-info">Under Verification</span>';
                                                                                        break;
                                                                                    case '3':
                                                                                        $status3 = '<span class="badge bg-success">Verified</span>';
                                                                                        break;
                                                                                }
                                                                                ?>
                                                                                <tr>
                                                                                    <td class="align-middle mt-2 mb-2">
                                                                                        <img src="<?php echo e($avatar3); ?>"
                                                                                            class="avatar-img ava-img">
                                                                                    </td>
                                                                                    <td class="align-middle">
                                                                                        <?php echo e($uss->name); ?> <br>
                                                                                        <h5 class=""
                                                                                            style="font-size:14px;color:#b3b3b3;">
                                                                                            <?php echo e($uss->email); ?>

                                                                                        </h5>
                                                                                        <?php echo $status3; ?>

                                                                                    </td>
                                                                                </tr>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </table>
                                                                    </td>
                                                                <?php else: ?>
                                                                    <td class="align-middle">-</td>
                                                                <?php endif; ?>
                                                            </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </table>

                                                </td>
                                            <?php else: ?>
                                                <td class="align-middle text-center">-</td>
                                            <?php endif; ?>

                                            <td class="align-middle text-end">
                                                
                                                <?php echo e(number_format($rows->idr_fee, 2)); ?>

                                                <br />
                                                <span style="font-size:14px;color:#ccc;">
                                                    
                                                    <?php echo e(number_format($rows->bnb_fee, 8)); ?>

                                                </span>
                                            </td>
                                            <td class="align-middle text-center">
                                                <a href="<?php echo e(url('agent/detail-transaction/' . $rows->id)); ?>"
                                                    class="btn btn-success btn-sm" data-type="attribute"
                                                    data-toggle="tooltip" data-placement="top" title="See details">
                                                    <i class="fas fa-list"></i>
                                                </a>
                                            </td>
                                            <?php $total += $rows->idr_fee; ?>
                                            <?php $bnb += $rows->bnb_fee; ?>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <form id="withdraw">
                                        <input type="hidden" id="total_idr" value="<?php echo e($total); ?>">
                                        <input type="hidden" id="total_bnb"
                                            value="<?php echo e(sprintf('%.8f', floatval($bnb))); ?>">
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php elseif(auth()->user()->agent_role_id == 2): ?>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Agen </th>
                                        
                                        <th scope="col">User </th>
                                        <th scope="col" class="text-end">Fee</th>
                                        <th scope="col" class="text-center">Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total = 0;
                                    $bnb = round(0, 8);
                                    $no = 1;
                                    ?>
                                    <?php $__currentLoopData = $partner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <?php
                                            if ($rows->avatar != null || $rows->avatar != '') {
                                                $avatar = url('/public/uploads/avatar/' . $this->avatar);
                                            } else {
                                                $avatar = url('/public/assets/images/profile.png');
                                            }
                                            switch ($rows->status) {
                                                case '1':
                                                    $status = '<span class="badge bg-warning">Unverified</span>';
                                                    break;
                                                case '2':
                                                    $status = '<span class="badge bg-info">Under Verification</span>';
                                                    break;
                                                case '3':
                                                    $status = '<span class="badge bg-success">Verified</span>';
                                                    break;
                                            }
                                            
                                            ?>
                                            <td class="mt-2 mb-2 align-middle">
                                                <img src="<?php echo e($avatar); ?>" class="avatar-img  ava-img">

                                            </td>

                                            <td class="align-middle">
                                                <?php echo e($rows->name); ?><br />
                                                <h5 class="" style="font-size:14px;color:#b3b3b3;">
                                                    <?php echo e($rows->email); ?>

                                                </h5>
                                                <?php echo $status; ?>

                                            </td>
                                            
                                            <?php if($rows->as_agent_referal != null || $rows->as_agent_referal != ''): ?>
                                                <td class="align-middle">
                                                    <table class="table table-bordered">
                                                        <?php
                                                        $user = App\Models\User::where('user_agent_referal', $rows->as_agent_referal)->get();
                                                        ?>
                                                        <?php  foreach ($user as $u){
                                                        
                                                        if ($rows->avatar != null || $rows->avatar != '') {
                                                            $avatar2 = url('/public/uploads/avatar/' . $this->avatar);
                                                        } else {
                                                            $avatar2 = url('/public/assets/images/profile.png');
                                                        }
                                                        switch ($rows->status) {
                                                            case '1':
                                                                $status2 = '<span class="badge bg-warning">Unverified</span>';
                                                                break;
                                                            case '2':
                                                                $status2 = '<span class="badge bg-info">Under Verification</span>';
                                                                break;
                                                            case '3':
                                                                $status2 = '<span class="badge bg-success">Verified</span>';
                                                                break;
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td class="align-middle mt-2 mb-2">
                                                                <img src="<?php echo e($avatar2); ?>"
                                                                    class="avatar-img ava-img">
                                                            </td>
                                                            <td>
                                                                <?php echo e($u->name); ?> <br>
                                                                <h5 class="" style="font-size:14px;color:#b3b3b3;">
                                                                    <?php echo e($u->email); ?>

                                                                </h5>
                                                                <?php echo $status2; ?>

                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </table>
                                                </td>
                                            <?php else: ?>
                                                <td class="align-middle">
                                                    -
                                                </td>
                                            <?php endif; ?>

                                            <td class="align-middle text-end">
                                                
                                                <?php echo e(number_format($rows->idr_fee, 2)); ?>

                                                <br />
                                                <span style="font-size:14px;color:#ccc;">
                                                    
                                                    <?php echo e(number_format($rows->bnb_fee, 8)); ?>

                                                </span>
                                            </td>
                                            <td class="align-middle text-center">
                                                <a href="<?php echo e(url('agent/detail-transaction/' . $rows->id)); ?>"
                                                    class="btn btn-success btn-sm" data-type="attribute"
                                                    data-toggle="tooltip" data-placement="top" title="See details">
                                                    <i class="fas fa-list"></i>
                                                </a>
                                            </td>
                                            <?php $total += $rows->idr_fee; ?>
                                            <?php $bnb += $rows->bnb_fee; ?>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <form id="withdraw">
                                        <input type="hidden" id="total_idr" value="<?php echo e($total); ?>">
                                        <input type="hidden" id="total_bnb"
                                            value="<?php echo e(sprintf('%.8f', floatval($bnb))); ?>">
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Phone</th>
                                        <th scope="col">Partner Fee</th>
                                        <th scope="col">Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total = 0;
                                    $bnb = round(0, 8);
                                    $no = 1;
                                    ?>
                                    <?php $__currentLoopData = $partner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <?php
                                            if ($rows->avatar != null || $rows->avatar != '') {
                                                $avatar = url('/public/uploads/avatar/' . $this->avatar);
                                            } else {
                                                $avatar = url('/public/assets/images/profile.png');
                                            }
                                            switch ($rows->status) {
                                                case '1':
                                                    $status = '<span class="badge bg-warning">Unverified</span>';
                                                    break;
                                                case '2':
                                                    $status = '<span class="badge bg-info">Under Verification</span>';
                                                    break;
                                                case '3':
                                                    $status = '<span class="badge bg-success">Verified</span>';
                                                    break;
                                            }
                                            
                                            ?>
                                            <td class="avatar avatar-lg align-middle mt-2 mb-2">
                                                <img src="<?php echo e($avatar); ?>" class="avatar-img">

                                            </td>

                                            <td class="align-middle"><?php echo e($rows->name); ?><br />
                                                <?php echo $status; ?>

                                            </td>
                                            <td class="align-middle"><?php echo e($rows->email); ?></td>
                                            <td class="align-middle"><?php echo e($rows->phone); ?></td>
                                            <td class="align-middle text-end">
                                                
                                                <?php echo e(number_format($rows->idr_fee, 2)); ?>

                                                <br />
                                                <span style="font-size:14px;color:#ccc;">
                                                    
                                                    <?php echo e(number_format($rows->bnb_fee, 8)); ?>

                                                </span>
                                            </td>
                                            <td class="align-middle text-center">
                                                <a href="<?php echo e(url('agent/detail-transaction/' . $rows->id)); ?>"
                                                    class="btn btn-success btn-sm" data-type="attribute"
                                                    data-toggle="tooltip" data-placement="top" title="See details">
                                                    <i class="fas fa-list"></i>
                                                </a>
                                            </td>
                                            <?php $total += $rows->idr_fee; ?>
                                            <?php $bnb += $rows->bnb_fee; ?>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <form id="withdraw">
                                        <input type="hidden" id="total_idr" value="<?php echo e($total); ?>">
                                        <input type="hidden" id="total_bnb"
                                            value="<?php echo e(sprintf('%.8f', floatval($bnb))); ?>">
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
                <!-- <div class="card-header">Example Card</div> -->

            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
    <script>
        $(document).ready(function() {
            $('.total_idr').html(rupiah($('#total_idr').val()));
            $('.total_bnb').html('≈ ' + $('#total_bnb').val() + ' BNB');
            console.log($('#total_bnb').val());
            $('.text').on('click', function() {
                var copytest = $(this).data('url');

            })
            $('#wa-link').on('click', function() {
                window.open($(this).data('url'), '_blank');
            })
            $('.withdraw_btn').on('click', function(e) {
                e.preventDefault();
                var data = {
                    total_idr: $('#total_idr').val(),
                    total_bnb: $('#total_bnb').val()
                }

                // console.log(data);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "<?php echo e(url('sendwallet')); ?>",
                    type: "POST",
                    data: data,
                    success: function(data) {
                        if (data.status == 201) {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'bottom-end',
                                showConfirmButton: false,
                                timer: 5000,
                                iconColor: 'white',
                                customClass: {
                                    popup: 'colored-toast'
                                },
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal
                                        .stopTimer)
                                    toast.addEventListener('mouseleave', Swal
                                        .resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            })
                            //reload window after 5 second
                            setTimeout(function() {
                                window.location.replace("<?php echo e(url('/dashboard')); ?>");
                            }, 5000);

                        } else if (data.status == 401) {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'bottom-end',
                                showConfirmButton: false,
                                timer: 5000,
                                iconColor: 'white',
                                customClass: {
                                    popup: 'colored-toast'
                                },
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal
                                        .stopTimer)
                                    toast.addEventListener('mouseleave', Swal
                                        .resumeTimer)
                                }
                            })
                            Toast.fire({
                                icon: 'warning',
                                title: data.message
                            })
                        } else {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'bottom-end',
                                showConfirmButton: false,
                                timer: 5000,
                                iconColor: 'white',
                                customClass: {
                                    popup: 'colored-toast'
                                },
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal
                                        .stopTimer)
                                    toast.addEventListener('mouseleave', Swal
                                        .resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            })
                        }

                    }
                });
            })

            $('.text').hover(function() {
                $(this).css('cursor', 'pointer');
            });

            $('.copy-btn').on('click', function() {
                var copytest = $(this).data('url');
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val(copytest).select();
                document.execCommand("copy");
                $temp.remove();
            });
            $('.copy-btn').hover(function() {
                $(this).css('cursor', 'pointer');
            });

            function rupiah(num) {
                return new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 0
                }).format(num);
            }
            // const rupiah = (number) => {
            //     return new Intl.NumberFormat("id-ID", {
            //         style: "currency",
            //         currency: "IDR"
            //     }).format(number).replace(/\D00$/, '');
            // }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/agent/done.blade.php ENDPATH**/ ?>
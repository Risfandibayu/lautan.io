<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        User
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <nav class="nav nav-borders">
        <a class="nav-link ms-0" href="/user/<?php echo e($row->id); ?>">Info Akun</a>
        <a class="nav-link" href="/user/verification/<?php echo e($row->id); ?>">Data Verifikasi</a>
        <a class="nav-link active" href="/user/assets/<?php echo e($row->id); ?>">Data Assets</a>
    </nav>
    <hr class="mt-0 mb-4" />
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <!-- <div class="card-header">Example Card</div> -->
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Coin</th>
                                <th scope="col">Name</th>
                                <th scope="col" class="text-end">Total Balance</th>
                                <th scope="col" class="text-end">Available Balance</th>
                                <th scope="col" class="text-end">Est. IDR Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php 
                                $blc = App\Models\Balance::where('user_id',$row->id)->where('currency_id',$rows->id)->where('active',1)->orderBy('id','DESC')->first();
                                if($blc) {
                                    $binance                 = NEW App\Libs\Binance;
                                    $send['url']             = '/ticker/24hr';
                                    $body['symbol']          = $rows->code;
                                    $send['body']            = $body; 
                                    $post                    = $binance->callApi($send);

                                    $balance = $blc->amount;
                                    $balanceIDR = $post->lastPrice * $blc->amount;
                                } else {
                                    $balance = '0.000000';
                                    $balanceIDR = 0;
                                }
                            ?>
                            <tr>
                                <td><img src="<?php echo e($rows->image); ?>" width="20px"></td>
                                <td><?php echo e($rows->code); ?></td>
                                <td><?php echo e($rows->name); ?></td>
                                <td class="text-end"><?php echo e($balance); ?></td>
                                <td class="text-end"><?php echo e($balance); ?></td>
                                <td class="text-end"><?php echo e($balanceIDR); ?></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\dev.lautan.io\resources\views/user/assets.blade.php ENDPATH**/ ?>
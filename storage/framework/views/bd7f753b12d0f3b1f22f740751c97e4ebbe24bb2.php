<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Lautan</title>
        <link href="<?php echo e(asset('public')); ?>/assets/css/styles.css" rel="stylesheet" />
        <link rel="shortcut icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <script data-search-pseudo-elements="" defer="" src="<?php echo e(asset('public')); ?>/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    </head>
    <body class="">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4 mt-5">
                        <div class="row justify-content-center">
                            <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11">
                                <!-- Social login form-->
                                <div class="card my-5">
                                    <div class="card-body p-5 text-center">
                                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/logo-nav.png" style="height:50px;    width: auto;"></a>
                                    </div>
                                    <hr class="my-0" />
                                    <div class="card-body p-5">
                                        <?php if(Session::has('message')): ?>
                                        <div class="alert alert-success" role="alert">
                                           <?php echo e(Session::get('message')); ?>

                                       </div>
                                        <?php endif; ?>
                                        <!-- Login form-->
                                        
                                        <form action="<?php echo e(route('reset.password.post')); ?>" method="POST">
                                            <?php echo csrf_field(); ?>
                                            <input type="hidden" name="token" value="<?php echo e($token); ?>">
                                            <!-- Form Group (email address)-->
                                            <div class="mb-3">
                                                <label class="text-gray-600 small" for="email_address">Email address</label>
                                                

                                                <input type="text" id="email_address" class="form-control form-control-solid" name="email" required autofocus>
                                                <?php if($errors->has('email')): ?>
                                                <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="mb-3">
                                                <label class="text-gray-600 small" for="password">New Password</label>
                                                

                                                <input type="password" id="password" class="form-control form-control-solid" name="password" required autofocus>
                                                <?php if($errors->has('password')): ?>
                                                    <span class="text-danger"><?php echo e($errors->first('password')); ?></span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="mb-3">
                                                <label class="text-gray-600 small" for="password-confirm">Password Confirmation</label>
                                                

                                                
                                                    <input type="password" id="password-confirm" class="form-control form-control-solid" name="password_confirmation" required autofocus>
                                                    <?php if($errors->has('password_confirmation')): ?>
                                                        <span class="text-danger"><?php echo e($errors->first('password_confirmation')); ?></span>
                                                    <?php endif; ?>
                                                
                                            </div>
                                            <!-- Form Group (password)-->
                                            <!-- Form Group (forgot password link)-->
                                            <!-- <div class="mb-3"><a class="small" href="/password/fotgot">Forgot your password?</a></div> -->
                                            <!-- Form Group (login box)-->
                                            <div class="d-flex align-items-center justify-content-between mb-0">
                                                <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer-admin mt-auto footer-dark">
                    <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">Copyright © Exlaut <?php echo e(date("Y")); ?></div>
                            <div class="col-md-6 text-md-end small">
                                <a href="#!">Privacy Policy</a>
                                ·
                                <a href="#!">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="<?php echo e(asset('public')); ?>/assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo e(asset('public')); ?>/assets/js/scripts.js"></script>
</body>

</html>


<?php /**PATH /home/lautanio/public_html/resources/views/auth/forgetPasswordLink.blade.php ENDPATH**/ ?>

<?php $__env->startSection('content'); ?>
 <section class="security-invisible"   style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;padding-top:180px;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-6 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex"><h3>Lautan.io launch new solutions For Indonesia</h3></div>
                            <div class="text">
                                Last week Lautan.io launched its service in Indonesia. Lautan.io's purpose is to explore new ways to disrupt the world of digital transaction and blockchain technology. First of all, we would. . . 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/1.png" class="img-fluid" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-6 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex"><h3>TKO is now available 
                                in Lautan.io</h3></div>
                            <div class="text">
                                Lautan.io has completed listing of TKO is now available to buy and sell here. Lautan.io now supports TKO at Lautan.io
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/2.png" class="img-fluid" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-6 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex"><h3>Manage your fund with 
                                Lautan.io</h3></div>
                            <div class="text">
                                Last week a Lautan.io launch its service Indonesia. Lautan.io purpose is to explore new ways to disrupt the world of digital transaction and blockchain technology. First of all, we would...
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/3.png" class="img-fluid" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section -->
    <style>
        .text {
            color: #5E6282;
        }

        .white {
            color: white !important;
        }

        .white h2 {
            color: white !important;
        }

        .sec-titlex {
            margin-bottom: 40px;
        }

        .sec-titlex h2 {
            font-weight: 600;
            color: #14183E;
        }

        .howtobox {
            background-color: white;
            height: 170px;
            text-align: center;
            padding-top: 70px;
            color: #1C1C1C;
        }
    </style>
    <section class="security-invisible" style="background-color:#2F80ED;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: white">
                            <div class="sec-titlex">
                                <h2 style="color: white">How To</h2>
                            </div>
                            <div class="text white" style="font-size:20px;line-height:20px;">It’s never late to learn something awesome. Now you can
simply watch this video tutorial and some information to find
out why Lautan.io rocks!</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="howtobox"> How to use Lautan.io </div>
                </div>
                <div class="col-lg-4">
                    <div class="howtobox"> How to become partner </div>
                </div>
                <div class="col-lg-4">
                    <div class="howtobox"> How to withdrawal </div>
                </div>
            </div>
            <div class="row" style="margin-top:30px">
                <div class="col-lg-4">
                    <div class="howtobox"> How to check your balance </div>
                </div>
                <div class="col-lg-4">
                    <div class="howtobox"> How to sell </div>
                </div>
                <div class="col-lg-4">
                    <div class="howtobox"> How to buy or <br> How to explore Crypto World </div>
                </div>
            </div>
        </div>
    </section>

    <section class="security-invisible" >
        <div class="container">
            <div class="row align-items-center">
               
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h2>Promotions</h2>
                            </div>
                            <h4 >Earn rewards on every crypto convert
transaction</h4>
                            <div class="text" style="font-size:20px;line-height:20px;">
                                
                                You can earn rewards simply by living your life, everyday, and everytime
converting your crypto asset through Lautan.io.</div>
                                <div class="btn-box" style="border-radius: 10px;"><button data-toggle="modal" data-target="#modal-signin" class="theme-btn-two">Claim Reward</button></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="<?php echo e(asset('public/Appway2')); ?>/images/rev/01 Promotions.png" style="max-width:500px;" alt=""> </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        .text {
            color: #5E6282;
        }

        .white {
            color: white !important;
        }

        .white h2 {
            color: white !important;
        }

        .sec-titlex {
            margin-bottom: 40px;
        }

        .sec-titlex h2 {
            font-weight: 600;
            color: #14183E;
        }

        .accordion-box .accordion {
            border-radius: 0px !important;
            border: 1px solid #C4CBD1;
            box-shadow: none !important;
        }
    </style>
    <section class="faq-section" style="margin-top:-170px">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-10 col-md-10 col-sm-10 content-column">
                    <div class="faq-content">
                        <div class="sec-titlex">
                            <center>
                                <h2>Getting Started
                                <br>
                                <span style="font-size:28px;color:#a9aaaa;">Learn all about Lautan.io, how it works, how to get started, and how to ask for help if you get stuck!</span>
                                </h2>
                            </center>
                        </div>
                        <ul class="accordion-box">
                            <li class="accordion block">
                                <div div class="acc-btn active">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Getting Started with Lautan.io</h4>
                                </div>
                                <div class="acc-content current">
                                    <div class="content">
                                        <div class="text">The basic steps to register and use Lautan.io.<br><br>

<span style="font-weight: 900;
    color: #2e302f;">Welcome!</span> <br><br>

We are glad to have you with us! If you want to learn more about using Lautan.io, you came to the right place! <br>
Lautan.io is a platform that provides a secure way where you can convert your digital asset in Indonesia and make everything become handy. We prepared a short guide to help you get started.
<br><br>

<span style="font-weight: 900;
    color: #2e302f;">STEP 1 :</span> Create an account and get verified<br><br>

Simply navigate to the Sign In button and fill in the required data. It will take you 1-2 minutes and you will need to verify by “clicking” the link on your email. When using our wallet service or you want to convert your digital assets, you need to verify your account by filling the data with your personal information. If everything is done our team will verify the KYC process accordingly. Those are required to protect both your safety and the security of our service. <br><br>

<span style="font-weight: 900;
    color: #2e302f;">STEP 2 :</span> Send digital asset to your account<br><br>

Before your first crypto withdrawal, you need to send your digital asset to your account in order to make your first deposit. Please make sure your wallet address before transferring your digital asset to your Lautan.io wallet.<br><br>

<span style="font-weight: 900;
    color: #2e302f;">STEP 3 :</span> Connect a bank account and transfer your funds / Check your available digital asset<br><br>

After you make your first deposit into your Lautan.io wallet, you can check your available digital asset in your dashboard. <br><br>

<span style="font-weight: 900;
    color: #2e302f;">STEP 4 :</span> Convert now !<br><br>

Before you make your first withdrawal, you can find the digital assets market price in our platform and choose the withdrawal channel available in Lautan.io platform. Please make sure that the name of the owner of the bank account matches the name of your Lautan account. <br><br>

<span style="font-weight: 900;
    color: #2e302f;">STEP 5 :</span> Enjoy, The funds is ready on your pocket<br><br>
    
    Finally, your money is in your pocket now. You can use all features offered by Lautan.io: you can sell, store, receive or send cryptocurrencies. You can also deposit your digital asset and withdraw your money at any time.
    <br><br>

Do you still have any questions? Use the support chat to contact our support team or send us an email <a target="_blank" href = "https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&to=welcome@Lautan.io">welcome@Lautan.io</a>
</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>What is Lautan?</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">A short description of our service.<br><br>

Lautan.io is a platform that provides a secure way where you can convert your digital asset in Indonesia and make everything become handy.<br><br>

Lautan.io offers integrated digital asset transactions direct to your pocket. Designed to make it easier for you to make transactions in Indonesia.<br><br>

We’re building a solution that will bring anyone who believes in crypto to join the digital asset revolution. The world is moving fast on to this revolution at an unprecedented pace.<br><br>

Then here we are, committed to making cryptocurrency accessible for the foreigner with multiple solutions at your fingertip.

</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>How does Lautan work?</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">How Lautan.io works, how do I register and what do I need in order to use Lautan?<br><br>

Lautan.io is designed to be user-friendly, fast and secure for everybody. Laut CryptCash helps you convert your digital assets online between two currencies in real-time with amazing ease.<br><br>

You can register in less than 3 minutes by clicking the Register button and simply following the provided instructions.<br><br>

All you need is a national document to verify your identity. Then you can start using, selling and storing cryptocurrencies!<br><br>

Take a quick look at our page about how Lautan.io works. <br><br>

</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Getting help and contacting the Lautan Support Team</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">How to get help from our support team and how to reach us.<br><br>

Here at Lautan.io, we are dedicated to providing the best possible help for our customers.<br><br>

If you have any questions about Lautan.io, simply contact our team by email  <a target="_blank" href = "https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&to=support@lautan.io">support@lautan.io</a> and we will try to answer your questions.<br><br>

Limited support is available in the evenings and during the weekends.<br><br>

<span style="font-weight: 900;
    color: #2e302f;">***</span><br>
<span style="font-weight: 900;
    color: #2e302f;">Security note :</span> Lautan.io does not offer phone support at the moment. If someone contacts you via phone claiming that they are part of our support team, please let us know about it immediately.<br>
<span style="font-weight: 900;
    color: #2e302f;">Lautan will never ask you to share usernames or passwords!</span>
</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block" style="margin-bottom:50px;">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Is Lautan a licensed service?</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">A brief description of our licenses.<br><br>

Yes, Lautan.io is a licensed service!  <br><br>

The company Lautan.io was established in the Republic of Indonesia, Lautan.io has been issued an operating license by the Ministry of Communication and Information Technology Republic Indonesia.
</div>
                                    </div>
                                </div>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/land/news.blade.php ENDPATH**/ ?>
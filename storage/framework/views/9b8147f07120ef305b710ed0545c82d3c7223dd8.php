
<?php $__env->startSection('content'); ?>
<!-- <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Akun
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header> -->
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <nav class="nav nav-borders">
        <a class="nav-link active ms-0" href="<?php echo e(url('account')); ?>">Account</a>
        <!-- <a class="nav-link" href="/tokocrypto">Tokocrypto API</a> -->
        <a class="nav-link" href="<?php echo e(url('password')); ?>">Change Password</a>
    </nav>
    <hr class="mt-0 mb-4" />
    <div class="row">
        <div class="col-xl-4">
            <!-- Profile picture card-->
            <div class="card mb-4 mb-xl-0">
                <div class="card-header">Photo</div>
                <form class="card-body text-center" action="<?php echo e(url('account')); ?>" method="POST" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <!-- Profile picture image-->
                    <img class="img-account-profile rounded-circle mb-2" src="public/assets/images/profile.png" style="width: auto;"/>
                    <!-- Profile picture help block-->
                    <div class="small font-italic text-muted">JPG or PNG max 2 MB</div>
                    <div class="mb-3">
                        <input class="form-control form-control-sm" id="formFileSm" type="file">
                    </div>
                    <!-- Profile picture upload button-->
                    <button class="btn btn-primary" type="submit">Upload</button>
                </form>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="card mb-4">
                <div class="card-header">Account</div>
                <div class="card-body">
                    <p class="small text-muted mb-0">Name</p>
                    <h5><?php echo e(Auth::user()->name); ?></h5>
                    <hr class="my-3" />
                    <p class="small text-muted mb-0">Email</p>
                    <h5><?php echo e(Auth::user()->email); ?></h5>
                    <hr class="my-3" />
                    <p class="small text-muted mb-0">Phone</p>
                    <h5><?php echo e(Auth::user()->phone); ?></h5>
                    <hr class="my-3" />
                    <?php if(Auth::user()->status == 1): ?>
                    <p class="small text-muted mb-0">Status</p>
                    <h5><span class="badge bg-warning">Unverified</span></h5>
                    <p class="small text-muted mb-0">*it is recommended to verify to be able to unlock all features</p>
                    <?php elseif(Auth::user()->status == 2): ?>
                    <p class="small text-muted mb-0">Status</p>
                    <h5><span class="badge bg-info">Under Verification</span></h5>
                    <p class="small text-muted mb-0">*the verification process takes a maximum of 1x24 hours</p>
                    <?php elseif(Auth::user()->status == 3): ?>
                    <p class="small text-muted mb-0">Status</p>
                    <h5><span class="badge bg-success">Verified</span></h5>
                    <?php endif; ?>
                </div>
                <div class="card-footer">
                    <?php if(Auth::user()->status == 1): ?>
                    <a href="<?php echo e(url('verification')); ?>" class="btn btn-success">Verify Now</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/account/index.blade.php ENDPATH**/ ?>
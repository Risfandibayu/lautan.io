<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="users"></i></div>
                        Partner
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl">

    <div class="card p-3">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at eleifend augue. Pellentesque dignissim bibendum dolor, sed euismod ipsum fringilla a. Mauris vestibulum purus purus. Nulla quis posuere nunc, eget sagittis ante. Nulla vel volutpat ante. Vestibulum gravida bibendum nisi. <br><br>Praesent a est tempor, gravida lacus et, scelerisque nibh. In in nibh sed leo lacinia semper ut non ante. Mauris vestibulum auctor sem ac malesuada. Duis commodo tincidunt porttitor. Nullam vitae vestibulum quam. Donec in interdum nunc, a sollicitudin risus. Donec maximus mauris quis velit egestas, eget venenatis erat cursus.</p>
        <a href="#" class="btn-req btn btn-primary">I want to be an Partner</a>
    </div>
</div>
<!-- Modal Confirm -->
<div class="modal fade" id="reqModal" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="modal-dialog" role="document" method="POST" action="/agent/req" enctype="multipart/form-data">
        <?php echo e(csrf_field()); ?>

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Partner Request</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at eleifend augue. Pellentesque dignissim bibendum dolor, sed euismod ipsum fringilla a. Mauris vestibulum purus purus. Nulla quis posuere nunc, eget sagittis ante. Nulla vel volutpat ante. Vestibulum gravida bibendum nisi.</p>
            </div>
            <div class="modal-footer">
                <button type="submit" name="save" value="1" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script type="text/javascript">
    $('.btn-req').click(function(){
      $('#reqModal').modal('show');        
    });
</script>
<?php $__env->stopPush(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\dev.lautan.io\resources\views/agent/landing.blade.php ENDPATH**/ ?>
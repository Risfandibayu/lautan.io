<?php $__env->startSection('content'); ?>
 <section class="security-invisible"   style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;padding-top:180px;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-6 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex"><h3>Lautan.io launch new 
                                solutions For Indonesia</h3></div>
                            <div class="text">
                                Last week a Lautan.io launch its service Indonesia. Lautan.io purpose is to explore new ways to disrupt the world of digital transaction and blockchain technology. First of all, we would...
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/1.png" class="img-fluid" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-6 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex"><h3>TKO is now available 
                                in Lautan.io</h3></div>
                            <div class="text">
                                Lautan.io has completed listing of TKO is now available to buy and sell here. Lautan.io now supports TKO at Lautan.io
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/2.png" class="img-fluid" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6 col-sm-6 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex"><h3>Manage your fund with 
                                Lautan.io</h3></div>
                            <div class="text">
                                Last week a Lautan.io launch its service Indonesia. Lautan.io purpose is to explore new ways to disrupt the world of digital transaction and blockchain technology. First of all, we would...
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/3.png" class="img-fluid" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section -->
    <style>
        .text {
            color: #5E6282;
        }

        .white {
            color: white !important;
        }

        .white h2 {
            color: white !important;
        }

        .sec-titlex {
            margin-bottom: 40px;
        }

        .sec-titlex h2 {
            font-weight: 600;
            color: #14183E;
        }

        .howtobox {
            background-color: white;
            height: 170px;
            text-align: center;
            padding-top: 70px;
            color: #1C1C1C;
        }
    </style>
    <section class="security-invisible" style="background-color:#2F80ED;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: white">
                            <div class="sec-titlex">
                                <h2 style="color: white">How To</h2>
                            </div>
                            <div class="text white">Lautan.io Video Tutorials and tips</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="howtobox"> How to use Lautan.io </div>
                </div>
                <div class="col-lg-4">
                    <div class="howtobox"> How to become partner </div>
                </div>
                <div class="col-lg-4">
                    <div class="howtobox"> How to withdrawal </div>
                </div>
            </div>
            <div class="row" style="margin-top:30px">
                <div class="col-lg-4">
                    <div class="howtobox"> How to check your balance </div>
                </div>
                <div class="col-lg-4">
                    <div class="howtobox"> How to sell </div>
                </div>
                <div class="col-lg-4">
                    <div class="howtobox"> How to buy or <br> How to explore Crypto World </div>
                </div>
            </div>
        </div>
    </section>

    <section class="security-invisible" style="background-color:#F8FBFF;">
        <div class="container">
            <div class="row align-items-center">
               
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h2>Promotions</h2>
                            </div>
                            <h4>Play, win and create your own discounts</h4>
                            <div class="text">
                                
                                At Lautan.io we believe that unity is everything. That’s why we’re giving back to the crypto community by creating technology that’s so easy to use that we’re using it ourselves.We are also using this technology to give back to our users by providing an exciting, and profitable rewards system that gives them an undeniably compelling reason to make Lautan.io their first choice cryptocurrency Instant Cashout.
                                Claim Reward</div>
                                <div class="btn-box" style="border-radius: 10px;"><a href="#" class="theme-btn-two">Claim Reward</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="https://lautan.s3.amazonaws.com/webcontent/media_n26rxt.png" alt=""> </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        .text {
            color: #5E6282;
        }

        .white {
            color: white !important;
        }

        .white h2 {
            color: white !important;
        }

        .sec-titlex {
            margin-bottom: 40px;
        }

        .sec-titlex h2 {
            font-weight: 600;
            color: #14183E;
        }

        .accordion-box .accordion {
            border-radius: 0px !important;
            border: 1px solid #C4CBD1;
            box-shadow: none !important;
        }
    </style>
    <section class="faq-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12 content-column">
                    <div class="faq-content">
                        <div class="sec-titlex">
                            <center>
                                <h2>Hai, apa yang bisa kami bantu?</h2>
                            </center>
                        </div>
                        <ul class="accordion-box">
                            <li class="accordion block">
                                <div div class="acc-btn active">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Getting started</h4>
                                </div>
                                <div class="acc-content current">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Account</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Identification verification</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Instant Cashout</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Funding</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Wallet services</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Limits and fees</h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Security </h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block">
                                <div class="acc-btn">
                                    <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                    <h4>Promotions </h4>
                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\dev.lautan.io\resources\views/land/news.blade.php ENDPATH**/ ?>

<?php $__env->startSection('content'); ?>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="users"></i></div>
                            Partner
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main page content-->
    <?php if($status): ?>
        <div class="container-xl mt-4">
            <div class="row">
                <div class="offset-xl-2 col-xl-8">
                    <?php if(session('warning')): ?>
                        <div class="alert alert-warning alert-icon" role="alert">
                            <div class="alert-icon-aside">
                                <i data-feather="alert-triangle"></i>
                            </div>
                            <div class="alert-icon-content">
                                <?php echo e(session('warning')); ?>

                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(session('error')): ?>
                        <div class="alert alert-danger alert-icon" role="alert">
                            <div class="alert-icon-aside">
                                <i data-feather="alert-triangle"></i>
                            </div>
                            <div class="alert-icon-content">
                                <?php echo e(session('error')); ?>

                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(session('success')): ?>
                        <div class="alert alert-success alert-icon" role="alert">
                            <div class="alert-icon-aside">
                                <i data-feather="check-circle"></i>
                            </div>
                            <div class="alert-icon-content">
                                <?php echo e(session('success')); ?>

                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="card mb-4">
                        <div class="card-header">Partner Verification</div>
                        <div class="card-body">
                            <div class="text-center">

                                
                                <?php if(Auth::user()->agentStatus == 1): ?>
                                    <div class="logo-img mt-n5 mb-n5">
                                        <lottie-player src="https://assets10.lottiefiles.com/packages/lf20_omullrhw.json"
                                            background="transparent" speed="1" style="width: 300px; height: 300px;" loop
                                            autoplay></lottie-player>
                                    </div>
                                    <p class="small mb-3 mt-2">
                                        Our team will verify your data, the partner verification process will take a maximum
                                        of
                                        1x24 hours, please be patient we will inform you of your partner verification status
                                        via
                                        email.
                                    </p>
                                <?php elseif(Auth::user()->agentStatus == 2): ?>
                                    <div class="logo-img mt-n5 mb-n5">
                                        <lottie-player src="https://assets1.lottiefiles.com/packages/lf20_mbavm7f4.json"
                                            background="transparent" speed="1" style="width: 300px; height: 300px;" loop
                                            autoplay></lottie-player>
                                    </div>
                                    <p class="small mb-3 mt-2">
                                        Your partner request not approved, please contact us via email for more details
                                    </p>
                                <?php elseif(Auth::user()->agentStatus == 3): ?>
                                    <div class="logo-img mt-n5 mb-n5">
                                        <lottie-player src="https://assets10.lottiefiles.com/packages/lf20_jbrw3hcz.json"
                                            background="transparent" speed="1" style="width: 300px; height: 300px;" loop
                                            autoplay></lottie-player>
                                    </div>
                                    <div class="banner">
                                        <div class="alert alert-success  text-center" id="invite" role="alert"
                                            data-url=<?php echo e($role); ?>>
                                            <div class="text">
                                                Your are verified as <b><?php echo e($role); ?>,</b> invite
                                                your
                                                partner to join
                                                us!
                                            </div>
                                            <div class="icon">
                                                Share your referral
                                                <br>
                                                <div class="share">
                                                    <span>Share</span>
                                                    <nav>
                                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                                        <a href="#"><i class="fa fa-google"></i></a>
                                                        <a href="#"><i class="fa fa-github"></i></a>
                                                    </nav>
                                                </div>
                                                
                                                
                                            </div>
                                        </div>

                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="container-xl px-4 mt-4">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-auto">
                            <h3 class="mb-0">Partner</h3>

                            <div class="d-flex">
                                <div class="justify-content-end">
                                    Share your referral :
                                    <br>
                                    <div class="share-div">
                                        <div class="share">
                                            <span>Share</span>
                                            <nav>
                                                <a href="#"><i class="fab fa-whatsapp"></i></a>
                                                <a href="#"><i class="fas fa-link"></i></a>
                                                <a href="#"><i class="fas fa-copy"></i></a>
                                            </nav>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- <div class="card-header">Example Card</div> -->
                <div class="card-body">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $partner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="avatar avatar-lg align-middle mt-2 mb-2"><img
                                            src="<?php echo e($rows->getAvatar()); ?>" class="avatar-img"></td>
                                    <td class="align-middle"><?php echo e($rows->name); ?></td>
                                    <td class="align-middle"><?php echo e($rows->email); ?></td>
                                    <td class="align-middle"><?php echo e($rows->phone); ?></td>
                                    <td class="align-middle"><?php echo $rows->getStatusLabel(); ?></td>

                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
    <script>
        $(document).ready(function() {
            $('.text').on('click', function() {
                var copytest = $(this).data('url');

            })
            $('.text').hover(function() {
                $(this).css('cursor', 'pointer');
            });

            $('.copy-btn').on('click', function() {
                var copytest = $(this).data('url');
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val(copytest).select();
                document.execCommand("copy");
                $temp.remove();
            });
            $('.copy-btn').hover(function() {
                $(this).css('cursor', 'pointer');
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/agent/done.blade.php ENDPATH**/ ?>
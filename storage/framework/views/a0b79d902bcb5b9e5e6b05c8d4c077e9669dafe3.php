
<?php $__env->startSection('content'); ?>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="users"></i></div>
                            Partner Request
                        </h1>
                    </div>
                    <!-- <div class="col-12 col-xl-auto mb-3"><a href="/user/create" class="btn btn-sm btn-light"><i data-feather="plus"></i>&nbsp; Tambah User</a></div> -->
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-xl px-4 mt-4">
        <div class="card">
            <!-- <div class="card-header">Example Card</div> -->
            <div class="card-body">
                <?php if(count($row)): ?>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col" style="width:130px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="avatar avatar-lg align-middle mt-2 mb-2"><img
                                            src="<?php echo e($rows->getAvatar()); ?>" class="avatar-img"></td>
                                    <td class="align-middle"><?php echo e($rows->name); ?></td>
                                    <td class="align-middle"><?php echo e($rows->email); ?></td>
                                    <td class="align-middle"><?php echo e($rows->phone); ?></td>
                                    <td class="text-right align-middle">
                                        <div class="btn-group">
                                            <a href="<?php echo e(url('user/verification') . '/' . $rows->id); ?>"
                                                class="btn btn-primary btn-sm"><i data-feather="eye"></i></a>
                                            <a href="<?php echo e($rows->fileKuisioner); ?>" target="_blank"
                                                class="btn btn-danger btn-sm"><i class="fas fa-file-pdf"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <p class="text-center"><small>Tidak ada data request partner<small></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <form method="POST" action="/agent/del" class="modal fade" id="alertModal" tabindex="-1" role="dialog"
        aria-labelledby="alertModalLabel" aria-hidden="true">
        <?php echo csrf_field(); ?>
        <input name="_method" type="hidden" value="DELETE">
        <input name="id" type="hidden" id="user-id">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                    Yakin akan menonaktifkan partner <span id="user-name"></span> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
    <script type="text/javascript">
        $('#alertModal').on('show.bs.modal', function(event) {
            var modal = $(this);
            var button = $(event.relatedTarget);
            $('#user-id').val(button.data('id'));
            $('#user-name').html(button.data('name'));
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/agent/req.blade.php ENDPATH**/ ?>

<?php $__env->startSection('content'); ?>
    <!-- <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <div class="container-xl px-4">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="page-header-content">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div class="row align-items-center justify-content-between pt-3">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <div class="col-auto mb-3">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <h1 class="page-header-title">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="page-header-icon"><i data-feather="download"></i></div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Penarikan
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </h1>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </header> -->
    <!-- Main page content-->
    <div class="container-xl mt-4">
        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                <?php if(session('warning')): ?>
                    <div class="alert alert-warning alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('warning')); ?>

                        </div>
                    </div>
                <?php endif; ?>

                <?php if(session('error')): ?>
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('error')); ?>

                        </div>
                    </div>
                <?php endif; ?>

                <?php if(session('success')): ?>
                    <div class="card alert alert-success alert-icon" role="alert">
                        <div class="card-header text-center h3">
                            <?php echo session('success'); ?>

                        </div>
                        <div class="card-body">
                            <div class="form">
                                <div class="row">
                                    <div class="col-md-3">
                                        Wallet Address
                                    </div>
                                    <div class="col-md-9">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control form-control-solid"
                                                value="<?php echo e(session()->get('currency_address')); ?>" id="wallet_address">
                                            <button class="btn btn-secondary" type="button" id="copy">Copy</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        Amount
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control form-control-solid" name="amount"
                                            value="<?php echo e(session()->get('amount')); ?>">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="alert alert-warning alert-icon" role="alert">
                                        <div class="alert-icon-aside">
                                            <i data-feather="alert-triangle"></i>
                                        </div>
                                        <div class="alert-icon-content">
                                            One sender address for one time transaction
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <a href="https://metamask.io/download.html" target="_blank" class="btn btn-light"
                                        style="padding: 5px 10px;">
                                        <img src="<?php echo e(asset('public')); ?>/assets/images/metamask.png"
                                            style="width: 40px;margin-right:10px;border-radius: 5px;">
                                        Open MetaMask
                                    </a>
                                </div>
                            </div>


                        </div>
                    </div>
                <?php endif; ?>

                <div class="card mb-4">
                    <div class="card-header">Top Up</div>
                    <div class="card-body">
                        <form method="POST" action="<?php echo e(url('deposit')); ?>">
                            <?php echo csrf_field(); ?>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Assets</label>
                                    <select class="form-control form-control-solid select2" name="code" required>
                                        <option>Select</option>
                                        <?php $__currentLoopData = $coin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($rows->code); ?>"
                                                <?php if($rows->code == request('coin')): ?> selected <?php endif; ?>><?php echo e($rows->name); ?> -
                                                <?php echo e(str_replace('BIDR', ' ', $rows->code)); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Sender Address</label>
                                    <input class="form-control form-control-solid" type="text"
                                        placeholder="Input Sender Address" name="sender" value="" required />
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Amount</label>
                                    <input class="form-control form-control-solid" type="text" placeholder="Input Amount"
                                        name="amount" value="" required />
                                </div>

                            </div>


                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-2">
                                <button class="btn btn-primary mt-4" type="submit">Submit</button>
                            </div>
                            <div class="col-md-10">
                                <div class="alert alert-warning alert-icon" role="alert">
                                    <div class="alert-icon-aside">
                                        <i data-feather="alert-triangle"></i>
                                    </div>
                                    <div class="alert-icon-content">
                                        We only accept sender address from Hot Wallet only <br>
                                        Such as : Metamask, Trust Wallet, etc
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script>
        $('#copy').on('click', function(e) {
            var copytest = $('#wallet_address').val();
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(copytest).select();
            document.execCommand("copy");
            $temp.remove();
            $(this).html('Copied to clipboard');
        })

        function myFunction() {
            /* Get the text field */
            var copyText = document.getElementById("myInput");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value);

            /* Alert the copied text */
            alert("Copied the text: " + copyText.value);


        };
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/deposit/index.blade.php ENDPATH**/ ?>
<?php echo $__env->make('emails.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<h1>Forget Password Email</h1>
   
You can reset password from bellow link:
<a href="<?php echo e(route('reset.password.get', $token)); ?>">Reset Password</a>
<?php echo $__env->make('emails.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/public_html/resources/views/emails/forgetPassword.blade.php ENDPATH**/ ?>
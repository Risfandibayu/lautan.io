
<?php $__env->startSection('js'); ?>
    <script>
        $(".select2-single").select2({
            minimumResultsForSearch: Infinity
        });
        var change = "";
        var changebox = "";
        $(document).on("change", "#idrleft_select", function() {
            change = "idrleft_select";
            hitung();
        });
        $(document).on("change", "#idrright_select", function() {
            change = "idrright_select";
            hitung();
        });
        $(document).on("change", "#curleft_select", function() {
            change = "curleft_select";
            hitung();
        });
        $(document).on("change", "#curright_select", function() {
            change = "curright_select";
            hitung();
        });
        $("#idrleft_price").keyup(function() {
            change = "idrleft_price";
            hitung();
        });
        $("#idrright_price").keyup(function() {
            change = "idrright_price";
            hitung();
        });
        $("#curleft_price").keyup(function() {
            change = "curleft_price";
            hitung();
        });
        $("#curright_price").keyup(function() {
            change = "curright_price";
            hitung();
        });

        $(document).on("change", "#changebox", function() {
            changebox = $('#changebox').val();
            if (changebox == 'BUY') {
                $('#idrleft').show();
                $('#idrright').hide();
                $('#curleft').hide();
                $('#curright').show();
                document.getElementById("btnnya").innerHTML =
                    '<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-signin">BUY</button>';
                hitung();
            } else {
                $('#idrleft').hide();
                $('#idrright').show();
                $('#curleft').show();
                $('#curright').hide();
                document.getElementById("btnnya").innerHTML =
                    '<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-signin">WITHDRAW</button>';
                hitung();
            }
        });
        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            }
        });

        function formatNumber(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        }

        function formatr(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "")
        }

        function formatCurrency(input, blur) {
            // appends $ to value, validates decimal side
            // and puts cursor back in right position.

            // get input value
            var input_val = input.val();

            // don't validate empty input
            if (input_val === "") {
                return;
            }

            // original length
            var original_len = input_val.length;

            // initial caret position 
            var caret_pos = input.prop("selectionStart");

            // check for decimal
            if (input_val.indexOf(",") >= 0) {

                // get position of first decimal
                // this prevents multiple decimals from
                // being entered
                var decimal_pos = input_val.indexOf(",");

                // split number by decimal point
                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                // add commas to left side of number
                left_side = formatNumber(left_side);

                // validate right side
                right_side = formatNumber(right_side);

                // On blur make sure 2 numbers after decimal
                if (blur === "blur") {
                    right_side += "";
                }

                // Limit decimal to only 2 digits
                right_side = right_side.substring(0, 2);

                // join number by .
                input_val = "" + left_side + "" + right_side;

            } else {
                // no decimal entered
                // add commas to number
                // remove all non-digits
                input_val = formatNumber(input_val);
                input_val = "" + input_val;

                // final formatting
                if (blur === "blur") {
                    input_val += "";
                }
            }

            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }

        function hitung() {
            changebox = $('#changebox').val();
            var idrleft_price = $('#idrleft_price').val();
            var idrleft_select = $('#idrleft_select').val();
            var curright_price = $('#curright_price').val();
            var curright_select = $('#curright_select').val();
            var idrright_price = $('#idrright_price').val();
            var idrright_select = $('#idrright_select').val();
            var curleft_price = $('#curleft_price').val();
            var curleft_select = $('#curleft_select').val();

            // console.log($('#curright_price').val());

            $.ajax({
                type: "GET",
                url: "/calculate",
                data: "idrleft_price=" + formatr(idrleft_price) + "&idrleft_select=" + idrleft_select +
                    "&curright_price=" + curright_price + "&curright_select=" + curright_select +
                    "&idrright_price=" + formatr(idrright_price) + "&idrright_select=" + idrright_select +
                    "&curleft_price=" + curleft_price + "&curleft_select=" + curleft_select + "&change=" + change +
                    "&changebox=" + changebox,
                success: function(result) {
                    if (result) {
                        if (change == 'idrleft_price' || change == 'idrleft_select') {
                            $('#curright_price').val(result);
                        } else if (change == 'curright_price' || change == 'curright_select') {
                            $('#idrleft_price').val(formatNumber(result));
                        } else if (change == 'idrright_price' || change == 'idrright_select') {
                            $('#curleft_price').val(result);
                        } else if (change == 'curleft_price' || change == 'curleft_select') {
                            $('#idrright_price').val(formatNumber(result));
                        }
                    }
                },

            });


        }
    </script>
    <script src="https://cdn.jsdelivr.net/gh/coinponent/coinponent@1.2.6/dist/coinponent.js"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section class="banner-style-ten">
        <div class="image-layer"
            style="margin-top:-150px;background-image:url('https://lautan.s3.amazonaws.com/webcontent/media_8iPKI9.png');background-repeat: no-repeat;background-position: right 0px top -80px;background-size:45%">
        </div>
        <div class="container" style="margin-top:-100px;">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex">
                                <h4 style="color: #F2994A;font-size:20px">DIGITAL TRANSACTION AGGREGATOR</h4>
                            </div>
                            <div class="sec-titlex">
                                <h2 style="font-size:40px;">The easy way to convert your digital asset in Indonesia</h2>
                            </div>
                            <br>
                            <div class="text" style="font-size: 20px;line-height:20px;">Platform that provides a secure
                                way where you can convert your digital asset in Indonesia and make everything become handy.
                                <br>
                                <br>
                                Lautan.io offers integrated digital asset transactions direct to your pocket. Designed to
                                make it easier for you to make transactions in Indonesia.
                            </div>
                            <!-- <div><button class="btn" style="background-color:#2F80ED;color:white;padding:10px 30px 10px 30px">Find out more</button></div> -->
                            <div class="btn-box" style="border-radius: 10px;"><button data-toggle="modal"
                                    data-target="#modal-signin" class="theme-btn-two">Find Out More</button></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms"
                                data-wow-duration="1500ms"> <img
                                    src="<?php echo e(asset('public/Appway2')); ?>/images/rev/01 Indonesia's Forefront.png"
                                    alt="" style="max-width: 500px;"> </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner-section end -->


    <!-- clients-section -->

    <section class="clients-section home-12"
        style="margin-top:-70px;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%">
        <div class="container">
            <div class="sec-titlex">
                <h2 style="font-size:40px;">Laut CryptCash
                </h2>
                <h4>Convert your digital asset now!</h4>
            </div>
            <div style="margin-bottom:50px;font-size:20px;line-height:20px;">Laut CryptCash helps you convert your digital
                assets online between two currencies in real-time with amazing ease.</div>

            <div class="row">
                <div class="col-md-12">

                    <div>
                        <div class="row">
                            <div class="col-md-5" id="curright">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="0" value="0.00256"
                                                id="curright_price" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="curright_select"
                                                style="width:100%;height:40px;">
                                                <option value="ETHBIDR">ETHBIDR</option>
                                                <option value="USDTBIDR">USDTBIDR</option>
                                                <option value="BNBBIDR">BNBBIDR</option>
                                                <option value="DOGEBIDR">DOGEBIDR</option>
                                                <option value="BUSDBIDR">BUSDBIDR</option>
                                                <option value="DOTBIDR">DOTBIDR</option>
                                                <option value="TKOBIDR">TKOBIDR</option>
                                                <option value="SXPBIDR">SXPBIDR</option>
                                                <option value="LAUTAN">LAUTAN</option>
                                                <option value="H00002">H00002</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--<div class="col-md-5" id="idrleft">-->
                            <!--    <div class="row">-->
                            <!--        <div class="col-md-6">-->
                            <!--            <div class="form-group">-->
                            <!--                <input type="text" class="form-control" placeholder="0" value="100000" id="idrleft_price" style="width:100%">-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--        <div class="col-md-6">-->
                            <!--            <div class="form-group">-->
                            <!--                <select class="form-control" id="idrleft_select" style="width:100%;height:40px;">-->
                            <!--                    <option value="IDR">IDR</option>-->
                            <!--                </select>-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="col-md-5" id="curleft" style="display:none">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="0" value="0.00256"
                                                id="curleft_price" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="curleft_select"
                                                style="width:100%;height:40px;">
                                                <option value="ETHBIDR">ETHBIDR</option>
                                                <option value="USDTBIDR">USDTBIDR</option>
                                                <option value="BNBBIDR">BNBBIDR</option>
                                                <option value="DOGEBIDR">DOGEBIDR</option>
                                                <option value="BUSDBIDR">BUSDBIDR</option>
                                                <option value="DOTBIDR">DOTBIDR</option>
                                                <option value="TKOBIDR">TKOBIDR</option>
                                                <option value="SXPBIDR">SXPBIDR</option>
                                                <option value="LAUTAN">LAUTAN</option>
                                                <option value="H00002">H00002</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" hidden>
                                <div class="form-group">
                                    <select class="form-control select2-single" id="changebox">
                                        <option value="BUY">BUY</option>
                                        <option value="SELL">SELL</option>
                                    </select>
                                </div>
                            </div>
                            <!--<div class="col-md-5" id="curright">-->
                            <!--    <div class="row">-->
                            <!--        <div class="col-md-6">-->
                            <!--            <div class="form-group">-->
                            <!--                <input type="text" class="form-control" placeholder="0" value="0.00256" id="curright_price" style="width:100%">-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--        <div class="col-md-6">-->
                            <!--            <div class="form-group">-->
                            <!--                <select class="form-control" id="curright_select" style="width:100%;height:40px;">-->
                            <!--                    <option value="ETHBIDR">ETHBIDR</option>-->
                            <!--                    <option value="USDTBIDR">USDTBIDR</option>-->
                            <!--                    <option value="BNBBIDR">BNBBIDR</option>-->
                            <!--                    <option value="DOGEBIDR">DOGEBIDR</option>-->
                            <!--                    <option value="BUSDBIDR">BUSDBIDR</option>-->
                            <!--                    <option value="DOTBIDR">DOTBIDR</option>-->
                            <!--                    <option value="TKOBIDR">TKOBIDR</option>-->
                            <!--                    <option value="SXPBIDR">SXPBIDR</option>-->
                            <!--                    <option value="LAUTAN">LAUTAN</option>-->
                            <!--                    <option value="H00002">H00002</option>-->
                            <!--                </select>-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="col-md-5" id="idrleft">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control"
                                                pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency"
                                                placeholder="0" value="100.000" id="idrleft_price" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="idrleft_select"
                                                style="width:100%;height:40px;">
                                                <option value="IDR">IDR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5" id="idrright" style="display:none">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="0" value="100000"
                                                id="idrright_price" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="idrright_select"
                                                style="width:100%;height:40px;">
                                                <option value="IDR">IDR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="btnnya">
                                <button type="button" class="btn btn-primary btn-block" data-toggle="modal"
                                    data-target="#modal-signin">Convert Now</button>
                            </div>
                        </div>
                        <!--<coin-ponent></coin-ponent>-->
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- main-footer end -->
    <section class="security-invisible"
        style=" margin-top:-50px; background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/titik.png),url(<?php echo e(asset('public/Appway2')); ?>/images/titik2.png);background-repeat: no-repeat,no-repeat;background-position: left 0px top 120px,right 0px top 576px;">
        <div class="container">
            <div class="row align-items-center">


                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInLeft" data-wow-delay="00ms"
                                data-wow-duration="1500ms"> <img
                                    src="<?php echo e(asset('public/Appway2')); ?>/images/rev/01 Digital asset interchange.png"
                                    alt="" style="max-width: 500px;"> </figure>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex">
                                <br>
                                <h4 style="color: #F2994A;font-size:20px">

                                    Easy to use | Reliable | For Everyone To Everyone
                                </h4>
                            </div>

                            <div class="sec-titlex">
                                <h2 style="font-size:40px;">The 1st digital asset converter platform</h2>
                            </div>
                            <br>
                            <div class="text" style="font-size: 20px;line-height:20px;">Designed for tourists and
                                foreign traders to convert their digital assets in Indonesia by using Lautan.io.
                            </div>
                            <!-- <div><button class="btn" style="background-color:#2F80ED;color:white;padding:10px 30px 10px 30px">Find out more</button></div> -->

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="clients-section home-12" style="margin-top:-80px;">
        <div class="container">
            <div class="sec-titlex" style="text-align: center;">
                <h2 style="font-size:40px;">Get started in 5 steps</h2>
            </div>
            <div class="row" style="margin-top: 80px;line-height:20px;">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-6 col-lg-3 col-md-6 col-sm-6" style="padding:10px; ">
                            <div align="center"
                                style="height: 190px;border:1px solid #EFEFEF;padding:45px 25px 25px 25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);background-color: white;">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/login_icon.png"
                                    style="margin-bottom:5px;width:40px;">
                                <div style="font-size: 18px;font-weight:700;color:#1C1C1C">1<br> Create an account and get
                                    verified</div>

                            </div>
                        </div>

                        <div class="col-6 col-lg-3 col-md-6 col-sm-6" style="padding:10px">
                            <div align="center"
                                style="height: 190px;border:1px solid #EFEFEF;padding:45px 25px 25px 25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);background-color: white;">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/send_icon.png"
                                    style="margin-bottom:15px;width:40px;">
                                <div style="font-size: 18px;font-weight:700;color:#1C1C1C">2<br> Send digital asset to your
                                    account</div>

                            </div>
                        </div>
                        <div class="col-6 col-lg-3 col-md-6 col-sm-6" style="padding:10px">
                            <div align="center"
                                style="height: 190px;border:1px solid #EFEFEF;padding:45px 25px 25px 25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);background-color: white;">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/check_icon.png"
                                    style="margin-bottom:15px;width:40px;">
                                <div style="font-size: 18px;font-weight:700;color:#1C1C1C">3<br> Check your available
                                    digital asset</div>

                            </div>
                        </div>
                        <div class="col-6 col-lg-3 col-md-6 col-sm-6" style="padding:10px">
                            <div align="center"
                                style="height: 190px;border:1px solid #EFEFEF;padding:45px 25px 25px 25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);background-color: white;">
                                <img src="<?php echo e(asset('public/Appway2')); ?>/images/convert_icon.png"
                                    style="margin-bottom:15px;width:45px;">
                                <div style="font-size: 18px;font-weight:700;color:#1C1C1C">4<br> Convert now!</div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3" style="text-align: center">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding:10px">
                        <div
                            style="height: 190px;border:1px solid #EFEFEF;padding:30px 25px 25px 25px;margin-left: -14px;margin-right: -14px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);background-color: white;">
                            <img src="<?php echo e(asset('public/Appway2')); ?>/images/enjoy_icon.png" style="width:65px;">
                            <!--<p style="font-size:13px !important">Enjoy</p>-->
                            <div style="font-size: 18px;font-weight:700;color:#1C1C1C;padding-top:14px;">

                                5<br> Enjoy<br>The funds is ready on your pocket</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:30px;margin-bottom:50px;">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <center><button data-toggle="modal" data-target="#modal-signin" class="btn"
                            style="background-color:white;color:#2F80ED;padding:10px 50px;border:1px solid #2F80ED">Here is
                            How</button></center>
                </div>
            </div>
        </div>
    </section><!-- partnership -->
    <section class="security-invisible"
        style="margin-top:-80px;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex">
                                <h4 style="color: #F2994A;font-size:20px">Doesn't have Indonesian bank account?</h4>
                            </div>
                            <div class="sec-titlex">
                                <h2 style="font-size:40px;">No worries Laut CryptCash <br>
                                    come to rescue</h2>
                            </div>
                            <div class="text" style="font-size:20px;line-height:20px;">Ease tourists and foreign traders
                                daily activities to make transaction in Indonesia.</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt" style="text-align:center;">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms"
                                style="text-align:center;" data-wow-duration="1500ms"> <img
                                    src="<?php echo e(asset('public/Appway2')); ?>/images/LAUTAN.gif" alt=""
                                    style="max-width: 230px;"> </figure>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
    <section class="security-invisible" style="margin-top:-160px">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28" style="padding-top:20px;">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex">
                                <h2 style="font-size: 40px">What are you waiting for?<br>
                                    Convert your digital asset now!</h2>
                            </div>
                            <div class="text" style="font-size:20px;line-height:20px">We offer excellent client services
                                to ensure your peace of mind digital asset transactions in Indonesia.</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms"
                                data-wow-duration="1500ms"> <img
                                    src="https://lautan.s3.amazonaws.com/webcontent/media_bjq70J.png" width="450px"
                                    class="img-fluid" alt=""> </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div style="text-align: center;padding:10px"> <img
                            src="https://lautan.s3.amazonaws.com/webcontent/media_PTmU8I.png" style="width: 56px">
                        <p class="text" style="margin-top: 20px;line-height:20px;color:#45586c;font-family: inherit;">
                            <span style="font-size:24px;font-weight: 700;">Easy To Try</span><br> 5 easy steps for
                            converting your digital asset
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div style="text-align: center;padding:10px"> <img
                            src="https://lautan.s3.amazonaws.com/webcontent/media_puPfFo.png" style="width: 56px">
                        <p class="text" style="margin-top: 20px;line-height:20px;color:#45586c;font-family: inherit;">
                            <span style="font-size:24px;font-weight: 700;">Fastest way</span><br> Fastest way to convert
                            your
                            digital asset in Indonesia
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div style="text-align: center;padding:10px"> <img
                            src="https://lautan.s3.amazonaws.com/webcontent/media_LTU27R.png" style="width: 56px">
                        <p class="text" style="margin-top: 20px;line-height:20px;color:#45586c;font-family: inherit;">
                            <span style="font-size:24px;font-weight: 700;">Secure</span><br> Every transaction is protected
                            with
                            Multi-factor Authentication, using
                            email verification
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-12 col-sm-12">&nbsp;</div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div style="text-align: center;padding:10px"> <img
                            src="https://lautan.s3.amazonaws.com/webcontent/media_GT6Wb8.png" style="width: 56px">
                        <p class="text" style="margin-top: 20px;;line-height:20px;color:#45586c;font-family: inherit;">
                            <span style="font-size:24px;font-weight: 700;">Trusted</span><br>Lautan.io be responsible based
                            on laws and regulations on privacy applicable in Indonesia
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div style="text-align: center;padding:10px">
                        <img src="https://lautan.s3.amazonaws.com/webcontent/media_KpTwqO.png" style="width: 56px">
                        <p class="text" style="margin-top: 20px;line-height:20px;color:#45586c;font-family: inherit;">
                            <span style="font-size:24px;font-weight: 700;">Convenient</span><br> Lautan.io offer best
                            experienced
                            for customer
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12">&nbsp;</div>
            </div>
            <div class="row" style="margin-top:40px">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <center><button data-toggle="modal" data-target="#modal-signin" class="btn"
                            style="background-color:#2F80ED;color:white;padding:10px 50px">Activate</button></center>
                </div>
            </div>
        </div>
    </section><!-- partnership -->



    <section class="clients-section home-12" style="margin-top:-50px;">
        <div class="container">
            <div class="sec-titlex" style="text-align: center;">
                <h2 style="font-size:40px;">Our Partners</h2>
            </div>
            <div class="row" style="margin-top:50px;">
                <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px;">
                    <div style="border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                        <img src="<?php echo e(asset('public/Appway2')); ?>/images/partner/logo.png" alt=""
                            style="margin-top: 40px;
    margin-bottom: 43px;">
                        <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">Tokocrypto</div>
                        <!--<p style="font-size:16px;line-height:20px;">Withdrawable from any ATM all around Indonesia</p>-->
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px">
                    <div style="border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                        <img src="<?php echo e(asset('public/Appway2')); ?>/images/partner/Logo Bank Permata.png" alt=""
                            style="margin-top: -20px;">
                        <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">Bank Permata</div>
                        <!--<p style="font-size:16px;line-height:20px;">No charge fee aka free when doing transaction</p>-->
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px">
                    <div style="border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                        <img src="<?php echo e(asset('public/Appway2')); ?>/images/partner/logo-faspay.jpg" alt=""
                            style="margin-top: -13px;">
                        <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">Faspay</div>
                        <!--<p style="font-size:16px;line-height:20px;">No charge fee aka free when doing transaction</p>-->
                    </div>
                </div>
            </div>

        </div>
    </section><!-- partnership -->
    <section class="security-invisible" style="margin-bottom: 30px;margin-top: -50px;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInLeft" data-wow-delay="00ms"
                                data-wow-duration="1500ms"> <img
                                    src="<?php echo e(asset('public/Appway2')); ?>/images/rev/01 Partnership .png"
                                    style="max-width:500px;" alt=""> </figure>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms"
                            style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h4 style="color: #F2994A;font-size:20px">Want to earn more?</h4>
                            </div>
                            <div class="sec-titlex">
                                <h2 style="font-size: 40px;color:#45586c;">Join our Partnership
                                    program</h2>
                            </div>
                            <div class="text" style="font-size:20px;line-height:20px;color:#45586c;">Lautan.io wants to
                                share the opportunity to earn additional income, grow with us and get the benefit for every
                                transaction on Lautan.io.
                                <br>
                                <br>
                                All Lautan.io users can join our partnership program, sharing the easy way to convert
                                digital assets, and enjoy the benefit!
                            </div>
                            <div><button data-toggle="modal" data-target="#modal-signin" class="btn"
                                    style="background-color:#2F80ED;color:white;padding:10px 30px 10px 30px;margin-bottom:70px;">Earn
                                    Now</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!--<coin-ponent></coin-ponent>-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/land/index.blade.php ENDPATH**/ ?>
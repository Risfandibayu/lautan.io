
<?php $__env->startSection('content'); ?>
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div class="container-xl px-4 mt-5">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11">
                            <!-- Social login form-->
                            <div class="card my-5">
                                <div class="card-body p-5 text-center">
                                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/logo-nav.png"
                                        style="height:50px;    width: auto;"></a>

                                </div>
                                <hr class="my-0" />
                                <div class="card-body p-5">
                                    <div class="success-alert d-none">
                                        <div class="alert alert-success alert-icon" role="alert">
                                            <div class="alert-icon-aside">
                                                <i data-feather="check-circle"></i>
                                            </div>
                                            <div class="alert-icon-content" id="success-text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="error-alert d-none">
                                        <div class="alert alert-danger alert-icon" role="alert">
                                            <div class="alert-icon-aside">
                                                <i data-feather="alert-octagon"></i>
                                            </div>
                                            <div class="alert-icon-content" id="error-text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Login form-->
                                    <form id="loginForm">
                                        <!-- Form Group (email address)-->
                                        <div class="mb-3">
                                            <label class="text-gray-600 small" for="emailExample">Email address</label>
                                            <input class="form-control form-control-solid" type="email" placeholder=""
                                                aria-label="Email Address" name="email" />
                                        </div>
                                        <!-- Form Group (password)-->
                                        <div class="mb-3">
                                            <label class="text-gray-600 small" for="passwordExample">Password</label>
                                            <input class="form-control form-control-solid" type="password" placeholder=""
                                                aria-label="Password" name="password" />
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between mb-0 ">
                                            <div class="d-grid gap-2">
                                                <button type="submit" class="btn btn-primary" id="btn-submit">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <hr class="my-0" />
                                <div class="card-body px-5 py-4">
                                    <div class="small text-center">
                                        <a href="<?php echo e(route('forget.password.get')); ?>"> Forgot password</a>
                                        or new user?
                                        <a href="<?php echo e(url('register')); ?>">Create an account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <footer class="footer-admin mt-auto footer-dark">
                <div class="container-xl px-4">
                    <div class="row">
                        <div class="col-md-6 small">Copyright © Exlaut <?php echo e(date('Y')); ?></div>
                        <div class="col-md-6 text-md-end small">
                            <a href="#!">Privacy Policy</a>
                            ·
                            <a href="#!">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $(document).ready(function() {
            $('#loginForm').on('submit', function(e) {
                e.preventDefault();
                clear_error();
                disable_login();
                var formData = $(this).serialize();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "<?php echo e(url('login')); ?>",
                    type: "POST",
                    data: formData,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 401) {
                            $('.error-alert').removeClass('d-none');
                            $('.success-alert').addClass('d-none');
                            var err_val = `<b>Validation error!</b>`;
                            $.each(data.errors, function(key, value) {
                                err_val += `<br>${value}`;
                            });
                            $('#error-text').html(err_val);
                            undisable_login();
                        } else if (data.status == 402) {
                            $('.error-alert').removeClass('d-none');
                            $('.success-alert').addClass('d-none');
                            $('#error-text').html(`<b>Error!</b> <br>${data.errors}`);
                            undisable_login();
                        } else {
                            $('.error-alert').addClass('d-none');
                            $('.success-alert').removeClass('d-none');
                            undisable_login();
                            $('#success-text').html(
                                `<b>Success!</b> <br>${data.message} <br>Redirecting...`);
                            setTimeout(function() {
                                window.location.href = "<?php echo e(url('dashboard')); ?>";
                            }, 2000);
                        }
                    },
                });
            });

            function clear_error() {
                $('.error-alert').addClass('d-none');
                $('.success-alert').addClass('d-none');
            }

            function disable_login() {
                $('#btn-submit').attr('disabled', true);
                $('#btn-submit').html('Processing...');
            }

            function undisable_login() {
                $('#btn-submit').attr('disabled', false);
                $('#btn-submit').html('Login');
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/login.blade.php ENDPATH**/ ?>
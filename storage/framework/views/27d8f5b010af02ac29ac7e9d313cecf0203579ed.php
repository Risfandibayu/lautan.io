<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Currency
                    </h1>
                </div>
                <div class="col-12 col-xl-auto mb-3"><a href="/currency/create" class="btn btn-sm btn-light"><i data-feather="plus"></i>&nbsp; Add Currency</a></div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Coin</th>
                        <th scope="col">Name</th>
                        <th scope="col">Wallet Address</th>
                        <th scope="col" class="text-end">Assets</th>
                        <th scope="col" class="text-end">Est. IDR Value</th>
                        <th scope="col" class="text-center">Status</th>
                        <th scope="col" style="width:130px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php 
                        $blc = App\Models\Balance::where('user_id',Auth::id())->where('currency_id',$rows->id)->where('active',1)->orderBy('id','DESC')->first();
                        if($blc) {
                            $binance                 = NEW App\Libs\Binance;
                            $send['url']             = '/ticker/24hr';
                            $body['symbol']          = $rows->code;
                            $send['body']            = $body; 
                            $post                    = $binance->callApi($send);

                            $balance = $blc->amount;
                            $balanceIDR = $post->lastPrice * $blc->amount;
                        } else {
                            $balance = '0.000000';
                            $balanceIDR = 0;
                        }
                    ?>
                    <tr>
                        <td><img src="<?php echo e($rows->image); ?>" width="20px"></td>
                        <td class="align-middle"><?php echo e($rows->code); ?></td>
                        <td class="align-middle"><?php echo e($rows->name); ?></td>
                        <td class="align-middle"><?php echo e($rows->address); ?></td>
                        <td class="align-middle text-end"><?php echo e($balance); ?></td>
                        <td class="align-middle text-end"><?php echo e($balanceIDR); ?></td>
                        <td class="align-middle text-center"><?php echo $rows->getActiveLabel(); ?></td>
                        <td class="text-right align-middle">
                        <div class="btn-group">
                            <!-- <a href="/currency/<?php echo e($rows->id); ?>" class="btn btn-light btn-sm"><i data-feather="eye"></i></a> -->
                            <a href="/currency/<?php echo e($rows->id); ?>/edit" class="btn btn-light btn-sm"><i data-feather="edit"></i></a>
                            <?php if($rows->active == 1): ?>
                                <button class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#alertModal" data-id="<?php echo e($rows->id); ?>" data-name="<?php echo e($rows->name); ?>"><i data-feather="slash"></i></button>
                            <?php else: ?>
                                <a href="/currency/activate/<?php echo e($rows->id); ?>" class="btn btn-success btn-sm"><i data-feather="check-circle"></i></a>
                            <?php endif; ?>
                        </div>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<form method="POST" action="/currency/del" class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
    <?php echo csrf_field(); ?>
    <input name="_method" type="hidden" value="DELETE">
    <input name="id" type="hidden" id="user-id">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
            <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body text-center">
            Yakin akan menonaktifkan currency <span id="user-name"></span> ?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
            <button type="submit" class="btn btn-danger">Ya</button>
        </div>
        </div>
    </div>
</form>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script type="text/javascript">
	$('#alertModal').on('show.bs.modal', function(event) {
        var modal 	= $(this);
        var button 	= $(event.relatedTarget);
		$('#user-id').val(button.data('id'));
        $('#user-name').html(button.data('name'));
    });
</script>
<?php $__env->stopPush(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Risfandi Bayu\Downloads\Compressed\exlaut-staging\exlaut-staging\resources\views/currency/index.blade.php ENDPATH**/ ?>
<?php echo $__env->make('emails.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;"
    align="left">
    Hi, Admin. <br>
    User <b><?php echo e($name); ?> - <?php echo e($email); ?> </b> have verified the KYC document upload, please check and
    confirm before 24 hours.
    <bt><br>
</p>
<?php echo $__env->make('emails.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/emails/kyc_admin.blade.php ENDPATH**/ ?>
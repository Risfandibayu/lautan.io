<?php $__env->startSection('content'); ?>
<!-- <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Akun
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header> -->
<!-- Main page content-->
<div class="container-xl mt-4">
    <div class="row">
        <div class="offset-xl-2 col-xl-8">
            <?php if(session('warning')): ?>
            <div class="alert alert-warning alert-icon" role="alert">
                <div class="alert-icon-aside">
                    <i data-feather="alert-triangle"></i>
                </div>
                <div class="alert-icon-content">
                    <?php echo e(session('warning')); ?>

                </div>
            </div>
            <?php endif; ?>

            <?php if(session('error')): ?>
            <div class="alert alert-danger alert-icon" role="alert">
                <div class="alert-icon-aside">
                    <i data-feather="alert-triangle"></i>
                </div>
                <div class="alert-icon-content">
                    <?php echo e(session('error')); ?>

                </div>
            </div>
            <?php endif; ?>

            <?php if(session('success')): ?>
            <div class="alert alert-success alert-icon" role="alert">
                <div class="alert-icon-aside">
                    <i data-feather="check-circle"></i>
                </div>
                <div class="alert-icon-content">
                    <?php echo e(session('success')); ?>

                </div>
            </div>
            <?php endif; ?>
            <div class="card mb-4">
                <div class="card-header">Account Verification</div>
                <div class="card-body">
                    <div class="step step-primary mb-5">
                        <div class="step-item">
                            <a class="step-item-link" href="#">Account Information</a>
                        </div>
                        <!-- <div class="step-item">
                            <a class="step-item-link" href="#">Bank Account</a>
                        </div> -->
                        <div class="step-item">
                            <a class="step-item-link" href="#">Upload File</a>
                        </div>
                        <div class="step-item active">
                            <a class="step-item-link disabled" href="#" tabindex="-1">Finish</a>
                        </div>
                    </div>
                    
                    <div class="text-center">
                        <img src="<?php echo e(asset('public')); ?>/assets/images/success.png" width="150px" class="my-5">
                        <?php if(Auth::user()->status == 2): ?>
                        <p class="small mb-3 mt-2">
                        Our team will verify your data, the verification process will take a maximum of 1x24 hours, please be patient we will inform you of your verification status via email.
                        </p>
                        <?php elseif(Auth::user()->status == 3): ?>
                        <p class="small mb-3 mt-2">
                        Your account is verified
                        </p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/public_html/resources/views/verification/done.blade.php ENDPATH**/ ?>

<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script>
        $(".select2-single").select2({
            minimumResultsForSearch: Infinity
        });
        var change = "";
        var changebox = "";
        $(document).on("change", "#idrleft_select", function() {
            change = "idrleft_select";
            hitung();
        });
        $(document).on("change", "#idrright_select", function() {
            change = "idrright_select";
            hitung();
        });
        $(document).on("change", "#curleft_select", function() {
            change = "curleft_select";
            hitung();
        });
        $(document).on("change", "#curright_select", function() {
            change = "curright_select";
            hitung();
        });
        $("#idrleft_price").keyup(function() {
            change = "idrleft_price";
            hitung();
        });
        $("#idrright_price").keyup(function() {
            change = "idrright_price";
            hitung();
        });
        $("#curleft_price").keyup(function() {
            change = "curleft_price";
            hitung();
        });
        $("#curright_price").keyup(function() {
            change = "curright_price";
            hitung();
        });

        $(document).on("change", "#changebox", function() {
            changebox = $('#changebox').val();
            if (changebox == 'BUY') {
                $('#idrleft').show();
                $('#idrright').hide();
                $('#curleft').hide();
                $('#curright').show();
                document.getElementById("btnnya").innerHTML = '<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-signin">BUY</button>';
                hitung();
            } else {
                $('#idrleft').hide();
                $('#idrright').show();
                $('#curleft').show();
                $('#curright').hide();
                document.getElementById("btnnya").innerHTML = '<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-signin">WITHDRAW</button>';
                hitung();
            }
        });
        $("input[data-type='currency']").on({
            keyup: function() {
              formatCurrency($(this));
            },
            blur: function() { 
              formatCurrency($(this), "blur");
            }
        });

        function formatNumber(n) {
          // format number 1000000 to 1,234,567
          return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        }
        
        function formatr(n) {
          // format number 1000000 to 1,234,567
          return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "")
        }
        
        function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(",") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(",");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "" + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += "";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}
        function hitung() {
            changebox = $('#changebox').val();
            var idrleft_price = $('#idrleft_price').val();
            var idrleft_select = $('#idrleft_select').val();
            var curright_price = $('#curright_price').val();
            var curright_select = $('#curright_select').val();
            var idrright_price = $('#idrright_price').val();
            var idrright_select = $('#idrright_select').val();
            var curleft_price = $('#curleft_price').val();
            var curleft_select = $('#curleft_select').val();
            

            $.ajax({
                type: "GET",
                url: "/calculate",
                data: "idrleft_price=" + formatr(idrleft_price) + "&idrleft_select=" + idrleft_select + "&curright_price=" + curright_price + "&curright_select=" + curright_select + "&idrright_price=" + formatr(idrright_price) + "&idrright_select=" + idrright_select + "&curleft_price=" + curleft_price + "&curleft_select=" + curleft_select + "&change=" + change + "&changebox=" + changebox,
                success: function(result) {
                    if (result) {
                        if (change == 'idrleft_price' || change == 'idrleft_select') {
                            $('#curright_price').val(result);
                        } else if (change == 'curright_price' || change == 'curright_select') {
                            $('#idrleft_price').val(formatNumber(result));
                        } else if (change == 'idrright_price' || change == 'idrright_select') {
                            $('#curleft_price').val(result);
                        } else if (change == 'curleft_price' || change == 'curleft_select') {
                            $('#idrright_price').val(formatNumber(result));
                        }
                    }
                }
            });
        }
    </script>
<script src="https://cdn.jsdelivr.net/gh/coinponent/coinponent@1.2.6/dist/coinponent.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable({
        "dom": 't',
        responsive: true,
    });
} );
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<section class="banner-style-ten" style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%">
            <div class="container" style="margin-top:-100px">
                <div class="sec-titlex">
                    <h2>Laut CryptCash<br>
Peace of mind</h2>
                </div>
                <div style="margin-bottom:50px;font-size:20px;line-height:20px;">Convert your digital assets online anytime with
a blink
of an eye, and hassle free when you travel to
Indonesia
.</div>
                <div class="row">
                <div class="col-md-12">

                    <div>
                        <div class="row">
                            <div class="col-md-5" id="curright">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="0" value="0.00256" id="curright_price" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="curright_select" style="width:100%;height:40px;">
                                                <option value="ETHBIDR">ETHBIDR</option>
                                                <option value="USDTBIDR">USDTBIDR</option>
                                                <option value="BNBBIDR">BNBBIDR</option>
                                                <option value="DOGEBIDR">DOGEBIDR</option>
                                                <option value="BUSDBIDR">BUSDBIDR</option>
                                                <option value="DOTBIDR">DOTBIDR</option>
                                                <option value="TKOBIDR">TKOBIDR</option>
                                                <option value="SXPBIDR">SXPBIDR</option>
                                                <option value="LAUTAN">LAUTAN</option>
                                                <option value="H00002">H00002</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--<div class="col-md-5" id="idrleft">-->
                            <!--    <div class="row">-->
                            <!--        <div class="col-md-6">-->
                            <!--            <div class="form-group">-->
                            <!--                <input type="text" class="form-control" placeholder="0" value="100000" id="idrleft_price" style="width:100%">-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--        <div class="col-md-6">-->
                            <!--            <div class="form-group">-->
                            <!--                <select class="form-control" id="idrleft_select" style="width:100%;height:40px;">-->
                            <!--                    <option value="IDR">IDR</option>-->
                            <!--                </select>-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="col-md-5" id="curleft" style="display:none">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="0" value="0.00256" id="curleft_price" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="curleft_select" style="width:100%;height:40px;">
                                                <option value="ETHBIDR">ETHBIDR</option>
                                                <option value="USDTBIDR">USDTBIDR</option>
                                                <option value="BNBBIDR">BNBBIDR</option>
                                                <option value="DOGEBIDR">DOGEBIDR</option>
                                                <option value="BUSDBIDR">BUSDBIDR</option>
                                                <option value="DOTBIDR">DOTBIDR</option>
                                                <option value="TKOBIDR">TKOBIDR</option>
                                                <option value="SXPBIDR">SXPBIDR</option>
                                                <option value="LAUTAN">LAUTAN</option>
                                                <option value="H00002">H00002</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" hidden>
                                <div class="form-group">
                                    <select class="form-control select2-single" id="changebox">
                                        <option value="BUY">BUY</option>
                                        <option value="SELL">SELL</option>
                                    </select>
                                </div>
                            </div>
                            <!--<div class="col-md-5" id="curright">-->
                            <!--    <div class="row">-->
                            <!--        <div class="col-md-6">-->
                            <!--            <div class="form-group">-->
                            <!--                <input type="text" class="form-control" placeholder="0" value="0.00256" id="curright_price" style="width:100%">-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--        <div class="col-md-6">-->
                            <!--            <div class="form-group">-->
                            <!--                <select class="form-control" id="curright_select" style="width:100%;height:40px;">-->
                            <!--                    <option value="ETHBIDR">ETHBIDR</option>-->
                            <!--                    <option value="USDTBIDR">USDTBIDR</option>-->
                            <!--                    <option value="BNBBIDR">BNBBIDR</option>-->
                            <!--                    <option value="DOGEBIDR">DOGEBIDR</option>-->
                            <!--                    <option value="BUSDBIDR">BUSDBIDR</option>-->
                            <!--                    <option value="DOTBIDR">DOTBIDR</option>-->
                            <!--                    <option value="TKOBIDR">TKOBIDR</option>-->
                            <!--                    <option value="SXPBIDR">SXPBIDR</option>-->
                            <!--                    <option value="LAUTAN">LAUTAN</option>-->
                            <!--                    <option value="H00002">H00002</option>-->
                            <!--                </select>-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="col-md-5" id="idrleft">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency" placeholder="0" value="100.000" id="idrleft_price" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="idrleft_select" style="width:100%;height:40px;">
                                                <option value="IDR">IDR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5" id="idrright" style="display:none">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="0" value="100000" id="idrright_price" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="idrright_select" style="width:100%;height:40px;">
                                                <option value="IDR">IDR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" id="btnnya">
                                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-signin">Convert Now</button>
                            </div>
                        </div>
                        <!--<coin-ponent></coin-ponent>-->
                    </div>

                </div>
            </div>
            </div>
        </section>
        <section class="clients-section home-12" >
            <div class="container">
                <div class="sec-titlex">
                    <h2>Market</h2>
                </div>
                <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Coin</th>
                        <th scope="col">Name</th>
                        <th scope="col" class="text-end">Last Price</th>
                        <th scope="col" class="text-end">24H Change</th>
                        <th scope="col" class="text-end">24H High</th>
                        <th scope="col" class="text-end">24H Low</th>
                        <th scope="col" class="text-end">24H Volume</th>
                        <!-- <th scope="col"></th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php 
                        if($rows->change > 0) {
                            $class = 'text-success';
                        } elseif($rows->change < 0) {
                            $class = 'text-danger';
                        } else {
                            $class = '';
                        }
                    ?>
                    <tr>
                        <td><img src="<?php echo e($rows->image); ?>" width="20px"></td>
                        <td><?php echo e($rows->code); ?></td>
                        <td><?php echo e($rows->name); ?></td>
                        <td class="<?php echo e($class); ?> text-end"><?php echo e($rows->price); ?></td>
                        <td class="<?php echo e($class); ?> text-end"><?php echo e($rows->change); ?>%</td>
                        <td class="text-end"><?php echo e($rows->highPrice); ?></td>
                        <td class="text-end"><?php echo e($rows->lowPrice); ?></td>
                        <td class="text-end"><?php echo e($rows->marketPrice); ?></td>
                        <!-- <td><a href="/buy" class="btn btn-sm btn-success"><i data-feather="shopping-bag"></i>&nbsp; Beli</a></td> -->
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
            </div>
        </section>
         <section class="security-invisible"  style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%;margin-bottom: 60px;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="sec-titlex">
                                <h2 >Worry No More,<br>
Laut CryptCash is in your hand!
</h2>
                            </div>
                            <div class="text" style="font-size:20px;line-height:20px;">We work hard to deliver this feature for our
client. Lautan.io finest solutions when you
travel to Indonesia.</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt" style="text-align:center;">
                        <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" style="text-align:center;" data-wow-duration="1500ms"> <img src="<?php echo e(asset('public/Appway2')); ?>/images/LAUTs.gif" alt="" style="max-width: 250px;"> </figure>
                    </div>
                    </div>
                </div>
            </div>
             
            <div class="row" style="margin-top:30px">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <center><button data-toggle="modal" data-target="#modal-signin" class="btn" style="background-color:#2F80ED;color:white;padding:10px 50px">Activate</button></center>
                </div>
            </div>
        </div>
    </section>
    <section class="clients-section home-12" style="margin-top:50px;margin-bottom:50px;">
    <div class="container">
        <div class="sec-titlex" style="text-align: center;">
            <h2 style="font-size:40px;">Our Partners</h2>
        </div>
        <div class="row" style="margin-top:50px;">
            <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px;">
                <div style="height: 235px;border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/partner/logo.png"  alt="" style="margin-top: 40px;
    margin-bottom: 43px;">
                    <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">Tokocrypto</div>
                    <!--<p style="font-size:16px;line-height:20px;">Withdrawable from any ATM all around Indonesia</p>-->
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px">
                <div style="height: 235px;border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/partner/Logo Bank Permata.png"  alt="" style="margin-top: -20px;">
                    <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">Bank Permata</div>
                    <!--<p style="font-size:16px;line-height:20px;">No charge fee aka free when doing transaction</p>-->
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px;margin-bottom:50px;">
                <div style="height: 235px;border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/partner/idc.jpg"  alt="" style="    padding-left: 65px;
    padding-right: 65px;
    padding-bottom: 19px;">
                    <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">IDcash</div>
                    <!--<p style="font-size:16px;line-height:20px;">When your card is lost, deactivation via the application is possible.</p>-->
                </div>
            </div>
        </div>
                
    </div>
</section><!-- partnership -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/land/products.blade.php ENDPATH**/ ?>
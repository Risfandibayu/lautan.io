<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <title>Lautan.io</title>

    <!-- Fav Icon -->
    <link rel="icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&display=swap"
        rel="stylesheet">

    <!-- Stylesheets -->
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/font-awesome-all.css" rel="stylesheet">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/flaticon.css" rel="stylesheet">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/owl.css" rel="stylesheet">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/jquery.fancybox.min.css" rel="stylesheet">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/animate.css" rel="stylesheet">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/flat.css" rel="stylesheet">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('public')); ?>/appway/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('public')); ?>/appway/css/select2-bootstrap.css">
    <link href="<?php echo e(asset('public/Appway2')); ?>/css/flag-icon.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.2.2/css/flat-ui.min.css" integrity="sha512-PvB3Q4vTvWD/9aiiELYI3uebup/4mtou3Mc/uGudC/Zl+C9BdKUkAI+5jORfA+fvLK4DpzC5VyEN7P2kK43hjg==" crossorigin="anonymous" referrerpolicy="no-referrer" />-->
    <style>
        .modal-body {
            height: 80vh;
            overflow-y: auto;
        }

        .err-text {
            font-size: small;
        }

        #term {
            color: white;
        }

        #term:hover {
            color: #9ecdfd;
        }

        #priv {
            color: white;
        }

        #priv:hover {
            color: #9ecdfd;
        }

    </style>
    <?php echo $__env->yieldContent('style'); ?>

</head>


<!-- page wrapper -->

<body class="boxed_wrapper">

    <!-- preloader -->
    <div class="preloader"></div>
    <!-- preloader -->

    <!-- main header -->
    <header class="main-header home-10">
        <div class="outer-container">
            <div class="container">
                <div class="main-box clearfix">
                    <div class="logo-box pull-left">
                        <figure class="logo"><a href="<?php echo e(url('/')); ?>"><img
                                    src="https://lautan.s3.amazonaws.com/webcontent/media_pkh7BF.png" width="230"
                                    style="margin-top:-17px" alt=""></a></figure>
                    </div>
                    <div class="menu-area pull-right clearfix">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler">
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                        </div>
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="<?php echo e('/' == request()->path() ? 'current ' : ''); ?>"><a
                                            href="<?php echo e(url('/')); ?>"
                                            style="<?php echo e('/' == request()->path() ? 'font-weight: 600;color: #3c8cd3' : ''); ?>">Home</a>
                                    </li>
                                    <li class="<?php echo e('about-us' == request()->path() ? 'current ' : ''); ?>"><a
                                            style="<?php echo e('about-us' == request()->path() ? 'font-weight: 600;color: #3c8cd3' : ''); ?>"
                                            href="<?php echo e(url('about-us')); ?>">About Us</a></li>
                                    <li class="<?php echo e('products' == request()->path() ? 'current ' : ''); ?>"><a
                                            style="<?php echo e('products' == request()->path() ? 'font-weight: 600;color: #3c8cd3' : ''); ?>"
                                            href="<?php echo e(url('products')); ?>">Products</a></li>
                                    <li class="<?php echo e('news' == request()->path() ? 'current ' : ''); ?>"><a
                                            style="<?php echo e('news' == request()->path() ? 'font-weight: 600;color: #3c8cd3' : ''); ?>"
                                            href="<?php echo e(url('news')); ?>">Info</a></li>
                                    <li class="<?php echo e('partnership' == request()->path() ? 'current ' : ''); ?>"><a
                                            style="<?php echo e('partnership' == request()->path() ? 'font-weight: 600;color: #3c8cd3' : ''); ?>"
                                            href="<?php echo e(url('partnership')); ?>">Partnership</a></li>
                                    <li><a class="btn btn-sm btn-primary"
                                            style="color:white;padding-left: 20px;padding-right: 20px;border: rgb(255, 255, 255) 1px solid;"
                                            data-toggle="modal" data-target="#modal-signin">Sign In</a></li>
                                    <!-- <li>
                                        <button class="btn btn-sm" style="background-color:#7b7e81;color:white;" data-toggle="modal" data-target="#modal-signin">Sign In</button>
                                    </li> -->
                                    <!-- <li></li> -->
                                    <!-- <li>
                                        <div class="dropdown">
                                            
                                            <button  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-left: -10px;padding-left: 20px;padding-right: 20px;border: rgb(27, 27, 27) 1px solid;color: black;background-color: white;">
                                                <span class="flag-icon flag-icon-idn"></span>
                                              Indonesia
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                              <a class="dropdown-item" href="#"><span class="flag-icon flag-icon-idn"></span>Indonesia</a>
                                            </div>
                                          </div>
                                    </li> -->
                                    <li class="dropdown"><a href="#"
                                            style="padding-left: 10px;padding-right: 10px;border: rgb(117, 117, 117) 1px solid;"><span
                                                class="flag-icon flag-icon-gbr"></span>&nbsp;&nbsp;English </a>
                                        <ul>
                                            <li><a href="<?php echo e(url('/')); ?>"><span
                                                        class="flag-icon flag-icon-gbr"></span>&nbsp;&nbsp;English</a>
                                            </li>
                                            <!-- <li><a href="blog-details.html">News Details</a></li> -->
                                        </ul>
                                    </li>
                                </ul>

                            </div>
                        </nav>
                        <!-- <div class="btn-box"><a href="#">Started Now</a></div> -->
                        <!-- <a href="contact.html" class="btn btn-sm btn-box" style="margin-left: -10px;padding-left: 20px;padding-right: 20px;border: rgb(27, 27, 27) 1px solid;color: black;">Indonesia</a> -->

                    </div>
                </div>
            </div>
        </div>

        <!--sticky Header-->
        <div class="sticky-header" style="background-color:#2F80ED;">
            <div class="container clearfix">
                <figure class="logo-box"><a href="<?php echo e(url('/')); ?>"><img
                            src="<?php echo e(asset('public/Appway2')); ?>/images/Lautan Putih-nav.png" style="height:40px"
                            alt=""></a></figure>
                <div class="menu-area">
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- main-header end -->
    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>

        <nav class="menu-box">
            <div class="nav-logo"><a href="/"><img
                        src="https://lautan.s3.amazonaws.com/webcontent/media_f19qFg.png" alt="" title=""></a></div>
            <div class="menu-outer">
                <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
            </div>
            <div class="contact-info">
                <h4>Contact Info</h4>
                <ul>
                    <!--<li>Denpasar, Bali</li>-->
                    <li><i class="fab fa-whatsapp"></i><a href="https://api.whatsapp.com/send?phone=+62816860871"> +62
                            816 860 871</a></li>
                    <li><i class="fa fa-envelope"></i><a href="mailto:hello@lautan.io"> hello@lautan.io</a></li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="clearfix">
                    <li><a target="_blank" href="https://instagram.com/lautan.io"><i class="fab fa-instagram"></i></a>
                    </li>
                    <li><a target="_blank" href="https://www.facebook.com/lautanio-105729085001639/"><i
                                class="fab fa-facebook-f"></i></a></li>
                    <li><a target="_blank" href="https://twitter.com/lautan_official"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li><a target="_blank" href="#"><i class="fab fa-telegram-plane"></i></a></li>
                    <!--<li><a href="#"><i class="fab fa-youtube"></i></a></li>-->
                </ul>
            </div>
        </nav>
    </div><!-- End Mobile Menu -->
    <?php echo $__env->yieldContent('content'); ?>

    <footer class="main-footer style-five" style="background-color: #2F80ED;"">
        <div class="                                                                           footer-top"
        style="margin-top:-100px">
        <div class="container" style="color:white;>
                <div class="                                                                           widget-section">
            <div class="row container">
                <div class="col-6 col-lg-3 col-md-6 col-sm-12">
                    <div class="links-widget footer-widget">
                        <h4 class="widget-title" style="font-size:19px;font-weight:bold;color:white">Lautan.io</h4>
                        <div class="widget-content">
                            <ul class="list clearfix">
                                <li><a href="<?php echo e(url('products')); ?>" style="color:white">Product</a></li>
                                <li><a href="<?php echo e(url('products')); ?>" style="color:white">Market</a></li>
                                <li><a href="<?php echo e(url('news')); ?>" style="color:white">News</a></li>
                                <li><a href="<?php echo e(url('news')); ?>" style="color:white">How To</a></li>
                                <li><a href="<?php echo e(url('news')); ?>" style="color:white">FAQ</a></li>
                                <li><a href="<?php echo e(url('partnership')); ?>" style="color:white">Partnership</a>
                                    &nbsp;&nbsp;<a href="<?php echo e(url('partnership')); ?>" class="btn btn-sm"
                                        style="background-color:#F2994A;color:white">Join Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6 col-sm-12">
                    <div class="links-widget footer-widget">
                        <h4 class="widget-title" style="font-size:19px;font-weight:bold;color:white">Company</h4>
                        <div class="widget-content">
                            <ul class="list clearfix">
                                <li><a href="<?php echo e(url('about-us')); ?>" style="color:white">About Us</a></li>
                                <li><a href="<?php echo e(url('contact-us')); ?>" style="color:white">Contact Us</a></li>
                                <li><a href="<?php echo e(url('/career')); ?>" style="color:white">Careers</a> &nbsp;&nbsp;<a
                                        href="<?php echo e(url('/career')); ?>" class="btn btn-sm"
                                        style="background-color:#F2994A;color:white">We're Hiring</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="contact-widget footer-widget">
                        <div class="widget-content"></div>
                        <figure class="footer-logo"><a href="<?php echo e(url('/')); ?>"><img
                                    src="https://lautan.s3.amazonaws.com/webcontent/media_f19qFg.png"
                                    class="img-fluid" alt=""></a></figure>
                        <!-- <div style="text-align:left;margin-top:30px"> -->
                        <!-- <p>
                                        <a href="/" target="_blank" style="padding:0px 20px 20px 0px"><img src="https://lautan.s3.amazonaws.com/webcontent/media_sYV8sj.png"></a> &nbsp;&nbsp; <a href="/" target="_blank" style="padding:0px 20px 20px 0px"><img src="https://lautan.s3.amazonaws.com/webcontent/media_pKN1v8.png"></a> &nbsp;&nbsp; <a href="/" target="_blank" style="padding:0px 20px 20px 0px"><img src="https://lautan.s3.amazonaws.com/webcontent/media_MYjQZ2.png"></a> &nbsp;&nbsp; <a href="/" target="_blank" style="padding:0px 20px 20px 0px"><img src="https://lautan.s3.amazonaws.com/webcontent/media_fNOYVF.png"></a> &nbsp;&nbsp; <a href="/" target="_blank" style="padding:0px 20px 20px 0px"><img src="https://lautan.s3.amazonaws.com/webcontent/media_2xe2GV.png"></a> &nbsp;&nbsp;
                                    </p> -->
                        <ul class="social-links clearfix" style="margin-top: 30px;">
                            <li><a target="_blank" href="https://instagram.com/lautan.io" style="border-radius:20%;"><i
                                        class="fab fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://www.facebook.com/lautanio-105729085001639/"
                                    style="border-radius:20%;"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a target="_blank" href="https://twitter.com/lautan_official"
                                    style="border-radius:20%;"><i class="fab fa-twitter"></i></a></li>
                            <li><a target="_blank" href="#" style="border-radius:20%;"><i
                                        class="fab fa-telegram-plane"></i></a></li>
                            <!--<li><a href="#" style="border-radius:20%;"><i class="fab fa-youtube"></i></a></li>-->
                        </ul>
                        <p style="color: white;font-size:16px;padding:40px 30px 30px 0px">Want to stay update with
                            lautan.io?</p>
                        <p style="color: white;font-size:18px;padding:0px 0px 30px 0px;font-weight:500;">Subscribe
                            Lautan.io newsletter right now!</p>
                        <form method="POST" action="<?php echo e(url('cfaspay')); ?>">
                            <?php echo csrf_field(); ?>
                            <div class="row col-12" style="margin-left:-30px;">

                                <div class="col-8">
                                    <div> <input type="text" name="email" class="form-control"
                                            placeholder="Enter your email address"> </div>
                                </div>
                                <div class="col-4">
                                    <!--<a href="<?php echo e(url('cfaspay')); ?>" class="btn btn-sm" style="background-color:#F2994A;color:white;padding:11px 25px 11px 25px;">Sign up Now</a> -->
                                    <button class="btn btn-sm"
                                        style="background-color:#F2994A;color:white;padding:11px 25px 11px 25px;"
                                        type="submit">Subscribe</button>
                                </div>
                            </div>
                        </form>
                        <!-- </div> -->
                    </div>
                </div>
            </div>


            <div style="margin-top:50px" class="container">
                <div class="copyright" style="font-size:25px;padding-bottom:30px;">Copyright &#169;
                    <?php echo e(DATE('Y')); ?> PT Lautan Harum Mewangi. All rights reserved</div>
                <div style="color: white;font-size:18px">Crypto Asset Trading is a risky activity because prices can
                    change significantly over time. Lautan.io is not responsible for changes in exchange rates of crypto
                    assets, the risk of loss from buying, selling, holding, investing, and trading any crypto assets. It
                    is the user’s responsibility to manage the aforementioned risks. Therefore, users are advised to be
                    careful and measure their financial condition and make sure that each user is ready to face risks
                    before making a decision. All decisions regarding crypto asset trading are conscious and independent
                    decisions of the user and there is no coercion whatsoever.</div>
                <div style="margin-top:20px;font-size:20px">
                    <a id="term" href="/user-agreement" style="margin-right:20px" target="_blank">User Agreement</a>
                    <a id="priv" href="/privacy-policy" target="_blank">Privacy Policy</a>
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>
    </footer>

    <div class="modal fade" id="modal-signin" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content rounded-0">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12" style="padding:50px">

                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                                        </br>
                                        <img src="<?php echo e(asset('public/Appway2')); ?>/images/01 SIGN IN  (1).png" alt="">
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                                        <div id="content_block_28">
                                            <div class="content-box" data-wow-delay="00ms"
                                                data-wow-duration="1500ms">
                                                <div class="sec-titlex" style="margin-bottom:5px">
                                                    <h2>Sign In</h2>
                                                </div>
                                                <div class="text"
                                                    style="color:#757575;font-size:15px;margin-bottom:15px">Welcome
                                                    back, please login to your account.</div>
                                                <div class="success-alert d-none ">
                                                    <div class="alert alert-success alert-icon" role="alert">
                                                        <div class="alert-icon-aside">
                                                            <i class="fas fa-check"></i>
                                                        </div>
                                                        <div class="alert-icon-content success-text err-text"
                                                            id="success-text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="error-alert d-none">
                                                    <div class="alert alert-danger alert-icon  text-center"
                                                        role="alert">
                                                        <div class="alert-icon-aside">
                                                            <i class="fas fa-exclamation-triangle"></i>
                                                        </div>
                                                        <div class="alert-icon-content error-text err-text"
                                                            id="error-text">

                                                        </div>
                                                    </div>
                                                </div>
                                                <form id="loginForm">
                                                    <!--<input type="hidden" name="_token" value="YVs4h1ibar0zITVZb5MwTk3be3xGyJmcCkYHdPXH">-->
                                                    <div class="form-group">
                                                        <label class="text"
                                                            style="color:#757575;font-size:15px;margin-bottom:0px">Email</label>
                                                        <input type="email"
                                                            style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;"
                                                            class="form-control email" name="email">
                                                        <span class="err-text text-danger d-none err_email"></span>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text"
                                                            style="color:#757575;font-size:15px;margin-bottom:0px">Password</label>
                                                        <input type="password"
                                                            style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;"
                                                            class="form-control password" name="password">
                                                        <span class="err-text text-danger d-none err_password"></span>

                                                    </div>
                                                    <div
                                                        style="color:#757575;text-align:right;font-size:12px;margin-top:10px;margin-bottom:10px;">
                                                        <a style="font-weight:bold;color:#2F80ED;cursor:pointer;"
                                                            href="<?php echo e(route('forget.password.get')); ?>">Forgot
                                                            password?</a>
                                                    </div>
                                                    <div><button type="submit" class="btn btn-block"
                                                            style="background-color:#2F80ED;color:white;padding:10px 40px;border-radius:8px;"
                                                            id="login-btn">Sign
                                                            In</button></div>
                                                </form>

                                                <div style="color:#757575;text-align:center;font-size:12px;"
                                                    class="mt-3">Don’t have
                                                    an account? &nbsp;&nbsp; <span
                                                        style="font-weight:bold;color:#2F80ED;cursor:pointer;"
                                                        data-toggle="modal" data-dismiss="modal"
                                                        data-target="#modal-signup">Sign Up</span></div>
                                                <!--<hr>
                                                <div><button class="btn btn-block" style="background-color:white;border:1px solid #cecece;font-size:14px;padding:10px 40px;border-radius:8px;">
                                                        <img src="https://lautan.s3.amazonaws.com/webcontent/media_QXFyc6.png" style="height:15px;margin-top:-5px;color:#404040;">&nbsp;&nbsp;&nbsp; Sign In with Google</button>
                                                </div>-->

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-signup" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content rounded-0">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12" style="padding:50px">

                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                                        <br />
                                        <img src="https://lautan.s3.amazonaws.com/webcontent/media_IiVz3q.png"
                                            style="weight: 100%" alt="">
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                                        <div id="content_block_28">
                                            <div class="content-box" data-wow-delay="00ms"
                                                data-wow-duration="1500ms">
                                                <div class="sec-titlex" style="margin-bottom:5px">
                                                    <h2>Sign Up</h2>
                                                </div>
                                                <div class="text"
                                                    style="color:#757575;font-size:15px;margin-bottom:15px">Please sign
                                                    up to create your account.
                                                </div>
                                                <div class="success-alert d-none">
                                                    <div class="alert alert-success alert-icon" role="alert">
                                                        <div class="alert-icon-aside">
                                                            <i class="fas fa-check"></i>
                                                        </div>
                                                        <div class="alert-icon-content success-text err-text"
                                                            id="success-text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="error-alert d-none">
                                                    <div class="alert alert-danger alert-icon  text-center"
                                                        role="alert">
                                                        <div class="alert-icon-aside">
                                                            <i class="fas fa-exclamation-triangle"></i>
                                                        </div>
                                                        <div class="alert-icon-content error-text err-text"
                                                            id="error-text">

                                                        </div>
                                                    </div>
                                                </div>
                                                <form id="register">
                                                    <!--<input type="hidden" name="_token" value="YVs4h1ibar0zITVZb5MwTk3be3xGyJmcCkYHdPXH">-->
                                                    <div class="form-group">
                                                        <label class="text"
                                                            style="color:#757575;font-size:15px;margin-bottom:0px">Full
                                                            Name</label>
                                                        <input type="text"
                                                            style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;"
                                                            class="form-control name" name="name"
                                                            placeholder="Enter your name">
                                                        <span class="err-text text-danger d-none err_name"></span>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text"
                                                            style="color:#757575;font-size:15px;margin-bottom:0px">Phone</label>
                                                        <div class="row">
                                                            <?php
                                                            use Illuminate\Support\Facades\DB;
                                                            $country = db::table('country')
                                                                ->select('*')
                                                                ->get();
                                                            
                                                            ?>


                                                            <div class="col-5">
                                                                <select
                                                                    style="background-color:#F5F5F5;color: #333333;font-size:14px;border-radius:8px;border:1px solid #cecece;"
                                                                    id="phonecode" onchange="displayCountryCode()"
                                                                    class="form-control" name="phonecode">
                                                                    <option value="+62" hidden selected>+62</option>
                                                                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $co): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="+<?php echo e($co->phonecode); ?>">
                                                                            <?php echo e($co->nicename); ?> (
                                                                            +<?php echo e($co->phonecode); ?> ) </option>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-7">
                                                                <input class="form-control phone" type="text"
                                                                    name="phone"
                                                                    placeholder="Please input your phone number"
                                                                    style="background-color:#F5F5F5;color: #333333;font-size:14px;border-radius:8px;border:1px solid #cecece;" />
                                                                <span
                                                                    class="err-text text-danger d-none err_phone"></span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text"
                                                            style="color:#757575;font-size:15px;margin-bottom:0px">Email</label>
                                                        <input type="email"
                                                            style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;"
                                                            class="form-control email" name="email"
                                                            placeholder="Please input your email address">
                                                        <span class="err-text text-danger d-none err_email"></span>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text"
                                                            style="color:#757575;font-size:15px;margin-bottom:0px">Password</label>
                                                        <input type="password"
                                                            style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;"
                                                            class="form-control password" name="password"
                                                            placeholder="Enter your Password">
                                                        <span class="err-text text-danger d-none err_password"></span>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text"
                                                            style="color:#757575;font-size:15px;margin-bottom:0px">Confirm
                                                            Password</label>
                                                        <input type="password"
                                                            style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;"
                                                            class="form-control" name="password_confirmation"
                                                            placeholder="Confirm Password">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text"
                                                            style="color:#757575;font-size:15px;margin-bottom:0px">Referal
                                                            Code</label>
                                                        <input type="text"
                                                            style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;"
                                                            class="form-control referal" name="refer"
                                                            placeholder="Optional">
                                                    </div>
                                                    <div><button type="submit" class="btn btn-block"
                                                            style="background-color:#2F80ED;color:white;padding:10px 40px;border-radius:8px;"
                                                            id="btn-submit">Sign
                                                            Up</button></div>
                                                </form>

                                                <div style="color:#757575;text-align:center;font-size:12px"
                                                    class="mt-3">Already have
                                                    an account? &nbsp;&nbsp; <span
                                                        style="font-weight:bold;color:#2F80ED;cursor:pointer;"
                                                        data-toggle="modal" data-dismiss="modal"
                                                        data-target="#modal-signin">Sign In</span></span></div>
                                                <!--<hr>
                                                <div><button class="btn btn-block" style="background-color:white;border:1px solid #cecece;font-size:14px;padding:10px 40px;border-radius:8px;">
                                                        <img src="https://lautan.s3.amazonaws.com/webcontent/media_QXFyc6.png" style="height:15px;margin-top:-5px;color:#404040;">&nbsp;&nbsp;&nbsp; Sign Up with Google</button>
                                                </div>-->

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!--Scroll to top-->
    <button class="scroll-top scroll-to-target" data-target="html">
        <span class="fa fa-arrow-up"></span>
    </button>


    <!-- jequery plugins -->
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/jquery.js"></script>
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/popper.min.js"></script>
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/owl.js"></script>
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/wow.js"></script>
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/validation.js"></script>
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/jquery.fancybox.js"></script>
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/appear.js"></script>
    <script src="<?php echo e(asset('public/Appway2')); ?>/js/scrollbar.js"></script>
    <!--<script src="<?php echo e(asset('public/Appway2')); ?>/js/jquery.paroller.min.js"></script>-->
    <!--<script src="<?php echo e(asset('public/Appway2')); ?>/js/tilt.jquery.js"></script>-->
    <script src="<?php echo e(asset('public')); ?>/appway/js/circle-progress.js"></script>
    <script src="<?php echo e(asset('public')); ?>/appway/js/jquery.countTo.js"></script>
    <script src="<?php echo e(asset('public')); ?>/appway/js/scrollbar.js"></script>
    <script src="<?php echo e(asset('public')); ?>/appway/js/jquery.paroller.min.js"></script>
    <script src="<?php echo e(asset('public')); ?>/appway/js/tilt.jquery.js"></script>
    <script src="<?php echo e(asset('public')); ?>/appway/js/select2.full.js"></script>
    <!-- main-js -->
    <script src="<?php echo e(asset('public')); ?>/appway/js/script.js"></script>
    <script>
        function displayCountryCode() {
            var countrycode = document.getElementById("phonecode");
            countrycode.options[countrycode.selectedIndex].text = countrycode.value;
            //   console.log(countrycode.value);
        }

        // document ready function
        $(document).ready(function() {
            $('#loginForm').on('submit', function(e) {
                e.preventDefault();
                clear_error();
                disable_login();
                var formData = $(this).serialize();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "<?php echo e(url('login')); ?>",
                    type: "POST",
                    data: formData,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 401) {
                            $('.error-alert').removeClass('d-none');
                            $('.success-alert').addClass('d-none');
                            var err_val = `<b>Validation error!</b>`;
                            $.each(data.errors, function(key, value) {
                                err_val += `<br>${value}`;
                            });
                            $('.error-text').html(err_val);
                            undisable_login();
                        } else if (data.status == 402) {
                            $('.error-alert').removeClass('d-none');
                            $('.success-alert').addClass('d-none');
                            $('.error-text').html(`<b>Error!</b> <br>${data.errors}`);
                            undisable_login();
                        } else {
                            $('.error-alert').addClass('d-none');
                            $('.success-alert').removeClass('d-none');
                            undisable_login();
                            $('.success-text').html(
                                `<b>Success!</b> <br>${data.message} <br>Redirecting...`);
                            setTimeout(function() {
                                window.location.href = "<?php echo e(url('dashboard')); ?>";
                            }, 2000);
                        }
                    },
                });
            });

            function clear_error() {
                $('.error-alert').addClass('d-none');
                $('.success-alert').addClass('d-none');
            }

            function disable_login() {
                $('login-btn').attr('disabled', true);
                $('login-btn').html('Processing...');
            }

            function undisable_login() {
                $('login-btn').attr('disabled', false);
                $('login-btn').html('Login');
            }

            // =========================================


            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $('.referal').on('keyup', function() {
                var referal = $(this).val();
                console.log(referal);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: `<?php echo e(url('reg')); ?>/${referal}`,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 'success') {
                            $('.referal').css('border', '1px solid #42ba96');
                            $('.referal').css('background-color', '#F5F5F5');
                        } else {
                            $('.referal').css('border', '1px solid red');
                            $('.referal').css('background-color', '#F5F5F5');
                        }
                    }
                })
            })

            $('#register').on('submit', function(e) {
                e.preventDefault();
                clear_error();
                disable_submit();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "<?php echo e(url('register')); ?>",
                    type: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if (data.status == 401) {
                            $('.error-alert').removeClass('d-none');
                            var msg = '<b>Validation Error</b>';
                            $.each(data.errors, function(key, value) {
                                msg += '<br>' + value;
                            });
                            $('.error-text').html(msg);
                            undisable_submit();
                        } else if (data.status == 201) {
                            $('.success-alert').removeClass('d-none');
                            $('.success-text').html(data.message);
                        } else {
                            $('.error-alert').removeClass('d-none');
                            $('.error-text').html(data.message);
                            undisable_submit();
                        }
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            });

            function disable_submit() {
                $('#btn-submit').attr('disabled', true);
                $('#btn-submit').html('Processing...');
            }

            function undisable_submit() {
                $('#btn-submit').attr('disabled', false);
                $('#btn-submit').html('Create Account');
            }

            function clear_error() {
                $('.error-alert').addClass('d-none');
            }


        });
    </script>
    <!-- main-js -->
    <!--<script src="<?php echo e(asset('public/Appway2')); ?>/js/script.js"></script>-->

    <?php echo $__env->yieldContent('js'); ?>
</body><!-- End of .page_wrapper -->

</html>
<?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/land/master.blade.php ENDPATH**/ ?>
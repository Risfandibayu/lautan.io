<?php $__env->startSection('content'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/codemirror.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/codemirror.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/mode/python/python.min.js"></script>

<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        
                        List Pages
                    </h1>
                </div>
                <div class="col-auto mb-3">
                    <button class="btn btn-success btn-sm btnmedia" onclick="btnaction()">Add Pages</button>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main page content-->
<div class="container-xl">
    <?php if(session('error')): ?>
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('error')); ?>

        </div>
    </div>
    <?php endif; ?>

    <?php if(session('success')): ?>
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            <?php echo e(session('success')); ?>

        </div>
    </div>
    <?php endif; ?>


    <div class="card">
        <div class="card-body">

            <div id="form" style="display: none">
                <form method="POST" action="<?php echo e(route('webadmin.addpages')); ?>">
                    <?php echo csrf_field(); ?>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Title</label>
                            <input class="form-control" type="text" name="title" id="title" onkeyup="copyslug()" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">slug</label>
                            <input class="form-control" type="text" name="slug" id="slug" required readonly />
                        </div>
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Content</label>
                            <div style="border:1px solid #ced4da">
                                <textarea type="text" class="form-control" name="content" id="editor" rows="10">Hello Brother</textarea>
                            </div>
                        </div>	
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Status</label>
                            <select class="form-control" name="status">
                                <option value="1">Active</option>
                                <option value="0">No Active</option>
                            </select>
                        </div>	
                        <hr class="my-4" />
                        <div class="col-md-6">
                            <button class="btn btn-danger btn-sm" onclick="btnaction()">Cancel</button>
                            <button class="btn btn-primary btn-sm" type="submit">Save Pages</button>
                        </div>
                    </div>
                    
                </form>
            </div>
            
            <div id="data">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Slug</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__empty_1 = true; $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <tr>
                            <td><?php echo e($row->id); ?></td>
                            <td><?php echo e($row->title); ?></td>
                            <td><?php echo e($row->slug); ?></td>
                            <td>
                                <?php if($row->status == 1): ?> <span class="text-success">Active</span>
                                <?php else: ?> <span class="text-danger">No Active</span>
                                <?php endif; ?>
                            </td>
                            <td>
                                <a href="/webadmin/pages/edit/<?php echo e($row->id); ?>"><i class="fa fa-edit" style="color:green"></i></a>

                                <?php if($row->status == 1): ?>
                                <a href="/webadmin/pages/noactive/<?php echo e($row->id); ?>"><i class="fa fa-eye" style="color:red"></i></a>
                                <?php else: ?>
                                <a href="/webadmin/pages/active/<?php echo e($row->id); ?>"><i class="fa fa-eye" style="color:green"></i></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <tr><td colspan="3">Data Not Found</td></tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<script>
var i=0;
function btnaction() {
    i++;
    $("#form").toggle();
    $("#data").toggle();

    $("#title").val('');
    $("#slug").val('');
    
    if(i==1){
    window.myCodeMirror = CodeMirror.fromTextArea(document.getElementById("editor"), {
        lineNumbers: true,
        mode: 'phyton',
    });
    } else {
        var editor = $('.CodeMirror')[0].CodeMirror;
        editor.setValue("Hello Brother");
        editor.refresh();
    }
}

function copyslug()
{
    var str = $("#title").val();
    str = str.replaceAll(" ", '-');
    str = str.toLowerCase();
    $("#slug").val(str);
}
</script>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH G:\exlaut-staging\resources\views/webadmin/pages.blade.php ENDPATH**/ ?>
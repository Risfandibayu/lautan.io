<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <title>Lautan</title>
    <link href="<?php echo e(asset('public')); ?>/assets/css/styles.css" rel="stylesheet" />
    <link rel="shortcut icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
    <script data-search-pseudo-elements="" defer=""
        src="<?php echo e(asset('public')); ?>/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
</head>

<body>
    <?php echo $__env->yieldContent('content'); ?>

    <script src="<?php echo e(asset('public/Appway2')); ?>/js/jquery.js"></script>
    <script src="<?php echo e(asset('public')); ?>/assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?php echo e(asset('public')); ?>/assets/js/scripts.js"></script>
    <?php echo $__env->yieldContent('script'); ?>
</body>

</html>
<?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/master.blade.php ENDPATH**/ ?>
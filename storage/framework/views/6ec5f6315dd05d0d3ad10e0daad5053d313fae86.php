
<?php $__env->startSection('content'); ?>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            User
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-xl px-4 mt-4">
        <nav class="nav nav-borders">
            <a class="nav-link ms-0 active" href="<?php echo e(url('user') . '/' . $row->id); ?>">Info Akun</a>
            <a class="nav-link" href="<?php echo e(url('user/verification') . '/' . $row->id); ?>">Data Verifikasi</a>
            <a class="nav-link" href="<?php echo e(url('user/assets') . '/' . $row->id); ?>">Data Assets</a>
        </nav>
        <hr class="mt-0 mb-4" />
        <div class="row">
            <div class="col-xl-4">
                <!-- Profile picture card-->
                <div class="card mb-4 mb-xl-0">
                    <div class="card-header">Foto Akun</div>
                    <form class="card-body text-center" action="/user/avatar/<?php echo e($row->id); ?>" method="POST"
                        enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <!-- Profile picture image-->
                        <img class="img-account-profile rounded-circle mb-2" style="width: auto;"
                            src="<?php echo e($row->getAvatar()); ?>" />
                        <!-- Profile picture help block-->
                        <div class="small font-italic text-muted">JPG atau PNG maksimal 2 MB</div>
                        <div class="mb-3">
                            <input class="form-control form-control-sm" id="formFileSm" type="file" name="avatar">
                        </div>
                        <!-- Profile picture upload button-->
                        <button class="btn btn-primary" type="submit">Upload Gambar</button>
                    </form>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="card mb-4">
                    <div class="card-header">Detail Akun</div>
                    <div class="card-body">
                        <p class="small text-muted mb-0">Nama</p>
                        <h5><?php echo e($row->name); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">Email</p>
                        <h5><?php echo e($row->email); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">No. Telepon</p>
                        <h5><?php echo e($row->phone); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">Status</p>
                        <h5><?php echo $row->getStatusLabel(); ?></h5>
                    </div>
                    <div class="card-footer">
                        <a href="/user/<?php echo e($row->id); ?>/edit" class="btn btn-light">Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/user/show.blade.php ENDPATH**/ ?>
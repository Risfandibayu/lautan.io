
<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
    });
} );
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="circle"></i></div>
                        Assets Management
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl">
    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Coin</th>
                        <th scope="col">Name</th>
                        <th scope="col" class="text-end">Total Balance</th>
                        <th scope="col" class="text-end">Available Balance</th>
                        <th scope="col" class="text-end">Est. IDR Value</th>
                        <th scope="col">Status</th>
                        <th scope="col" width="200px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php 
                        $blc = App\Models\Balance::where('user_id',Auth::id())->where('currency_id',$rows->id)->where('active',1)->orderBy('id','DESC')->first();
                        if($blc) {
                            $binance                 = NEW App\Libs\Binance;
                            $send['url']             = '/ticker/24hr';
                            $body['symbol']          = $rows->code;
                            $send['body']            = $body; 
                            $post                    = $binance->callApi($send);

                            $balance = $blc->amount;
                            $balanceIDR = $post->lastPrice * $blc->amount;
                        } else {
                            $balance = '0.000000';
                            $balanceIDR = 0;
                        }
                    ?>
                    <tr>
                        <td><img src="<?php echo e($rows->image); ?>" width="20px"></td>
                        <td><?php echo e($rows->code); ?></td>
                        <td><?php echo e($rows->name); ?></td>
                        <td class="text-end"><?php echo e($balance); ?></td>
                        <td class="text-end"><?php echo e($balance); ?></td>
                        <td class="text-end"><?php echo e($balanceIDR); ?></td>
                        <td class="text-success">online</td>
                        <td class="text-right">
                            <a href="<?php echo e(url('deposit')); ?>?coin=<?php echo e($rows->code); ?>" class="btn btn-sm btn-outline-primary"><i data-feather="upload"></i>&nbsp; Deposit</a>
                            <a href="<?php echo e(url('withdraw')); ?>?coin=<?php echo e($rows->code); ?>" class="btn btn-sm btn-outline-primary"><i data-feather="download"></i>&nbsp; Withdraw</a>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/balance/index.blade.php ENDPATH**/ ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="/landing-assets/css/style.css" rel="stylesheet">
    <title>Lautan.io xxx</title>
  </head>
  <body>
    <header>
    <div class="top">
        <div class="container">
            <a href="mailto:support@lautan.io" target="_blank"><i class="fe fe-mail"></i> support@lautan.io</a>
            <a href="https://t.me/lautan" target="_blank"><i class="fe fe-send"></i> t.me/lautan</a>
        </div>
    </div>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <div class="d-flex flex-grow-1">
                <a class="navbar-brand d-lg-inline-block" href="/"> <img src="/landing-assets/images/logo.png" alt=""> </a>
                <div class="w-100 text-end">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#myNavbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
            <div class="collapse navbar-collapse flex-grow-1 text-end" id="myNavbar">
                <ul class="navbar-nav ms-auto flex-nowrap">
                    <li class="nav-item nav-active">
                        <a href="/" class="nav-link m-2 menu-item">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link m-2 menu-item">Buy</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link m-2 menu-item">Sell</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link m-2 menu-item">Contact</a>
                    </li>

                    <li class="nav-item login">
                        <a href="/login" class="nav-link m-2 menu-item">Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    </header>

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active" style="background-image:url('/landing-assets/images/slider1.jpg')">
                <div class="layer"></div>
                <div class="carousel-caption d-md-block">
                    <h1>The <span class="orange">1st</span> digital exchange aggregator platform company in <span class="red">Indo</span>nesia</h1>
                    <p>Get an affordable, faster, professional exchanger wherever you are, whenever you are</p>
                    <button id="btn-buy" class="btn btn-primary buy" style="margin-right: 15px;">Buy</button>
                    <button id="btn-sell" class="btn btn-primary sell">Sell</button>
                </div>
            </div>
            <div class="carousel-item" style="background-image:url('/landing-assets/images/slider2.jpg')">
                <div class="layer"></div>
                <div class="carousel-caption d-md-block">
                    <h1>The <span class="orange">1st</span> digital exchange aggregator platform company in <span class="red">Indo</span>nesia</h1>
                    <p>Get an affordable, faster, professional exchanger wherever you are, whenever you are</p>
                    <button id="btn-buy" class="btn btn-primary buy" style="margin-right: 15px;">Buy</button>
                    <button id="btn-sell" class="btn btn-primary sell">Sell</button>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="container">
        <div class="card mb-5">
            <div class="card-body">
            <form>
                <div>
                    <input type="number" class="form-control" value="1">
                </div>
                <div class="mb-3">
                    <select class="form-select">
                        <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($rows->code); ?>"><?php echo e($rows->code); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div>
                    <input type="number" class="form-control" value="540890616">
                </div>
                <div class="mb-3">
                    <select class="form-select">
                        <option selected>IDR</option>
                    </select>
                </div>
                <div class="mb-3 text-center">
                    <button type="button" id="btn-form-buy" class="btn btn-primary buy" data-bs-toggle="modal" data-bs-target="#exampleModal">Buy</button>
                    <button type="button" id="btn-form-sell" style="display:none;" class="btn btn-primary sell" data-bs-toggle="modal" data-bs-target="#exampleModal">Sell</button>
                </div>
                
                </form>
            </div>
        </div>
    </div>

    <div class="bar-footer d-sm-block d-md-none">
        <i class="fe fe-x-circle" id="close"></i>
        <a href="/login" class="btn btn-primary login" style="margin-right: 15px;">Login</a>
        <a href="/register" class="btn btn-primary register">Register</a>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Under Development
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('#close').click(function(e){
                $('.bar-footer').hide();
            });

            $('#btn-buy').click(function(e){
                $('#btn-form-buy').show();
                $('#btn-form-sell').hide();
            });

            $('#btn-sell').click(function(e){
                $('#btn-form-sell').show();
                $('#btn-form-buy').hide();
            });
        });
    </script>
  </body>
</html><?php /**PATH G:\exlaut-staging\resources\views/landing.blade.php ENDPATH**/ ?>
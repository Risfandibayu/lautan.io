
<?php $__env->startSection('content'); ?>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            User
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-xl px-4 mt-4">
        <nav class="nav nav-borders">
            <a class="nav-link ms-0" href="<?php echo e(url('user') . '/' . $row->id); ?>">Info Akun</a>
            <a class="nav-link active" href="<?php echo e(url('user/verification') . '/' . $row->id); ?>">Data Verifikasi</a>
            <a class="nav-link" href="<?php echo e(url('user/assets') . '/' . $row->id); ?>">Data Assets</a>
        </nav>
        <hr class="mt-0 mb-4" />
        <div class="row">
            <div class="col-xl-6">
                <?php if($row->agentStatus == 1): ?>
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <p>This user wants to be an agent</p>
                                    <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#alertApprove"
                                        data-id="<?php echo e($row->id); ?>" data-status="3"
                                        data-name="Approve verifikasi agent <?php echo e($row->name); ?>?">Approve</button>
                                    <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#alertReject"
                                        data-id="<?php echo e($row->id); ?>" data-status="4"
                                        data-name="Reject verifikasi agent <?php echo e($row->name); ?>?">Reject</button>
                                </div>
                                <div class="col-md-4 d-flex justify-content-center text-center">
                                    <a href="<?php echo e($row->fileKuisioner); ?>" class="text-center text-dark"
                                        style="text-decoration: none" target="_blank">
                                        <img src="<?php echo e(asset('public/assets/icon/file.svg')); ?>" alt="file"
                                            style="width: 50%">
                                        <br>
                                        See Kuisioner
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="card mb-4">
                    <div class="card-header">Detail Akun</div>
                    <div class="card-body">
                        <p class="small text-muted mb-0">Nama</p>
                        <h5><?php echo e($row->name); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">Email</p>
                        <h5><?php echo e($row->email); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">No. Telepon</p>
                        <h5><?php echo e($row->phone); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">No. KTP</p>
                        <h5><?php echo e($row->nationalId); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">Alamat</p>
                        <h5><?php echo e($row->address); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">Status</p>
                        <h5><?php echo $row->getStatusLabel(); ?></h5>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-header">Data Bank</div>
                    <div class="card-body">
                        <p class="small text-muted mb-0">Nama Bank</p>
                        <h5><?php echo e($row->bankName); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">Nama Akun Bank</p>
                        <h5><?php echo e($row->bankAccount); ?></h5>
                        <hr class="my-3" />
                        <p class="small text-muted mb-0">No. Rekening</p>
                        <h5><?php echo e($row->bankNumber); ?></h5>
                    </div>
                </div>
                <?php if($row->status == 2): ?>
                    <div class="card mb-4">
                        <div class="card-body">
                            <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#alertModal"
                                data-id="<?php echo e($row->id); ?>" data-status="3"
                                data-name="Approve verifikasi user <?php echo e($row->name); ?>?">Approve</button>
                            <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#alertModal"
                                data-id="<?php echo e($row->id); ?>" data-status="4"
                                data-name="Reject verifikasi user <?php echo e($row->name); ?>?">Reject</button>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">File Verifikasi</div>
                    <div class="card-body row">
                        
                        <div class="col-md-6">
                            <p class="small text-muted mb-0 text-center mb-2">Foto KTP</p>
                            <div class="pic getMoodal" data-bs-toggle="modal" data-bs-target="#modal1"
                                data-src="<?php echo e($row->fileNationalId); ?>" data-name="Foto Ktp">

                                <img src="<?php echo e($row->fileNationalId); ?>" class="file-img">
                                <div class="overlay">
                                    <div class="info">
                                        <i class="fas fa-eye text-light display-6 "></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p class="small text-muted mb-0 text-center mb-2">Foto Selfie</p>
                            <div class="pic getMoodal" data-bs-toggle="modal" data-bs-target="#modal1"
                                data-src="<?php echo e($row->filePhoto); ?>" data-name="Foto Selfie">

                                <img src="<?php echo e($row->filePhoto); ?>" class="file-img">
                                <div class="overlay">
                                    <div class="info ">
                                        <i class="fas fa-eye text-light display-6"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">

            <!--Content-->
            <div class="modal-content">

                <!--Body-->
                <div class="modal-body mb-0 p-0">

                    <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                        <img src="" alt="ktp" class="file-img" id="modalImage">
                    </div>

                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <span class="mr-4 h3" id="modalTitle"></span>
                </div>

            </div>
            <!--/.Content-->

        </div>
    </div>

    <form method="POST" action="<?php echo e(url('/user/verification')); ?>" class="modal fade" id="alertModal" tabindex="-1"
        role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
        <?php echo csrf_field(); ?>
        <input name="id" type="hidden" id="user-id">
        <input name="status" type="hidden" id="user-status">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center" id="user-name">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-success">Ya</button>
                </div>
            </div>
        </div>
    </form>

    <form method="POST" action="<?php echo e(url('/agent/reject')); ?>" class="modal fade" id="alertReject" tabindex="-1"
        role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
        <?php echo csrf_field(); ?>
        <input name="id" type="hidden" id="rej-id">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center" id="rej-name">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-success">Ya</button>
                </div>
            </div>
        </div>
    </form>

    <form method="POST" action="<?php echo e(url('agent/approve')); ?>" class="modal fade" id="alertApprove" tabindex="-1"
        role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
        <?php echo csrf_field(); ?>
        <input name="id" type="hidden" id="app-id">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Approve Agent</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label class="small mb-1">Agent Role</label>
                    <select class="form-select form-control-solid" name="role">
                        <?php $__currentLoopData = $role; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($rows->id); ?>"><?php echo e($rows->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Process</button>
                </div>
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
    <script type="text/javascript">
        $('.getMoodal').on('click', function() {
            var url = $(this).data('src');
            var name = $(this).data('name');
            $('#modalImage').attr('src', url);
            $('#modalTitle').html(name);
            console.log(url);
            console.log(name);
        })
        $(".pic").hover(
            function() {
                $(".info", this).css("display", "block");
            },
            function() {
                $(".info", this).css("display", "none");
            });
        $('#alertModal').on('show.bs.modal', function(event) {
            var modal = $(this);
            var button = $(event.relatedTarget);
            $('#user-id').val(button.data('id'));
            $('#user-name').html(button.data('name'));
            $('#user-status').val(button.data('status'));
        });

        $('#alertReject').on('show.bs.modal', function(event) {
            var modal = $(this);
            var button = $(event.relatedTarget);
            $('#rej-id').val(button.data('id'));
            $('#rej-name').html(button.data('name'));
        });

        $('#alertApprove').on('show.bs.modal', function(event) {
            var modal = $(this);
            var button = $(event.relatedTarget);
            $('#app-id').val(button.data('id'));
            $('#app-name').html(button.data('name'));
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/user/verification.blade.php ENDPATH**/ ?>
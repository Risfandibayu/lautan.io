<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Exlaut</title>
        <link href="<?php echo e(asset('public')); ?>/assets/css/styles.css" rel="stylesheet" />
        <link rel="shortcut icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo e(asset('public')); ?>/favicon.ico" type="image/x-icon">
        <script data-search-pseudo-elements="" defer="" src="<?php echo e(asset('public')); ?>/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    </head>
    <body>
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4 mt-5">
                        <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-9">
                                <!-- Social registration form-->
                                <div class="card my-5">
                                    <div class="card-body p-5 text-center">
                                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/logo-nav.png" style="height:50px;width:auto;"></a>
                                    </div>
                                    <hr class="my-0" />
                                    <div class="card-body p-5">
                                    <?php if(session('error')): ?>
                                    <div class="alert alert-danger alert-icon" role="alert">
                                        <div class="alert-icon-aside">
                                            <i data-feather="alert-triangle"></i>
                                        </div>
                                        <div class="alert-icon-content">
                                            <?php echo e(session('error')); ?>

                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if(session('success')): ?>
                                    <div class="alert alert-success alert-icon" role="alert">
                                        <div class="alert-icon-aside">
                                            <i data-feather="check-circle"></i>
                                        </div>
                                        <div class="alert-icon-content">
                                            <?php echo e(session('success')); ?>

                                        </div>
                                    </div>
                                    <?php endif; ?>
                                        <!-- Register form-->
                                        <form method="POST" action="<?php echo e(url('register')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <!-- Form Row-->
                                            <div class="row gx-3">
                                                <div class="col-md-6">
                                                    <!-- Form Group (first name)-->
                                                    <div class="mb-3">
                                                        <label class="text-gray-600 small">Name</label>
                                                        <input class="form-control form-control-solid" type="text" placeholder="Please input your name" aria-label="Name" name="name" value="<?php echo e(old('name')); ?>" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <!-- Form Group (last name)-->
                                                    <div class="mb-3">
                                                        <label class="text-gray-600 small">Phone</label>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <select class="form-control form-control-solid" name="country">
                                                                    <option value="+62">+62</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input class="form-control form-control-solid" type="text" placeholder="Please input your phone number" aria-label="Phone" name="phone" value="<?php echo e(old('phone')); ?>" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Group (email address)-->
                                            <div class="mb-3">
                                                <label class="text-gray-600 small">Email address</label>
                                                <input class="form-control form-control-solid" type="email" placeholder="Please input your email address" aria-label="Email Address" name="email" value="<?php echo e(old('email')); ?>" required />
                                            </div>
                                            <!-- Form Row-->
                                            <div class="row gx-3">
                                                <div class="col-md-6">
                                                    <!-- Form Group (choose password)-->
                                                    <div class="mb-3">
                                                        <label class="text-gray-600 small">Password</label>
                                                        <input class="form-control form-control-solid" type="password" placeholder="" aria-label="Password" name="password" value="<?php echo e(old('password')); ?>" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <!-- Form Group (confirm password)-->
                                                    <div class="mb-3">
                                                        <label class="text-gray-600 small">Confirm Password</label>
                                                        <input class="form-control form-control-solid" type="password" placeholder="" aria-label="Confirm Password" name="repassword" value="<?php echo e(old('repassword')); ?>" required />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Group (form submission)-->
                                            <div class="d-flex align-items-center justify-content-between">
                                                <div class="form-check">
                                                    <input class="form-check-input" id="checkTerms" type="checkbox" value="" required />
                                                    <label class="form-check-label" for="checkTerms">
                                                        I accept the
                                                        <a href="#!">terms &amp; conditions</a>
                                                        .
                                                    </label>
                                                </div>
                                                <button type="submit" class="btn btn-primary">Create Account</button>
                                            </div>
                                        </form>
                                    </div>
                                    <hr class="my-0" />
                                    <div class="card-body px-5 py-4">
                                        <div class="small text-center">
                                            Have an account?
                                            <a href="/login">Sign in!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer-admin mt-auto footer-dark">
                    <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">Copyright © Exlaut <?php echo e(date("Y")); ?></div>
                            <div class="col-md-6 text-md-end small">
                                <a href="#!">Privacy Policy</a>
                                ·
                                <a href="#!">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="<?php echo e(asset('public')); ?>/assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo e(asset('public')); ?>/assets/js/scripts.js"></script>
</body>

</html>
<?php /**PATH /home/lautanio/dev.lautan.io/resources/views/register.blade.php ENDPATH**/ ?>
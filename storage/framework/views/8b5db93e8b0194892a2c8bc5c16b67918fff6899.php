
<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                responsive: true,
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            User
                        </h1>
                    </div>
                    <!-- <div class="col-12 col-xl-auto mb-3"><a href="/user/create" class="btn btn-sm btn-light"><i data-feather="plus"></i>&nbsp; Tambah User</a></div> -->
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-xl px-4 mt-4">
        <div class="card">
            <!-- <div class="card-header">Example Card</div> -->
            <div class="card-body">
                <table class="table" id="table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Status</th>
                            <th scope="col" style="width:130px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="avatar avatar-lg align-middle mt-2 mb-2"><img src="<?php echo e($rows->getAvatar()); ?>"
                                        class="avatar-img"></td>
                                <td class="align-middle"><?php echo e($rows->name); ?></td>
                                <td class="align-middle"><?php echo e($rows->email); ?></td>
                                <td class="align-middle"><?php echo e($rows->phone); ?></td>
                                <td class="align-middle"><?php echo $rows->getStatusLabel(); ?></td>
                                <td class="text-right align-middle">
                                    <div class="btn-group">
                                        <a href="/user/<?php echo e($rows->id); ?>" class="btn btn-light btn-sm"><i
                                                data-feather="eye"></i></a>
                                        <a href="/user/<?php echo e($rows->id); ?>/edit" class="btn btn-light btn-sm"><i
                                                data-feather="edit"></i></a>
                                        <button class="btn btn-danger btn-sm" data-bs-toggle="modal"
                                            data-bs-target="#alertModal" data-id="<?php echo e($rows->id); ?>"
                                            data-name="<?php echo e($rows->name); ?>"><i data-feather="trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <form method="POST" action="/user/del" class="modal fade" id="alertModal" tabindex="-1" role="dialog"
        aria-labelledby="alertModalLabel" aria-hidden="true">
        <?php echo csrf_field(); ?>
        <input name="_method" type="hidden" value="DELETE">
        <input name="id" type="hidden" id="user-id">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                    Yakin akan menonaktifkan user <span id="user-name"></span> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
    <script type="text/javascript">
        $('#alertModal').on('show.bs.modal', function(event) {
            var modal = $(this);
            var button = $(event.relatedTarget);
            $('#user-id').val(button.data('id'));
            $('#user-name').html(button.data('name'));
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/user/index.blade.php ENDPATH**/ ?>
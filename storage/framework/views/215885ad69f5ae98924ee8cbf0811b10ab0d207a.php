
<?php $__env->startSection('content'); ?>
    <header class="page-header page-header-compact page-header-light bg-white mt-3">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="users"></i></div>
                            Partner
                        </h1>
                    </div>
                    <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-xl">

        <div class="card p-3">
            <p>The partnership program is not only profitable but also as transparent as possible - detailed statistics and
                the number of referrals for each level are available in the personal account of each participant in the
                "partner dashboard" tab.<br><br>

                In order to join our partnership program, you must register on the site or enter your personal account.</p>
            <a href="#" class="btn-req btn btn-primary">I want to be an Partner</a>
        </div>
    </div>
    <!-- Modal Confirm -->
    <div class="modal fade" id="reqModal" tabindex="-1" role="dialog" aria-hidden="true">
        <form class="modal-dialog" role="document" method="POST" action="<?php echo e(route('agent.req')); ?>"
            enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Partner Request</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>In order to join our partnership program, you must register on the site or enter your personal
                        account.<br><br>
                        Please complete the following <a href="https://form.jotform.com/212988569312467"
                            target="_blank">Questionnaire</a>
                        and upload the file when it's
                        finished
                        <br>
                    </p>
                    <input type="file" name="fileKuisioner" id="file" class="form-control"
                        accept="application/pdf,.doc, .docx">
                </div>
                <div class="modal-footer">
                    
                    <input type="submit" value="Send Request" class="btn btn-primary" disabled>
                </div>
            </div>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
    <script type="text/javascript">
        $('.btn-req').click(function() {
            $('#reqModal').modal('show');
        });

        $('input:file').change(
            function() {
                if ($(this).val()) {
                    $('input:submit').removeAttr('disabled');
                    // $('.btn-send').removeAttr('disabled')
                } else {
                    // $('.btn-send').attr('disabled', true)
                    $('input:submit').attr('disabled', true);
                }
            }
        )
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/agent/landing.blade.php ENDPATH**/ ?>
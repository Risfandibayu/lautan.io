<?php $__env->startSection('content'); ?>
 <section class="security-invisible"   style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;padding-top:180px;">
        <div class="container">
           <div class="sec-titlex" style="text-align: center;">
            <h2 style="font-size:40px;">Career</h2>
        </div>
        <div class="row" style="margin-top:50px;margin-bottom:50px;">
            <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px;">
                <div style="border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                    <a target="_blank" href="https://www.linkedin.com/posts/pt-lautan-harum-mewangi_fullstackdeveloper-activity-6858257098996891648-L9aF">
                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/cr/fs.jpg"  alt="" ></a>
                    <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">Fullstack Developer</div>
                    <!--<p style="font-size:16px;line-height:20px;">Withdrawable from any ATM all around Indonesia</p>-->
                </div>
            </div>
             <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px;">
                <div style="border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                    <a target="_blank" href="https://www.linkedin.com/posts/pt-lautan-harum-mewangi_accountingofficer-activity-6858256980012859393-OsTY">
                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/cr/ac.jpg"  alt="" ></a>
                    <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">Accounting Officer</div>
                    <!--<p style="font-size:16px;line-height:20px;">Withdrawable from any ATM all around Indonesia</p>-->
                </div>
            </div>
             <div class="col-lg-4 col-md-12 col-sm-12" style="padding:15px;">
                <div style="border:1px solid #EFEFEF;padding:25px;box-shadow: 5px 15px 27px rgba(0, 0, 0, 0.1);">
                    <a target="_blank" href="https://www.linkedin.com/posts/pt-lautan-harum-mewangi_businessdevelopment-activity-6858256705009123328-dWQa">
                    <img src="<?php echo e(asset('public/Appway2')); ?>/images/cr/bs.jpg"  alt="" ></a>
                    <div style="font-size: 18px;font-weight:700;color:#333333;text-align:center;">Business Development Officer</div>
                    <!--<p style="font-size:16px;line-height:20px;">Withdrawable from any ATM all around Indonesia</p>-->
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- banner-section -->
   
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/land/career.blade.php ENDPATH**/ ?>
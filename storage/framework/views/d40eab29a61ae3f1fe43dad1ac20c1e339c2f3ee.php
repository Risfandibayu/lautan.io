
<?php $__env->startSection('content'); ?>
<!-- <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Akun
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header> -->
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <nav class="nav nav-borders">
        <a class="nav-link ms-0" href="<?php echo e(url('account')); ?>">Account</a>
        <!-- <a class="nav-link" href="/tokocrypto">Tokocrypto API</a> -->
        <a class="nav-link active" href="<?php echo e(url('password')); ?>">Change Password</a>
    </nav>
    <hr class="mt-0 mb-4" />
    <div class="row">
        <div class="col-xl-8">
            <div class="card mb-4">
                <div class="card-header">Change Password</div>
                <div class="card-body">
                    <?php if(session('error')): ?>
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('error')); ?>

                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(session('success')): ?>
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('success')); ?>

                        </div>
                    </div>
                    <?php endif; ?>
                    <form method="POST" action="<?php echo e(url('password')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="mb-3">
                            <label class="small mb-1">Old Password</label>
                            <input class="form-control form-control-solid" type="password" placeholder="" name="oldpassword" value="<?php echo e(old('oldpassword')); ?>" required />
                        </div>
                        <div class="mb-3">
                            <label class="small mb-1">New Password</label>
                            <input class="form-control form-control-solid" type="password" placeholder="" name="password" value="<?php echo e(old('password')); ?>" required />
                        </div>
                        <div class="mb-3">
                            <label class="small mb-1">RePassword</label>
                            <input class="form-control form-control-solid" type="password" placeholder="" name="repassword" value="<?php echo e(old('repassword')); ?>" required />
                        </div>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\lautan.io\resources\views/account/password.blade.php ENDPATH**/ ?>
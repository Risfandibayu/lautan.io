@extends('index')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
        "dom": 't'
    });
} );
</script>
@endsection
@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Partner Roles
                    </h1>
                </div>
                @if(count($row) < 5)
                <div class="col-12 col-xl-auto mb-3"><a href="/agentrole/create" class="btn btn-sm btn-light"><i data-feather="plus"></i>&nbsp; Tambah Roles</a></div>
                @endif
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            @if(count($row))
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Fees</th>
                        <th scope="col" style="width:130px;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $rows)
                    <tr>
                        <td class="align-middle">{{ $rows->name }}</td>
                        <td class="align-middle">{{ $rows->fees }}%</td>
                        <td class="text-right align-middle">
                        <div class="btn-group">
                            <a href="/agentrole/{{ $rows->id }}/edit" class="btn btn-light btn-sm"><i data-feather="edit"></i></a>
                            <button class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#alertModal" data-id="{{ $rows->id }}" data-name="{{ $rows->name }}"><i data-feather="trash"></i></button>
                        </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else 
            <p class="text-center"><small>Tidak ada data partner role<small></p>
            @endif
        </div>
    </div>
</div>
<form method="POST" action="/agentrole/del" class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
        @csrf
        <input name="_method" type="hidden" value="DELETE">
        <input name="id" type="hidden" id="user-id">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                Yakin akan menonaktifkan role <span id="user-name"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-danger">Ya</button>
            </div>
            </div>
        </div>
    </form>
@endsection
@push('script')
<script type="text/javascript">
	$('#alertModal').on('show.bs.modal', function(event) {
        var modal 	= $(this);
        var button 	= $(event.relatedTarget);
		$('#user-id').val(button.data('id'));
        $('#user-name').html(button.data('name'));
    });
</script>
@endpush
                
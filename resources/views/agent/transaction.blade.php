@extends('index')
@section('content')
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="users"></i></div>
                            Partner
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container-xl px-4 mt-4">

        <div class="card">
            <div class="card-header">Transaction withdraw detail for <b>{{ $user->name }}</b> </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Date</th>
                                <th scope="col" class="text-center">BNB Coin</th>
                                <th scope="col" class="text-center">IDR Value</th>
                                <th scope="col" class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_idr = 0;
                            $total_bnb = 0; ?>
                            @foreach ($transaction as $trx)
                                <tr>
                                    <td class="align-middle">
                                        {{ $loop->iteration }}
                                    </td>
                                    <td>
                                        {{-- date format --}}
                                        {{ date('d-m-Y', strtotime($trx->updated_at)) }}
                                    </td>
                                    <td class="text-end">
                                        {{ sprintf('%.8f', floatval($trx->amount_bnb)) . ' BNB' }}
                                    </td>
                                    <td class="text-end">
                                        <div class="row">
                                            <div class="col-md-6 text-end">Rp</div>
                                            <div class="col-md-6 ">
                                                {{ number_format(round($trx->amount_idr, 2), 2) }}</div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        {!! $trx->status == 1 ? '<span class="badge alert-primary">Active income</span>' : '<span class="badge alert-warning">Already redeemed</span>' !!}
                                    </td>
                                    @if ($trx->status == 1)
                                        <?php
                                        $total_idr += $trx->amount_idr;
                                        $total_bnb += $trx->amount_bnb; ?>
                                    @endif
                                </tr>
                            @endforeach
                            <tr>
                                {{-- <td colspan="4"><b>{{ 'Rp ' . $total_idr . ' = ' . $total_bnb . 'BNB' }}</b></td> --}}
                                <th colspan="2" class="text-end">Total active income</th>
                                <th class="text-end">{{ sprintf('%.8f', floatval($total_bnb)) }}</th>
                                <th class="text-end">
                                    <div class="row">
                                        <div class="col-md-6 text-end">Rp</div>
                                        <div class="col-md-6 ">
                                            {{ number_format(round($total_idr, 2), 2) }}</div>
                                    </div>
                                </th>
                                <th></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

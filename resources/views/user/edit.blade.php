@extends('index')
@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Edit User
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="row">
        <div class="col-xl-12">
            <form class="card mb-4" method="POST" action="/user/{{ $row->id }}">
                @csrf
                <input name="_method" type="hidden" value="PUT">
                <div class="card-header">Edit User</div>
                <div class="card-body">
                    @if(session('error'))
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('error') }}
                        </div>
                    </div>
                    @endif

                    @if(session('success'))
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('success') }}
                        </div>
                    </div>
                    @endif
                    
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Nama</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan nama user" name="name" value="{{ $row->name }}" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Email</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan email user" name="email" value="{{ $row->email }}" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">No. Handphone</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan no. handphone user" name="phone" value="{{ $row->phone }}" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">No. KTP</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan no. KTP" name="nationalId" value="{{ $row->nationalId }}" />
                        </div>
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Alamat</label>
                            <textarea class="form-control form-control-solid" name="address">{{ $row->address }}</textarea>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Nama Bank</label>
                            <select class="form-control form-control-solid" name="bankCode">
                                <option>Pilih</option>
                                @foreach($bank as $rows)
                                    <option value="{{ $rows->code }}" @if($row->bankCode == $rows->code) selected @endif>{{ $rows->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Nama Pemilik Rekening</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan nama pemilik rekening" name="bankAccount" value="{{ $row->bankAccount }}" />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Nomor Rekening</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan nomor rekening" name="bankNumber" value="{{ $row->bankNumber }}" />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Ganti Password</label>
                            <input class="form-control form-control-solid" type="password" placeholder="Kosongkan jika tidak ingin mengganti password" name="password" />
                        </div>
                    </div>
                        
                </div>

                <div class="card-footer">
                    <a class="btn btn-light mr-3" href="/user">Batal</a>
                    <button class="btn btn-primary" type="submit">Proses</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
                
@extends('index')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable({
        // Dom : t,
        responsive: true,
    });
} );
</script>
@endsection
@section('content')
<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="activity"></i></div>
                        Markets
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl">

    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Coin</th>
                        <th scope="col">Name</th>
                        <th scope="col" class="text-end">Last Price</th>
                        <th scope="col" class="text-end">24H Change</th>
                        <th scope="col" class="text-end">24H High</th>
                        <th scope="col" class="text-end">24H Low</th>
                        <th scope="col" class="text-end">24H Volume</th>
                        <!-- <th scope="col"></th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $rows)
                    @php 
                        if($rows->change > 0) {
                            $class = 'text-success';
                        } elseif($rows->change < 0) {
                            $class = 'text-danger';
                        } else {
                            $class = '';
                        }
                    @endphp
                    <tr>
                        <td><img src="{{ $rows->image }}" width="20px"></td>
                        <td>{{ $rows->code }}</td>
                        <td>{{ $rows->name }}</td>
                        <td class="{{ $class }} text-end">{{ $rows->price }}</td>
                        <td class="{{ $class }} text-end">{{ $rows->change }}%</td>
                        <td class="text-end">{{ $rows->highPrice }}</td>
                        <td class="text-end">{{ $rows->lowPrice }}</td>
                        <td class="text-end">{{ $rows->marketPrice }}</td>
                        <!-- <td><a href="/buy" class="btn btn-sm btn-success"><i data-feather="shopping-bag"></i>&nbsp; Beli</a></td> -->
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
                
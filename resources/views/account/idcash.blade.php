@extends('index')
@section('content')
    <!-- Main page content-->
    <div class="container-xl mt-4">
        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                @if (session('success'))
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('success') }}
                        </div>
                    </div>
                @endif
                <div class="card mb-4">
                    <!-- <div class="card-header"></div> -->
                    <div class="card-body text-center">
                        <img src="https://idcash.id/landing/img/idcash-logo/logo_hd.png" width="500px">
                        <p><br><br>Please download IDCash Apps to manage your IDCash account, IDCash balance and IDCash
                            card<br></p>
                        <a href="https://apps.apple.com/us/app/idcash/id1489893894" target="_blank"><img
                                src="https://idcash.s3.amazonaws.com/App-Store-buttons_Apple.png" width="500px"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.idcash" target="_blank"><img
                                src="https://idcash.s3.amazonaws.com/google-play-badge-300x89.png" width="500px"></a>
                    </div>
                    <div class="card-footer" style="background:#fff;">
                        <a class="btn btn-sm btn-primary float-end" href="{{ url('withdraw') }}">Continue Withdraw</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

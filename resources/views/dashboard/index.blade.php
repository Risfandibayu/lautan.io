@extends('index')
@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                responsive: true,
                "dom": 't'
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#table2').DataTable({
                responsive: true,
                "dom": 't'
            });
        });
    </script>
@endsection
@section('content')
    <header class="page-header page-header-compact page-header-light bg-white mt-3">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="home"></i></div>
                            Dashboard
                        </h1>
                    </div>
                    <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-xl">
        <div class="row">
            <div class="col-md-12 text-end">
                <a class="btn btn-sm btn-transparent-dark" href="{{ url('market') }}">Show All</a>
            </div>
        </div>
        <div class="row">
            @if (Auth::user()->type == 'user')
                @foreach ($row as $rows)
                    @php
                        $blc = App\Models\Balance::where('user_id', Auth::id())
                            ->where('currency_id', $rows->id)
                            ->where('active', 1)
                            ->orderBy('id', 'DESC')
                            ->first();
                        if ($blc) {
                            $binance = new App\Libs\Binance();
                            $send['url'] = '/ticker/24hr';
                            $body['symbol'] = $rows->code;
                            $send['body'] = $body;
                            $post = $binance->callApi($send);
                        
                            $balance = $blc->amount;
                            $balanceIDR = $post->lastPrice * $blc->amount;
                        } else {
                            $balance = '0.000000';
                            $balanceIDR = 0;
                        }
                    @endphp
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div class="avatar avatar-lg"><img class="avatar-img img-fluid"
                                            src="{{ $rows->image }}"></div>
                                    <div class="ms-3">
                                        <div class="fs-4 fw-500">{{ $rows->name }}</div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        @if (Auth::user()->type == 'user')
                                            <h5 class="" style="font-size:14px;">Available Balance</h5>
                                        @else
                                            <h5 class="" style="font-size:14px;">Portfolio Assets</h5>
                                        @endif
                                    </div>
                                    <div class="col-md-6 text-end">
                                        <h5 class="" style="font-size:14px;">
                                            {{ number_format($balance, 8) }}
                                             {{ str_replace('BIDR', ' ', $rows->code) }}</h5>
                                        <h4 class="" style="font-size:14px;color:#ccc;">≈
                                            {{ number_format($balanceIDR, 2) }}
                                            IDR</h4>
                                    </div>
                                </div>
                                @if (Auth::user()->type == 'user')
                                    <hr>
                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <a href="{{ url('deposit') }}?coin={{ $rows->code }}"
                                                class="btn btn-sm btn-outline-primary"><i data-feather="upload"></i>&nbsp;
                                                Top Up</a>
                                        </div>
                                        <div class="col-md-6 text-end">
                                            <a href="{{ url('withdraw') }}?coin={{ $rows->code }}"
                                                class="btn btn-sm btn-outline-primary"><i data-feather="download"></i>&nbsp;
                                                Convert</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                @foreach ($row as $rows)
                    @php
                        $blc = db::table('balances')
                            ->selectRaw('id,code, sum(amount) as amount')
                            ->where('currency_id', $rows->id)
                            ->where('active', 1)
                            ->groupBy('code')
                            ->orderBy('id', 'DESC')
                            ->first();
                        if ($blc) {
                            $binance = new App\Libs\Binance();
                            $send['url'] = '/ticker/24hr';
                            $body['symbol'] = $rows->code;
                            $send['body'] = $body;
                            $post = $binance->callApi($send);
                        
                            $balance = $blc->amount;
                            $balanceIDR = $post->lastPrice * $blc->amount;
                        } else {
                            $balance = '0.000000';
                            $balanceIDR = 0;
                        }
                    @endphp
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div class="avatar avatar-lg"><img class="avatar-img img-fluid"
                                            src="{{ $rows->image }}"></div>
                                    <div class="ms-3">
                                        <div class="fs-4 fw-500">{{ $rows->name }}</div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        @if (Auth::user()->type == 'user')
                                            <h5 class="" style="font-size:14px;">Available Balance</h5>
                                        @else
                                            <h5 class="" style="font-size:14px;">Portfolio Assets</h5>
                                        @endif
                                    </div>
                                    <div class="col-md-6 text-end">
                                        <h5 class="" style="font-size:14px;">
                                            {{ number_format($balance, 8) }}
                                             {{ str_replace('BIDR', ' ', $rows->code) }}</h5>
                                        <h4 class="" style="font-size:14px;color:#ccc;">≈
                                            {{ number_format($balanceIDR, 2) }} IDR</h4>
                                    </div>
                                </div>
                                @if (Auth::user()->type == 'user')
                                    <hr>
                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <a href="{{ url('deposit') }}?coin={{ $rows->code }}"
                                                class="btn btn-sm btn-outline-primary"><i data-feather="upload"></i>&nbsp;
                                                Top Up</a>
                                        </div>
                                        <div class="col-md-6 text-end">
                                            <a href="{{ url('withdraw') }}?coin={{ $rows->code }}"
                                                class="btn btn-sm btn-outline-primary"><i data-feather="download"></i>&nbsp;
                                                Convert</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

        <div class="row mt-4">
            @if (Auth::user()->type == 'user')
                <div class="col-md-4">
                    <div class="card card-header-actions">
                        <div class="card-header">

                            <div>
                                <button class="btn btn-light btn-icon">
                                    <i data-feather="shield"></i>
                                </button>
                            </div>
                            Account Verification
                        </div>
                        <div class="card-body text-center">
                            @if (Auth::user()->status == 1)
                                <div class="step step-danger mb-5 mt-4">
                                    <div class="step-item active">
                                        <a class="step-item-link" href="#" style="font-size:14px;">Unverified</a>
                                    </div>
                                    <div class="step-item">
                                        <a class="step-item-link disabled" href="#"
                                            style="font-size:14px;">Verification</a>
                                    </div>
                                    <div class="step-item">
                                        <a class="step-item-link disabled" href="#"
                                            style="font-size:14px;">Verified</a>
                                    </div>
                                </div>
                                <p>It is recommended to verify to be able to unlock all features</p>
                                <a href="{{ url('verification') }}" class="btn btn-sm btn-danger">Verify Now</a>
                            @elseif(Auth::user()->status == 2)
                                <div class="step step-warning mb-5 mt-4">
                                    <div class="step-item">
                                        <a class="step-item-link" href="#" style="font-size:14px;">Unverified</a>
                                    </div>
                                    <div class="step-item active">
                                        <a class="step-item-link" href="#" style="font-size:14px;">Verification</a>
                                    </div>
                                    <div class="step-item">
                                        <a class="step-item-link disabled" href="#"
                                            style="font-size:14px;">Verified</a>
                                    </div>
                                </div>
                                <p>Your account is under verification, the verification process takes a maximum of 1x24
                                    hours</p>
                            @elseif(Auth::user()->status == 3)
                                <div class="step step-success mb-5 mt-4">
                                    <div class="step-item">
                                        <a class="step-item-link" href="#" style="font-size:14px;">Unverified</a>
                                    </div>
                                    <div class="step-item">
                                        <a class="step-item-link" href="#" style="font-size:14px;">Verification</a>
                                    </div>
                                    <div class="step-item active">
                                        <a class="step-item-link" href="#" style="font-size:14px;">Verified</a>
                                    </div>
                                </div>
                                <p>Your account is verified</p>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
            <div class="@if (Auth::user()->type == 'user') col-md-8 @else col-md-12 @endif">
                <div class="card card-header-actions">
                    <div class="card-header">

                        <div>
                            <button class="btn btn-light btn-icon">
                                <i data-feather="bar-chart-2"></i>
                            </button>
                        </div>
                        Top Market
                    </div>
                    <div class="card-body">
                        <table class="table" id="table">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Coin</th>
                                    <th scope="col">Name</th>
                                    <th scope="col" class="text-end">Last Price</th>
                                    <th scope="col" class="text-end">24H Change</th>
                                    <th scope="col" class="text-end">24H High</th>
                                    <th scope="col" class="text-end">24H Low</th>
                                    <!-- <th scope="col">24H Volume</th> -->
                                    <!-- <th scope="col"></th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($market as $rows)
                                    @php
                                        if ($rows->change > 0) {
                                            $class = 'text-success';
                                        } elseif ($rows->change < 0) {
                                            $class = 'text-danger';
                                        } else {
                                            $class = '';
                                        }
                                    @endphp
                                    <tr>
                                        <td><img src="{{ $rows->image }}" width="20px"></td>
                                        <td>{{ $rows->code }}</td>
                                        <td>{{ $rows->name }}</td>
                                        <td class="{{ $class }} text-end">{{ $rows->price }}</td>
                                        <td class="{{ $class }} text-end">{{ $rows->change }}%</td>
                                        <td class="text-end">{{ $rows->highPrice }}</td>
                                        <td class="text-end">{{ $rows->lowPrice }}</td>
                                        <!-- <td>{{ rupiah($rows->marketPrice) }}</td> -->
                                        <!-- <td><a href="/buy" class="btn btn-sm btn-success"><i data-feather="shopping-bag"></i>&nbsp; Beli</a></td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @if (Auth::user()->type == 'user')
                @else
                    <div class="card card-header-actions" style="margin-top: 20px">
                        <div class="card-header">

                            <div>
                                <button class="btn btn-light btn-icon">
                                    <i data-feather="bar-chart-2"></i>
                                </button>
                            </div>
                            Total Asset
                        </div>
                        <div class="card-body">
                            <table class="table" id="table2">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Coin</th>
                                        <th scope="col">Name</th>
                                        <th scope="col" class="text-end">Total Balance</th>
                                        <th scope="col" class="text-end">Available Balance</th>
                                        <th scope="col" class="text-end">Est. IDR Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($assets as $rows)
                                        @php
                                            // $blc = App\Models\Balance::where('currency_id',$rows->id)->where('active',1)
                                            // // ->groupBy('code')
                                            // ->orderBy('id','DESC')->first();
                                            $blc = db::table('balances')
                                                ->selectRaw('id,code, sum(amount) as amount')
                                                ->where('currency_id', $rows->id)
                                                ->where('active', 1)
                                                ->groupBy('code')
                                                ->orderBy('id', 'DESC')
                                                ->first();
                                            if ($blc) {
                                                $binance = new App\Libs\Binance();
                                                $send['url'] = '/ticker/24hr';
                                                $body['symbol'] = $rows->code;
                                                $send['body'] = $body;
                                                $post = $binance->callApi($send);
                                            
                                                $balance = $blc->amount;
                                                $balanceIDR = $post->lastPrice * $blc->amount;
                                            } else {
                                                $balance = '0.000000';
                                                $balanceIDR = 0;
                                            }
                                        @endphp
                                        <tr>
                                            <td><img src="{{ $rows->image }}" width="20px"></td>
                                            <td>{{ $rows->code }}</td>
                                            <td>{{ $rows->name }}</td>
                                            <td class="text-end">{{ $balance }}</td>
                                            <td class="text-end">{{ $balance }}</td>
                                            <td class="text-end">{{ $balanceIDR }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif


            </div>
        </div>
    </div>
@endsection

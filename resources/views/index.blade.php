<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Lautan.io</title>
    <link href="{{ asset('public') }}/assets/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
    <link rel="stylesheet" href="{{ asset('public/assets/css/customstyle.css') }}">
    <link rel="shortcut icon" href="{{ asset('public') }}/favicon.ico" type="image/x-icon">
    <link rel="icon" href="{{ asset('public') }}/favicon.ico" type="image/x-icon">
    <script data-search-pseudo-elements="" defer=""
        src="{{ asset('public') }}/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    <link rel="stylesheet" href="{{ asset('public/assets/js/sweetalert/sweetalert2.min.css') }}">
    @yield('style')
</head>

<body class="nav-fixed">
    <nav class="topnav navbar navbar-expand justify-content-between justify-content-sm-start navbar-light bg-white"
        id="sidenavAccordion">
        <!-- Sidenav Toggle Button-->
        <button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 me-2 ms-lg-2 me-lg-0" id="sidebarToggle"><i
                data-feather="menu"></i></button>
        <!-- Navbar Brand-->
        <!-- * * Tip * * You can use text or an image for your navbar brand.-->
        <!-- * * * * * * When using an image, we recommend the SVG format.-->
        <!-- * * * * * * Dimensions: Maximum height: 32px, maximum width: 240px-->
        <a class="navbar-brand pe-3 ps-4 ps-lg-2" href="{{ url('dashboard') }}"><img
                src="{{ asset('public') }}/landing-assets/images/logo.png" style="height:100%;width:100px;"></a>
        <!-- Navbar Search Input-->
        <!-- * * Note: * * Visible only on and above the lg breakpoint-->
        <!-- <form class="form-inline me-auto d-none d-lg-block me-3">
                <div class="input-group input-group-joined input-group-solid">
                    <input class="form-control pe-0" type="search" placeholder="Search" aria-label="Search" />
                    <div class="input-group-text"><i data-feather="search"></i></div>
                </div>
            </form> -->
        <!-- Navbar Items-->
        <ul class="navbar-nav align-items-center ms-auto">

            <!-- Navbar Search Dropdown-->
            <!-- * * Note: * * Visible only below the lg breakpoint-->
            <li class="nav-item dropdown no-caret me-3 d-lg-none">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="searchDropdown" href="#"
                    role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        data-feather="search"></i></a>
                <!-- Dropdown - Search-->
                <div class="dropdown-menu dropdown-menu-end p-3 shadow animated--fade-in-up"
                    aria-labelledby="searchDropdown">
                    <form class="form-inline me-auto w-100">
                        <div class="input-group input-group-joined input-group-solid">
                            <input class="form-control pe-0" type="text" placeholder="Search for..."
                                aria-label="Search" aria-describedby="basic-addon2" />
                            <div class="input-group-text"><i data-feather="search"></i></div>
                        </div>
                    </form>
                </div>
            </li>
            <!-- Alerts Dropdown-->
            <li class="nav-item dropdown no-caret d-none d-sm-block me-3 dropdown-notifications">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownAlerts"
                    href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"><i data-feather="bell"></i></a>
                <div class="dropdown-menu dropdown-menu-end border-0 shadow animated--fade-in-up"
                    aria-labelledby="navbarDropdownAlerts">
                    <h6 class="dropdown-header dropdown-notifications-header">
                        <i class="me-2" data-feather="bell"></i>
                        Notification
                    </h6>
                    <a class="dropdown-item dropdown-notifications-item" href="#!">
                        <div class="dropdown-notifications-item-content">
                            No notifications
                        </div>
                    </a>
                </div>
            </li>
            <!-- Messages Dropdown-->
            <!-- <li class="nav-item dropdown no-caret d-none d-sm-block me-3 dropdown-notifications">
                    <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownMessages" href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="mail"></i></a>
                    <div class="dropdown-menu dropdown-menu-end border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownMessages">
                        <h6 class="dropdown-header dropdown-notifications-header">
                            <i class="me-2" data-feather="mail"></i>
                            Pesan
                        </h6>
                        <a class="dropdown-item dropdown-notifications-item" href="#!">
                            <div class="dropdown-notifications-item-content">
                                <div class="dropdown-notifications-item-content-text">Tidak ada pesan</div>
                            </div>
                        </a>
                        
                    </div>
                </li> -->
            <!-- User Dropdown-->
            <li class="nav-item dropdown no-caret dropdown-user me-3 me-lg-4">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownUserImage"
                    href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"><i data-feather="user"></i></a>
                <div class="dropdown-menu dropdown-menu-end border-0 shadow animated--fade-in-up"
                    aria-labelledby="navbarDropdownUserImage">
                    <h6 class="dropdown-header d-flex align-items-center">
                        <img class="dropdown-user-img" src="{{ asset('public') }}/assets/images/profile.png" />
                        <div class="dropdown-user-details">
                            <div class="dropdown-user-details-name">{{ Auth::user()->name }}</div>
                            <div class="dropdown-user-details-email">{{ Auth::user()->email }}</div>
                        </div>
                    </h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ url('account') }}">
                        <div class="dropdown-item-icon"><i data-feather="settings"></i></div>
                        Account
                    </a>
                    <a class="dropdown-item" href="{{ url('logout') }}">
                        <div class="dropdown-item-icon"><i data-feather="log-out"></i></div>
                        Logout
                    </a>
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sidenav sidenav-light">
                <div class="sidenav-menu">
                    <div class="nav accordion" id="accordionSidenav">
                        <div class="d-flex align-items-center px-2 py-2 pt-3 pb-3" style="background: #118EEB;">
                            <div class="avatar avatar-lg"><img class="avatar-img "
                                    src="{{ asset('public') }}/assets/images/profile.png"></div>
                            <div class="ms-3">
                                <div class="fs-4 fw-500" style="color: #fff;">{{ Auth::user()->name }}</div>
                                <div class="small" style="color: #fff;">{{ Auth::user()->email }}</div>
                            </div>
                        </div>
                        @if (Auth::user()->type == 'user')
                            <div class="sidenav-menu-heading">Navigation</div>
                            <a class="nav-link collapsed @if ($menu == 'dashboard') active @endif"
                                href="{{ url('dashboard') }}">
                                <div class="nav-link-icon"><i data-feather="home"></i></div>
                                Dashboard
                                <!-- <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div> -->
                            </a>

                            <a class="nav-link collapsed @if ($menu == 'market') active @endif"
                                href="{{ url('market') }}">
                                <div class="nav-link-icon"><i data-feather="activity"></i></div>
                                Markets
                                <!-- <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div> -->
                            </a>

                            <!-- <div class="sidenav-menu-heading">Dompet</div> -->
                            <a class="nav-link collapsed @if ($menu == 'balance') active @endif"
                                href="{{ url('balance') }}">
                                <div class="nav-link-icon"><i data-feather="circle"></i></div>
                                Assets
                                <!-- <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div> -->
                            </a>
                            <a class="nav-link collapsed @if ($menu == 'transaction') active @endif"
                                href="{{ url('transaction') }}">
                                <div class="nav-link-icon"><i data-feather="clock"></i></div>
                                Transaction
                                <!-- <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div> -->
                            </a>
                            <a class="nav-link collapsed @if ($menu == 'agent') active @endif"
                                href="{{ url('agent/landing') }}">
                                <div class="nav-link-icon"><i data-feather="users"></i></div>
                                Partner
                                <!-- <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div> -->
                            </a>
                            <!--<a  class="nav-link collapsed @if ($menu == 'voucher') active @endif" href="#">-->
                            <!--    <div class="nav-link-icon"><i data-feather="bookmark"></i></div>-->
                            <!--    Voucher-->
                            <!-- <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div> -->
                            <!--</a>-->
                            <!--<a  class="nav-link collapsed @if ($menu == 'reward') active @endif" href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#collapseReward" aria-expanded="true">-->
                            <!--    <div class="nav-link-icon"><i data-feather="award"></i></div>-->
                            <!--    Reward-->
                            <!--    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>-->
                            <!--</a>-->
                            <div class="collapse" id="collapseReward" data-bs-parent="#accordionSidenav"
                                style="">
                                <nav class="sidenav-menu-nested nav">
                                    <a class="nav-link" href="#">Points Center</a>
                                    <a class="nav-link" href="#">Gift</a>
                                    <a class="nav-link" href="#">Affiliate Program</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed @if ($menu == 'reward') active @endif"
                                href="{{ url('news') }}" target="_blank">
                                <div class="nav-link-icon"><i data-feather="life-buoy"></i></div>
                                Help
                                <!--<div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>-->
                            </a>
                        @else
                            <div class="sidenav-menu-heading">Main</div>
                            <a class="nav-link collapsed @if ($menu == 'dashboard') active @endif"
                                href="{{ url('dashboard') }}">
                                <div class="nav-link-icon"><i data-feather="activity"></i></div>
                                Dashboard
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>

                            <div class="sidenav-menu-heading">Master</div>
                            <a class="nav-link collapsed @if ($menu == 'user') active @endif"
                                href="{{ url('user') }}">
                                <div class="nav-link-icon"><i data-feather="user"></i></div>
                                User
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <a class="nav-link collapsed @if ($menu == 'verification') active @endif"
                                href="{{ url('verification/list') }}">
                                <div class="nav-link-icon"><i data-feather="file"></i></div>
                                Verifikasi
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <a class="nav-link collapsed @if ($menu == 'transaction') active @endif"
                                href="{{ url('transaction/adm') }}">
                                <div class="nav-link-icon"><i data-feather="clock"></i></div>
                                Transaction
                                <!-- <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div> -->
                            </a>
                            <a class="nav-link collapsed @if ($menu == 'agent') active @endif"
                                href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#collapseAgent"
                                aria-expanded="true">
                                <div class="nav-link-icon"><i data-feather="users"></i></div>
                                Partner
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseAgent" data-bs-parent="#accordionSidenav"
                                style="">
                                <nav class="sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{ url('agentrole') }}">Partner Roles</a>
                                    <a class="nav-link" href="{{ url('agent') }}">Partner List</a>
                                    <a class="nav-link" href="{{ url('agentrequest') }}">Partner Request</a>
                                </nav>
                            </div>

                            <!-- STARTCMS ------------------------------------- -->
                            <a class="nav-link collapsed @if ($menu == 'webadmin') active @endif"
                                href="javascript:void(0);" data-bs-toggle="collapse"
                                data-bs-target="#collapseWebadmin" aria-expanded="true">
                                <div class="nav-link-icon"><i data-feather="users"></i></div>
                                Web Admin
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseWebadmin" data-bs-parent="#accordionSidenav"
                                style="">
                                <nav class="sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{ url('webadmin/media') }}">Upload Media</a>
                                    <a class="nav-link" href="{{ url('webadmin/pages') }}">Pages</a>
                                    <a class="nav-link" href="{{ url('webadmin/customvar') }}">Custom
                                        Variable</a>
                                </nav>
                            </div>
                            <!-- ENDCMS ------------------------------------- -->

                            @if (Auth::user()->type == 'superadmin')
                                <a class="nav-link collapsed @if ($menu == 'currency') active @endif"
                                    href="{{ url('currency') }}">
                                    <div class="nav-link-icon"><i data-feather="circle"></i></div>
                                    Currency
                                    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <a class="nav-link collapsed @if ($menu == 'admin') active @endif"
                                    href="{{ url('admin') }}">
                                    <div class="nav-link-icon"><i data-feather="users"></i></div>
                                    Akses Admin
                                    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <a class="nav-link collapsed @if ($menu == 'datalog') active @endif"
                                    href="{{ url('datalog') }}">
                                    <div class="nav-link-icon"><i data-feather="server"></i></div>
                                    Data Log
                                    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                            @endif
                        @endif

                        <!-- <div class="sidenav-menu-heading">Transaksi</div>
                            <a class="nav-link collapsed @if ($menu == 'transaction') active @endif" href="/transaction">
                                <div class="nav-link-icon"><i data-feather="grid"></i></div>
                                History Transaksi
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="sidenav-menu-heading">Akun</div>
                            <a class="nav-link collapsed @if ($menu == 'account') active @endif" href="/account">
                                <div class="nav-link-icon"><i data-feather="user"></i></div>
                                Info Akun
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <a class="nav-link collapsed @if ($menu == 'password') active @endif" href="/password">
                                <div class="nav-link-icon"><i data-feather="lock"></i></div>
                                Ganti Password
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <a class="nav-link collapsed" href="/logout">
                                <div class="nav-link-icon"><i data-feather="power"></i></div>
                                Logout
                                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a> -->


                    </div>
                </div>
                <!-- Sidenav Footer-->
                <!-- <div class="sidenav-footer">
                        <div class="sidenav-footer-content">
                            <div class="sidenav-footer-subtitle">Logged in as:</div>
                            <div class="sidenav-footer-title">{{ Auth::user()->name }}</div>
                        </div>
                    </div> -->
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                @yield('content')
            </main>
            <footer class="footer-admin mt-auto footer-light">
                <div class="container-xl px-4">
                    <div class="row">
                        <div class="col-md-6 small">Copyright © Lautan.io {{ DATE('Y') }}</div>
                        <div class="col-md-6 text-md-end small">
                            <a href="/user-agreement" target="_blank">User Agreement</a>
                            ·
                            <a href="/privacy-policy" target="_blank">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('public') }}/assets/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="{{ asset('public/assets/js/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                theme: 'bootstrap-5',
                width: 'resolve'
            });
        });
    </script>
    @stack('script')
    @yield('js')
</body>

</html>

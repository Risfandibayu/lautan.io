@extends('index')
@section('content')
    <div class="container-xl mt-4">
        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                @if (session('warning'))
                    <div class="alert alert-warning alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('warning') }}
                        </div>
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('error') }}
                        </div>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('success') }}
                        </div>
                    </div>
                @endif
                <div class="card mb-4">
                    <div class="card-header">Convert</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('withdraw/info') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Assets</label>
                                    <select class="select2 code row" name="code" required>

                                        <option disabled>Select</option>
                                        @foreach ($row as $rows)
                                            <option value="{{ $rows->id }}"
                                                @if ($rows->code == request('coin')) selected @endif>

                                                {{ $rows->name }} -
                                                {{ str_replace('BIDR', ' ', $rows->code) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Amount</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="" name="amount"
                                            id="amont">
                                        <span class="input-group-text readonly-color"><i
                                                class="fas fa-exchange-alt"></i></span>
                                        <input type="text" class="form-control" id="idrvalue" name="idrvalue">
                                    </div>
                                </div>
                                {{-- <h3>Aplication Fee & Tax</h3>
                                <ul class="">
                                    <li class="list-group-item ">TKO Fee: <span class="text-end" id="tko_fee"></span>
                                    </li>
                                    <li class="list-group-item ">Gross Fee: <span class="text-end"
                                            id="gross_fee"></span></li>
                                    <li class="list-group-item ">Angent Fee: <span class="text-end"
                                            id="agent_fee"></span></li>
                                    <li class="list-group-item ">Lautan Fee: <span class="text-end"
                                            id="lautan_fee"></span></li>
                                    <li class="list-group-item ">Platform Fee: <span class="text-end"
                                            id="platform_fee"></span></li>
                                    <li class="list-group-item ">Transaction Tax: <span class="text-end"
                                            id="trans_tax"></span></li>
                                    <li class="list-group-item ">Total: <span class="text-end" id="total"></span>
                                    </li>
                                    <li class="list-group-item ">Coin Fee: <span class="text-end"
                                            id="coin_fee"></span>
                                    </li>
                                </ul> --}}

                                <h6 class="small mt-2">Convert to</h6>
                                <hr>
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="type" id="type"
                                            value="bankk" onchange="handleChange(this);" checked>
                                        <label class="form-check-label" for="type">
                                            Bank Account
                                        </label>

                                        <?php
                                        use Illuminate\Support\Facades\DB;
                                        $b = db::table('user_bank')
                                            ->select('user_bank.*', 'user_bank.id as bid', 'banks.*')
                                            ->join('banks', 'banks.code', '=', 'user_bank.bank_code')
                                            ->where('user_id', Auth::user()->id)
                                            ->get();
                                        ?>


                                        @if (count($b) >= 3)
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#edit_bank"
                                                style="float: right;" class="btn btn-sm btn-outline-warning">Edit</a>
                                            <div class="reveal-if-active">
                                                <select class="require-if-active form-control form-control-solid select2"
                                                    data-require-pair="#type" name="bank_acc" required>-->
                                                    <option value="" selected disabled>Choose Bank Account</option>
                                                    @foreach ($b as $rows)
                                                        <option value="{{ $rows->bid }}">{{ $rows->name }} |
                                                            {{ $rows->bank_account }} | {{ $rows->account_number }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @elseif(count($b) == null)
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#add_bank"
                                                style="float: right;" class="btn btn-sm btn-outline-primary">Add</a>
                                            <div class="reveal-if-active">
                                                <select class="require-if-active form-control form-control-solid select2"
                                                    data-require-pair="#type" name="bank_acc" required>-->
                                                    <option value="" selected disabled>Add Bank Account</option>
                                                </select>
                                            </div>
                                        @else
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#edit_bank"
                                                style="float: right;" class="btn btn-sm btn-outline-warning">Edit</a>
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#add_bank"
                                                style="float: right;" class="btn btn-sm btn-outline-primary">Add</a>
                                            <div class="reveal-if-active">
                                                <select class="require-if-active form-control form-control-solid select2"
                                                    data-require-pair="#type" name="bank_acc" required>-->
                                                    <option value="" selected disabled>Choose Bank Account</option>
                                                    @foreach ($b as $rows)
                                                        <option value="{{ $rows->bid }}">{{ $rows->name }} |
                                                            {{ $rows->bank_account }} | {{ $rows->account_number }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @endif



                                    </div>
                                    <br>
                                </div>
                                <hr class="my-4" />
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary float-end" type="submit">Next</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_bank" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Bank Account</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="POST" action="{{ url('add_bank') }}" tabindex="-1" aria-hidden="true">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label class="small mb-1">Bank Name</label>
                                <input type="hidden" name="coin"
                                    value="{{ isset($_GET['coin']) ? $_GET['coin'] : '' }}">
                                <select class="form-control form-control-solid" name="bank_code">
                                    <option>Pilih</option>
                                    @foreach ($bank as $rows)
                                        <option value="{{ $rows->code }}"
                                            @if (Auth::user()->bankCode == $rows->code) selected @endif>{{ $rows->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="small mb-1">Bank Account Name</label>
                                <input class="form-control form-control-solid" type="text"
                                    placeholder="Input your Bank Account Name" name="bank_account" value=""
                                    required />
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="small mb-1">Bank Account Number</label>
                                <input class="form-control form-control-solid" type="text"
                                    placeholder="Bank Account Number" name="account_number" value="" required />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_bank" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Bank Account</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Bank Name</th>
                                    <th scope="col">Bank Account Name</th>
                                    <th scope="col">Bank Account Number</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($b as $ban)
                                    <tr>
                                        <th scope="row">{{ $ban->name }}</th>
                                        <td>{{ $ban->bank_account }}</td>
                                        <td>{{ $ban->account_number }}</td>
                                        <td>
                                            <form method="POST" action="{{ url('del_bank') }}/{{ $ban->bid }}"
                                                tabindex="-1" aria-hidden="true">
                                                @csrf

                                                <a class="btn btn-sm btn-outline-warning" data-bs-dismiss="modal"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#edit_bank{{ $ban->bid }}">edit</a>

                                                <button type="submit"
                                                    class="btn btn-sm btn-outline-danger ">delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    @foreach ($b as $ban)
        <div class="modal fade" id="edit_bank{{ $ban->bid }}" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Bank Account</h5>
                        <button type="button" class="btn-close" data-bs-toggle="modal" data-bs-target="#edit_bank"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="POST" action="{{ url('edit_bank') }}/{{ $ban->bid }}" tabindex="-1"
                        aria-hidden="true">
                        @csrf
                        <div class="modal-body">
                            <div class="row">

                                <?php
                                $ba = db::table('user_bank')
                                    ->where('id', $ban->bid)
                                    ->first();
                                ?>

                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Bank Name</label>
                                    <select class="form-control form-control-solid" name="bank_code">-->
                                        <option>Pilih</option>
                                        @foreach ($bank as $rows)
                                            <option value="{{ $rows->code }}"
                                                @if ($ba->bank_code == $rows->code) selected @endif>{{ $rows->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Bank Account Name</label>
                                    <input class="form-control form-control-solid" type="text"
                                        value="{{ $ba->bank_account }}" placeholder="Input your Bank Account Name"
                                        name="bank_account" value="" required />
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Bank Account Number</label>
                                    <input class="form-control form-control-solid" type="text"
                                        value="{{ $ba->account_number }}" placeholder="Bank Account Number"
                                        name="account_number" value="" required />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <script type="text/javascript">
        function handleChange(src) {
            if (src.value == 'bank') {
                $("#bank").show();
            } else {
                $("#bank").hide();
            }
        }
    </script>
@endsection

@section('style')
    <style>
        .reveal-if-active {
            opacity: 0;
            max-height: 0;
            overflow: hidden;
            font-size: 16px;
            transform: scale(0.8);
            transition: 0.5s;
        }

        .reveal-if-active label {
            display: block;
            margin: 0 0 3px 0;
        }

        .reveal-if-active input[type=text] {
            width: 100%;
        }

        input[type="radio"]:checked~.reveal-if-active,
        input[type="checkbox"]:checked~.reveal-if-active {
            opacity: 1;
            max-height: 2000px;
            padding: 10px 20px;
            transform: scale(1);
            overflow: visible;
        }
    </style>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            firstReload();



            function firstReload() {
                var code = $('.code').val();
                $('#amont').val(1);
                var amont = 1;
                hitung(code, amont);
            }
            $('#amont').on('keyup', function() {
                var amont = $(this).val();
                var code = $('.code').val();

                hitung(code, amont);
            });
            $('.code').on('change', function() {
                var code = $(this).val();
                var amont = $('#amont').val();

                hitung(code, amont);
            });

            function hitung(code, amont = 1) {
                $.ajax({
                    method: 'GET',
                    url: "{{ url('withdraw/getCryptoCurrency') }}" + '?code=' + code + '&amont=' + amont,
                    success: function(rs) {
                        if (rs.status == 200) {
                            $('#idrvalue').val(rupiah(rs.data.idrvalue));
                            $('#total_fee').html(rupiah(rs.data.totalFee));
                            $('#gross_fee').html(rupiah(rs.data.grossFee));
                            $('#agent_fee').html(rupiah(rs.data.agentFee));
                            $('#lautan_fee').html(rupiah(rs.data.lautanFee));
                            $('#tko_fee').html(rupiah(rs.data.tkoFee));
                            $('#platform_fee').html(rupiah(rs.data.platformFee));
                            $('#trans_tax').html(rupiah(rs.data.transTax));
                            $('#total').html(rupiah(rs.data.totalFee));
                            $('#coin_fee').html(rs.data.feeInCoin);
                        }
                    }
                })
            }
            const rupiah = (number) => {
                return new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR"
                }).format(number).replace(/\D00$/, '');
            }
        });
    </script>
    <script>
        var FormStuff = {

            init: function() {
                this.applyConditionalRequired();
                this.bindUIActions();
            },

            bindUIActions: function() {
                $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
            },

            applyConditionalRequired: function() {

                $(".require-if-active").each(function() {
                    var el = $(this);
                    if ($(el.data("require-pair")).is(":checked")) {
                        el.prop("required", true);
                    } else {
                        el.prop("required", false);
                    }
                });

            }

        };

        FormStuff.init();
    </script>
@endsection

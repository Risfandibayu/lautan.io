@extends('index')
@section('content')
    <!-- <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
                                                                                                                                                                            <div class="container-xl px-4">
                                                                                                                                                                                <div class="page-header-content">
                                                                                                                                                                                    <div class="row align-items-center justify-content-between pt-3">
                                                                                                                                                                                        <div class="col-auto mb-3">
                                                                                                                                                                                            <h1 class="page-header-title">
                                                                                                                                                                                                <div class="page-header-icon"><i data-feather="download"></i></div>
                                                                                                                                                                                                Penarikan
                                                                                                                                                                                            </h1>
                                                                                                                                                                                        </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                            </div>
                                                                                                                                                                        </header>
                                                                                                                                                                        <!-- Main page content-->
    <div class="container-xl mt-4">
        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                @if (session('warning'))
                    <div class="alert alert-warning alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('warning') }}
                        </div>
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('error') }}
                        </div>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('success') }}
                        </div>
                    </div>
                @endif
                <div class="card mb-4">
                    <div class="card-header">Withdraw Confirmation</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('withdraw') }}">
                            @csrf
                            <input type="hidden" name="code" value="{{ session('code') }}">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Assets</label>
                                    <input class="form-control form-control-solid" type="text" name="coin"
                                        value="{{ session('coin') }}" required readonly />
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Amount</label>
                                    <input class="form-control form-control-solid" type="text" name="amount"
                                        value="{{ session('amount') }}" required readonly />
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Amount IDR</label>
                                    <input class="form-control form-control-solid " type="text" name="amountIdr"
                                        value="{{ number_format(session('amountIdr')) }}" required readonly />
                                    @if (session('amountIdr') < 10000)
                                        <span style="color:red">
                                            <small>Amount below the minimum withdrawal. Amount must be more than
                                                10.000</small>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="small mb-1">Transaction Fee & Tax</label>

                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-solid"
                                            value="{{ 'IDR ' . number_format(session('feeTransaction.totalFee')) }}">
                                        <span class="input-group-text form-control-solid"> ≈ </span>
                                        <input type="text" class="form-control form-control-solid"
                                            value="{{ session('feeTransaction.feeInCoin') . ' ' . session('coin') }}">
                                    </div>
                                </div>
                                <h6 class="small mt-2">Withdraw to</h6>
                                <hr>
                                @if (session('type') == 'bank')
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Name</label>
                                        <input class="form-control form-control-solid" type="text" name="bankName"
                                            value="{{ session('bankName') }}" required disabled />
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Account Name</label>
                                        <input class="form-control form-control-solid" type="text" name="bankAccount"
                                            value="{{ session('bankAccount') }}" required disabled />
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Account Number</label>
                                        <input class="form-control form-control-solid" type="text" name="bankNumber"
                                            value="{{ session('bankNumber') }}" required disabled />
                                    </div>
                                @elseif(session('type') == 'bankk')
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Name</label>
                                        <input class="form-control form-control-solid" type="text" name="bankName"
                                            value="{{ session('bankName') }}" required disabled />
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Account Name</label>
                                        <input class="form-control form-control-solid" type="text" name="bankAccount"
                                            value="{{ session('bankAccount') }}" required disabled />
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Bank Account Number</label>
                                        <input class="form-control form-control-solid" type="text" name="bankNumber"
                                            value="{{ session('bankNumber') }}" required disabled />
                                    </div>
                                @else
                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">IDCash</label>
                                        <input class="form-control form-control-solid" type="text" name="bankNumber"
                                            value="{{ Auth::user()->idcashVa }}" required disabled />
                                    </div>
                                @endif


                                @if (session('amountIdr') < 10000)
                                    <hr>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-primary float-end" href="{{ url('withdraw') }}">Back</a>
                                    </div>
                                @else
                                    <h6 class="small mt-2">Confirmation</h6>
                                    <hr>

                                    <div class="col-md-12 mb-3">
                                        <label class="small mb-1">Please enter the pin we sent via email to continue the
                                            transaction</label>
                                        <input class="form-control form-control-solid" type="password" name="pin" value=""
                                            required />
                                    </div>
                                    <hr class="my-4" />
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <button class="btn btn-primary float-end" type="submit">Submit</button>
                                    </div>
                                @endif
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

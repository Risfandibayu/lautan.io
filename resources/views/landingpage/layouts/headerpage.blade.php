<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Lautan.io</title>
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Mulish:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
<link href="/appway/css/font-awesome-all.css" rel="stylesheet">
<link href="/appway/css/flaticon.css" rel="stylesheet">
<link href="/appway/css/owl.css" rel="stylesheet">
<link href="/appway/css/bootstrap.css" rel="stylesheet">
<link href="/appway/css/jquery.fancybox.min.css" rel="stylesheet">
<link href="/appway/css/animate.css" rel="stylesheet">
<link href="/appway/css/style.css" rel="stylesheet">
<link href="/appway/css/responsive.css" rel="stylesheet">
<link rel="stylesheet" href="/appway/css/select2.min.css">
<link rel="stylesheet" href="/appway/css/select2-bootstrap.css">

{!! $header->content !!}
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Lautan</title>
    <link href="{{ asset('public') }}/assets/css/styles.css" rel="stylesheet" />
    <link rel="shortcut icon" href="{{ asset('public') }}/favicon.ico" type="image/x-icon">
    <link rel="icon" href="{{ asset('public') }}/favicon.ico" type="image/x-icon">
    {{-- pwa --}}
    <meta name="theme-color" content="#fff">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    {{-- pwa --}}
    <script data-search-pseudo-elements="" defer=""
        src="{{ asset('public') }}/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
</head>

<body>
    @yield('content')

    <script src="{{ asset('public/Appway2') }}/js/jquery.js"></script>
    <script src="{{ asset('public') }}/assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('public') }}/assets/js/scripts.js"></script>
    @yield('script')
    <script src="{{ asset('public/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
</body>

</html>

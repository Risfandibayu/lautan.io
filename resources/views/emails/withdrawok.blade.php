@include('emails.header')
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;"
    align="left">
    Hi, {{ $name }} <br><br>
    Withdrawal request has been successful. Withdrawal details : <br>
</p>
<table
    style="color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em;">
    <tr>
        <td style="width: 150px;">Amount</td>
        <td>: {{ number_format($amount, 0, ',', '.') }} IDR</td>
    </tr>
    <tr>
        <td>Fee</td>
        <td>: {{ number_format(session('trxFee.totalFee'), 0, ',', '.') }} IDR</td>
        {{-- <td>: {{ number_format(session('trxFee.totalFee'),0,",",".") + number_format($fee,0,",",".")}} IDR</td> --}}
    </tr>
    <tr>
        <td>Remain</td>
        <td>: {{ number_format($remain, 0, ',', '.') }} IDR</td>
    </tr>
    <tr>
        <td>Bank</td>
        <td>: {{ $bname }}</td>
    </tr>
    <tr>
        <td>Account</td>
        <td>: {{ $bacc }}</td>
    </tr>
    <tr>
        <td>Time</td>
        <td>: {{ $time }}</td>
    </tr>
    <tr>
        <td>TRX ID</td>
        <td>: {{ $trx }}</td>
    </tr>
</table>
<br>
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;"
    align="left">
    Your withdrawal has been successful. Please check the balance in your bank account. <br><br>

    Thank you for using Lautan.io
</p>
@include('emails.footer')

@include('emails.header')
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
    Hi, {{ $name }} <br><br>
    Pendaftaran Anda berhasil, silahkan klik link dibawah ini untuk masuk ke akun Anda. <bt><br>

    <a href="{{ $url }}">{{ $url }}</a>
</p>
@include('emails.footer')
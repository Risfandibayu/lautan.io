@include('emails.header')
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;"
    align="left">
    Hi, Admin. <br>
    User <b>{{ $name }} - {{ $email }} </b> have verified the KYC document upload, please check and
    confirm before 24 hours.
    <bt><br>
</p>
@include('emails.footer')

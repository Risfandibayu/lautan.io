@include('emails.header')
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
    Hi, {{ $name }} <br><br>
    You have made a withdrawal request, please enter the pin below to continue the transaction. <bt><br>

    <strong>{{ $pin }}</strong>
</p>
@include('emails.footer')
@extends('index')
@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="file"></i></div>
                        Transaction Confirmation
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="row">

        <div class="col-xl-6">
            <div class="card mb-4">
                            <div class="card-header">Data Withdraw</div>
                            <div class="card-body">
                                <p class="small text-muted mb-0">Coin</p>
                                <h5>{{ $trx->code }}</h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Amount</p>
                                <h5>{{ $trx->amount }}</h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Amount IDR</p>
                                <h5>Rp {{ number_format($post->lastPrice * $trx->amount,0,",",".") }}</h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Bank Name</p>
                                <h5>{{ $bank->bn }}</h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Bank Account Name</p>
                                <h5>{{ $bank->bank_account }}</h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Bank Account Number</p>
                                <h5>{{ $bank->account_number }}</h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Time</p>
                                <h5>{{ $trx->time}}</h5>
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">Withdrawal Input OTP screenshoot</p>
                                <img src="{{$trx->cwimg1}}" class="img-fluid" style="width:200px;">
                                <hr class="my-3" />
                                <p class="small text-muted mb-0">OTP Code Screenshot (email)</p>
                                <img src="{{$trx->cwimg2}}" class="img-fluid" style="width:200px;">
                                <hr class="my-3" />
                                
                            </div>
                        </div>
            </div>
        <div class="col-xl-6">
        <div class="card mb-4">
                        <div class="card-header">Detail User</div>
                        <div class="card-body">
                            <p class="small text-muted mb-0">Nama</p>
                            <h5>{{ $trx->name }}</h5>
                            <hr class="my-3" />
                            <p class="small text-muted mb-0">Email</p>
                            <h5>{{ $trx->email }}</h5>
                            <hr class="my-3" />
                            <p class="small text-muted mb-0">No. Telepon</p>
                            <h5>{{ $trx->phone }}</h5>
                            <hr class="my-3" />
                        </div>
                    </div>
        </div>
        
            
            
                
            
        <div class="col-xl-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#alertModalw" data-id="{{ $trx->trid }}" data-status="3" data-name="Approve verifikasi user {{ $trx->name }}?">Approve</button>
                        <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#alertModalrejw" data-id="{{ $trx->trid }}" data-status="4" data-name="Reject verifikasi user {{ $trx->name }}?">Reject</button>
                    </div>
                </div>
            
            </div>


            <form method="POST" action="{{ action('TransactionController@wtrx_confirm') }}" class="modal fade" id="alertModalw" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
                @csrf
                <input name="id" type="hidden" id="user-id" value="{{$trx->trid}}">
                {{-- <input name="status" type="hidden" id="user-status"> --}}
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center" id="user-name">
                        Konfirmasi Bukti Withdraw
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-success">Ya</button>
                    </div>
                    </div>
                </div>
            </form>
            
            <form method="POST" action="{{ action('TransactionController@wtrx_reject') }}" class="modal fade" id="alertModalrejw" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
                @csrf
                <input name="id" type="hidden" id="user-id" value="{{$trx->trid}}">
                {{-- <input name="status" type="hidden" id="user-status"> --}}
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi!</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-center" id="user-name">
                        Reject Bukti Withdraw
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-success">Ya</button>
                    </div>
                    </div>
                </div>
            </form>
    </div>
</div>
@endsection
                
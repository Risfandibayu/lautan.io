@extends('index')
@section('content')

<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        {{-- <div class="page-header-icon"><i data-feather="clock"></i></div> --}}
                        List Custom Variable
                    </h1>
                </div>
                <div class="col-auto mb-3">
                    <button class="btn btn-success btn-sm btnmedia" onclick="btnaction()">Add Custom Variable</button>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main page content-->
<div class="container-xl">
    @if(session('error'))
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            {{ session('error') }}
        </div>
    </div>
    @endif

    @if(session('success'))
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            {{ session('success') }}
        </div>
    </div>
    @endif


    <div class="card">
        <div class="card-body">

            <div id="form" style="display: none">
                <form method="POST" action="{{ route('webadmin.addcustomvar') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Type</label>
                            <input class="form-control" type="text" name="type" required />
                        </div>
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Code</label>
                            <input class="form-control" type="text" name="code" required />
                        </div>
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Value</label>
                            <input class="form-control" type="text" name="value" required />
                        </div>	
                        <hr class="my-4" />
                        <div class="col-md-6">
                            <button class="btn btn-danger btn-sm" onclick="btnaction()">Cancel</button>
                            <button class="btn btn-primary btn-sm" type="submit">Save Custom Variable</button>
                        </div>
                    </div>
                    
                </form>
            </div>
            
            <div id="data">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Type</th>
                            <th scope="col">Code</th>
                            <th scope="col">Value</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->type }}</td>
                            <td>{{ $row->code }}</td>
                            <td>{{ $row->value }}</td>
                            <td>
                                <a href="/webadmin/customvar/edit/{{ $row->id }}"><i class="fa fa-edit" style="color:green"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="3">Data Not Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection

<script>
function btnaction() {
    $("#form").toggle();
    $("#data").toggle();
}
</script>
                
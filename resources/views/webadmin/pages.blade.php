@extends('index')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/codemirror.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/codemirror.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.47.0/mode/python/python.min.js"></script>

<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        {{-- <div class="page-header-icon"><i data-feather="clock"></i></div> --}}
                        List Pages
                    </h1>
                </div>
                <div class="col-auto mb-3">
                    <button class="btn btn-success btn-sm btnmedia" onclick="btnaction()">Add Pages</button>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main page content-->
<div class="container-xl">
    @if(session('error'))
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            {{ session('error') }}
        </div>
    </div>
    @endif

    @if(session('success'))
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            {{ session('success') }}
        </div>
    </div>
    @endif


    <div class="card">
        <div class="card-body">

            <div id="form" style="display: none">
                <form method="POST" action="{{ route('webadmin.addpages') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Title</label>
                            <input class="form-control" type="text" name="title" id="title" onkeyup="copyslug()" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">slug</label>
                            <input class="form-control" type="text" name="slug" id="slug" required readonly />
                        </div>
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Content</label>
                            <div style="border:1px solid #ced4da">
                                <textarea type="text" class="form-control" name="content" id="editor" rows="10">Hello Brother</textarea>
                            </div>
                        </div>	
                        <div class="col-md-12 mb-3">
                            <label class="small mb-1">Status</label>
                            <select class="form-control" name="status">
                                <option value="1">Active</option>
                                <option value="0">No Active</option>
                            </select>
                        </div>	
                        <hr class="my-4" />
                        <div class="col-md-6">
                            <button class="btn btn-danger btn-sm" onclick="btnaction()">Cancel</button>
                            <button class="btn btn-primary btn-sm" type="submit">Save Pages</button>
                        </div>
                    </div>
                    
                </form>
            </div>
            
            <div id="data">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Slug</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->slug }}</td>
                            <td>
                                @if($row->status == 1) <span class="text-success">Active</span>
                                @else <span class="text-danger">No Active</span>
                                @endif
                            </td>
                            <td>
                                <a href="/webadmin/pages/edit/{{ $row->id }}"><i class="fa fa-edit" style="color:green"></i></a>

                                @if($row->status == 1)
                                <a href="/webadmin/pages/noactive/{{ $row->id }}"><i class="fa fa-eye" style="color:red"></i></a>
                                @else
                                <a href="/webadmin/pages/active/{{ $row->id }}"><i class="fa fa-eye" style="color:green"></i></a>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="3">Data Not Found</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection

<script>
var i=0;
function btnaction() {
    i++;
    $("#form").toggle();
    $("#data").toggle();

    $("#title").val('');
    $("#slug").val('');
    
    if(i==1){
    window.myCodeMirror = CodeMirror.fromTextArea(document.getElementById("editor"), {
        lineNumbers: true,
        mode: 'phyton',
    });
    } else {
        var editor = $('.CodeMirror')[0].CodeMirror;
        editor.setValue("Hello Brother");
        editor.refresh();
    }
}

function copyslug()
{
    var str = $("#title").val();
    str = str.replaceAll(" ", '-');
    str = str.toLowerCase();
    $("#slug").val(str);
}
</script>
                
@extends('index')
@section('content')

<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        {{-- <div class="page-header-icon"><i data-feather="clock"></i></div> --}}
                        Edit Custom Variable
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main page content-->
<div class="container-xl">
    @if(session('error'))
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            {{ session('error') }}
        </div>
    </div>
    @endif

    @if(session('success'))
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            {{ session('success') }}
        </div>
    </div>
    @endif


    <div class="card">
        <div class="card-body">

            <div id="form">
                <form method="POST" action="{{ route('webadmin.editcustomvar') }}">
                    @csrf
                    <input type="hidden" class="form-control" name="id" value="{{ $data->id }}">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Type</label>
                            <input class="form-control" type="text" name="type" required value="{{ $data->type }}" />
                        </div>
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Code</label>
                            <input class="form-control" type="text" name="code" required value="{{ $data->code }}" />
                        </div>
                        <div class="col-md-4 mb-3">
                            <label class="small mb-1">Value</label>
                            <input class="form-control" type="text" name="value" required value="{{ $data->value }}" />
                        </div>	
                        <hr class="my-4" />
                        <div class="col-md-6">
                            <button class="btn btn-danger btn-sm" onclick="btnaction()">Cancel</button>
                            <button class="btn btn-primary btn-sm" type="submit">Update Custom Variable</button>
                        </div>
                    </div>
                    
                </form>
            </div>

        </div>
    </div>
</div>

@endsection
                
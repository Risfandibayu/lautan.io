@extends('index')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
    });
} );
</script>
@endsection
@section('content')
<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="circle"></i></div>
                        Assets Management
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl">
    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Coin</th>
                        <th scope="col">Name</th>
                        <th scope="col" class="text-end">Total Balance</th>
                        <th scope="col" class="text-end">Available Balance</th>
                        <th scope="col" class="text-end">Est. IDR Value</th>
                        <th scope="col">Status</th>
                        <th scope="col" width="200px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $rows)
                    @php 
                        $blc = App\Models\Balance::where('user_id',Auth::id())->where('currency_id',$rows->id)->where('active',1)->orderBy('id','DESC')->first();
                        if($blc) {
                            $binance                 = NEW App\Libs\Binance;
                            $send['url']             = '/ticker/24hr';
                            $body['symbol']          = $rows->code;
                            $send['body']            = $body; 
                            $post                    = $binance->callApi($send);

                            $balance = $blc->amount;
                            $balanceIDR = $post->lastPrice * $blc->amount;
                        } else {
                            $balance = '0.000000';
                            $balanceIDR = 0;
                        }
                    @endphp
                    <tr>
                        <td><img src="{{ $rows->image }}" width="20px"></td>
                        <td>{{ $rows->code }}</td>
                        <td>{{ $rows->name }}</td>
                        <td class="text-end">{{ $balance }}</td>
                        <td class="text-end">{{ $balance }}</td>
                        <td class="text-end">{{ $balanceIDR }}</td>
                        <td class="text-success">online</td>
                        <td class="text-right">
                            <a href="{{url('deposit')}}?coin={{ $rows->code }}" class="btn btn-sm btn-outline-primary"><i data-feather="upload"></i>&nbsp; Deposit</a>
                            <a href="{{url('withdraw')}}?coin={{ $rows->code }}" class="btn btn-sm btn-outline-primary"><i data-feather="download"></i>&nbsp; Withdraw</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
                
@extends('index')
@section('style')
    <link href="{{ asset('public/Appway2') }}/css/flag-icon.css" rel="stylesheet">
    <script>
        function displayCountryCode() {
            var countrycode = document.getElementById("phonecode");
            countrycode.options[countrycode.selectedIndex].text = countrycode.value;
            // console.log(countrycode.value);
        }
    </script>
@endsection
@section('content')
    <!-- <header class="page-header page-header-compact page-header-light bg-white mt-3">
            <div class="container-xl px-4">
                <div class="page-header-content">
                    <div class="row align-items-center justify-content-between pt-3">
                        <div class="col-auto mb-3">
                            <h1 class="page-header-title">
                                <div class="page-header-icon"><i data-feather="user"></i></div>
                                Akun
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </header> -->
    <!-- Main page content-->
    <div class="container-xl mt-4">
        <div class="row">
            <div class="offset-xl-2 col-xl-8">
                @if (session('warning'))
                    <div class="alert alert-warning alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('warning') }}
                        </div>
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('error') }}
                        </div>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('success') }}
                        </div>
                    </div>
                @endif
                <div class="card mb-4">
                    <div class="card-header">Account Verification</div>
                    <div class="card-body">
                        <div class="step step-primary mb-5">
                            <div class="step-item active">
                                <a class="step-item-link" href="#">Account Information</a>
                            </div>
                            <!-- <div class="step-item">
                                    <a class="step-item-link disabled" href="#">Bank Account</a>
                                </div> -->
                            <div class="step-item">
                                <a class="step-item-link disabled" href="#">Upload File</a>
                            </div>
                            <div class="step-item">
                                <a class="step-item-link disabled" href="#" tabindex="-1">Finish</a>
                            </div>
                        </div>
                        <form method="POST" action="{{ url('verification') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label class="small mb-1">Name</label>
                                    <input class="form-control form-control-solid" type="text"
                                        placeholder="Please insert your full name" name="name"
                                        value="{{ Auth::user()->name }}" required />
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="small mb-1">Email</label>
                                    <input class="form-control form-control-solid" type="text"
                                        placeholder="Please insert your e-mail" name="email"
                                        value="{{ Auth::user()->email }}" required disabled />
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label class="small mb-1">Phone</label>
                                    <div class="row">

                                        <?php
                                        use Illuminate\Support\Facades\DB;
                                        $country = db::table('country')
                                            ->select('*')
                                            ->get();
                                        
                                        ?>


                                        <div class="col-md-4">
                                            <select class="form-control form-control-solid" id="phonecode" name="phonecode"
                                                onchange="displayCountryCode()">
                                                <option selected hidden value="{{ Auth::user()->phonecode }}">
                                                    {{ Auth::user()->phonecode }}</option>
                                                @foreach ($country as $co)
                                                    <option value="+{{ $co->phonecode }}">{{ $co->nicename }} (
                                                        +{{ $co->phonecode }} ) </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control form-control-solid" type="text"
                                                placeholder="Please insert your phone number" name="phone"
                                                value="{{ Auth::user()->phone }}" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="small mb-1">National ID/Passport ID</label>
                                    <input class="form-control form-control-solid" type="text"
                                        placeholder="Please insert your ID number" name="nationalId"
                                        value="{{ Auth::user()->nationalId }}" required />
                                </div>
                                <div class="mb-3">
                                    <label class="small mb-1">Address</label>
                                    <input class="form-control form-control-solid" type="text"
                                        placeholder="Please insert your detail address" name="address"
                                        value="{{ Auth::user()->address }}" required />
                                </div>
                                <hr class="my-4" />
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary float-end" type="submit">Next</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
